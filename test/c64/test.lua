

-- XEX format implies Atari XL/XE
PRG{
    -- path to the output file
    OutputPath = "test.prg";
    -- memory pool for absolute addressing
    AbsoluteRAM{
        -- name of segment allocated in this pool
        Name = "code";
        -- bottom address of the pool
        Bottom = 0x0810;
        -- top address off the pool
        Top = 0x9fff;
    };
    -- memory pool for zero-page addressing
    ZeroPageRAM{
        -- name of segment allocated in this pool
        Name = "zp";
        -- bottom address of the pool
        Bottom = 0x02;
        -- top address off the pool
        Top = 0xff;
    };
    -- internal segment source taking 6502 assembly files as inputs
    Asm65{
        -- base path for all assemply files handled by this input
        BasePath = ".";
        -- patter of files to input
        NamePattern = "\\.asm$";
    };
    -- stage that will be placed in RUN section of XEX file
    StageDefault{
        Entry = "main";
    };
};

