
  .macro adw a, b
    clc
    lda :a
    adc :b
    sta :a
    bcc _adw:@  ;unique label
    inc 1+:a    ;required order to handle indexed addressing modes
_adw:@
    .endm
