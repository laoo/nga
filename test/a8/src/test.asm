
.icl "../macros/macros.inc"

.6502
.seg code

.seg zp
ptr .ds 2
.ends

.proc main

    jsr loadSub

@   jsr sub
    adw ptr #1 ;test
    jmp @-

.endp


.proc loadSub

    ldx #0
@   lda [address(storage)]sub,x
    sta sub,x
    inx
    cpx #<[size(storage)]sub
    bcc @-
    rts

.endp

.ends
