
require "config"

A8{}

xex = XEX{
  OutputPath = "nga.xex";
};

code = BCode{
  xex;
  Domain( 0x2000, 0x3fff );
};

storage = BData{
  xex;
  Domain( 0x8000, 0xbfff )
};

dcode = DCode{
  storage;
  Domain( 0x4000, 0x7fff );
};

zp = SArea{
  Domain( 0x00, 0xff )
};

Segments{
  Assembler{ BasePath = "src";  NamePattern = "\\.asm$"; };
};

