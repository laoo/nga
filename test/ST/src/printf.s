    code

    public printf
printf:
    move.l  (sp)+,ret
    move.w  #CConws,-(sp)
    trap    #1
    addq.l  #2,sp
    move.l  ret(pc),-(sp)
    rts

ret:
    ds.l 1
