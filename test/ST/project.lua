
require "config"

ST{
  default = Ctx{
    CODE = Code{};
    DATA = Data{};
    BSS = Bss{};
  };
  TOS{
    OutputFile = "nga.tos";
    Stage{
      Parents{ "default" };
      Exports{ "main" };
      Vasm68k{ BasePath = "src";  NamePattern = "\\.s$"; };
    };
  };
}

