
;based on Bastian Schick's startup.inc

.68000
.local gd

Init            = 1*4
InitGPURead     = 2*4
BIOSVersion     = 3*4
ROMWriteEnable  = 4*4
ROMSetPage      = 5*4
ROMSetPages     = 6*4
GetCartSerial   = 7*4
GetCardSerial   = 8*4
CardIn          = 9*4
FileOpen        = 10*4
FileClose       = 11*4
FileSeek        = 12*4
FileRead        = 13*4
FileWrite       = 14*4
FileTell        = 15*4
FileSize        = 16*4
FileAsyncPos    = 17*4
FileAsyncWait   = 18*4
FileAsyncActive = 19*4
FileInfo        = 20*4
DirOpen         = 21*4
DirRead         = 22*4
DirClose        = 23*4
DoReset         = 24*4
SetLED          = 25*4
DebugString     = 26*4

MINVERSION      = $100

ASIC_SPI_STATUS_HAVE_DATA_BIT     = 3

ASIC_SPI_STATUS_PACKET_START      = 1<<4
ASIC_SPI_STATUS_SLAVE_SELECT      = 1<<0
ASIC_SPI_STATUS_LATCH_FULL        = 1<<5

ASIC_SPI_STATUS                   = $F16002
ASIC_SPI_DATA                     = $F16004
ASIC_SPI_DATA_BYTE                = $F16005

.seg code

;|==============================================================================
;| u32 GD_HWVersion()
;|------------------------------------------------------------------------------
;| Get the GD hardware, nn.nn BCD format, high word FIRMWARE, low word ASIC
;|==============================================================================

.proc HWVersion
    move.w    #ASIC_SPI_STATUS_PACKET_START,ASIC_SPI_STATUS ; request a packet start
    bsr.s     WaitData                                    ; wait for GD to acknowledge
    move.w    #12,d0                                        ; hw version
    bsr.s     ExchangeWord                                ; send command word

    moveq     #0,d0                                         ; command data size word
    bsr.s     ExchangeWord                                ; send 0 for param size
    move.w    #ASIC_SPI_STATUS_PACKET_START,ASIC_SPI_STATUS ; we're done, process it GD!

    ; recieve the size of the block
    bsr.s     WaitData                                    ; wait for data to be available
    bsr.s     ExchangeWord                                ; read word
    swap      d0
    bsr.s     ExchangeWord                                ; read word

    clr.w     ASIC_SPI_STATUS                             ; end of packet

    move.w    #500,d0
@   dbra      d0,@-                                       ; pause to let the micro finish up
    rts
.endp

.proc WaitData
@   move.w    ASIC_SPI_STATUS,d0
    btst.l    #ASIC_SPI_STATUS_HAVE_DATA_BIT,d0
    bne.s     @-                                          ; wait for data available bit to be low

     ; lower slave select to ack packet start, keep packet flag high
    move.w    #ASIC_SPI_STATUS_PACKET_START|ASIC_SPI_STATUS_SLAVE_SELECT,ASIC_SPI_STATUS

@   move.w    ASIC_SPI_STATUS,d0
    btst.l    #ASIC_SPI_STATUS_HAVE_DATA_BIT,d0
    beq.s     @-                                          ; wait for data available bit to be high (slave ack)
    rts
.endp

.proc ExchangeWord
    move.w    d0,ASIC_SPI_DATA                            ; send byte
@   tst.w     ASIC_SPI_STATUS                             ; wait until not busy
    bmi.s     @-
    move.b    ASIC_SPI_DATA_BYTE,d0                       ; read byte
    ror.w     #8,d0                                       ; shift

    move.w    d0,ASIC_SPI_DATA                            ; send byte
@   tst.w     ASIC_SPI_STATUS                             ; wait until not busy
    bmi.s     @-
    move.b    ASIC_SPI_DATA_BYTE,d0                       ; read byte
    rts
.endp

;|==============================================================================
;| u16 GD_Install(void *buffer)
;|------------------------------------------------------------------------------
;| Install the GD BIOS to the given address. 0 success, -ve failure
;| a0 - *buffer
;|==============================================================================

.proc Install
    ; first make sure the firmware is new enough for the GDBIOS
    bsr.w     HWVersion                                   ; get FW & ASIC version
    swap      d0                                          ; want firmware
    cmp.w     #$111,d0                                    ; version 1.11 is the first with GDBIOS
    bge.s     @+                                          ; >=, install!

    clr.l     GDB_Base
    moveq     #-1,d0                                      ; failed
    rts
                   
@   ; drain SPI latch incase of FIFO DMA termiation
    move.w    ASIC_SPI_STATUS,d0
    and.w     #ASIC_SPI_STATUS_LATCH_FULL,d0
    beq.s     @+
    tst.w     ASIC_SPI_DATA                               ; read result
    bra.s     @-
@   ; request GDBIOS block
    move.w    #ASIC_SPI_STATUS_PACKET_START,ASIC_SPI_STATUS ; request a packet start
    bsr.s     WaitData                                    ; wait for GD to acknowledge
    move.w    #$80,d0                                     ; install command
    bsr.s     ExchangeWord                                ; send command word

    moveq     #0,d0                                       ; command data size word
    bsr.s     ExchangeWord                                ; send 0 for param size
    move.w    #ASIC_SPI_STATUS_PACKET_START,ASIC_SPI_STATUS ; we're done, process it GD!

    ; recieve the size of the block
    bsr.w     WaitData                                    ; wait for data to be available
    bsr.s     ExchangeWord                                ; read word, this is the size of the data
    move.w    #ASIC_SPI_STATUS_PACKET_START,ASIC_SPI_STATUS ; we're done with this block GD!

    ; recieve the block of data
    move.w    d0,d1                                       ; data block size in d1
    move.l    a0,GDB_Base                                 ; base of the GD BIOS

nextBlock
    bsr.w     WaitData                                    ; wait for data to be ready

    move.w    d1,d0                                       ; bytes to read
    cmp.w     #512,d1                                     ; max of 512 per packet
    ble.s     @+
    move.w    #512,d0
@   sub.w     d0,d1                                       ; subtract what we've read
     
    subq.w    #1,d0                                       ; dbra adjust

readBytes
    move.w    d0,ASIC_SPI_DATA                            ; send byte
@   tst.w     ASIC_SPI_STATUS                             ; wait until not busy
    bmi.s     @-
    move.b    ASIC_SPI_DATA_BYTE,(a0)+                    ; read byte
    dbra      d0,readBytes

    move.w    #ASIC_SPI_STATUS_PACKET_START,ASIC_SPI_STATUS ; we're done with this block GD!

    tst.w     d1                                          ; are we done?
    bne.s     nextBlock                                   ; no, read more blocks

     ; see if the BIOS we have is new enough
    moveq     #0,d0                                       ; return 0
    move.l    GDB_Base,a0
    cmp.w     #MINVERSION,(a0)
    bge.s     @+
    subq.w    #1,d0

@   clr.w     ASIC_SPI_STATUS                             ; end of packet
    jmp       Init(a0)                                    ; do any specific initialistaion
    
.endp

;|==============================================================================
;| const char *GD_BINtoASCII(char *ascii, void *bin)
;|------------------------------------------------------------------------------
;| a0 - char *bin
;| a1 - char *ascii
;|==============================================================================

.proc BINtoASCII

    move.l    a1,-(sp)
    movem.l   d2-d3/a2,-(sp)
     
    lea       ASCII(pc),a2                                ; conversion table

    moveq     #0,d0
    move.b    (a0)+,d0                                    ; fill buffer
    lsl.w     #8,d0
    move.b    (a0)+,d0
     
    moveq     #16,d2                                      ; bits available
    moveq     #0,d3                                       ; character count out

@   lsl.l     #5,d0                                       ; 5 bits per char
    swap      d0                                          ; just char bits
    add.w     d3,d0                                       ; rotate encoding
    and.w     #31,d0                                      ; clip to 31 chars
    move.b    ( a2,d0.w ),(a1)+                             ; ASCII byte
    clr.w     d0
    swap      d0

    addq.w    #1,d3                                       ; one more char encoded
    cmp.w     #25,d3                                      ; are we done?
    bgt.s     @+
    beq.s     @-                                          ; the last 2 bits are 0 (actually encodes 130 bits)
  
    subq.w    #5,d2                                       ; 5 less bits
    cmp.w     #5,d2                                       ; must have at least 5 bits to encode more
    bge.s     @-

    addq.w    #8,d2                                       ; one more byte available

    moveq     #0,d1
    move.b    (a0)+,d1                                    ; get extra byte
    ror.w     d2,d1                                       ; move byte into correct position
    or.w      d1,d0                                       ; or into buffer
    bra.s     @-

@   clr.b     (a1)+                                       ; add terminating character
    movem.l   (sp)+,d2-d3/a2
    move.l    (sp)+,d0                                    ; return the ascii buffer address
    rts

ASCII
    .by "KPLGQ0416FCMXZ8RV9SB325HYNTADJ7W"

.endp
.ends

.seg bss
GDB_Base  .ds.l 1
.ends


.endl
