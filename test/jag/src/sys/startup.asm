
;based on Bastian Schick's startup.inc

.68000
.local sys

.seg code
.proc startup

    move.w  #$2700,sr
    lea     INITSTACK,sp
    lea     _BASE,a0
    move.l  #BIG_IO|BIG_PIX|BIG_INST,d0       ; big endian
    move.l  d0,G_END-_BASE(a0)
    move.l  d0,D_END
    moveq   #0,d0
    move.l  d0,G_CTRL-_BASE(a0)               ; stop gpu
    move.l  d0,D_CTRL                         ; stop dsp
    move.l  d0,G_FLAGS-_BASE(a0)              ; disable GPU-IRQs
    ; disable DSP-IRQs
    move.l  #D_EXT1ENA|D_TIM2CLR|D_TIM1CLR|D_I2SCLR|D_CPUCLR|D_EXT0ENA,D_FLAGS    ; #%10001111100000000
    move.l  #J_SYNCLR|J_ASYNCLR|J_TIM2CLR|J_TIM1CLR|J_DSPCLR|J_EXTCLR,J_INT       ; #%111111<<8 clear and disable IRQs

    move.l  d0,0
    moveq   #4,d0
    move.l  d0,4
    moveq   #0,d0
    move.l  d0,OLP-_BASE(a0)                  ; set OP to STOP-OBJECT
    move.w  d0,OBF-_BASE(a0)                  ; clear OP-Flag
    move.l  d0,BORD1-_BASE(a0)                ; border black
    move.w  d0,BWNOCRY-_BASE(a0)              ; set CRY mode to color/not BW
    move.w  d0,BG-_BASE(a0)                   ; background black
    move.l  d0,PIT0-_BASE(a0)                 ; stop PIT
    move.l  d0,JPIT1                          ; stop JPIT1
    move.l  d0,JPIT3                          ; stop JPIT2
    move.l  #(C_JERCLR|C_PITCLR|C_OPCLR|C_GPUCLR|C_VIDCLR)<<16,INT1-_BASE(a0)     ; clear pending irqs
    move.w  #$7fff,VI-_BASE(a0)               ; no VI
    lea     dummy_irq(pc),a0
    move.l  a0,USER0

    ;Video init
    move.w  #PAL_VMID-PAL_HEIGHT,d0         ; vdb
    move.w  #PAL_VMID+PAL_HEIGHT,d1         ; vde
    move.w  #((PAL_WIDTH>>1)-1)|$0400,d2    ; hde
    move.w  #PAL_HMID-(PAL_WIDTH>>1)+4,d3   ; hdb
    btst.b  #VIDTYPEBIT,CONFIG+1
    beq.s   @+                              ;skip NTSC
    move.w  #NTSC_VMID-NTSC_HEIGHT,d0
    move.w  #NTSC_VMID+NTSC_HEIGHT,d1
    move.w  #((NTSC_WIDTH>>1)-1)|$0400,d2
    move.w  #NTSC_HMID-(NTSC_WIDTH>>1)+4,d3
@   move.w  d0,VDB-_BASE(a0)
    move.w  #-1,VDE-_BASE(a0)
    move.w  d2,HDE-_BASE(a0)
    move.w  d3,HDB1-_BASE(a0)
    move.w  d3,HDB2-_BASE(a0)

    move.w  d0,displayBegin
    move.w  d1,displayEnd

    move.w #PWIDTH4|BGEN|CSYNC|VIDEN,VMODE

    move.w #$2100,sr
    rts

.proc dummy_irq
    move.l #(C_JERCLR|C_PITCLR|C_OPCLR|C_GPUCLR|C_VIDCLR)<<16,INT1
    rte
.endp

.endp

.ends

.seg bss
displayBegin  .ds.w 1
displayEnd    .ds.w 1
.ends

.endl
