;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;       Hardware equates for JAGUAR System
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

.68000

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;      GENERIC DEFINES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

DRAM            =     $000000           ; First system RAM location
USERRAM         =     $004000           ; Beginning of non-reserved RAM
ENDRAM          =     $200000           ; End of DRAM
INITSTACK       =     ENDRAM-4          ; Good place to initialize stack

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;      CPU REGISTERS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

LEVEL0          =     $100              ; 68000 Level 0 Autovector Interrupt
USER0           =     $100              ; Pseudonym for LEVEL0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Masks for INT1 CPU Interrupt Control Register

C_VIDENA        =     $0001             ; Enable Video time-base interrupts
C_GPUENA        =     $0002             ; Enable GPU register-write interrupt
C_OPENA         =     $0004             ; Enable OP stop-object interrupt
C_PITENA        =     $0008             ; Enable PIT interrupt
C_JERENA        =     $0010             ; Enable Jerry interrupt

C_VIDCLR        =     $0100             ; Clear pending video interrupts
C_GPUCLR        =     $0200             ; Clear pending GPU interrupts
C_OPCLR         =     $0400             ; Clear pending OP interrupts
C_PITCLR        =     $0800             ; Clear pending PIT interrupts
C_JERCLR        =     $1000             ; Clear pending Jerry interrupts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;       JAGUAR REGISTERS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

_BASE           =     $F00000           ; TOM Internal Register Base

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;       TOM REGISTERS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

MEMCON1         =     $F00000           ; Memory Configuration Register One
MEMCON2         =     $F00002           ; Memory Configuration Register Two
HC              =     $F00004           ; Horizontal Count
VC              =     $F00006           ; Vertical Count
LPH             =     $F00008           ; Horizontal Lightpen
LPV             =     $F0000A           ; Vertical Lightpen
OB0             =     $F00010           ; Current Object Phrase
OB1             =     $F00012
OB2             =     $F00014
OB3             =     $F00016
OLP             =     $F00020           ; Object List Pointer
ODP             =     $F00024           ; Object Data Pointer
OBF             =     $F00026           ; Object Processor Flag
VMODE           =     $F00028           ; Video Mode
BORD1           =     $F0002A           ; Border Color (Red & Green)
BORD2           =     $F0002C           ; Border Color (Blue)
HP              =     $F0002e           ; Horizontal Period
HBB             =     $F00030           ; Horizontal Blanking Begin
HBE             =     $F00032           ; Horizontal Blanking End
HS              =     $F00034           ; Horizontal Sync
HVS             =     $F00036           ; Horizontal Vertical Sync
HDB1            =     $F00038           ; Horizontal Display Begin One
HDB2            =     $F0003A           ; Horizontal Display Begin Two
HDE             =     $F0003C           ; Horizontal Display End
VP              =     $F0003e           ; Vertical Period
VBB             =     $F00040           ; Vertical Blanking Begin
VBE             =     $F00042           ; Vertical Blanking End
VS              =     $F00044           ; Vertical Sync
VDB             =     $F00046           ; Vertical Display Begin
VDE             =     $F00048           ; Vertical Display End
VEB             =     $F0004a           ; Vertical equalization Begin
VEE             =     $F0004c           ; Vertical equalization End
VI              =     $F0004E           ; Vertical Interrupt
PIT0            =     $F00050           ; Programmable Interrupt Timer (Lo)
PIT1            =     $F00052           ; Programmable Interrupt Timer (Hi)
HEQ             =     $F00054           ; Horizontal equalization End
BWNOCRY         =     $F00056           ; "If you set bit 2 in the undocumented register F00056, you get "black and white CRY" mode.
BG              =     $F00058           ; Background Color

INT1            =     $F000E0           ; CPU Interrupt Control Register
INT2            =     $F000E2           ; CPU Interrupt Resume Register

CLUT            =     $F00400           ; Color Lookup Table

LBUFA           =     $F00800           ; Line Buffer A
LBUFB           =     $F01000           ; Line Buffer B
LBUFC           =     $F01800           ; Line Buffer Current

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;      OBJECT PROCESSOR EQUATES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

BITOBJ          =     0
SCBITOBJ        =     1
GPUOBJ          =     2
BRANCHOBJ       =     3
STOPOBJ         =     4

O_REFLECT       =     $00002000         ; OR with top LONG of BITMAP object
O_RMW           =     $00004000
O_TRANS         =     $00008000
O_RELEASE       =     $00010000

O_DEPTH1        =     (0<<12)           ; DEPTH Field for BITMAP objects
O_DEPTH2        =     (1<<12)
O_DEPTH4        =     (2<<12)
O_DEPTH8        =     (3<<12)
O_DEPTH16       =     (4<<12)
O_DEPTH32       =     (5<<12)

O_NOGAP         =     (1<<15)           ; Phrase GAP between image phrases
O_1GAP          =     (2<<15)
O_2GAP          =     (3<<15)
O_3GAP          =     (4<<15)
O_4GAP          =     (5<<15)
O_5GAP          =     (6<<15)
O_6GAP          =     (7<<15)

O_BREQ          =     (0<<14)           ; CC field of BRANCH objects
O_BRGT          =     (1<<14)
O_BRLT          =     (2<<14)
O_BROP          =     (3<<14)
O_BRHALF        =     (4<<14)

O_STOPINTS      =     $00000008         ; Enable Interrupts in STOP object

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;      VIDEO INITIALIZATION CONSTANTS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

NTSC_WIDTH      =     1409              ; Width of screen in pixel clocks
NTSC_HMID       =     823               ; Middle of screen in pixel clocks
NTSC_HEIGHT     =     241               ; Height of screen in scanlines
NTSC_VMID       =     266               ; Middle of screen in halflines

PAL_WIDTH       =     1381              ; Same as above for PAL...
PAL_HMID        =     843
PAL_HEIGHT      =     287
PAL_VMID        =     322

;;; The following mask will extract the PAL/NTSC flag bit from the
;;; CONFIG register. NTSC = Bit Set, PAL = Bit Clear

VIDTYPE         =     $10
VIDTYPEBIT      =     4

;;; The following are Video Mode register flags

VIDEN           =     $0001             ; Enable video-time base generator

CRY16           =     $0000             ; Enable 16-bit CRY mode
RGB24           =     $0002             ; Enable 24-bit RGB mode
DIRECT16        =     $0004             ; Enable 16-bit DIRECT mode
RGB16           =     $0006             ; Enable 16-bit RGB mode

GENLOCK         =     $0008             ; Not Supported in Jaguar Console
INCEN           =     $0010             ; Enable encrustation
BINC            =     $0020             ; Select local border color
CSYNC           =     $0040             ; Enable composite sync
BGEN            =     $0080             ; Clear line buffer to BG
VARMOD          =     $0100             ; Enable variable-color resolution mode

PWIDTH1         =     $0000             ; Select pixels 1 clock wide
PWIDTH2         =     $0200             ; Select pixels 2 clocks wide
PWIDTH3         =     $0400             ; Select pixels 3 clocks wide
PWIDTH4         =     $0600             ; Select pixels 4 clocks wide
PWIDTH5         =     $0800             ; Select pixels 5 clocks wide
PWIDTH6         =     $0A00             ; Select pixels 6 clocks wide
PWIDTH7         =     $0C00             ; Select pixels 7 clocks wide
PWIDTH8         =     $0E00             ; Select pixels 8 clocks wide

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;       GPU REGISTERS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

G_FLAGS         =     $F02100           ; GPU Flags
G_MTXC          =     $F02104           ; GPU Matrix Control
G_MTXA          =     $F02108           ; GPU Matrix Address
G_END           =     $F0210C           ; GPU Data Organization
G_PC            =     $F02110           ; GPU Program Counter
G_CTRL          =     $F02114           ; GPU Operation Control/Status
G_HIDATA        =     $F02118           ; GPU Bus Interface high data
G_REMAIN        =     $F0211C           ; GPU Division Remainder
G_DIVCTRL       =     $F0211C           ; GPU Divider control
G_RAM           =     $F03000           ; GPU Internal RAM
G_ENDRAM        =     G_RAM+(4*1024)    ; 4K bytes

;;;
;;; GPU Flags Register equates
;;;

G_CPUENA        =     $00000010         ; CPU Interrupt enable bits
G_DSPENA        =     $00000020         ; DSP Interrupt enable bits
G_PITENA        =     $00000040         ; PIT Interrupt enable bits
G_OPENA         =     $00000080         ; Object Processor Interrupt enable bits
G_BLITENA       =     $00000100         ; Blitter Interrupt enable bits
G_CPUCLR        =     $00000200         ; CPU Interrupt clear bits
G_DSPCLR        =     $00000400         ; DSP Interrupt clear bits
G_PITCLR        =     $00000800         ; PIT Interrupt clear bits
G_OPCLR         =     $00001000         ; Object Processor Interrupt clear bits
G_BLITCLR       =     $00002000         ; Blitter Interrupt clear bits

;;;
;;; GPU Control/Status Register
;;;

GPUGO           =     $00000001         ; Start and Stop the GPU
;GPUINT0        =     $00000004         ; generate a GPU type 0 interrupt

G_CPULAT        =     $00000040         ; Interrupt Latches
G_DSPLAT        =     $00000080
G_PITLAT        =     $00000100
G_OPLAT         =     $00000200
G_BLITLAT       =     $00000400

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;       BLITTER REGISTERS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

A1_BASE         =     $F02200           ; A1 Base Address
A1_FLAGS        =     $F02204           ; A1 Control Flags
A1_CLIP         =     $F02208           ; A1 Clipping Size
A1_PIXEL        =     $F0220C           ; A1 Pixel Pointer
A1_STEP         =     $F02210           ; A1 Step (Integer Part)
A1_FSTEP        =     $F02214           ; A1 Step (Fractional Part)
A1_FPIXEL       =     $F02218           ; A1 Pixel Pointer (Fractional)
A1_INC          =     $F0221C           ; A1 Increment (Integer Part)
A1_FINC         =     $F02220           ; A1 Increment (Fractional Part)
A2_BASE         =     $F02224           ; A2 Base Address
A2_FLAGS        =     $F02228           ; A2 Control Flags
A2_MASK         =     $F0222C           ; A2 Address Mask
A2_PIXEL        =     $F02230           ; A2 PIXEL
A2_STEP         =     $F02234           ; A2 Step (Integer)

B_CMD           =     $F02238           ; Command
B_COUNT         =     $F0223C           ; Counters
B_SRCD          =     $F02240           ; Source Data
B_DSTD          =     $F02248           ; Destination Data
B_DSTZ          =     $F02250           ; Destination Z
B_SRCZ1         =     $F02258           ; Source Z (Integer)
B_SRCZ2         =     $F02260           ; Source Z (Fractional)
B_PATD          =     $F02268           ; Pattern Data
B_IINC          =     $F02270           ; Intensity Increment
B_ZINC          =     $F02274           ; Z Increment
B_STOP          =     $F02278           ; Collision stop control

B_I3            =     $F0227C           ; Blitter Intensity 3
B_I2            =     $F02280           ; Blitter Intensity 2
B_I1            =     $F02284           ; Blitter Intensity 1
B_I0            =     $F02288           ; Blitter Intensity 0

B_Z3            =     $F0228C           ; Blitter Z 3
B_Z2            =     $F02290           ; Blitter Z 2
B_Z1            =     $F02294           ; Blitter Z 1
B_Z0            =     $F02298           ; Blitter Z 0

;;;
;;; BLITTER Command Register equates
;;;

SRCEN           =     $00000001         ; d00:      source data read (inner loop)
SRCENZ          =     $00000002         ; d01:      source Z read (inner loop)
SRCENX          =     $00000004         ; d02:      source data read (realign)
DSTEN           =     $00000008         ; d03:      dest data read (inner loop)
DSTENZ          =     $00000010         ; d04:      dest Z read (inner loop)
DSTWRZ          =     $00000020         ; d05:      dest Z write (inner loop)
CLIP_A1         =     $00000040         ; d06:      A1 clipping enable
UPDA1F          =     $00000100         ; d08:      A1 update step fraction
UPDA1           =     $00000200         ; d09:      A1 update step
UPDA2           =     $00000400         ; d10:      A2 update step
DSTA2           =     $00000800         ; d11:      reverse usage of A1 and A2
GOURD           =     $00001000         ; d12:      enable Gouraud shading
ZBUFF           =     $00002000         ; d13:      polygon Z data updates
TOPBEN          =     $00004000         ; d14:      intensity carry into byte
TOPNEN          =     $00008000         ; d15:      intensity carry into nibble
PATDSEL         =     $00010000         ; d16:      Select pattern data
ADDDSEL         =     $00020000         ; d17:      diagnostic
                                        ; d18-d20:  Z comparator inhibit
ZMODELT         =     $00040000         ;           source < destination
ZMODEEQ         =     $00080000         ;           source = destination
ZMODEGT         =     $00100000         ;           source > destination
                                        ; d21-d24:  Logic function control
LFU_NAN         =     $00200000         ;           !source & !destination
LFU_NA          =     $00400000         ;           !source & destination
LFU_AN          =     $00800000         ;           source & !destination
LFU_A           =     $01000000         ;           source & destination

CMPDST          =     $02000000         ; d25: pixel compare pattern & dest
BCOMPEN         =     $04000000         ; d26: bit compare write inhibit
DCOMPEN         =     $08000000         ; d27: data compare write inhibit
BKGWREN         =     $10000000         ; d28: data write back
BUSHI           =     $20000000         ; d29: blitter priority
SRCSHADE        =     $40000000         ; d30: shade src data w/IINC value

;;;
;;; The following are ALL 16 possible logical operations of the LFUs
;;;

LFU_ZERO        =     $00000000         ; All Zeros
LFU_NSAND       =     $00200000         ; NOT Source AND NOT Destination
LFU_NSAD        =     $00400000         ; NOT Source AND Destination
LFU_NOTS        =     $00600000         ; NOT Source
LFU_SAND        =     $00800000         ; Source AND NOT Destination
LFU_NOTD        =     $00A00000         ; NOT Destination
LFU_N_SXORD     =     $00C00000         ; NOT (Source XOR Destination)
LFU_NSORND      =     $00E00000         ; NOT Source OR NOT Destination
LFU_SAD         =     $01000000         ; Source AND Destination
LFU_SXORD       =     $01200000         ; Source XOR Destination
LFU_D           =     $01400000         ; Destination
LFU_NSORD       =     $01600000         ; NOT Source OR Destination
LFU_S           =     $01800000         ; Source
LFU_SORND       =     $01A00000         ; Source OR NOT Destination
LFU_SORD        =     $01C00000         ; Source OR Destination
LFU_ONE         =     $01E00000         ; All Ones

; These are some common combinations with less boolean names

LFU_REPLACE     =     $01800000         ; Source REPLACEs destination
LFU_XOR         =     $01200000         ; Source XOR with destination
LFU_CLEAR       =     $00000000         ; CLEAR destination

;;;
;;; BLITTER Flags (A1 or A2) register equates
;;;

; Pitch d00-d01:
;       distance between pixel phrases
PITCH1          =     $00000000         ; 0 phrase gap
PITCH2          =     $00000001         ; 1 phrase gap
PITCH4          =     $00000002         ; 3 phrase gap
PITCH3          =     $00000003         ; 2 phrase gap

; Pixel d03-d05
;       bit depth (2^n)
PIXEL1          =     $00000000         ; n = 0 
PIXEL2          =     $00000008         ; n = 1 
PIXEL4          =     $00000010         ; n = 2 
PIXEL8          =     $00000018         ; n = 3 
PIXEL16         =     $00000020         ; n = 4 
PIXEL32         =     $00000028         ; n = 5 

; Z offset d06-d08
;       offset from phrase of pixel data from its corresponding
;       Z data phrases
ZOFFS0          =     $00000000         ; offset = 0    UNUSED
ZOFFS1          =     $00000040         ; offset = 1
ZOFFS2          =     $00000080         ; offset = 2
ZOFFS3          =     $000000C0         ; offset = 3
ZOFFS4          =     $00000100         ; offset = 4
ZOFFS5          =     $00000140         ; offset = 5
ZOFFS6          =     $00000180         ; offset = 6
ZOFFS7          =     $000001C0         ; offset = 7    UNUSED

; Width d09-d14
;       width used for address generation
;       This is a 6-bit floating point value in pixels
;       4-bit unsigned exponent
;       2-bit mantissa with implied 3rd bit of 1
WID2            =     $00000800         ; 1.00 X 2^1  ( 4<<9)
WID4            =     $00001000         ; 1.00 X 2^2  ( 8<<9)
WID6            =     $00001400         ; 1.10 X 2^2  (10<<9)
WID8            =     $00001800         ; 1.00 x 2^3  (12<<9)
WID10           =     $00001A00         ; 1.01 X 2^3  (13<<9)
WID12           =     $00001C00         ; 1.10 X 2^3  (14<<9)
WID14           =     $00001E00         ; 1.11 X 2^3  (15<<9)
WID16           =     $00002000         ; 1.00 X 2^4  (16<<9)
WID20           =     $00002200         ; 1.01 X 2^4  (17<<9)
WID24           =     $00002400         ; 1.10 X 2^4  (18<<9)
WID28           =     $00002600         ; 1.11 X 2^4  (19<<9)
WID32           =     $00002800         ; 1.00 X 2^5  (20<<9)
WID40           =     $00002A00         ; 1.01 X 2^5  (21<<9)
WID48           =     $00002C00         ; 1.10 X 2^5  (22<<9)
WID56           =     $00002E00         ; 1.11 X 2^5  (23<<9)
WID64           =     $00003000         ; 1.00 X 2^6  (24<<9)
WID80           =     $00003200         ; 1.01 X 2^6  (25<<9)
WID96           =     $00003400         ; 1.10 X 2^6  (26<<9)
WID112          =     $00003600         ; 1.11 X 2^6  (27<<9)
WID128          =     $00003800         ; 1.00 X 2^7  (28<<9)
WID160          =     $00003A00         ; 1.01 X 2^7  (29<<9)
WID192          =     $00003C00         ; 1.10 X 2^7  (30<<9)
WID224          =     $00003E00         ; 1.11 X 2^7  (31<<9)
WID256          =     $00004000         ; 1.00 X 2^8  (32<<9)
WID320          =     $00004200         ; 1.01 X 2^8  (33<<9)
WID384          =     $00004400         ; 1.10 X 2^8  (34<<9)
WID448          =     $00004600         ; 1.11 X 2^8  (35<<9)
WID512          =     $00004800         ; 1.00 X 2^9  (36<<9)
WID640          =     $00004A00         ; 1.01 X 2^9  (37<<9)
WID768          =     $00004C00         ; 1.10 X 2^9  (38<<9)
WID896          =     $00004E00         ; 1.11 X 2^9  (39<<9)
WID1024         =     $00005000         ; 1.00 X 2^10 (40<<9)
WID1280         =     $00005200         ; 1.01 X 2^10 (41<<9)
WID1536         =     $00005400         ; 1.10 X 2^10 (42<<9)
WID1792         =     $00005600         ; 1.11 X 2^10 (43<<9)
WID2048         =     $00005800         ; 1.00 X 2^11 (44<<9)
WID2560         =     $00005A00         ; 1.01 X 2^11 (45<<9)
WID3072         =     $00005C00         ; 1.10 X 2^11 (46<<9)
WID3584         =     $00005E00         ; 1.11 X 2^11 (47<<9)

; X add control d16-d17
;       controls the update of the X pointer on each pass
;       round the inner loop
XADDPHR         =     $00000000         ; 00 - add phrase width and truncate
XADDPIX         =     $00010000         ; 01 - add pixel size (add 1)   
XADD0           =     $00020000         ; 10 - add zero
XADDINC         =     $00030000         ; 11 - add the increment

; Y add control d18
;       controls the update of the Y pointer within the inner loop.
;       it is overridden by the X add control if they are in add increment
YADD0           =     $00000000         ; 00 - add zero
YADD1           =     $00040000         ; 01 - add 1

; X sign d19
;       add or subtract pixel size if X add control = 01 (XADDPIX)
XSIGNADD        =     $00000000         ; 0 - add pixel size
XSIGNSUB        =     $00080000         ; 1 - subtract pixel size

; Y sign d20
;       add or subtract pixel size if Y add control = 01 (YADD1)
YSIGNADD        =     $00000000         ; 0 - add 1
YSIGNSUB        =     $00100000         ; 1 - sub 1


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;       JERRY REGISTERS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

JPIT1           =     $F10000           ; Timer 1 Pre-Scaler
JPIT2           =     $F10002           ; Timer 1 Divider
JPIT3           =     $F10004           ; Timer 2 Pre-Scaler
JPIT4           =     $F10006           ; Timer 2 Divider

J_INT           =     $F10020           ; Jerry Interrupt control (to TOM)
        
JOYSTICK        =     $F14000           ; Joystick register and mute
JOYBUTS         =     $F14002           ; Joystick register
CONFIG          =     $F14002           ; Also has NTSC/PAL

SCLK            =     $F1A150           ; SSI Clock Frequency
SMODE           =     $F1A154           ; SSI Control

L_I2S           =     $F1A148           ; I2S Serial Interface
R_I2S           =     $F1A14C           ; I2S Serial Interface
LTXD            =     $F1A148           ; Synonyms
RTXD            =     $F1A14C
LRXD            =     $F1A148           ; Synonyms
RRXD            =     $F1A14C

R_DAC           =     $F1A148           ; Swapped on Purpose!
L_DAC           =     $F1A14C

ASICLK          =     $F10034           ; Asynchronous Clock Register
ASICTRL         =     $F10032           ; Asynchronous Control Register
ASISTAT         =     $F10032           ; Asynchronous Status Register
ASIDATA         =     $F10030           ; Asynchronous Data Register

;================================================================
; UART Definitions (new in this file as of 24-Apr-95)
;================================================================

; UART control register Masks 
; All unused bits in the control register need to be written as zeros !	      
; With exception of U_CLRERR these are valid for read in ASISTAT, too	    

U_MODD          =     (1<<0)            ; selects odd parity
U_MPAREN        =     (1<<1)            ; enable parity
U_MTXOPOL       =     (1<<2)            ; transmit output polarity (if set: active low)
U_MRXIPOL       =     (1<<3)            ; receive input polarity (if set: invert input)
U_MTINTEN       =     (1<<4)            ; enable transmitter interrupts
U_MRINTEN       =     (1<<5)            ; enable reciever interrupts
U_MCLRERR       =     (1<<6)            ; clear error (only use if U_SERIN is 
                                        ; inactive otherwise the UART locks up.
                                        ; By default input is active low. This
                                        ; depends on U_MRXIPOL) 
U_MTXBRK        =     (1<<14)           ; transmit break

; UART control register (ONLY) bit numbers
U_CLRERR        =     6                 ;

; UART control AND status register (SHARED) bit numbers 

U_ODD           =     0                 ; selects odd parity
U_PAREN         =     1                 ; enable parity
U_TXOPOL        =     2                 ; transmit output polarity (if set: active low)
U_RXIPOL        =     3                 ; receive input polarity (if set: invert input)
U_TINTEN        =     4                 ; enable transmitter interrupts
U_RINTEN        =     5                 ; enable reciever interrupts
U_TXBRK         =     14                ; transmit break

; UART status register (ONLY) bit numbers

U_ERR           =     15                ; error condition exists
U_SERIN         =     13                ; state of UART1 Pin (serial input data)
U_OE            =     11                ; overrun error
U_FE            =     10                ; frame error
U_PE            =     9                 ; parity error
U_TBE           =     8                 ; transitter buffer empty
U_RBF           =     7                 ; receiver buffer full


;;;
;;; Jerry Interrupt Control Flags
;;;

J_EXTENA        =     $0001             ; Enable external interrupts
J_DSPENA        =     $0002             ; Enable DSP interrupts
J_TIM1ENA       =     $0004             ; Enable Timer 1 interrupts
J_TIM2ENA       =     $0008             ; Enable Timer 2 interrupts
J_ASYNENA       =     $0010             ; Enable Asyncronous Serial interrupts
J_SYNENA        =     $0020             ; Enable Syncronous Serial interrupts

J_EXTCLR        =     $0100             ; Clear pending external interrupts
J_DSPCLR        =     $0200             ; Clear pending DSP interrupts
J_TIM1CLR       =     $0400             ; Clear pending Timer 1 interrupts
J_TIM2CLR       =     $0800             ; Clear pending Timer 2 interrupts
J_ASYNCLR       =     $1000             ; Clear pending Asynch. Serial interrupts
J_SYNCLR        =     $2000             ; Clear pending Synch. Serial interrupts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;     JERRY Joystick equates
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Bits when LONGword is formatted as below (from JOYTEST\JT_LOOP.S).
;
; Format: xxApxxBx RLDU147* xxCxxxox 2580369#

JOY_UP          =     20                ;joypad
JOY_DOWN        =     21
JOY_LEFT        =     22
JOY_RIGHT       =     23

FIRE_A          =     29                ;fire buttons
FIRE_B          =     25
FIRE_C          =     13
OPTION          =     9
PAUSE           =     28

KEY_STAR        =     16                ;keypad
KEY_7           =     17
KEY_4           =     18
KEY_1           =     19

KEY_0           =     4
KEY_8           =     5
KEY_5           =     6
KEY_2           =     7

KEY_HASH        =     0
KEY_9           =     1
KEY_6           =     2
KEY_3           =     3

ANY_JOY         =     $00F00000         ; AND joyedge with this...
                                        ; joypad was pressed if result is not 0
ANY_FIRE        =     $32002200         ; AND joyedge with this...
                                        ; A,B C, Option or Pause was pressed
                                        ; if result is not 0
ANY_KEY         =     $000F00FF         ; AND joyedge with this... 123456789*0#
                                        ; was pressed if result is not 0

;;;
;;;     ROM Tables built into Jerry - 128 samples each
;;;     16 bit samples sign extended to 32
;;;

ROM_TABLE       =     $F1D000           ; Base of tables

ROM_TRI         =     $F1D000           ; A triangle wave
ROM_SINE        =     $F1D200           ; Full amplitude SINE
ROM_AMSINE      =     $F1D400           ; Linear (?) ramp SINE
ROM_12W         =     $F1D600           ; SINE(X)+SINE(2*X) : (was ROM_SINE12W)
ROM_CHIRP16     =     $F1D800           ; SHORT SWEEP
ROM_NTRI        =     $F1DA00           ; Triangle w/NOISE
ROM_DELTA       =     $F1DC00           ; Positive spike
ROM_NOISE       =     $F1DE00           ; Guess

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;       JERRY Registers (DSP)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

D_FLAGS         =     $F1A100           ; DSP Flags
D_MTXC          =     $F1A104           ; DSP Matrix Control
D_MTXA          =     $F1A108           ; DSP Matrix Address
D_END           =     $F1A10C           ; DSP Data Organization
D_PC            =     $F1A110           ; DSP Program Counter
D_CTRL          =     $F1A114           ; DSP Operation Control/Status
D_MOD           =     $F1A118           ; DSP Modulo Instruction Mask
D_REMAIN        =     $F1A11C           ; DSP Division Remainder
D_DIVCTRL       =     $F1A11C           ; DSP Divider control
D_MACHI         =     $F1A120           ; DSP Hi byte of MAC operations
D_RAM           =     $F1B000           ; DSP Internal RAM
D_ENDRAM        =     D_RAM+(8*1024)    ; 8K bytes

;;;
;;; JERRY Flag Register equates
;;;

D_CPUENA        =     $00000010         ; CPU Interrupt Enable Bit
D_I2SENA        =     $00000020         ; I2S Interrupt Enable Bit
D_TIM1ENA       =     $00000040         ; Timer 1 Interrupt Enable Bit
D_TIM2ENA       =     $00000080         ; Timer 2 Interrupt Enable Bit
D_EXT0ENA       =     $00000100         ; External Interrupt 0 Enable Bit
D_EXT1ENA       =     $00010000         ; External Interrupt 1 Enable Bit

D_CPUCLR        =     $00000200         ; CPU Interrupt Clear Bit
D_I2SCLR        =     $00000400         ; I2S Interrupt Clear Bit
D_TIM1CLR       =     $00000800         ; Timer 1 Interrupt Clear Bit
D_TIM2CLR       =     $00001000         ; Timer 2 Interrupt Clear Bit
D_EXT0CLR       =     $00002000         ; External Interrupt 0 Clear Bit
D_EXT1CLR       =     $00020000         ; External Interrupt 1 Clear Bit

;;;
;;; JERRY Control/Status Register
;;;

DSPGO           =     $00000001         ; Start DSP
DSPINT0         =     $00000004         ; Generate a DSP Interrupt 0

D_CPULAT        =     $00000040         ; Interrupt Latches
D_I2SLAT        =     $00000080
D_TIM1LAT       =     $00000100
D_TIM2LAT       =     $00000200
D_EXT1LAT       =     $00000400
D_EXT2LAT       =     $00010000

;;;
;;; JERRY Modulo Instruction Masks
;;;

MODMASK2        =     $FFFFFFFE         ; 2 byte circular buffer
MODMASK4        =     $FFFFFFFC         ; 4 byte circular buffer
MODMASK8        =     $FFFFFFF8         ; 8 byte circular buffer
MODMASK16       =     $FFFFFFF0         ; 16 byte circular buffer
MODMASK32       =     $FFFFFFE0         ; 32 byte circular buffer
MODMASK64       =     $FFFFFFC0         ; 64 byte circular buffer
MODMASK128      =     $FFFFFF80         ; 128 byte circular buffer
MODMASK256      =     $FFFFFF00         ; 256 byte circular buffer
MODMASK512      =     $FFFFFE00         ; 512 byte circular buffer
MODMASK1K       =     $FFFFFC00         ; 1k circular buffer
MODMASK2K       =     $FFFFF800         ; 2k circular buffer
MODMASK4K       =     $FFFFF000         ; 4k circular buffer
MODMASK8K       =     $FFFFE000         ; 8k circular buffer
MODMASK16K      =     $FFFFC000         ; 16k circular buffer
MODMASK32K      =     $FFFF8000         ; 32k circular buffer
MODMASK64K      =     $FFFF0000         ; 64k circular buffer
MODMASK128K     =     $FFFE0000         ; 128k circular buffer
MODMASK256K     =     $FFFC0000         ; 256k circular buffer
MODMASK512K     =     $FFF80000         ; 512k circular buffer
MODMASK1M       =     $FFF00000         ; 1M circular buffer
        
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SHARED equates for TOM (GPU) and JERRY (DSP)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;
;;; Control/Status Registers
;;;

RISCGO          =     $00000001         ; Start GPU or DSP
CPUINT          =     $00000002         ; Allow the GPU/DSP to interrupt CPU
FORCEINT0       =     $00000004         ; Cause an INT 0 on GPU or DSP
SINGLE_STEP     =     $00000008         ; Enter SINGLE_STEP mode
SINGLE_GO       =     $00000010         ; Execute one instruction

REGPAGE         =     $00004000         ; Register Bank Select
DMAEN           =     $00008000         ; Enable DMA LOAD and STORE

;;;
;;; Flags Register
;;;

ZERO_FLAG       =     $00000001         ; ALU Zero Flag
CARRY_FLAG      =     $00000002         ; ALU Carry Flag
NEGA_FLAG       =     $00000004         ; ALU Negative Flag

IMASK           =     $00000008         ; Interrupt Service Mask

;;;
;;; Matrix Control Register
;;;

MATRIX3         =     $00000003         ; use for 3x1 Matrix
MATRIX4         =     $00000004         ; etc...
MATRIX5         =     $00000005
MATRIX6         =     $00000006
MATRIX7         =     $00000007
MATRIX8         =     $00000008
MATRIX9         =     $00000009
MATRIX10        =     $0000000A
MATRIX11        =     $0000000B
MATRIX12        =     $0000000C
MATRIX13        =     $0000000D
MATRIX14        =     $0000000E
MATRIX15        =     $0000000F
        
MATROW          =     $00000000         ; Access Matrix by Row
MATCOL          =     $00000010         ; Access Matrix by Column

;;;
;;; Data Organisation Register
;;;

BIG_IO          =     $00010001         ; Use 32-bit registers as big-endian
BIG_PIX         =     $00020002         ; Pixel organisation is big-endian
BIG_INST        =     $00040004         ; Word program fetches are big-endian

;;;
;;; Divide Unit Control
;;;

DIV_OFFSET      =     $00000001         ; Divide 16.16 values if set

