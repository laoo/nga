
Config{
  TmpPath = "tmp";
  OutPath = "out";
  LstPath = "out/nga.lst";
  ListingTextColumn = 26;
};

Jaguar{}

abs = ABS{
  OutputPath = "nga.abs";
};

code = BCode{
  abs;
  Domain( 0x4000, 0x1fffff )
};

data = BData{
  abs;
  Domain( 0x4000, 0x1fffff )
};

bss = SArea{
  Domain( 0x4000, 0x7fff )
};

Segments{
  Assembler{ BasePath = "src";  NamePattern = "\\.asm$"; };
};

