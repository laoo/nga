#ifndef GE5_LALRSTATE_HPP
#define GE5_LALRSTATE_HPP

namespace GE5Gen
{

struct LALRState
{

  struct Action
  {
    enum Type
    {
      Shift = 1,
      Reduce = 2,
      Goto = 3,
      Accept = 4
    };

    int symbolIndex;
    Type action;
    int targetIndex;
  };

  std::vector< Action > action;
};

}

#endif //GE5_LALRSTATE_HPP
