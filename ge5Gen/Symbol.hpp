#ifndef GE5_SYMBOL_HPP
#define GE5_SYMBOL_HPP

namespace GE5Gen
{

struct Symbol
{
  enum Type
  {
    Nonterminal = 0,
    Content = 1,
    Noise = 2,
    End = 3,
    GroupStart = 4,
    GroupEnd = 5,
    Error = 7
  } type;
  std::u16string name;
  size_t idx;
  std::optional<size_t> groupIdx;
};

}

#endif //GE5_SYMBOL_HPP
