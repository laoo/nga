#ifndef GE5_PRODUCTION_HPP
#define GE5_PRODUCTION_HPP

namespace GE5Gen
{
struct Production
{
  int nonterminal;
  std::vector<int> symbolIndices;
  std::u16string name;
};

}

#endif //GE5_PRODUCTION_HPP
