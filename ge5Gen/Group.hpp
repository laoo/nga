#ifndef GE5_GROUP_HPP
#define GE5_GROUP_HPP

namespace GE5Gen
{

struct Group
{
public:

  enum AdvanceMode
  {
    Token = 0,
    Character = 1
  };

  enum EndingMode
  {
    Open = 0,
    Closed = 1
  };
 
  std::u16string mName;
  int mContainerIndex;
  int mStartIndex;
  int mEndIndex;
  AdvanceMode mAdvanceMode;
  EndingMode mEndingMode;
  std::vector<int> mNesting;
};

}

#endif //GE5_GROUP_HPP
