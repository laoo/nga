#include "stdafx.h"
#include "Exception.hpp"
#include "ParserServer.hpp"
#include "Record.hpp"
#include "EGTReader.hpp"

namespace GE5Gen
{
namespace
{
std::string makeName( std::u16string src )
{
  std::stringstream ss;
  for ( auto c : src )
  {
    char c8 = (char)c;
    if ( (char16_t)c8 == c && ( isalnum( c8 ) || c8 == '_' ) )
    {
      ss << c8;
    }
    else
    {
      ss << "u" << std::setfill( '0' ) << std::setw( 4 ) << std::hex << (int)c << std::dec;
    }
  }
  return ss.str();
}

std::string makePrint( std::u16string src )
{
  std::stringstream ss;
  for ( auto c : src )
  {
    char c8 = (char)c;
    if ( (char16_t)c8 == c && isprint( c8 ) )
    {
      ss << c8;
    }
    else
    {
      ss << "u" << std::setfill( '0' ) << std::setw( 4 ) << std::hex << (int)c << std::dec;
    }
  }
  return ss.str();
}
}

std::shared_ptr<ParserServer> ParserServer::create( std::filesystem::path const& aTableFileName )
{
  std::ifstream fin{ aTableFileName, std::ios::binary };

  if ( fin.good() )
  {
    return create( fin );
  }
  else
    throw EGTException{ "Can't open EGT file" };
}

std::shared_ptr<ParserServer> ParserServer::create( char const* aBegin, char const* aEnd )
{
  return std::make_shared<ParserServer>( aBegin, aEnd );
}


std::shared_ptr<ParserServer> ParserServer::create( std::ifstream & fin )
{
  fin.seekg( 0, std::ios::end );
  size_t length = ( size_t )fin.tellg();
  fin.seekg( 0, std::ios::beg );

  std::vector<char> buf( length );
  fin.read( &buf[0], length );
  return create( buf );
}

std::shared_ptr<ParserServer> ParserServer::create( std::vector<char> const& tables )
{
  return create( &tables.front(), &tables.front() + tables.size() );
}

ParserServer::ParserServer( char const* aBegin, char const* aEnd ) : mDFAInitialState{}, mLALRInitialState{}
{
  char const* next = readHeader( aBegin );

  EGTReader reader{ next, aEnd };

  for ( ;; )
  {
    Record record = reader.read();
    if ( !record.visit( *this ) )
      break;
  }

  for ( auto & p : mProductionTable )
  {
    std::basic_stringstream<char16_t, std::char_traits<char16_t>, std::allocator<char16_t> > ss;
    ss << '<' << mSymbolTable[p.nonterminal]->name << '>' << ' ' << ':' << ':' << '=';

    if ( p.symbolIndices.empty() )
    {
      ss << ' ' << '<' << '>';
    }
    else
    {
      for ( auto it = p.symbolIndices.cbegin(); it != p.symbolIndices.cend() && ( ss << ' ', true ); ++it )
      {
        Symbol const& s = *mSymbolTable[*it];
        if ( s.type == Symbol::Nonterminal )
        {
          ss << '<' << s.name << '>';
        }
        else if ( s.type == Symbol::Content )
        {
          ss << s.name;
        }
        else
        {
          assert( false );
        }
      }
    }
    p.name = ss.str();
  }

}

char const* ParserServer::readHeader( char const* aHeader ) const
{
#ifdef WIN32
  static wchar_t const header[] = L"GOLD Parser Tables/v5.0";
  typedef wchar_t const wtype;
#else
  static char16_t const header[] = u"GOLD Parser Tables/v5.0";
  typedef char16_t const wtype;
#endif

  bool eq = std::equal( header, header + sizeof( header ) / sizeof( wtype ), reinterpret_cast< wtype* >( aHeader ) );

  if ( !eq )
    throw EGTException{ "GOLD Parser Tables/v5.0" };

  return aHeader + sizeof header;
}

void ParserServer::addProperty( int index, std::u16string name, std::u16string value )
{
  mProperties.insert( std::make_pair( std::move( name ), std::move( value ) ) );
}

void ParserServer::setTableCounts( int symbolTable, int characterSetTable, int ruleTable, int dfaTable, int lalrTable, int groupTable )
{
  mSymbolTable.resize( symbolTable );
  mCharacterSetTable.resize( characterSetTable );
  mProductionTable.resize( ruleTable );
  mDFATable.resize( dfaTable );
  mLALRTable.resize( lalrTable );
  mGroupTable.resize( groupTable );
}

void ParserServer::setInitialState( int dfa, int lalr )
{
  mDFAInitialState = dfa;
  mLALRInitialState = lalr;

}

void ParserServer::setCharacterSetTable( int index, int unicodePlane, std::vector< CharacterSetTable::CharacterSetRange > ranges )
{
  mCharacterSetTable[index].reset( new CharacterSetTable{ std::move( ranges ) } );
}

void ParserServer::addSymbol( int index, std::u16string name, Symbol::Type type )
{
  mSymbolTable[index].reset( new  Symbol{ type, std::move( name ), ( size_t )index, std::optional<size_t>{} } );
}

void ParserServer::addGroup( int index, std::u16string name, int containerIndex, int startIndex, int endIndex, Group::AdvanceMode advanceMode, Group::EndingMode endingMode, std::vector<int> nesting )
{
  mGroupTable[index] = Group{ std::move( name ), containerIndex, startIndex, endIndex, advanceMode, endingMode, std::move( nesting ) };

  mSymbolTable[containerIndex]->groupIdx = index;
  mSymbolTable[startIndex]->groupIdx = index;
  mSymbolTable[endIndex]->groupIdx = index;
}

void ParserServer::addProduction( int index, int nonterminal, std::vector<int> symbolIndices )
{
  mProductionTable[index] = Production{ nonterminal, std::move( symbolIndices ) };
}

void ParserServer::addDFAState( int index, std::optional<int> terminalIndex, std::vector< DFAState::Edge > edges )
{
  mDFATable[index] = DFAState{ std::move( terminalIndex ), std::move( edges ) };
}

void ParserServer::addLALRState( int index, std::vector< LALRState::Action > actions )
{
  mLALRTable[index] = LALRState{ std::move( actions ) };
}

void ParserServer::emitStaticCode( std::string const& nsName, std::ostream & out ) const
{
  std::map<size_t, std::string> terminals;
  std::map<size_t, std::string> nonterminals;

  for ( size_t i = 0; i < mSymbolTable.size(); ++i )
  {
    if ( mSymbolTable[i]->type == Symbol::Type::Content )
    {
      std::stringstream ss;
      ss << "T" << makeName( mSymbolTable[i]->name );
      terminals.insert( std::make_pair( i, ss.str() ) );
    }
    else if ( mSymbolTable[i]->type == Symbol::Type::Nonterminal )
    {
      std::stringstream ss;
      ss << "std::shared_ptr<" << makeName( mSymbolTable[i]->name ) << ">";
      nonterminals.insert( std::make_pair( i, ss.str() ) );
    }
  }

  out << "#pragma once\n";
  out << "namespace assembler\n";
  out << "{\n";
  emitTerminals( out, terminals, nonterminals );
  emitReductors( out, nonterminals, terminals );
  out << "\n";
  out << "namespace " << nsName << "\n";
  out << "{\n";
  out << "\n";
  out << "class Reductor : public " << "nga::Reductor\n";
  out << "{\n";
  out << "  static constexpr int const initialDFAState = " << mDFAInitialState << ";\n";
  out << "  static constexpr int const initialLALRState = " << mLALRInitialState << ";\n";
  emitCharacterSetTable( out );
  emitSymbolTable( out );
  emitGroupTable( out );
  emitProductionTable( out );
  emitDFATable( out );
  emitLALRTable( out );
  emitDispatch( nsName, out, terminals, nonterminals );
  out << "};\n";
  out << "}\n";
  out << "}\n";
}

void ParserServer::emitTerminals( std::ostream& out, std::map<size_t, std::string>& terminals, std::map<size_t, std::string>& nonterminals ) const
{
  for ( size_t i = 0; i < mSymbolTable.size(); ++i )
  {
    if ( mSymbolTable[i]->type == Symbol::Type::Content )
    {
      out << "\n";
      out << "// " << makePrint( mSymbolTable[i]->name ) << std::endl;
      out << "#ifndef DECLARED_" << terminals.at( i ) << "\n";
      out << "#define DECLARED_" << terminals.at( i ) << "\n";
      out << "struct " << terminals.at( i ) << " : public std::string_view {};\n";
      out << "#endif // DECLARED_" << terminals.at( i ) << "\n";
    }
  }
}

void ParserServer::emitReductors( std::ostream& out, std::map<size_t, std::string>& nonterminals, std::map<size_t, std::string>& terminals ) const
{
  out << "\n";
  for ( auto& p : mProductionTable )
  {
    out << "// " << boost::locale::conv::from_utf( boost::locale::conv::utf_to_utf<wchar_t>( p.name ), "UTF-8" ) << "\n";
    out << "std::shared_ptr<void> reduce" << makeName( mSymbolTable[p.nonterminal]->name ) << "( nga::ICompilationContext* ctx";
    if ( !p.symbolIndices.empty() )
      out << ", ";
    for ( size_t i = 0; i < p.symbolIndices.size(); ++i )
    {
      Symbol const& s = *mSymbolTable[p.symbolIndices[i]];
      if ( s.type == Symbol::Nonterminal )
      {
        out << nonterminals.at( p.symbolIndices[i] ) << " n" << i;
      }
      else if ( s.type == Symbol::Content )
      {
        out << terminals.at( p.symbolIndices[i] ) << " t" << i;
      }
      else
      {
        assert( false );
      }
      if ( i < p.symbolIndices.size() - 1 )
        out << ", ";
    }
    out << " );\n";
  }
}

void ParserServer::emitCharacterSetTable( std::ostream & out ) const
{
  size_t cstSize{};
  std::vector<std::pair<size_t, size_t>> ranges;
  for ( auto const& cst : mCharacterSetTable )
  {
    std::pair<size_t, size_t> r{};
    r.first = cstSize;
    cstSize += cst->ranges.size();
    r.second = cstSize;
    ranges.push_back( r );
  }

  out << "  static constexpr nga::CharacterSetRangeList::Range rs[]\n";
  out << "  {\n";
  for ( size_t i = 0; i < mCharacterSetTable.size(); ++i )
  {
    std::stringstream ss;
    for ( size_t j = 0; j < mCharacterSetTable[i]->ranges.size(); ++j )
    {
      ss << "nga::CharacterSetRangeList::Range{ ";
      ss << "u'\\u" << std::setfill( '0' ) << std::setw( 4 ) << std::hex << (int)mCharacterSetTable[i]->ranges[j].start << "'";
      ss << ", ";
      ss << "u'\\u" << std::setw( 4 ) << std::hex << (int)mCharacterSetTable[i]->ranges[j].end << "'";
      ss << " }";
      if ( j < mCharacterSetTable[i]->ranges.size() - 1 )
        ss << ", ";
    }
    out << "    " << ss.str();
    if ( i < mCharacterSetTable.size() - 1 )
      out << ",";
    out << "\n";
  }

  out << "  };\n";
  out << "  static constexpr std::array<nga::CharacterSetRangeList," << mCharacterSetTable.size() << "> characterSetTable\n";
  out << "  {\n";

  for ( size_t i = 0; i < ranges.size(); ++i )
  {
    out << "    nga::CharacterSetRangeList{ rs + " << ranges[i].first << ", rs + " << ranges[i].second << " }";
    if ( i < ranges.size() - 1 )
      out << ",";
    out << "\n";
  }
  out << "  };\n";
}

void ParserServer::emitSymbolTable( std::ostream & out ) const
{
  out << "  static constexpr std::array<nga::Symbol," << mSymbolTable.size() << "> symbolTable\n";
  out << "  {\n";
  for ( size_t i = 0; i < mSymbolTable.size(); ++i )
  {
    out << "    nga::Symbol{ ";
    out << "u\"" << makePrint( mSymbolTable[i]->name ) << "\", ";
    switch ( mSymbolTable[i]->type )
    {
      case Symbol::Type::Nonterminal:
        out << "nga::Symbol::Nonterminal, ";
        break;
      case Symbol::Type::Content:
        out << "nga::Symbol::Content, ";
        break;
      case Symbol::Type::Noise:
        out << "nga::Symbol::Noise, ";
        break;
      case Symbol::Type::End:
        out << "nga::Symbol::End, ";
        break;
      case Symbol::Type::GroupStart:
        out << "nga::Symbol::GroupStart, ";
        break;
      case Symbol::Type::GroupEnd:
        out << "nga::Symbol::GroupEnd, ";
        break;
      case Symbol::Type::Error:
        out << "nga::Symbol::Error, ";
        break;
      default:
        assert( false );
        break;
    }
    out << mSymbolTable[i]->idx << ", {";
    if ( mSymbolTable[i]->groupIdx )
    {
      out << *mSymbolTable[i]->groupIdx;
    }
    out << "} }";
    if ( i < mSymbolTable.size() - 1 )
      out << ",";
    out << "\n";
  }
  out << "  };\n";
}

void ParserServer::emitGroupTable( std::ostream & out ) const
{
  if ( mGroupTable.empty() )
    return;
  out << "  static constexpr std::array<nga::Group," << mGroupTable.size() << "> groupTable\n";
  out << "  {\n";
  for ( size_t i = 0; i < mGroupTable.size(); ++i )
  {
    out << "    nga::Group{ ";
    out << "u\"" << makePrint( mGroupTable[i].mName ) << "\", ";
    out << mGroupTable[i].mContainerIndex << ", ";
    out << mGroupTable[i].mStartIndex << ", ";
    out << mGroupTable[i].mEndIndex << ", ";
    switch ( mGroupTable[i].mAdvanceMode )
    {
      case Group::AdvanceMode::Token:
        out << "nga::Group::Token, ";
        break;
      case Group::AdvanceMode::Character:
        out << "nga::Group::Character, ";
        break;
      default:
        assert( false );
        break;
    }
    switch ( mGroupTable[i].mEndingMode )
    {
      case Group::EndingMode::Open:
        out << "nga::Group::Open }";
        break;
      case Group::EndingMode::Closed:
        out << "nga::Group::Closed }";
        break;
      default:
        assert( false );
        break;
    }
    if ( i < mGroupTable.size() - 1 )
      out << ",";
    out << "\n";
    if ( mGroupTable[i].mNesting.size() > 0 )
      throw GE5Gen::Exception{ "emitGroupTable nesting" };
  }
  out << "  };\n";
}

void ParserServer::emitProductionTable( std::ostream & out ) const
{

  std::vector<std::pair<size_t, size_t>> ranges{};
  std::vector<int> sit{};
  for ( auto const& p : mProductionTable )
  {
    std::pair<size_t, size_t> r{};
    r.first = sit.size();
    std::copy( p.symbolIndices.cbegin(), p.symbolIndices.cend(), std::back_inserter( sit ) );
    r.second = sit.size();
    ranges.push_back( r );
  }

  out << "  static constexpr std::array<nga::Production," << mProductionTable.size() << "> productionTable\n";
  out << "  {\n";
  for ( size_t i = 0; i < mProductionTable.size(); ++i )
  {
    out << "    nga::Production{ " << mProductionTable[i].nonterminal << " }";
    if ( i < mProductionTable.size() - 1 )
      out << ",";
    out << " // " << i << ": " << boost::locale::conv::from_utf( boost::locale::conv::utf_to_utf<wchar_t>( mProductionTable[i].name ), "UTF-8" ) << "\n";
  }
  out << "  };\n";
}

void ParserServer::emitDFATable( std::ostream & out ) const
{
  std::vector<std::pair<size_t, size_t>> ranges{};
  std::vector<DFAState::Edge> et{};
  for ( auto const& dfa : mDFATable )
  {
    std::pair<size_t, size_t> r{};
    r.first = et.size();
    std::copy( dfa.edges.cbegin(), dfa.edges.cend(), std::back_inserter( et ) );
    r.second = et.size();
    ranges.push_back( r );
  }
  out << "  static constexpr nga::DFAState::Edge edges[]\n";
  out << "  {\n";
  bool next{};
  for ( size_t i = 0; i < mDFATable.size(); ++i )
  {
    if ( !mDFATable[i].edges.empty() )
    {
      std::stringstream ss;
      for ( size_t j = 0; j < mDFATable[i].edges.size(); ++j )
      {
        ss << "nga::DFAState::Edge{ " << mDFATable[i].edges[j].charSetIndex << ", " << mDFATable[i].edges[j].targetIndex << " }";
        if ( j < mDFATable[i].edges.size() - 1 )
          ss << ", ";
      }
      if ( next )
      {
        out << ",\n";
        next = false;
      }
      out << "    " << ss.str();
    }
    if ( i < mDFATable.size() - 1 )
      next = true;
  }
  out << "\n  };\n";
  out << "  static constexpr std::array<nga::DFAState," << mDFATable.size() << "> dfaTable\n";
  out << "  {\n";
  for ( size_t i = 0; i < mDFATable.size(); ++i )
  {
    if ( ranges[i].first != ranges[i].second )
    {
      out << "    nga::DFAState{ edges + " << ranges[i].first << ", edges + " << ranges[i].second << ", {";
    }
    else
    {
      out << "    nga::DFAState{ nullptr, nullptr, {";
    }
    if ( mDFATable[i].terminalIndex )
      out << " " << *mDFATable[i].terminalIndex << " ";
    out << "} }";
    if ( i < mDFATable.size() - 1 )
      out << ",";
    out << "\n";
  }
  out << "  };\n";
}

void ParserServer::emitLALRTable( std::ostream & out ) const
{
  std::vector<std::pair<size_t, size_t>> ranges{};
  std::vector<LALRState::Action> as{};
  for ( auto const& lalr : mLALRTable )
  {
    std::pair<size_t, size_t> r{};
    r.first = as.size();
    std::copy( lalr.action.cbegin(), lalr.action.cend(), std::back_inserter( as ) );
    r.second = as.size();
    ranges.push_back( r );
  }
  out << "  static constexpr nga::LALRState::Action as[]\n";
  out << "  {\n";
  for ( size_t i = 0; i < mLALRTable.size(); ++i )
  {
    if ( !mLALRTable[i].action.empty() )
    {
      for ( size_t j = 0; j < mLALRTable[i].action.size(); ++j )
      {
        out << "    nga::LALRState::Action{ ";
        switch ( mLALRTable[i].action[j].action )
        {
          case LALRState::Action::Shift:
            out << "nga::LALRState::Action::Shift, ";
            break;
          case LALRState::Action::Reduce:
            out << "nga::LALRState::Action::Reduce, ";
            break;
          case LALRState::Action::Goto:
            out << "nga::LALRState::Action::Goto, ";
            break;
          case LALRState::Action::Accept:
            out << "nga::LALRState::Action::Accept, ";
            break;
          default:
            assert( false );
        }
        out << mLALRTable[i].action[j].symbolIndex << ", ";
        out << mLALRTable[i].action[j].targetIndex << " }";
        if ( j < mLALRTable[i].action.size() - 1 || i < mLALRTable.size() - 1 )
          out << ",";
        out << "// " << i << std::endl;
      }
    }
  }
  out << "  };\n";
  out << "  static constexpr std::array<nga::LALRState," << mLALRTable.size() << "> lalrTable\n";
  out << "  {\n";
  for ( size_t i = 0; i < mLALRTable.size(); ++i )
  {
    if ( ranges[i].first != ranges[i].second )
    {
      out << "    nga::LALRState{ as + " << ranges[i].first << ", as + " << ranges[i].second << " }";
    }
    else
    {
      out << "    nga::LALRState{ nullptr, nullptr }";
    }
    if ( i < mLALRTable.size() - 1 )
      out << ",";
    out << "\n";
  }
  out << "  };\n";
}


void ParserServer::emitDispatch( std::string const& nsName, std::ostream & out, std::map<size_t, std::string> const& terminals, std::map<size_t, std::string> const& nonterminals ) const
{
  out << std::endl;
  out << "public:\n";
  out << "  Reductor( nga::ICompilationContext* ctx ) : nga::Reductor{ctx,{characterSetTable.data(),characterSetTable.size()},{symbolTable.data(),symbolTable.size()},{groupTable.data(),groupTable.size()},{productionTable.data(),productionTable.size()},{dfaTable.data(),dfaTable.size()},{lalrTable.data(),lalrTable.size()},initialDFAState,initialLALRState}{}\n";
  out << "  ~Reductor() override = default;\n";
  out << std::endl;
  out << "  std::shared_ptr<void> reduce( std::vector<nga::Token> & stack, size_t reduction ) const override\n";
  out << "  {\n";
  out << "    switch ( reduction )\n";
  out << "    {\n";
  for ( size_t i = 0; i < mProductionTable.size(); ++i )
  {
    out << "    case " << i << ": //" << boost::locale::conv::from_utf( boost::locale::conv::utf_to_utf<wchar_t>( mProductionTable[i].name ), "UTF-8" ) << "\n";
    out << "    {\n";
    for ( int j = (int)mProductionTable[i].symbolIndices.size() - 1; j >= 0; --j )
    {
      Symbol const& s = *mSymbolTable[mProductionTable[i].symbolIndices[j]];
      if ( s.type == Symbol::Nonterminal )
      {
        out << "      " << nonterminals.at( mProductionTable[i].symbolIndices[j] ) << " n" << j << "{ stack.back().cast<" << makePrint( mSymbolTable[mProductionTable[i].symbolIndices[j]]->name ) << ">() };";
      }
      else if ( s.type == Symbol::Content )
      {
        out << "      " << terminals.at( mProductionTable[i].symbolIndices[j] ) << " t" << j << "{ stack.back().lexeme.content };";
      }
      out << " stack.pop_back();\n";
    }
    out << "      return reduce" << makeName( mSymbolTable[mProductionTable[i].nonterminal]->name )  << "( mCtx";
    if ( !mProductionTable[i].symbolIndices.empty() )
      out << ", ";
    for ( int j = 0; j < (int)mProductionTable[i].symbolIndices.size(); ++j )
    {
      Symbol const& s = *mSymbolTable[mProductionTable[i].symbolIndices[j]];
      out << "std::move( ";
      if ( s.type == Symbol::Nonterminal )
      {
        out << "n";
      }
      else if ( s.type == Symbol::Content )
      {
        out << "t";
      }
      out << j << " )";
      if ( j < (int)mProductionTable[i].symbolIndices.size() - 1 )
        out << ", ";
    }
    out <<  " );\n";
    out << "    }\n";
  }
  out << "    default:\n";
  out << "      return {};\n";
  out << "    }\n";
  out << "  }\n";
  out << std::endl;
}

}



