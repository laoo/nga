#pragma once

#include "IInputSource.hpp"

namespace nga
{

class IInputTextSource : public IInputSource
{
public:
  IInputTextSource() : IInputSource{} {}
  virtual ~IInputTextSource() = default;

  virtual std::pair<Pos, std::string_view> nextLine() = 0;
  virtual std::pair<Pos, std::string_view> currentLine() const = 0;
  virtual std::string_view line( uint32_t pos ) const { return {}; }
  virtual Pos currentPos() const = 0;
 
};

}

