#pragma once

#include "Output.hpp"
#include "Segment.hpp"

namespace nga::output
{

class TOS : public Output
{
public:

  enum class SegmentType
  {
    CODE,
    DATA,
    BSS
  };

  TOS( sol::table const& tab );
  ~TOS() override = default;

  static std::shared_ptr<Output> load( std::string_view name, sol::table const& tab );

  void setEntry( Value v ) override;
  void addSegment( Segment* segment ) override;
  void commit() override;

private:
  struct Block
  {
    int32_t offset; //from code start or data start
    std::span<uint8_t const> data;
    std::span<int32_t const> relocations;
    int32_t absOffset;

    bool operator<( Block const & right ) const
    {
      return offset < right.offset;
    }
  };

  cppcoro::generator<int32_t> relocations() const;


private:
  std::filesystem::path mPath;
  std::vector<Block> mCodeBlocks;
  std::vector<Block> mDataBlocks;
  uint32_t mCodeSize;
  uint32_t mDataSize;
  uint32_t mBssSize;
  uint32_t mRelocations;
};

}

