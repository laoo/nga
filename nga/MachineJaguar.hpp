#pragma once

#include "Machine.hpp"

namespace nga::machine
{

class MachineJaguar : public Machine
{
public:

  MachineJaguar();
  ~MachineJaguar() override = default;

  static void registerLUA( sol::state& lua );

};

}

