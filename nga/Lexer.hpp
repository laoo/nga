#pragma once

#include "GrammarStructs.hpp"

namespace nga
{

class Lexer
{
public:
  Lexer( std::shared_ptr<Reductor const> r );

  void resetText( std::string_view text );
  Lexeme produceLexeme();
  bool setMacroMode( nga::Lexeme& lexeme );

private:
  Lexeme lookaheadDFA( Group::AdvanceMode advanceMode );
  int lookahead( size_t aIdx );
  std::string_view lookaheadString( size_t size ) const;
  void comsumeBuffer( size_t aCnt );
  Lexeme makeFirstIdLabel( Lexeme lexeme );
  Lexeme produceMacro();

private:
  std::shared_ptr<Reductor const> mReductor;
  Symbol const* mEOF;
  Symbol const* mWhitespace;
  Symbol const* mComment;
  Symbol const* mId;
  Symbol const* mLabel;
  Symbol const* mMacroInvLit;
  Symbol const* mMacroInvParam;
  int const mInitialDFA;
  std::string_view mText;
  size_t mOffset;
  std::vector<Lexeme> mGroupStack;
  int mLexems;
  bool mMacroMode;


  static constexpr int EOFChar = 0xdb00;

};

}