#include "pch.hpp"
#include "Machine6502.hpp"
#include "Ex.hpp"
#include "MachineContext.hpp"
#include "Segment.hpp"
#include "Project.hpp"
#include "MachineA8.hpp"
#include "MachineC64.hpp"

namespace nga::machine
{

Machine6502::Machine6502( Type type ) : Machine{ Processor::PROC_6502, type }
{
}

void Machine6502::registerLUA( sol::state& lua )
{
  MachineA8::registerLUA( lua );
  MachineC64::registerLUA( lua );
}

}

