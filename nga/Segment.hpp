#pragma once

#include "Value.hpp"
#include "SegmentReferenceKind.hpp"

namespace nga
{
class SegmentMatch;
class IChunk;
class MachineContext;
class SegmentDescriptor;
class SegmentProcessor;
class ILinkingContext;
class ISymbol;

class Segment : public std::enable_shared_from_this<Segment>
{
public:

  Segment();
  ~Segment() = default;
  Segment( Segment const& ) = delete;
  Segment & operator=( Segment const& ) = delete;

  struct ChunkOffset
  {
    Segment* seg;
    size_t off;
  };

  void append( std::shared_ptr<IChunk const> chunk );
  void emit( ILinkingContext& lc );
  void setSegmentDescriptorName( std::string name );
  void setMachineContext( MachineContext * ctx );
  void sizeSegment( ILinkingContext& ctx );
  void setAddress( int32_t off );
  void addRelocation( int32_t off );

  ChunkOffset chunkOffset();

  void emitByte( int v );
  void emitBlock( std::span<uint8_t const> block );
  void reserve( size_t size );

  Value chunkOffsetValue( ILinkingContext& ctx, size_t off );
  Value byteOffsetValue( ILinkingContext& ctx, size_t off );
  std::optional<int32_t> address() const;
  std::span<int32_t const> relocations() const;
  std::span<uint8_t const> data() const;
  uint32_t size() const;
  int addressSize() const;
  SegmentDescriptor & segmentDescriptor();
  void addAnonymousLabel();
  std::optional<Value> anonymousLabelAddressFromCurrentChunk( ILinkingContext& ctx, size_t processedChunkIndex, int distance );
  bool constructed() const;
  void flagConstructed();
  uint8_t newZone();
  bool isExecutable();
  void registerReference( std::string reference, nga::SegmentReferenceKind kind );

  void createProcessor( std::shared_ptr<ISymbol const> pEntrySymbol  );
  void createProcessor();
  std::shared_ptr<SegmentProcessor> getProcessor();
  void iterateReferences();
  void visit( std::shared_ptr<ISymbol const> pEntrySymbol );
  void visit();
  void commit();
  MachineContext* getMachineContext() const;
  void printListing( std::ostream& out ) const;
  void setDependentSegment( Segment* segment );

private:
  SegmentDescriptor& lazyGetSegmentDescriptor();

private:
  std::string mSegmentDescriptorName;
  std::vector<std::shared_ptr<IChunk const>> mChunks;
  std::vector<std::pair<uint32_t,uint32_t>> mChunkOffSizes;
  class References
  {
    std::string mData;
  public:

    struct It
    {
      std::string_view ctx;
      std::string_view name;
      nga::SegmentReferenceKind kind;
    };

    void add( std::string const& ref, nga::SegmentReferenceKind kind );
    cppcoro::generator<It> iterate() const;
  private:
    char * find( std::string const& ref );
  } mReferences;
  std::vector<int32_t> mRelocations;
  std::vector<uint8_t> mData;
  std::vector<size_t> mAnonymousLabels;
  std::optional<int32_t> mAddress;
  std::shared_ptr<SegmentProcessor> mProcessor;
  uint32_t mSegmentSize;
  uint32_t mZones;
  SegmentDescriptor * mSegmentDescriptor;
  MachineContext * mMachineContext;
  Segment * mDependentSegment;
  bool mConstructed;
};

}
