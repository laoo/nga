#include "pch.hpp"
#include "TOS.hpp"
#include "Ex.hpp"
#include "Value.hpp"
#include "Project.hpp"
#include "Segment.hpp"
#include "SegmentDescriptor.hpp"

namespace nga::output
{

std::shared_ptr<Output> TOS::load( std::string_view name, sol::table const & tab )
{
  if ( name != "TOS" )
    return {};

  return std::make_shared<TOS>( tab );
}

TOS::TOS( sol::table const & tab ) : mPath{}, mCodeBlocks{}, mDataBlocks{}, mCodeSize{}, mDataSize{}, mBssSize{}, mRelocations{}
{
  if ( auto opt = tab.get<sol::optional<std::string_view>>( "OutputFile" ) )
  {
    mPath = std::filesystem::absolute( project().config().getOut() / *opt );
  }
  else
  {
    throw Ex{ "No OutputFile in TOS Output" };
  }
}

void TOS::setEntry( Value v )
{
  auto [seg, off] = v.segmentOffset();

  if ( seg->address() != 0 || off != 0 )
    throw Ex{ "TOS main symbol must start the code segment" };
}

void TOS::addSegment( Segment* segment )
{
  INTERNAL_ERROR; //TODO

  //if ( !segment->segmentDescriptor().emptyDomain() )
  //  throw Ex{ "Absolute segments are not supported in TOS output" };

  //SegmentType type;
  //if ( auto outAlloc = std::dynamic_pointer_cast<OutAllocator>( segment->segmentDescriptor().outAllocator() ) )
  //{
  //  type = outAlloc->getType();
  //}
  //else
  //  throw Ex{ "No TOS output allocator found" };

  //switch ( type )
  //{
  //case SegmentType::CODE:
  //  mCodeSize += segment->size();
  //  mRelocations += (uint32_t)segment->relocations().size();
  //  mCodeBlocks.push_back( { *segment->address(), segment->data(), segment->relocations() }  );
  //  break;
  //case SegmentType::DATA:
  //  mDataSize += segment->size();
  //  mRelocations += (uint32_t)segment->relocations().size();
  //  mDataBlocks.push_back( { *segment->address(), segment->data(), segment->relocations() } );
  //  break;
  //case SegmentType::BSS:
  //  mBssSize += segment->size();
  //  break;
  //default:
  //  INTERNAL_ERROR;
  //}
}

void TOS::commit()
{
  if ( mCodeBlocks.empty() )
  {
    throw Ex{ "Empty output" };
  }

  std::sort( mCodeBlocks.begin(), mCodeBlocks.end() );
  if ( mCodeBlocks.back().offset + mCodeBlocks.back().data.size() != mCodeSize )
    throw Ex{ "Inconsistent code blocks: last address is different than code size" };

  std::sort( mDataBlocks.begin(), mDataBlocks.end() );
  if ( mDataBlocks.back().offset + mDataBlocks.back().data.size() != mDataSize )
    throw Ex{ "Inconsistent data blocks: last address is different than data size" };

  struct Image
  {
    std::vector<uint8_t> data;

    void put( uint8_t v )
    {
      data.push_back( v );
    }

    void put( uint16_t v )
    {
      data.push_back( ( v >> 8 ) & 0xff );
      data.push_back( v & 0xff );
    }

    void put( uint32_t v )
    {
      data.push_back( ( v >> 24 ) & 0xff );
      data.push_back( ( v >> 16 ) & 0xff );
      data.push_back( ( v >> 8 ) & 0xff );
      data.push_back( v & 0xff );
    }
  } image;


  
  //PRG_magic
  image.put( (uint16_t)0x601a );

  //PRG_tsize
  image.put( mCodeSize );
  //PRG_dsize
  image.put( mDataSize );
  //PRG_bsize
  image.put( mBssSize );
  //PRG_ssize
  image.put( (uint32_t)0 );
  //PRG_res1
  image.put( (uint32_t)0 ); //0 for symbols in Digital Research format
  //PRGFLAGS
  image.put( (uint32_t)0 );
  //ABSFLAG
  image.put( (uint16_t)0 );

  int32_t textOffset = 0;
  for ( auto & block : mCodeBlocks )
  {
    block.absOffset = textOffset;
    std::copy( block.data.begin(), block.data.end(), std::back_inserter( image.data ) );
    textOffset += (uint32_t)block.data.size();
  }

  for ( auto & block : mDataBlocks )
  {
    block.absOffset = textOffset;
    std::copy( block.data.begin(), block.data.end(), std::back_inserter( image.data ) );
    textOffset += (uint32_t)block.data.size();
  }

  if ( mRelocations == 0 )
  {
    //Fixup Offset
    image.put( (uint32_t)0 );
  }
  else
  {
    uint32_t off = 0; //0 offset won't be relocated ever

    for ( int32_t rel : relocations() )
    {
      if ( off )
      {
        uint32_t newOff = rel;
        while ( ( newOff - off ) > 254 )
        {
          image.put( (uint8_t)1 );
          off += 254;
        }
        image.put( (uint8_t)( newOff - off ) );
        off = newOff;
      }
      else
      {
        off = rel;
        image.put( off );
      }
    }

    image.put( ( uint8_t )0 );
  }

  std::filesystem::create_directories( mPath.parent_path() );

  std::ofstream fout{ mPath, std::ios::binary };
  fout.write( (char const*)image.data.data(), image.data.size() );
 }

 cppcoro::generator<int32_t> TOS::relocations() const
 {
   for ( auto const & block : mCodeBlocks )
   {
     for ( int32_t rel : block.relocations )
     {
       co_yield ( block.absOffset + rel );
     }
   }
   for ( auto const & block : mDataBlocks )
   {
     for ( int32_t rel : block.relocations )
     {
       co_yield ( block.absOffset + rel );
     }
   }
 }

}
