#include "pch.hpp"
#include "Machine68000.hpp"
#include "Ex.hpp"
#include "Project.hpp"

namespace nga::machine
{

Machine68000::Machine68000( Type type ) : Machine{ Processor::PROC_68000, type }
{
  std::fill( mSectionAllocationOffsets.begin(), mSectionAllocationOffsets.end(), 0 );
}

void Machine68000::registerLUA( sol::state& lua )
{
  lua["ST"] = []( sol::table const& tab )
  {
    auto machine = machine::Machine68000::load( tab );
    project().setMachine( std::move( machine ) );
  };
}

std::shared_ptr<Machine> Machine68000::load( sol::table const & tab )
{
    return std::make_shared<Machine68000>( Type::ST );
}

uint32_t Machine68000::allocateVirtualSegment( nga::output::TOS::SegmentType t, uint32_t size )
{
  switch ( t )
  {
  case nga::output::TOS::SegmentType::BSS:
  case nga::output::TOS::SegmentType::CODE:
  case nga::output::TOS::SegmentType::DATA:
    return allocateVirtualSegment( (uint32_t)t - 1, size );
  default:
    throw Ex{ "Unsupported virtual segment" };
  }
}

uint32_t Machine68000::allocateVirtualSegment( uint32_t idx, uint32_t size )
{
  uint32_t result = mSectionAllocationOffsets[idx];
  mSectionAllocationOffsets[idx] += size;
  return result;
}

}

