#include "pch.hpp"
#include "Segment.hpp"
#include "Ex.hpp"
#include "ISymbol.hpp"
#include "Project.hpp"
#include "MachineContext.hpp"
#include "SegmentDescriptor.hpp"
#include "IChunk.hpp"
#include "SegmentProcessor.hpp"
#include "SegmentReferenceKind.hpp"

namespace nga
{
namespace
{
}

Segment::Segment() : mSegmentDescriptorName{}, mChunks{}, mChunkOffSizes{}, mReferences{}, mRelocations{}, mData{}, mAddress{}, mProcessor{}, mSegmentSize{}, mZones{}, mSegmentDescriptor{}, mMachineContext{}, mDependentSegment{}, mConstructed{}
{
}

Segment::ChunkOffset Segment::chunkOffset()
{
  return ChunkOffset{ this, mChunks.size() };
}

void Segment::append( std::shared_ptr<IChunk const> chunk )
{
  mChunks.push_back( std::move( chunk ) );
  mChunkOffSizes.push_back( {} );
}

void Segment::setSegmentDescriptorName( std::string name )
{
  mSegmentDescriptorName = std::move( name );
}

void Segment::setMachineContext( MachineContext * mctx )
{
  mMachineContext = mctx;
}

void Segment::sizeSegment( ILinkingContext& ctx )
{
  for ( size_t i = 0; i < mChunks.size(); ++i )
  {
    mChunkOffSizes[i] = { mSegmentSize, mChunks[i]->size( ctx ) };
    mSegmentSize += mChunkOffSizes[i].second;
    mMachineContext->advanceProgress();
  }

  mData.reserve( mSegmentSize );
}

Value Segment::chunkOffsetValue( ILinkingContext& ctx, size_t off )
{
  if ( off >= mChunkOffSizes.size() )
  {
    throw Ex{ "Segment offset out of range" };
  }
  return nga::Value::create( this, (int)mChunkOffSizes[off].first );
}

Value Segment::byteOffsetValue( ILinkingContext& ctx, size_t off )
{
  return nga::Value::create( this, (int)off );
}

std::optional<int32_t> Segment::address() const
{
  return mAddress;
}

std::span<int32_t const> Segment::relocations() const
{
  return std::span<int32_t const>{ mRelocations.data(), mRelocations.size() };
}

std::span<uint8_t const> Segment::data() const
{
  return std::span<uint8_t const>{ mData.data(), mData.size() };
}

void Segment::setAddress( int32_t off )
{
  mAddress = off;
}

void Segment::emit( ILinkingContext& lc )
{
  assert( mChunks.size() == mChunkOffSizes.size() );

  for ( size_t i = 0; i < mChunks.size(); ++i )
  {
    lc.setProcessedChunkIndex( i );
    mChunks[i]->emit( lc );
    mMachineContext->advanceProgress();
  }
}

uint32_t Segment::size() const
{
  return mSegmentSize;
}

int Segment::addressSize() const
{
  if ( mSegmentDescriptor )
    return mSegmentDescriptor->addressSize();
  else
    INTERNAL_ERROR;
}

SegmentDescriptor & Segment::segmentDescriptor()
{
  if ( mSegmentDescriptor )
    return *mSegmentDescriptor;
  else
    INTERNAL_ERROR;
}

void Segment::addAnonymousLabel()
{
  mAnonymousLabels.push_back( mChunks.size() );
}

std::optional<Value> Segment::anonymousLabelAddressFromCurrentChunk( ILinkingContext& ctx, size_t processedChunkIndex, int distance )
{
  if ( distance < 0 )
  {
    auto it = std::lower_bound( mAnonymousLabels.crbegin(), mAnonymousLabels.crend(), processedChunkIndex, std::greater<>{} );
    if ( it == mAnonymousLabels.crend() )
      return {};

    it += -distance - 1;
    if ( it >= mAnonymousLabels.crend() )
      return {};

    return chunkOffsetValue( ctx, *it );
  }
  else
  {
    auto it = std::upper_bound( mAnonymousLabels.cbegin(), mAnonymousLabels.cend(), processedChunkIndex );
    if ( it == mAnonymousLabels.cend() )
      return {};

    it += distance - 1;
    if ( it >= mAnonymousLabels.cend() )
      return {};

    return chunkOffsetValue( ctx, *it );
  }
}

bool Segment::constructed() const
{
  return mConstructed;
}

void Segment::flagConstructed()
{
  mConstructed = true;
}

uint8_t Segment::newZone()
{
  if ( ++mZones > 255 )
    throw Ex{} << "Too many zones in segment. Maximum 255 allowed";

  return (uint8_t)mZones;
}

bool Segment::isExecutable()
{
  return lazyGetSegmentDescriptor().executable();
}

void Segment::registerReference( std::string reference, nga::SegmentReferenceKind kind )
{
  mReferences.add( reference, kind );
}

void Segment::createProcessor( std::shared_ptr<ISymbol const> pEntrySymbol )
{
  createProcessor();

  if ( pEntrySymbol )
  {
    mSegmentDescriptor->setEntry( pEntrySymbol->value( *mProcessor ) );
  }
}


void Segment::createProcessor()
{
  if ( mProcessor )
    INTERNAL_ERROR;

  if ( !mMachineContext )
    INTERNAL_ERROR;

  mConstructed = lazyGetSegmentDescriptor().implicitlyConstructed();

  mProcessor = SegmentProcessor::create( *this, *mMachineContext );
}

std::shared_ptr<SegmentProcessor> Segment::getProcessor()
{
  return mProcessor;
}

void Segment::iterateReferences()
{
  for ( auto it : mReferences.iterate() )
  {
    mProcessor->visit( it.ctx, it.name, it.kind );
  }
}

void Segment::visit( std::shared_ptr<ISymbol const> pEntrySymbol )
{
  assert( !mProcessor );

  createProcessor( pEntrySymbol );
  iterateReferences();
}

void Segment::visit()
{
  if ( mProcessor )
    return;

  createProcessor();
  iterateReferences();
}

void Segment::commit()
{
  segmentDescriptor().commitSegment( this );
}

MachineContext* Segment::getMachineContext() const
{
  return mMachineContext;
}

void Segment::printListing( std::ostream& out ) const
{
  if ( !mAddress )
    INTERNAL_ERROR;

  size_t textColumn = project().config().getListingTextColumn();
  int adrSize = mSegmentDescriptor->addressSize();

  for ( size_t i = 0; i < mChunks.size(); ++i )
  {
    auto [multiple, str, pos] = mChunks[i]->listingLine();
    project().trackFile( pos.id, out );

    auto [off, size] = mChunkOffSizes[i];
    out << std::hex << std::setfill( '0' ) << std::setw( adrSize ) << ( *mAddress + off ) << ' ';
    for ( uint32_t j = 0; j < size; ++j )
    {
      if ( adrSize + 1 + 2 * j >= textColumn )
        break;
      out << std::setw( 2 ) << (int)mData[off + j];
    }
    for ( int j = adrSize + 1 + 2 * size; j < textColumn; ++j )
    {
      out << ' ';
    }
    
    for ( ;; )
    {
      if ( str.empty() )
        out << '\n';
      else
      {
        out << std::setfill( ' ' ) << std::setw( 3 ) << std::dec << pos.pos << ' ' << str << '\n';
      }
      if ( multiple )
      {
        for ( int j = 0; j < textColumn; ++j )
        {
          out << ' ';
        }
        std::tie(multiple, str, pos) = mChunks[i]->listingLine();
      }
      else
      {
        break;
      }
    }
  }

  if ( mDependentSegment )
    mDependentSegment->printListing( out );
}

void Segment::setDependentSegment( Segment* segment )
{
  mDependentSegment = segment;
}

SegmentDescriptor& Segment::lazyGetSegmentDescriptor()
{
  if ( !mSegmentDescriptor )
  {
    if ( auto match = mMachineContext->segmentDescriptor( mSegmentDescriptorName ) )
    {
      mSegmentDescriptor = match.get();
    }
    else
    {
      throw Ex{} << "Segment descriptor '" << mSegmentDescriptorName << "' not found";
    }

    mSegmentDescriptor->registerSegment( this );
  }

  return *mSegmentDescriptor;
}

void Segment::emitByte( int v )
{
  if ( v >= -128 && v < 256 )
  {
    mData.push_back( v & 0xff );
  }
  else
  {
    throw Ex{ "Expected byte value" };
  }
}

void Segment::emitBlock( std::span<uint8_t const> block )
{
  std::ranges::copy( block, std::back_inserter( mData ) );
}

void Segment::reserve( size_t size )
{
  mData.resize( mData.size() + size, 0 );
}

void Segment::addRelocation( int32_t off )
{
  mRelocations.push_back( off );
}

void Segment::References::add( std::string const& ref, nga::SegmentReferenceKind kind )
{
  //a bit of trickery:
  //the characters have codes 59,60,61,62 respectively and are used to index table of SegmentReferenceKind on decoding side
  char separator;
  switch ( kind )
  {
  case nga::SRK_JSR:
    separator = '<';
    break;
  case nga::SRK_CTOR:
    separator = '>';
    break;
  case nga::SRK_JSR | nga::SRK_CTOR:
    separator = '=';
    break;
  default:
    separator = ';';
  }

  if ( auto p = find( ref ) )
  {
    switch ( *p )
    {
    case ';':
      *p = separator;
      break;
    case '<':
      if ( ( kind & nga::SRK_CTOR ) != 0 )
        *p = '=';
      break;
    case '>':
      if ( ( kind & nga::SRK_JSR ) != 0 )
        *p = '=';
      break;
    default:
      break;
    }
  }
  else
  {
    std::ranges::copy( ref, std::back_inserter( mData ) );
    mData.push_back( separator );
  }
}

cppcoro::generator<Segment::References::It> Segment::References::iterate() const
{
  auto const& data = mData;

  size_t off = 0;
  for ( ;; )
  {
    //a bit of trickery:
    //the characters have codes 59,60,61,62 respectively and are encoded depending on found SegmentReferenceKind, here are just used to index following table
    static constexpr std::array<nga::SegmentReferenceKind, 4> tab{ nga::SRK_DEFAULT, nga::SRK_JSR, nga::SRK_JSR | nga::SRK_CTOR, nga::SRK_CTOR };

    size_t off1 = data.find( '#', off );
    if ( off1 == std::string::npos )
      break;
    size_t off2 = data.find_first_of( ";<=>", off1 );
    co_yield Segment::References::It{ std::string_view{ &data[off1+1], off2 - off1 - 1}, std::string_view{ &data[off], off1 - off }, tab[data[off2] - ';'] };
    off = off2 + 1;
  }
}

//returns pointer to the ";<=>" separator if found, nullptr otherwise
char * Segment::References::find( std::string const& ref )
{
  size_t off = 0;
  for ( ;; )
  {
    off = mData.find( ref, off );
    if ( off == std::string::npos )
      return nullptr;
    char * end = &mData[off += ref.size()];
    if ( *end >= ';' && *end <= '>' )
      return end;
    off = mData.find_first_of( ";<=>", off );
  }
}

}
