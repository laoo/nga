#include "pch.hpp"
#include "SourceDataFile.hpp"
#include "Ex.hpp"

namespace nga
{

SourceDataFile::SourceDataFile( std::filesystem::path path ) : IInputDataSource{}, mPath{ std::move( path ) }
{
  if ( !std::filesystem::exists( mPath ) )
  {
    throw Ex{} << "File " << mPath << " does not exists";
  }

  size_t size = std::filesystem::file_size( mPath );
  mContent.resize( size );
  std::ifstream fin{ mPath, std::ios::binary };
  fin.read( mContent.data(), size );
}

std::filesystem::path const & SourceDataFile::path() const
{
  return mPath;
}

std::span<uint8_t const> SourceDataFile::data() const
{
  return std::span<uint8_t const>{ (uint8_t const*)mContent.data(), mContent.size() };
}

std::istringstream SourceDataFile::stream() const
{
  return std::istringstream( mContent );
}

}

