#pragma once

#include "IInputSource.hpp"

namespace nga
{

class IInputDataSource : public IInputSource
{
public:
  IInputDataSource() : IInputSource{} {}
  virtual ~IInputDataSource() = default;

  virtual std::span<uint8_t const> data() const = 0;
  virtual std::istringstream stream() const = 0;
};

}

