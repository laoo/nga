#include "pch.hpp"
#include "Project.hpp"
#include "Ex.hpp"
#include "Log.hpp"
#include "Output.hpp"
#include "MachineContext.hpp"
#include "SegmentDescriptor.hpp"
#include "Input.hpp"
#include "Worker.hpp"
#include "Machine.hpp"
#include "IInputTextSource.hpp"

namespace nga
{

using namespace std::literals::string_view_literals;

Project::Project() : mConfig{}, mWorker{ std::make_unique<Worker>() }, mSourceFiles{}
{
  mSourceFiles.push_back( {} ); //inputs are counted from 1. 0 is reserved
}

Project::~Project()
{
}

void Project::processLUA( std::filesystem::path const& path )
{
  try
  {
    sol::state lua;
    lua.open_libraries();

    registerLUA( lua );
    Machine::registerLUA( lua );
    MachineContext::registerLUA( lua );
    SegmentDescriptor::registerLUA( lua );
    Input::registerLUA( lua );
    Output::registerLUA( lua );

    lua.script_file( path.string() );

    machine()->postprocess( lua );
  }
  catch ( sol::error const& err )
  {
    throw Ex{} << err.what();
  }
}

void Project::registerLUA( sol::state& lua )
{
  lua["Config"] = [this]( sol::table const& tab )
  {
    if ( auto opt = tab.get<sol::optional<std::string>>( "BinPath" ) )
      mConfig.bin = std::filesystem::absolute( *opt );

    if ( auto opt = tab.get<sol::optional<std::string>>( "TmpPath" ) )
      mConfig.tmp = std::filesystem::absolute( *opt );

    if ( auto opt = tab.get<sol::optional<std::string>>( "OutPath" ) )
      mConfig.out = std::filesystem::absolute( *opt );

    if ( auto opt = tab.get<sol::optional<std::string>>( "LstPath" ) )
      mConfig.lst = std::filesystem::absolute( *opt );

    if ( auto opt = tab.get<sol::optional<int>>( "MaxMacroExpansion" ) )
      mConfig.maxMacroExpansion = *opt;

    if ( auto opt = tab.get<sol::optional<int>>( "ListingTextColumn" ) )
      mConfig.listingTextColumn = *opt;
  };

}

Segment* Project::newSegment()
{
  std::scoped_lock<std::mutex> lock{ mMutex };
  mGlobalSegmentTable.push_back( std::make_unique<Segment>() );
  return mGlobalSegmentTable.back().get();
}

void Project::addReference( Segment* src, Segment* dst, nga::SegmentReferenceKind kind )
{
  if ( src == dst )
    return;

  kind |= dst->isExecutable() ? nga::SRK_JMP : 0;

  auto it = std::ranges::find_if( mReferences, [=]( Reference const& r ) { return r.src == src && r.dst == dst; } );

  if ( it == mReferences.end() )
  {
    mReferences.push_back( { src, dst, kind } );
  }
  else
  {
    it->kind = combine( it->kind, kind );
  }
}

void Project::async( std::function<void()> const& fun )
{
  mWorker->run( fun );
}

void Project::wait()
{
  mWorker->wait();
}

void Project::setMachine( std::shared_ptr<Machine> machine )
{
  if ( mMachine )
    throw Ex{ "Machine redefinition" };
  mMachine = std::move( machine );
}

std::shared_ptr<Machine> Project::machine() const
{
  if ( mMachine )
  {
    return mMachine;
  }
  else
    throw Ex{ "Undefined machine" };
}

Project::Config const& Project::config() const
{
  return mConfig;
}

void Project::registerSource( std::shared_ptr<IInputSource> inputSource )
{
  inputSource->setId( (uint32_t)mSourceFiles.size() );
  mSourceFiles.push_back( std::move( inputSource ) );
}

std::string Project::posString( Pos const& pos ) const
{
  return std::format( "{}({})", posPath( pos ).generic_string(), pos.pos );
}

std::filesystem::path const& Project::posPath( Pos const& pos ) const
{
  if ( pos.id == 0 || pos.id > mSourceFiles.size() )
    throw Ex{ "Bad position" };

  if ( !mSourceFiles[pos.id] )
    INTERNAL_ERROR;

  return mSourceFiles[pos.id]->path();
}

std::string_view Project::posLine( Pos const& pos ) const
{
  if ( pos.id == 0 || pos.id > mSourceFiles.size() )
    INTERNAL_ERROR;

  if ( auto textSource = std::dynamic_pointer_cast<IInputTextSource>( mSourceFiles[pos.id] ) )
  {
    return textSource->line( pos.pos - 1 );
  }

  return {};
}

void Project::trackFile( uint32_t id, std::ostream& out )
{
  static int sourceId = 0;

  if ( sourceId != id )
  {
    sourceId = id;
    if ( id == 0 )
    {
      out << "==========   (dependent segment)   ==========\n";
    }
    else
    {
      out << "==========   " << posPath( Pos{ id, 1 } ).string() << "   ==========\n";
    }
  }
}

std::string_view Project::strPool( std::string_view str )
{
  if ( str.empty() )
    return {};

  auto it = mStringPool.find( str );
  if ( it == mStringPool.cend() )
  {
    auto [it2, success] = mStringPool.insert( std::string{ str } );
    assert( success );
    return { *it2 };
  }
  else
  {
    return { *it };
  }
}

std::string_view Project::strPool( std::string str )
{
  if ( str.empty() )
    return {};

  auto it = mStringPool.find( str );
  if ( it == mStringPool.cend() )
  {
    auto [it2, success] = mStringPool.insert( std::move( str ) );
    assert( success );
    return { *it2 };
  }
  else
  {
    return { *it };
  }
}

void Project::process()
{
  assert( mMachine );
  mMachine->process();
}

Project& project()
{
  static Project p;
  return p;
}

Project::Config::Config() : bin{}, tmp{}, out{}, maxMacroExpansion{ 1024 }, listingTextColumn{ 20 }
{
}


std::filesystem::path const& Project::Config::getBin() const
{
  if ( bin.empty() )
    throw Ex{ "No BinPath in Config" };

  return bin;
}

std::filesystem::path const& Project::Config::getTmp() const
{
  if ( tmp.empty() )
    throw Ex{ "No TmpPath in Config" };

  std::filesystem::create_directories( tmp );

  return tmp;
}

std::filesystem::path const& Project::Config::getOut() const
{
  if ( out.empty() )
    throw Ex{ "No OutPath in Config" };

  std::filesystem::create_directories( out );

  return out;
}

std::filesystem::path const& Project::Config::getLst() const
{
  if ( !lst.empty() )
    std::filesystem::create_directories( lst.parent_path() );

  return lst;
}

size_t Project::Config::getMaxMacroExpansion() const
{
  return maxMacroExpansion;
}

size_t Project::Config::getListingTextColumn() const
{
  return listingTextColumn;
}

}
