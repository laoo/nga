#pragma once

#include "SegmentStore.hpp"

namespace nga
{

class Input : public SegmentStore
{
public:
  Input();
  virtual ~Input();

  static void registerLUA( sol::state & lua );
};

}
