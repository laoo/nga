#include "pch.hpp"
#include "Worker.hpp"
#include "Ex.hpp"
#include "Log.hpp"

namespace nga
{

Worker::Worker() : mThreadGroup{}, mService{}, mWork{ boost::asio::io_service::work( mService ) }, mFinishCondition{}, mAsyncMutex{}, mWorkingJobs{ 0 }, mExPtr{}
{
#ifndef ONE_THREAD
  unsigned int CPUs = boost::thread::hardware_concurrency();

  while ( CPUs-- )
  {
    mThreadGroup.create_thread( [this]() { mService.run(); } );
  }
#endif
}

Worker::~Worker() noexcept( true )
{
  try
  {
    mWork.reset();
    mThreadGroup.join_all();
  }
  catch ( ... )
  {
  }
}

}
