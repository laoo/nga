#pragma once

#include "Output.hpp"
#include "Value.hpp"

namespace nga::output
{

class BIN : public Output
{
public:
  BIN( sol::table const& tab );
  ~BIN() override = default;

  static void registerLUA( sol::state& lua );

  void setEntry( Value v ) override;
  void addSegment( Segment* segment ) override;
  void commit() override;

private:
  struct Block
  {
    int32_t address;
    std::span<uint8_t const> data;
  };

private:
  std::filesystem::path mPath;
  std::optional<Value> mEntry;
  std::vector<Segment*> mSegments;
};

}

