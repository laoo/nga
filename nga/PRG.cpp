#include "pch.hpp"
#include "PRG.hpp"
#include "Ex.hpp"
#include "Value.hpp"
#include "Segment.hpp"

namespace nga::output
{

std::shared_ptr<Output> PRG::load( std::string_view name, sol::table const & tab )
{
  if ( name != "PRG" )
    return {};

  return std::make_shared<PRG>( tab );
}
  
PRG::PRG( sol::table const& tab ) : mPath{}, mEntry{}, mBlocks{}
{
  if ( auto opt = tab.get<sol::optional<std::string>>( "OutputPath" ) )
  {
    mPath = std::move( *opt );
  }
  else
  {
    throw Ex{ "No Path in PRG Output" };
  }
}

void PRG::setEntry( Value v )
{
  if ( mEntry )
    throw Ex{ "Entry redefinition" };

  auto [inst, off] = v.segmentOffset();

  if ( !inst )
    throw Ex{ "Entry must be a segment offset" };

  mEntry = *inst->address() + off;
}

void PRG::addSegment( Segment* segment )
{
  INTERNAL_ERROR; //TODO
}

void PRG::commit()
{
  if ( mBlocks.empty() )
  {
    throw Ex{ "Empty output" };
  }

  std::sort( mBlocks.begin(), mBlocks.end(), []( Block const& left, Block const& right )
  {
    return left.address < right.address;
  } );

  if ( mBlocks.front().address < 0x810 )
    throw Ex{ "PRG file can't start at address smaller than $810" };

  if ( mBlocks.back().address + mBlocks.back().data.size() > 0x9fff )
    throw Ex{ "PRG file must end befor $a000" };

  std::ofstream fout{ mPath, std::ios::binary };

  //load address 0x0801
  fout.put( 0x01 );
  fout.put( 0x08 );

  int32_t cursor = 0x801;

  std::array<uint8_t, 15> basicLoader =
  {
    0x0C, 0x08, 0x0A, 0x00, 0x9E, 0x20, 0x32, 0x30, 0x36, 0x34, 0x00, 0x00, 0x00, 0x00, 0x00
  };

  fout.write( (char const*)basicLoader.data(), basicLoader.size() );

  cursor += (int32_t)basicLoader.size();

  assert( cursor == 2064 );

  if ( *mEntry != cursor )
  {
    if ( mBlocks.front().address < cursor + 3 )
    {
      throw Ex{} << "Can't produce PRG file starting at " << std::hex << "$" << *mEntry;
    }

    fout.put( 0x4c ); //jmp
    fout.put( *mEntry & 0x00ff ); //lo
    fout.put( ( *mEntry & 0xff00 ) >> 8 ); //lo

    cursor += 3;
  }

  for ( auto const& block : mBlocks )
  {
    if ( block.address < cursor )
      INTERNAL_ERROR;

    while ( cursor < block.address )
    {
      fout.put( 0 );
      cursor += 1;
    }

    fout.write( (char const*)block.data.data(), block.data.size() );

    cursor += (int32_t)block.data.size();
  }
}

}
