#pragma once

namespace nga
{

class CpuType
{
public:
  enum Type
  {
    UNDEFINED,
    NO_CPU,
    MOS6502,
    G65SC02,
    WDC65C02,
    M68000,
    TOM,
    JERRY
  };

  CpuType( Type t = UNDEFINED );

  std::string_view name() const;

  int32_t valueSize( int32_t value ) const;

  static std::string_view supportedCPUs();

  operator Type() const;

private:

  Type mType;
};

}
