#pragma once

#include "Segment.hpp"

namespace nga
{
class ISymbol;
class MachineContext;

class SegmentStore
{
public:
  SegmentStore();
  virtual ~SegmentStore();

  Segment * newSegment();

  void declareSymbol( std::string_view name, std::shared_ptr<ISymbol const> symbol );

  std::span<std::pair<std::string, std::shared_ptr<ISymbol const>> const> symbols() const;

  void setMachineContext( MachineContext * ctx );

  MachineContext const* getMachineContext() const;
private:

  mutable std::mutex mMutex;
  std::vector<std::pair<std::string, std::shared_ptr<ISymbol const>>> mSymbols;
  std::vector<Segment*> mAllSegments;
  MachineContext const* mCtx;

};

}
