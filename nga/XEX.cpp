#include "pch.hpp"
#include "XEX.hpp"
#include "Ex.hpp"
#include "Segment.hpp"
#include "Project.hpp"
#include "Machine.hpp"
#include "Log.hpp"


namespace nga::output
{
using namespace std::string_view_literals;

void XEX::registerLUA( sol::state& lua )
{
  lua["XEX"] = []( sol::table const& tab ) -> std::shared_ptr<Output>
  {
    return std::make_shared<XEX>( tab );
  };
}

XEX::XEX( sol::table const& tab ) : mPath{}, mEntry{}, mSegments{}
{
  if ( auto opt = tab.get<sol::optional<std::string>>( "OutputPath" ) )
  {
    mPath = std::move( *opt );
  }
  else
  {
    throw Ex{ "No Path in XEX Output" };
  }
}

void XEX::setEntry( Value v )
{
  if ( mEntry )
    throw Ex{ "Entry redefinition" };

  mEntry = std::move( v );
}

void XEX::addSegment( Segment* segment )
{
  mSegments.push_back( segment );
}

void XEX::commit()
{
  if ( mSegments.empty() )
  {
    throw Ex{ "Empty output" };
  }

  std::ranges::sort( mSegments, []( int left, int right ) { return left < right; }, []( Segment* s ) { return *s->address(); } );

  handleListing( mSegments );

  std::vector<Block> blocks;

  for ( auto const& segment : mSegments )
  {
    blocks.push_back( { *segment->address(), segment->data() } );
  }

  if ( blocks.front().address < 0x2000 )
    L_WARNING << "XEX file started at address $" << std::hex << blocks.front().address << " which is smaller than $2000";

  if ( blocks.back().address + blocks.back().data.size() > 0xbfff )
    L_WARNING << "XEX file ends at address $" << std::hex << blocks.back().address + blocks.back().data.size() << " which is grater than $c000";
  
  std::vector<uint8_t> buffer;


  buffer.push_back( 0xff );
  buffer.push_back( 0xff );

  size_t endIdx;
  int32_t endAdr;
  for ( size_t i = 0; i < blocks.size(); ++i )
  {
    auto const& block = blocks[i];
    if ( i == 0 )
    {
      endAdr = block.address + (int32_t)( block.data.size() - 1 );
      buffer.push_back( block.address & 0x00ff );
      buffer.push_back( ( block.address & 0xff00 ) >> 8 );
      endIdx = buffer.size();
      buffer.push_back( endAdr & 0x00ff );
      buffer.push_back( ( endAdr & 0xff00 ) >> 8 );
    }
    else
    {
      if ( block.address == endAdr + 1 )
      {
        endAdr = block.address + (int32_t)( block.data.size() - 1 );
        buffer[endIdx] = endAdr & 0x00ff;
        buffer[endIdx+1] = ( endAdr & 0xff00 ) >> 8;
      }
      else
      {
        endAdr = block.address + (int32_t)( block.data.size() - 1 );
        buffer.push_back( block.address & 0x00ff );
        buffer.push_back( ( block.address & 0xff00 ) >> 8 );
        endIdx = buffer.size();
        buffer.push_back( endAdr & 0x00ff );
        buffer.push_back( ( endAdr & 0xff00 ) >> 8 );
      }
    }

    buffer.reserve( buffer.size() + block.data.size() );
    std::copy( block.data.begin(), block.data.end(), std::back_inserter( buffer ) );
  }

  if ( mEntry )
  {
    auto [inst, off] = mEntry->segmentOffset();

    if ( !inst )
      throw Ex{ "Entry must be a segment offset" };

    auto optAddress = inst->address();

    if ( !optAddress )
      INTERNAL_ERROR;

    int address = *optAddress + off;

    //RUN
    buffer.push_back( 0xe2 );
    buffer.push_back( 0x02 );
    buffer.push_back( 0xe3 );
    buffer.push_back( 0x02 );
    buffer.push_back( address & 0x00ff );
    buffer.push_back( ( address & 0xff00 ) >> 8 );
  }

  std::ofstream fout{ project().config().getOut() / mPath, std::ios::binary };
  fout.write( (char const*)buffer.data(), buffer.size() );
}

}
