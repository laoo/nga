#pragma once

#include "IInputTextSource.hpp"

namespace nga
{

class SourceTextFile : public IInputTextSource
{
public:
  SourceTextFile( std::filesystem::path path );
  ~SourceTextFile() override = default;

  std::pair<Pos, std::string_view> nextLine() override;
  std::pair<Pos, std::string_view> currentLine() const override;
  std::filesystem::path const& path() const override;
  std::string_view line( uint32_t pos ) const override;
  Pos currentPos() const override;

private:
  std::string mContent;
  std::vector<std::string_view> mLines;
  std::filesystem::path mPath;
  uint32_t mCurrentLine;
};

}

