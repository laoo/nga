#pragma once

#include <cctype>
#include <charconv>
#include <deque>
#include <exception>
#include <filesystem>
#include <fstream>
#include <functional>
#include <iostream>
#include <sstream>
#include <variant>
#include <string_view>
#include <map>
#include <cassert>
#include <functional>
#include <regex>
#include <mutex>
#include <optional>
#include <ranges>
#include <memory>
#include <atomic>
#include <condition_variable>
#include <thread>
#include <set>
#include <span>
#include <cstdint>
#include <type_traits>
#include <unordered_map>
#include <utility>

#include <boost/asio/io_service.hpp>
#include <boost/locale.hpp>
#include <boost/thread.hpp>
#include <boost/context/fiber.hpp>

#include <cppcoro/generator.hpp>

#pragma warning( push )
#pragma warning( disable : 4244 )
#include <boost/process.hpp>
#pragma warning( pop )

#define SOL_ALL_SAFETIES_ON 1
#define SOL_PRINT_ERRORS 0
#include <sol/sol.hpp>

#pragma warning( push )
#pragma warning( disable : 4267 )
#include <ortools/sat/cp_model.h>
#pragma warning( pop )

namespace ort = operations_research;
namespace sat = operations_research::sat;
