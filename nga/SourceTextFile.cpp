#include "pch.hpp"
#include "SourceTextFile.hpp"
#include "Ex.hpp"

namespace nga
{

SourceTextFile::SourceTextFile( std::filesystem::path path ) : IInputTextSource{}, mPath{ std::move( path ) }, mCurrentLine{}
{
  if ( !std::filesystem::exists( mPath ) )
  {
    throw Ex{} << "File " << mPath << " does not exists";
  }

  size_t size = std::filesystem::file_size( mPath );
  mContent.resize( size );
  {
    std::ifstream fin{ mPath, std::ios::binary };
    fin.read( mContent.data(), size );
  }

  size_t start = 0; char last = 0;
  size_t i;
  for ( i = 0; i < mContent.size(); ++i )
  {
    char c = mContent[i];
    if ( c == '\n' )
    {
      if ( last != '\r' )
      {
        mLines.push_back( std::string_view{ &mContent[start], i - start } );
        last = c;
      }
      else
      {
        last = 0;
      }
      start = i + 1;
    }
    else if ( c == '\r' )
    {
      if ( last != '\n' )
      {
        mLines.push_back( std::string_view{ &mContent[start], i - start } );
        last = c;
      }
      else
      {
        last = 0;
      }
      start = i + 1;
    }
  }
  if ( start < i )
    mLines.push_back( std::string_view{ &mContent[start], i - start } );
}

std::pair<Pos, std::string_view> SourceTextFile::nextLine()
{
  if ( mCurrentLine++ >= mLines.size() )
    return {};

  return { currentPos(), mLines[mCurrentLine - 1] };
}

std::pair<Pos, std::string_view> SourceTextFile::currentLine() const
{
  if ( mCurrentLine > mLines.size() )
    return {};

  return { currentPos(), mLines[mCurrentLine - 1] };
}

std::filesystem::path const & SourceTextFile::path() const
{
  return mPath;
}

std::string_view SourceTextFile::line( uint32_t pos ) const
{
  if ( pos < mLines.size() )
    return mLines[pos];
  return {};
}

Pos SourceTextFile::currentPos() const
{
  return pos( mCurrentLine );
}

}

