#pragma once

#include "Output.hpp"

namespace nga::output
{

class PRG : public Output
{
public:
  PRG( sol::table const& tab );
  ~PRG() override = default;

  static std::shared_ptr<Output> load( std::string_view name, sol::table const& tab );

  void setEntry( Value v ) override;
  void addSegment( Segment* segment ) override;
  void commit() override;

private:
  struct Block
  {
    int32_t address;
    std::span<uint8_t const> data;
  };

private:
  std::filesystem::path mPath;
  std::optional<int32_t> mEntry;
  std::vector<Block> mBlocks;
};

}

