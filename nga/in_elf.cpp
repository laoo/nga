#include "pch.hpp"
#include "in_elf.hpp"
#include "Ex.hpp"
#include "Project.hpp"
#include "IChunk.hpp"
#include "SegmentStore.hpp"
#include "Segment.hpp"
#include "ISymbol.hpp"
#include "ILinkingContext.hpp"
#include "MachineContext.hpp"
#include "SourceDataFile.hpp"
#include <elfio/elfio.hpp>

namespace elf
{

template<class... Ts> struct Visitor : Ts... { using Ts::operator()...; };


struct TextChunk : public nga::IChunk
{
  struct Import
  {
    std::variant<std::string,nga::Segment*> nameId;
    int32_t offset;
    uint32_t type;
    int32_t addend;
  };

  TextChunk() : pos{}
  {
  }

  ~TextChunk() override = default;

  void setPosCpu( nga::Pos pos, nga::CpuType cpu = nga::CpuType::UNDEFINED ) override
  {
    this->pos = pos;
  }

  nga::Pos getPos() const override
  {
    return pos;
  }

  void emit( nga::ILinkingContext & lc ) const override
  {
    int32_t segmentAddress = lc.segmentAddress();
    int32_t srcOff = 0;
    std::pair<nga::Segment const*, int> importValue;

    for ( auto const& import : imports )
    {
      std::visit( Visitor{
        [&]( std::string const& str ) { importValue = lc.symbolValue( {}, str ).segmentOffset(); },
        [&]( nga::Segment* seg ) { importValue = seg->byteOffsetValue( lc, 0 ).segmentOffset(); }
      }, import.nameId );

      int32_t importOffset = importValue.second + import.addend;
      if ( importValue.first )
      {
        importOffset += *importValue.first->address();
      }

      while ( srcOff < import.offset )
      {
        lc.emitI8( data[srcOff++] );
      }

      switch ( import.type )
      {
      case 0:   //RELOC_NUMBER( R_68K_NONE, 0 )		/* No reloc */
        INTERNAL_ERROR;
        break;
      case 1:   //RELOC_NUMBER( R_68K_32, 1 )		/* Direct 32 bit  */
        if ( importValue.first )
        {
          lc.addRelocation( srcOff );
        }
        lc.emitI8( ( importOffset >> 24 ) & 0xff );
        lc.emitI8( ( importOffset >> 16 ) & 0xff );
        lc.emitI8( ( importOffset >> 8 ) & 0xff );
        lc.emitI8( importOffset & 0xff );
        srcOff += 4;
        break;
      case 2:   //RELOC_NUMBER( R_68K_16, 2 )		/* Direct 16 bit  */
        if ( importValue.first )
          INTERNAL_ERROR;
        if ( importOffset >= -32768 && importOffset < 32768 )
        {
          lc.emitI8( ( importOffset >> 8 ) & 0xff );
          lc.emitI8( importOffset & 0xff );
          srcOff += 2;
        }
        else
          throw Ex{ "symbol does not fit in 16 bit" };
        break;
      case 3:   //RELOC_NUMBER( R_68K_8, 3 )		/* Direct 8 bit  */
        INTERNAL_ERROR;
        break;
      case 4:   //RELOC_NUMBER( R_68K_PC32, 4 )		/* PC relative 32 bit */
        INTERNAL_ERROR;
        break;
      case 5:   //RELOC_NUMBER( R_68K_PC16, 5 )		/* PC relative 16 bit */
        {
          int32_t rel = importOffset - ( segmentAddress + srcOff );
          if ( rel >= -32768 && rel < 32768 )
          {
            lc.emitI8( ( rel >> 8 ) & 0xff );
            lc.emitI8( rel & 0xff );
            srcOff += 2;
          }
          else
            throw Ex{ "PC relative addressing too far" };
        }
        break;
      case 6:   //RELOC_NUMBER( R_68K_PC8, 6 )		/* PC relative 8 bit */
        INTERNAL_ERROR;
        break;
      default:
        INTERNAL_ERROR;
      }
      //RELOC_NUMBER( R_68K_GOT32, 7 )		/* 32 bit PC relative GOT entry */
      //RELOC_NUMBER( R_68K_GOT16, 8 )		/* 16 bit PC relative GOT entry */
      //RELOC_NUMBER( R_68K_GOT8, 9 )		/* 8 bit PC relative GOT entry */
      //RELOC_NUMBER( R_68K_GOT32O, 10 )	/* 32 bit GOT offset */
      //RELOC_NUMBER( R_68K_GOT16O, 11 )	/* 16 bit GOT offset */
      //RELOC_NUMBER( R_68K_GOT8O, 12 )	/* 8 bit GOT offset */
      //RELOC_NUMBER( R_68K_PLT32, 13 )	/* 32 bit PC relative PLT address */
      //RELOC_NUMBER( R_68K_PLT16, 14 )	/* 16 bit PC relative PLT address */
      //RELOC_NUMBER( R_68K_PLT8, 15 )		/* 8 bit PC relative PLT address */
      //RELOC_NUMBER( R_68K_PLT32O, 16 )	/* 32 bit PLT offset */
      //RELOC_NUMBER( R_68K_PLT16O, 17 )	/* 16 bit PLT offset */
      //RELOC_NUMBER( R_68K_PLT8O, 18 )	/* 8 bit PLT offset */
      //RELOC_NUMBER( R_68K_COPY, 19 )		/* Copy symbol at runtime */
      //RELOC_NUMBER( R_68K_GLOB_DAT, 20 )	/* Create GOT entry */
      //RELOC_NUMBER( R_68K_JMP_SLOT, 21 )	/* Create PLT entry */
      //RELOC_NUMBER( R_68K_RELATIVE, 22 )	/* Adjust by program base */ 

    }

    while ( srcOff < data.size() )
    {
      lc.emitI8( data[srcOff++] );
    }
  }

  uint32_t size( nga::ILinkingContext & lc ) const override
  {
    return (uint32_t)data.size();
  }

  std::tuple<bool, std::string_view, nga::Pos> listingLine() const override
  {
    return {};
  }


  std::vector<Import> imports;
  std::vector<uint8_t> data;
  nga::Pos pos;
  nga::Segment * segment;
};

class Symbol : public nga::ISymbol
{
public:
  Symbol( nga::Segment* segment, size_t offset ) : mSegment{ std::move( segment ) }, mOffset{ offset } {}
  ~Symbol() override = default;

  uint32_t size( nga::ILinkingContext & lc, nga::CpuType cpu ) const override
  {
    return 0;
  }
  nga::Value value( nga::ILinkingContext & lc ) const override
  {
    return mSegment->byteOffsetValue( lc, mOffset );
  }

  nga::Segment* segment() const override
  {
    return mSegment;
  }

  void visit( nga::ILinkingContext& lc, nga::SegmentReferenceKind kind ) const override
  {
  }


private:
  nga::Segment* mSegment;
  size_t mOffset;
};

class AbsSymbol : public nga::ISymbol
{
public:
  AbsSymbol( int value ) : mValue{ value } {}
  ~AbsSymbol() override = default;

  uint32_t size( nga::ILinkingContext & lc, nga::CpuType cpu ) const override
  {
    return 0;
  }
  nga::Value value( nga::ILinkingContext & lc ) const override
  {
    return nga::Value::create( mValue );
  }

  nga::Segment* segment() const override
  {
    return nullptr;
  }

  void visit( nga::ILinkingContext& lc, nga::SegmentReferenceKind kind ) const override
  {
  }

private:
  int mValue;
};


void import( std::filesystem::path elfSrc, nga::SegmentStore & segmentStore )
{
  auto src = std::make_shared<nga::SourceDataFile>( elfSrc );
  nga::project().registerSource( src );

  //making a copy.
  std::istringstream ss = src->stream();

  ELFIO::elfio elf;

  if ( !elf.load( ss ) )
  {
    throw Ex{} << "Error reading ELF file " << elfSrc;
  }

  if ( elf.get_machine() != EM_68K )
  {
    throw Ex{} << "ELF " << elfSrc << " is not an EM_68K";
  }

  if ( elf.get_type() != ET_REL )
  {
    throw Ex{} << "ELF " << elfSrc << " is not a relocatable file";
  }

  auto type = elf.get_type();

  std::shared_ptr<ELFIO::symbol_section_accessor> sym;

  struct Segment
  {
    size_t sectionId;
    std::shared_ptr<ELFIO::relocation_section_accessor> relocs;
    nga::Segment* segment;
    std::shared_ptr<TextChunk> textChunk;
  };

  std::vector<Segment> segments;

  for ( auto sec : elf.sections )
  {
    auto name = sec->get_name();

    switch ( sec->get_type() )
    {
    case SHT_PROGBITS:
      {
        if ( ( sec->get_flags() & SHF_ALLOC ) == 0 )
          break;
        if ( sec->get_size() == 0 )
          break;
        auto it = std::find_if( segments.begin(), segments.end(), [&]( Segment const& s )
        {
          return s.sectionId == (size_t)sec->get_index();
        } );
        if ( it == segments.end() )
        {
          segments.push_back( Segment{ sec->get_index(), {}, segmentStore.newSegment() } );
          segments.back().segment->setSegmentDescriptorName( ( sec->get_flags()& SHF_EXECINSTR ) == 0 ? "Data" : "Code" );
          it = segments.begin() + ( segments.size() - 1 );
        }
        it->segment->setSegmentDescriptorName( sec->get_name() );

        it->textChunk = std::make_shared<TextChunk>();
        it->textChunk->setPosCpu( src->pos( 0 ) );
        it->textChunk->data =  std::vector<uint8_t>( (uint8_t*)sec->get_data(), (uint8_t*)sec->get_data() + sec->get_size() );
      }
      break;
    case SHT_NOBITS:
      if ( ( sec->get_flags() & SHF_ALLOC ) == 0 )
        break;
      if ( sec->get_size() != 0 )
        INTERNAL_ERROR;
      break;
    case SHT_SYMTAB:
      sym = std::make_shared<ELFIO::symbol_section_accessor>( elf, sec );
      break;
    case SHT_RELA:
      {
        auto rela = std::make_shared<ELFIO::relocation_section_accessor>( elf, sec );
        auto it = std::find_if( segments.begin(), segments.end(), [&]( Segment const& s )
        {
          return s.sectionId == (size_t)sec->get_info();
        } );
        if ( it == segments.end() )
        {
          segments.push_back( Segment{ sec->get_index(), std::move( rela ), segmentStore.newSegment() } );
        }
        else
        {
          it->relocs = std::move( rela );
        }
      }
      break;
    case SHT_REL:
      INTERNAL_ERROR;
    default:
      break;
    }
  }

  size_t symbols = sym->get_symbols_num();
  for ( size_t i = 1; i < symbols; ++i )
  {
    std::string name;
    ELFIO::Elf64_Addr value;
    ELFIO::Elf_Xword size;
    unsigned char bind;
    unsigned char symbol_type;
    ELFIO::Elf_Half section_index;
    unsigned char other;

    sym->get_symbol( i, name, value, size, bind, symbol_type, section_index, other );

    auto it = std::find_if( segments.cbegin(), segments.cend(), [&]( Segment const& s )
    {
      return s.sectionId == section_index;
    } );

    if ( bind == STB_GLOBAL )
    {
      switch ( symbol_type )
      {
      case STT_NOTYPE:
      case STT_OBJECT:
      case STT_FUNC:
        if ( it == segments.cend() )
        {
          switch ( section_index )
          {
          case 0xfff1:
            segmentStore.declareSymbol( name, std::make_shared<AbsSymbol>( (int)value ) );
            break;
          case 0: //global
            break;
          default:
            INTERNAL_ERROR;
          }
        }
        else
        {
          segmentStore.declareSymbol( name, std::make_shared<Symbol>( it->segment, (size_t)value ) );
        }
        break;
      default:
        break;
      }
    }
    else if ( bind == STB_LOCAL && type == STT_SECTION )
    {
      segmentStore.declareSymbol( name, std::make_shared<Symbol>( it->segment, (size_t)value ) );
    }
  }

  for ( auto & seg : segments )
  {
    if ( seg.relocs )
    {
      size_t relocations = seg.relocs->get_entries_num();
      for ( size_t i = 0; i < relocations; ++i )
      {
        ELFIO::Elf64_Addr roffset;
        ELFIO::Elf_Word rsymbol;
        ELFIO::Elf_Word rtype;
        ELFIO::Elf_Sxword raddend;

        seg.relocs->get_entry( i, roffset, rsymbol, rtype, raddend );

        std::string sname;
        ELFIO::Elf_Xword ssize;
        ELFIO::Elf64_Addr svalue;
        unsigned char sbind;
        unsigned char stype;
        ELFIO::Elf_Half ssection_index;
        unsigned char sother;

        sym->get_symbol( rsymbol, sname, svalue, ssize, sbind, stype, ssection_index, sother );

        auto it = std::find_if( segments.cbegin(), segments.cend(), [&]( Segment const& s )
        {
          return s.sectionId == ssection_index;
        } );

        if ( sbind == STB_GLOBAL )
        {
          switch ( stype )
          {
          case STT_NOTYPE:
          case STT_OBJECT:
          case STT_FUNC:
            if ( it == segments.cend() )
            {
              TextChunk::Import imp{};

              imp.nameId = std::move( sname );
              imp.offset = (int32_t)roffset;
              imp.type = rtype;
              imp.addend = (int32_t)raddend;

              seg.textChunk->imports.push_back( imp );
            }
            else
            {
              INTERNAL_ERROR;
            }
            break;
          default:
            break;
          }
        }
        else if ( sbind == STB_LOCAL && stype == STT_SECTION )
        {
          TextChunk::Import imp{};

          imp.nameId = it->segment;
          imp.offset = (int32_t)roffset;
          imp.type = rtype;
          imp.addend = (int32_t)raddend;

          seg.textChunk->imports.push_back( imp );
          //segmentStore.addImplicitSegment( it->segment );
          INTERNAL_ERROR;
          //disabling implicit segments for now. I'll solve it some other way
        }
        else
        {
          INTERNAL_ERROR;
        }
      }
    }
    if ( seg.textChunk )
    {
      seg.segment->append( seg.textChunk );
    }
  }
  return;



}

}

