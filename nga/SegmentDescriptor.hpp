#pragma once

#include "Segment.hpp"
#include "Ex.hpp"

namespace nga
{
class Output;

class SegmentDescriptor
{
public:

  struct DomRange
  {
    int32_t min;
    int32_t max;
  };

  SegmentDescriptor( sol::table const & tab );
  virtual ~SegmentDescriptor() = default;

  static void registerLUA( sol::state & lua );

  int addressSize() const;
  bool emptyDomain() const;
  ort::Domain const& domain() const;
  void setName( std::string name );
  std::string const& getName() const;

  virtual void setEntry( Value v )
  {
    throw Ex{ "Only BBlock can have entry" };
  }
  virtual bool implicitlyConstructed() const
  {
    return true;
  }
  virtual bool executable() const = 0;
  virtual int32_t namedOperatorSize( std::string_view operatorName, std::string_view arg ) const
  {
    throw Ex{} << "Unknown named operator " << operatorName;
  }
  virtual Value namedOperatorValue( nga::ILinkingContext & ctx, Segment* segment, std::string_view operatorName, std::string_view arg ) const
  {
    throw Ex{} << "Unknown named operator " << operatorName;
  }

  virtual int32_t concreteOperatorSize( std::string_view operatorName ) const
  {
    throw Ex{} << "Bad parent segment descriptor for operator " << operatorName;
  }
  virtual Value concreteOperatorValue( nga::ILinkingContext & ctx, Segment* segment, std::string_view operatorName ) const
  {
    throw Ex{} << "Bad parent segment descriptor for operator " << operatorName;
  }

  virtual void registerDependantSegment( Segment* segment )
  {
    INTERNAL_ERROR;
  }

  virtual void commitDependantSegment( Segment* segment )
  {
    INTERNAL_ERROR;
  }

  virtual void registerSegment( Segment* segment ) = 0;
  virtual void commitSegment( Segment* segment ) = 0;

  virtual std::shared_ptr<Output> output() const { return {}; }

protected:

  void populate( std::pair<sol::object, sol::object> tablePair );

private:
  void addDomain( DomRange d );
  int defaultAddressSize();

  ort::Domain mDomain;
  std::string mName;
  int mAddressSize;
};

class BootSegmentDescriptor : public SegmentDescriptor
{
public:

  BootSegmentDescriptor( bool executable, sol::table const& tab );
  virtual ~BootSegmentDescriptor() = default;

  void setEntry( Value v ) override;
  void registerSegment( Segment* segment ) override;
  void commitSegment( Segment* segment ) override;
  bool executable() const override;
  std::shared_ptr<Output> output() const override;

private:
  std::shared_ptr<Output> mOutput;
  bool mExecutable;

};

class BootCodeSegmentDescriptor : public BootSegmentDescriptor
{
public:

  BootCodeSegmentDescriptor( sol::table const& tab ) : BootSegmentDescriptor{ true, tab } {}
  virtual ~BootCodeSegmentDescriptor() = default;
};

class BootDataSegmentDescriptor : public BootSegmentDescriptor
{
public:

  BootDataSegmentDescriptor( sol::table const& tab ) : BootSegmentDescriptor{ false, tab } {}
  virtual ~BootDataSegmentDescriptor() = default;

protected:
  int32_t concreteOperatorSize( std::string_view operatorName ) const override;
  Value concreteOperatorValue( nga::ILinkingContext & ctx, Segment* segment, std::string_view operatorName ) const override;
  void registerDependantSegment( Segment* segment ) override;

private:
  std::vector<std::pair<Segment*, Segment*>> mSegmentMapping;
};

class DependentSegmentDescriptor : public SegmentDescriptor
{
public:

  DependentSegmentDescriptor( bool executable, sol::table const& tab );
  virtual ~DependentSegmentDescriptor() = default;
  int32_t namedOperatorSize( std::string_view operatorName, std::string_view arg ) const override;
  Value namedOperatorValue( nga::ILinkingContext & ctx, Segment* segment, std::string_view operatorName, std::string_view arg ) const override;
  void registerSegment( Segment* segment ) override;
  void commitSegment( Segment* segment ) override;
  bool implicitlyConstructed() const override;
  bool executable() const override;

protected:


private:
  SegmentDescriptor* mParent;
  bool mExecutable;
};

class DependentCodeSegmentDescriptor : public DependentSegmentDescriptor
{
public:

  DependentCodeSegmentDescriptor( sol::table const& tab ) : DependentSegmentDescriptor{ true, tab } {}
  virtual ~DependentCodeSegmentDescriptor() = default;
};

class DependentDataSegmentDescriptor : public DependentSegmentDescriptor
{
public:

  DependentDataSegmentDescriptor( sol::table const& tab ) : DependentSegmentDescriptor{ false, tab } {}
  virtual ~DependentDataSegmentDescriptor() = default;

protected:
  int32_t concreteOperatorSize( std::string_view operatorName ) const override;
  Value concreteOperatorValue( nga::ILinkingContext & ctx, Segment* segment, std::string_view operatorName ) const override;

};


class StaticAreaSegmentDescriptor : public SegmentDescriptor
{
public:

  StaticAreaSegmentDescriptor( sol::table const& tab );
  virtual ~StaticAreaSegmentDescriptor() = default;

  void registerSegment( Segment* segment ) override;
  void commitSegment( Segment* segment ) override;
  bool executable() const override;

};

class DynamicAreaSegmentDescriptor : public SegmentDescriptor
{
public:

  DynamicAreaSegmentDescriptor( sol::table const& tab );
  virtual ~DynamicAreaSegmentDescriptor() = default;

  void registerSegment( Segment* segment ) override;
  void commitSegment( Segment* segment ) override;
  bool executable() const override;

};

}
