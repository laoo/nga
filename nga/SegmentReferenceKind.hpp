#pragma once

namespace nga
{

using SegmentReferenceKind = int;
//access type
static constexpr SegmentReferenceKind SRK_REF = 0b0000;
static constexpr SegmentReferenceKind SRK_JMP = 0b0010;
static constexpr SegmentReferenceKind SRK_RTS = 0b0100;
static constexpr SegmentReferenceKind SRK_JSR = 0b0110;
static constexpr SegmentReferenceKind SRK_CTOR = 0b1000;

static constexpr SegmentReferenceKind SRK_DEFAULT = SRK_REF;

static SegmentReferenceKind combine( SegmentReferenceKind src, SegmentReferenceKind dst )
{
  return src | dst;
}

}
