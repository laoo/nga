#include "pch.hpp"
#include "Assembler.hpp"
#include "assembler/in_assembler.hpp"
#include "Ex.hpp"
#include "Log.hpp"
#include "Project.hpp"
#include "Input.hpp"

namespace nga::input
{

Assembler::Assembler( sol::table const & tab )
{
  if ( auto opt = tab.get<sol::optional<std::string_view>>( "BasePath" ) )
  {
    mBasePath = *opt;
  }
  else
  {
    throw Ex{ "No BasePath in Assembler Input node" };
  }

  if ( auto opt = tab.get<sol::optional<std::string_view>>( "NamePattern" ) )
  {
    try
    {
      mNamePattern = std::regex( opt->cbegin(), opt->cend() );
    }
    catch ( std::regex_error const& ex )
    {
      throw Ex{} << "Bad BasePath in Assembler Input node: " << ex.what();
    }
  }
  else
  {
    throw Ex{ "No NamePattern in Assembler Input node" };
  }

  std::filesystem::recursive_directory_iterator it{ mBasePath };
  for ( auto const& p : it )
  {
    if ( p.is_regular_file() )
    {
      std::cmatch m;
      if ( std::regex_search( p.path().filename().string().c_str(), m, mNamePattern ) )
      {
        project().async( [&, path=p.path()]
          {
            assembler::assemble( path, *this );
          } );
      }
    }
  }
}

}
