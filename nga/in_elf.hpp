#pragma once

namespace nga
{
class SegmentStore;
}

namespace elf
{

void import( std::filesystem::path elfSrc, nga::SegmentStore & segmentStore );

}
