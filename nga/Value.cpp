#include "pch.hpp"
#include "Value.hpp"
#include "Segment.hpp"
#include "ILinkingContext.hpp"
#include "Ex.hpp"

namespace nga
{

Value::Value( Segment* instance, int32_t offset ) : inst{ instance }, value{ offset }
{
}

Value::Value( Value&& other ) : inst{ other.inst }, value{ other.value }
{
}

Value Value::create( int32_t value )
{
  return Value{ nullptr, value };
}

Value Value::create( Segment* instance, int32_t offset )
{
  return Value{ instance, offset };
}

Value& Value::operator=( Value&& other )
{
  if ( this != &other )
  {
    inst = other.inst;
    value = other.value;
  }
  return *this;
}

Value::~Value()
{
}

std::optional<int32_t> Value::evaluate() const
{
  if ( inst )
    return std::nullopt;
  else
    return value;
}

int Value::size( nga::CpuType cpu ) const
{
  if ( inst )
  {
    return inst->addressSize();
  }
  else
  {
    return cpu.valueSize( value );
  }
}

std::pair<Segment *, int32_t> Value::segmentOffset() const
{
  return { inst, value };
}

Segment * Value::segment() const
{
  return inst;
}

bool Value::isOffset() const
{
  return inst != nullptr;
}

int32_t Value::evaluate( ILinkingContext& ctx ) const
{
  int32_t v = value + ( inst ? ctx.segmentAddress( inst ) : 0 );

  return v;
}

Value Value::operator-() &&
{
  value = -value;
  return std::move( *this );
}

Value Value::operator*( Value && right ) &&
{
  if ( inst )
  {
    if ( right.inst )
    {
      throw Ex{ "Can't multiply segment offsets" };
    }
  }
  else
  {
    inst = right.inst;
  }

  value *= right.value;
  return std::move( *this );
}

Value Value::operator/( Value&& right ) &&
{
  if ( right.inst )
  {
    throw Ex{ "Can't divide by segment offset" };
  }

  if ( right.value == 0 )
  {
    throw Ex{ "Divide by zero" };
  }

  value /= right.value;
  return std::move( *this );
}

Value Value::operator%( Value&& right ) &&
{
  if ( right.inst )
  {
    throw Ex{ "Can't modulo by segment offset" };
  }

  if ( right.value == 0 )
  {
    throw Ex{ "modulo by zero" };
  }

  value %= right.value;
  return std::move( *this );
}

Value Value::operator+( Value&& right ) &&
{
  if ( inst )
  {
    if ( right.inst )
    {
      throw Ex{ "Can't add segment offsets" };
    }
  }
  else
  {
    inst = right.inst;
  }

  value += right.value;
  return std::move( *this );
}

Value Value::operator-( Value&& right ) &&
{
  if ( inst )
  {
    if ( right.inst )
    {
      if ( inst != right.inst )
      {
        throw Ex{ "Can't subtract offsets of different segments" };
      }
    }
  }
  else
  {
    //dubious usefulness 
    inst = right.inst;
  }

  value -= right.value;
  return std::move( *this );
}

Value Value::operator<<( Value&& right ) &&
{
  if ( inst || right.inst )
  {
    throw Ex{ "Can't shift segment offsets" };
  }

  value <<= right.value;
  return std::move( *this );
}

Value Value::operator>>( Value&& right ) &&
{
  if ( inst || right.inst )
  {
    throw Ex{ "Can't shift segment offsets" };
  }

  value >>= right.value;
  return std::move( *this );
}

Value Value::operator&( Value&& right ) &&
{
  if ( inst || right.inst )
  {
    throw Ex{ "Can't and segment offsets" };
  }

  value &= right.value;
  return std::move( *this );
}

Value Value::operator|( Value&& right ) &&
{
  if ( inst || right.inst )
  {
    throw Ex{ "Can't or segment offsets" };
  }

  value |= right.value;
  return std::move( *this );
}

}
