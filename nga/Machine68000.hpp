#pragma once

#include "Machine.hpp"
#include "TOS.hpp"

namespace nga::machine
{

class Machine68000 : public Machine
{
public:

  Machine68000( Type type );
  ~Machine68000() override = default;

  static void registerLUA( sol::state& lua );
  static std::shared_ptr<Machine> load( sol::table const& tab );

  uint32_t allocateVirtualSegment( nga::output::TOS::SegmentType type, uint32_t size );

private:

  uint32_t allocateVirtualSegment( uint32_t idx, uint32_t size );

private:
  std::array<uint32_t,3> mSectionAllocationOffsets;
};

}

