#pragma once

#include "Segment.hpp"

namespace nga
{
class Output;
class SegmentProcessor;
class SegmentDescriptorClass;

class Machine
{
public:

  enum struct Type
  {
    A8,
    C64,
    ST,
    JAGUAR
  };

  enum struct Processor
  {
    PROC_6502,
    PROC_68000,
    PROC_JAGUAR,
  };

  virtual ~Machine();

  static void registerLUA( sol::state & lua );
  void postprocess( sol::state& lua );

  Type type() const;
  Processor proc() const;

  void process();
  void setContext( std::shared_ptr<MachineContext> ctx );

  void registerSegmentProcessor( std::shared_ptr<SegmentProcessor> processor );
  bool allocateSegmentsOfThisClass( Segment* proc );
  bool allocatePendingSegments();

  void pushProcessor( std::shared_ptr<SegmentProcessor> proc );
  std::shared_ptr<SegmentProcessor> popProcessor();

private:
  Segment* getEntrySegment() const;
protected:
  Machine( Processor proc, Type type );

protected:
  Processor mProc;
  Type mType;
  std::shared_ptr<MachineContext> mMachineContext;
  std::vector<std::shared_ptr<SegmentDescriptorClass>> mSegmentDescriptorClasses;
  std::unordered_map<Segment*,SegmentDescriptorClass*> mSegmentDescriptorClassMap;
  std::deque<std::shared_ptr<SegmentProcessor>> mSegmentProcessorsQueue;
};

}

