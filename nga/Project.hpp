#pragma once

#include "Segment.hpp"
#include "SegmentReferenceKind.hpp"

namespace nga
{

class Worker;
class Machine;
class IInputSource;
struct Pos;

class Project
{
  //singleton
  Project();
  Project( Project const& ) = delete;
  friend Project & project();

public:

  struct Config
  {
  private:
    friend class Project;
    Config();
    std::filesystem::path bin;
    std::filesystem::path tmp;
    std::filesystem::path out;
    std::filesystem::path lst;
    size_t maxMacroExpansion;
    size_t listingTextColumn;
  public:
    std::filesystem::path const& getBin() const;
    std::filesystem::path const& getTmp() const;
    std::filesystem::path const& getOut() const;
    std::filesystem::path const& getLst() const;
    size_t getMaxMacroExpansion() const;
    size_t getListingTextColumn() const;
  };

  ~Project();

  void processLUA( std::filesystem::path const& path );
  void process();

  void async( std::function<void()> const& fun );
  void wait();

  void setMachine( std::shared_ptr<Machine> machine );
  std::shared_ptr<Machine> machine() const;
  Config const& config() const;

  void registerSource( std::shared_ptr<IInputSource> inputSource );
  std::string posString( Pos const& pos ) const;
  std::filesystem::path const& posPath( Pos const& pos ) const;
  std::string_view posLine( Pos const& pos ) const;
  void trackFile( uint32_t id, std::ostream & out );

  std::string_view strPool( std::string_view str );
  std::string_view strPool( std::string str );

  void registerLUA( sol::state & lua );

  Segment * newSegment();
  void addReference( Segment* src, Segment* dst, nga::SegmentReferenceKind kind );

private:

  struct Reference
  {
    Segment* src;
    Segment* dst;
    SegmentReferenceKind kind;
  };

  Config mConfig;

  std::shared_ptr<Machine> mMachine;
  std::unique_ptr<Worker> mWorker;  //must be last

  std::set<std::string, std::less<>> mStringPool;
  std::vector<std::shared_ptr<IInputSource>> mSourceFiles;
  std::vector<std::unique_ptr<Segment>> mGlobalSegmentTable;
  std::vector<Reference> mReferences;
  std::mutex mMutex;

};

Project & project();


}
