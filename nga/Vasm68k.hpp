#pragma once

#include "Input.hpp"

namespace nga::input
{

class Vasm68k : public Input
{
public:
  Vasm68k( sol::table const& tab );
  ~Vasm68k() override = default;

private:
  void processVasm( std::filesystem::path path );

private:
  std::filesystem::path mBasePath;
  std::regex mNamePattern;
};

}

