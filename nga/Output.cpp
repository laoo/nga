#include "pch.hpp"
#include "Output.hpp"
#include "Ex.hpp"
#include "PRG.hpp"
#include "XEX.hpp"
#include "BIN.hpp"
#include "ABS.hpp"
#include "J64.hpp"
#include "TOS.hpp"
#include "Project.hpp"
#include "MachineContext.hpp"
#include "Input.hpp"
#include "Machine.hpp"
#include "Value.hpp"

namespace nga
{
namespace
{
template<typename T>
void regOutput( std::string_view nv, sol::state& lua )
{
  lua[nv] = [name = std::string{ nv }]( sol::table const& tab ) -> std::shared_ptr<Output>
  {
    return T::load( name, tab );
  };
}
}


using namespace std::string_view_literals;

void Output::registerLUA( sol::state & lua )
{
  output::XEX::registerLUA( lua );
  output::BIN::registerLUA( lua );
  output::J64::registerLUA( lua );
  output::ABS::registerLUA( lua );
  regOutput<output::PRG>( "PRG"sv, lua );
  regOutput<output::TOS>( "TOS"sv, lua );
}

Output::Output()
{
}

void Output::handleListing( std::span<Segment*> segments ) const
{
  auto lstPath = project().config().getLst();
  if ( lstPath.empty() )
    return;

  std::ofstream fout{ lstPath };

  for ( auto segment : segments )
  {
    segment->printListing( fout );
  }
}

Output::~Output()
{
}

}
