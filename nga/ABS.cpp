#include "pch.hpp"
#include "ABS.hpp"
#include "Ex.hpp"
#include "Segment.hpp"
#include "Project.hpp"
#include "Machine.hpp"
#include "Log.hpp"

namespace nga::output
{
namespace
{
struct Buffer
{
  std::vector<uint8_t> data;

  Buffer( size_t size ) : data{}
  {
    data.reserve( size );
  }

  void putB( int byte )
  {
    data.push_back( (uint8_t)byte );
  }

  void putBEW( int word )
  {
    putB( word >> 8 );
    putB( word & 0xff );
  }

  void putBEL( int longword )
  {
    putBEW( longword >> 16 );
    putBEW( longword & 0xffff );
  }
};
}

using namespace std::string_view_literals;



void ABS::registerLUA( sol::state& lua )
{
  lua["ABS"] = []( sol::table const& tab ) -> std::shared_ptr<Output>
  {
    return std::make_shared<ABS>( tab );
  };
}

ABS::ABS( sol::table const& tab ) : mPath{}, mEntry{}, mSegments{}
{
  if ( auto opt = tab.get<sol::optional<std::string>>( "OutputPath" ) )
  {
    mPath = std::move( *opt );
  }
  else
  {
    throw Ex{ "No Path in ABS Output" };
  }
}

void ABS::setEntry( Value v )
{
  if ( mEntry )
    throw Ex{ "Entry redefinition" };

  mEntry = std::move( v );
}

void ABS::addSegment( Segment* segment )
{
  mSegments.push_back( segment );
}

void ABS::commit()
{
  if ( mSegments.empty() )
  {
    throw Ex{ "Empty output" };
  }

  std::ranges::sort( mSegments, []( int left, int right ) { return left < right; }, []( Segment* s ) { return *s->address(); } );

  handleListing( mSegments );

  struct Block
  {
    int32_t address;
    std::span<uint8_t const> data;
  };

  std::vector<Block> blocks;

  for ( auto const& segment : mSegments )
  {
    blocks.push_back( { *segment->address(), segment->data() } );
  }

  if ( mEntry )
  {
    auto [inst, off] = mEntry->segmentOffset();

    if ( !inst )
      throw Ex{ "Entry must be a segment offset" };

    auto optAddress = inst->address();

    if ( !optAddress )
      INTERNAL_ERROR;

    int address = *optAddress + off;

    if ( address != blocks.front().address )
      throw Ex{ "entry is not at the beginning" };
  }

  if ( blocks.front().address < 0x4000 )
    L_WARNING << "ABS file started at address $" << std::hex << blocks.front().address << " which is lower than allowed $4000";

  if ( blocks.back().address + blocks.back().data.size() >= 0x200000 )
    L_WARNING << "ABS file ends at address $" << std::hex << blocks.back().address + blocks.back().data.size() << " which is grater than $1fffff";

  size_t textSize = blocks.back().address + blocks.back().data.size() - blocks.front().address;

  Buffer buffer{ textSize + 36 };

  buffer.putBEW( 0x601B );    // Magic Number (0x601B)
  buffer.putBEL( (int)textSize ); // TEXT segment size
  buffer.putBEL( 0 ); // DATA segment size
  buffer.putBEL( 0 ); // BSS segment size
  buffer.putBEL( 0 ); // Symbol table size (?)
  buffer.putBEL( 0 ); //
  buffer.putBEL( (int)blocks.front().address ); // TEXT base address
  buffer.putBEW( 0xffff ); // Flags (?)
  buffer.putBEL( (int)( blocks.front().address + textSize ) ); // DATA base address
  buffer.putBEL( (int)( blocks.front().address + textSize ) ); // BSS base address

  int32_t cursor = blocks.front().address;
  for ( size_t i = 0; i < blocks.size(); ++i )
  {
    auto const& block = blocks[i];

    for ( ;cursor < block.address; ++cursor )
    {
      buffer.putB( 0xff );
    }

    std::copy( block.data.begin(), block.data.end(), std::back_inserter( buffer.data ) );

    cursor += (int32_t)block.data.size();
  }

  std::ofstream fout{ project().config().getOut() / mPath, std::ios::binary };
  fout.write( (char const*)buffer.data.data(), buffer.data.size() );
}

}
