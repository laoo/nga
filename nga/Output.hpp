#pragma once

namespace nga
{

class MachineContext;
class SegmentProcessor;
class Value;
class Segment;

class Output
{
public:
  virtual ~Output();

  static void registerLUA( sol::state & lua );

  virtual void commit() = 0;
  virtual void setEntry( Value v ) = 0;
  virtual void addSegment( Segment* segment ) = 0;

protected:
  Output();
  void handleListing( std::span<Segment*> segments ) const;

protected:

};

}
