#include "pch.hpp"
#include "SegmentDescriptor.hpp"
#include "Ex.hpp"
#include "Project.hpp"
#include "Machine.hpp"
#include "Output.hpp"
#include "IChunk.hpp"
#include "ILinkingContext.hpp"

namespace nga
{
namespace
{
class ISegmentFilter
{
public:
  virtual ~ISegmentFilter() = default;

  virtual uint32_t size( nga::ILinkingContext& lc ) const = 0;
  virtual void emit( nga::ILinkingContext& lc ) const = 0;
};

class DependentSegmentChunk : public nga::IChunk
{
  nga::Pos mPos;
  std::unique_ptr<ISegmentFilter> mSegmentFilter;

public:

  DependentSegmentChunk( std::unique_ptr<ISegmentFilter> segmentFilter );
  ~DependentSegmentChunk() override = default;

  uint32_t size( nga::ILinkingContext& lc ) const override;
  void emit( nga::ILinkingContext& lc ) const override;
  void setPosCpu( Pos pos, CpuType cpu = CpuType::Type::UNDEFINED ) override;
  Pos getPos() const override;
  std::tuple<bool, std::string_view, nga::Pos> listingLine() const override;

};


class CopySegmentFilter : public ISegmentFilter
{
  Segment* mSegment;
public:
  CopySegmentFilter( Segment* segment ) : mSegment{ segment }
  {
  }

  uint32_t size( nga::ILinkingContext& lc ) const override
  {
    return lc.segmentSize( mSegment );
  }

  void emit( nga::ILinkingContext& lc ) const override
  {
    lc.emitBlock( lc.segmentData( mSegment ) );
  }

};

}

SegmentDescriptor::SegmentDescriptor( sol::table const& tab ) : mDomain{}, mName{}, mAddressSize{ defaultAddressSize() }
{
}

void SegmentDescriptor::registerLUA( sol::state & lua )
{
  lua["BCode"] = []( sol::table const& tab ) -> SegmentDescriptor*
  {
    return new BootCodeSegmentDescriptor( tab );
  };
  lua["BootCode"] = lua["BCode"];

  lua["BData"] = []( sol::table const& tab ) -> SegmentDescriptor*
  {
    return new BootDataSegmentDescriptor( tab );
  };
  lua["BootData"] = lua["BData"];

  lua["DCode"] = []( sol::table const& tab ) -> SegmentDescriptor*
  {
    return new DependentCodeSegmentDescriptor( tab );
  };
  lua["DependentCode"] = lua["DCode"];

  lua["DData"] = []( sol::table const& tab ) -> SegmentDescriptor*
  {
    return new DependentDataSegmentDescriptor( tab );
  };
  lua["DependentData"] = lua["DData"];

  lua["SArea"] = []( sol::table const& tab ) -> SegmentDescriptor*
  {
    return new StaticAreaSegmentDescriptor( tab );
  };
  lua["StaticArea"] = lua["SArea"];

  lua["DArea"] = []( sol::table const& tab ) -> SegmentDescriptor*
  {
    return new DynamicAreaSegmentDescriptor( tab );
  };
  lua["DynamicArea"] = lua["DArea"];

  lua["Domain"] = []( int32_t l, int32_t u ) -> DomRange
  {
    if ( l > u )
      throw Ex{} << "Bad Domain(" << l << ", " << u << " )";
    return { l, u };
  };
}

int SegmentDescriptor::addressSize() const
{
  return mAddressSize;
}

bool SegmentDescriptor::emptyDomain() const
{
  return mDomain.IsEmpty();
}

ort::Domain const& SegmentDescriptor::domain() const
{
  return mDomain;
}

void SegmentDescriptor::setName( std::string name )
{
  mName = std::move( name );
}

std::string const& SegmentDescriptor::getName() const
{
  return mName;
}

void SegmentDescriptor::populate( std::pair<sol::object, sol::object> kv )
{
  sol::object const& value = kv.second;
  sol::type t = value.get_type();

  switch ( t )
  {
  case sol::type::userdata:
    if ( value.is<DomRange>() )
    {
      addDomain( value.as<DomRange>() );
      break;
    }
    [[fallthrough]];
  default:
    throw Ex{ "Unsupported argument to segment descriptor" };
  }
}

void SegmentDescriptor::addDomain( DomRange d )
{
  mDomain = mDomain.UnionWith( ort::Domain{ d.min, d.max } );

  if ( mDomain.IsEmpty() )
  {
    return;
  }

  switch ( project().machine()->proc() )
  {
  case Machine::Processor::PROC_6502:
    if ( mDomain.Min() <= std::numeric_limits<uint8_t>::max() )
      mAddressSize = 1;
    else
      mAddressSize = 2;
    break;
  case Machine::Processor::PROC_68000:
  case Machine::Processor::PROC_JAGUAR:
    if ( mDomain.Max() <= std::numeric_limits<int16_t>::max() && mDomain.Min() >= std::numeric_limits<int16_t>::min() )
      mAddressSize = 2;
    else
      mAddressSize = 4;
    break;
  default:
    INTERNAL_ERROR;
  }
}

int SegmentDescriptor::defaultAddressSize()
{
  switch ( project().machine()->proc() )
  {
  case Machine::Processor::PROC_6502:
    return 2;
    break;
  case Machine::Processor::PROC_68000:
  case Machine::Processor::PROC_JAGUAR:
    return 4;
    break;
  default:
    INTERNAL_ERROR;
  }
}


BootSegmentDescriptor::BootSegmentDescriptor( bool executable, sol::table const& tab ) : SegmentDescriptor{ tab }, mOutput{}, mExecutable{ executable }
{
  auto s = tab.size();
  for ( auto kv : tab )
  {
    sol::object const& value = kv.second;
    sol::type t = value.get_type();

    switch ( t )
    {
    case sol::type::userdata:
      if ( value.is<std::shared_ptr<Output>>() )
      {
        mOutput = value.as<std::shared_ptr<Output>>();
        break;
      }
      [[fallthrough]];
    default:
      populate( kv );
    }
  }

  if ( !mOutput )
    throw Ex{ "Output must be set before creating BBlock" };
}

void BootSegmentDescriptor::setEntry( Value v )
{
  mOutput->setEntry( std::move( v ) );
}

void BootSegmentDescriptor::registerSegment( Segment* segment )
{
}

void BootSegmentDescriptor::commitSegment( Segment* segment )
{
  mOutput->addSegment( segment );
}

bool BootSegmentDescriptor::executable() const
{
  return mExecutable;
}

std::shared_ptr<Output> BootSegmentDescriptor::output() const
{
  return mOutput;
}

DependentSegmentDescriptor::DependentSegmentDescriptor( bool executable, sol::table const& tab ) : SegmentDescriptor{ tab }, mParent{}, mExecutable{ executable }
{
  auto s = tab.size();
  for ( auto kv : tab )
  {
    sol::object const& value = kv.second;
    sol::type t = value.get_type();

    switch ( t )
    {
    case sol::type::userdata:
      if ( value.is<SegmentDescriptor*>() )
      {
        mParent = value.as<SegmentDescriptor*>();
        break;
      }
      [[fallthrough]];
    default:
      populate( kv );
    }
  }

  if ( !mParent )
    throw Ex{ "Parent segment descriptor must be set before creating dependant block descriptor" };
}

int32_t DependentSegmentDescriptor::namedOperatorSize( std::string_view operatorName, std::string_view arg ) const
{
  if ( arg == mParent->getName() )
    return mParent->concreteOperatorSize( operatorName );
  else
    return mParent->namedOperatorSize( operatorName, arg );
}

Value DependentSegmentDescriptor::namedOperatorValue( nga::ILinkingContext & ctx, Segment* segment, std::string_view operatorName, std::string_view arg ) const
{
  if ( arg == mParent->getName() )
    return mParent->concreteOperatorValue( ctx, segment, operatorName );
  else
    return mParent->namedOperatorValue( ctx, segment, operatorName, arg );
}

void DependentSegmentDescriptor::registerSegment( Segment* segment )
{
  mParent->registerDependantSegment( segment );
}

void DependentSegmentDescriptor::commitSegment( Segment* segment )
{
}

bool DependentSegmentDescriptor::implicitlyConstructed() const
{
  return false;
}

bool DependentSegmentDescriptor::executable() const
{
  return mExecutable;
}

StaticAreaSegmentDescriptor::StaticAreaSegmentDescriptor( sol::table const& tab ) : SegmentDescriptor{ tab }
{
  for ( auto kv : tab )
  {
    sol::object const& value = kv.second;
    sol::type t = value.get_type();

    switch ( t )
    {
    case sol::type::userdata:
      [[fallthrough]];
    default:
      populate( kv );
    }
  }
}

void StaticAreaSegmentDescriptor::registerSegment( Segment* segment )
{
}

void StaticAreaSegmentDescriptor::commitSegment( Segment* segment )
{
}

bool StaticAreaSegmentDescriptor::executable() const
{
  return false;
}

DynamicAreaSegmentDescriptor::DynamicAreaSegmentDescriptor( sol::table const& tab ) : SegmentDescriptor{ tab }
{
  for ( auto kv : tab )
  {
    sol::object const& value = kv.second;
    sol::type t = value.get_type();

    switch ( t )
    {
    case sol::type::userdata:
      [[fallthrough]];
    default:
      populate( kv );
    }
  }
}

void DynamicAreaSegmentDescriptor::registerSegment( Segment* segment )
{
}

void DynamicAreaSegmentDescriptor::commitSegment( Segment* segment )
{
}

bool DynamicAreaSegmentDescriptor::executable() const
{
  return false;
}

int32_t BootDataSegmentDescriptor::concreteOperatorSize( std::string_view operatorName ) const
{
  using namespace std::string_view_literals;

  struct OpSize
  {
    std::string_view name;
    int32_t size;
  };

  static constexpr OpSize tab[] = {
    { "address"sv, 2 },
    { "size"sv, 2 }
  };


  for ( OpSize const& op : tab )
  {
    if ( op.name == operatorName )
    {
      return op.size;
    }
  }

  throw Ex{} << "Unknown named operator " << operatorName;
}

Value BootDataSegmentDescriptor::concreteOperatorValue( nga::ILinkingContext & ctx, Segment* depSegment, std::string_view operatorName ) const
{
  auto it = std::ranges::find_if( mSegmentMapping, [=]( Segment* s ) { return s == depSegment; }, &std::pair<Segment*, Segment*>::first );
  if ( it == mSegmentMapping.end() )
    INTERNAL_ERROR;

  if ( operatorName == "address" )
  {
    return it->second->chunkOffsetValue( ctx, 0 );
  }
  else if ( operatorName == "size" )
  {
    return Value::create( ctx.segmentSize( it->second ) );
  }

  throw Ex{} << "Unknown named operator " << operatorName;
}

void BootDataSegmentDescriptor::registerDependantSegment( Segment* segment )
{
  auto pSegment = project().newSegment();
  pSegment->setMachineContext( segment->getMachineContext() );
  pSegment->setSegmentDescriptorName( getName() );
  pSegment->append( std::make_shared<DependentSegmentChunk>( std::make_unique<CopySegmentFilter>( segment ) ) );
  pSegment->createProcessor();
  pSegment->setDependentSegment( segment );
  mSegmentMapping.emplace_back( segment, pSegment );
  nga::project().addReference( segment, pSegment, nga::SRK_DEFAULT );

}


int32_t DependentDataSegmentDescriptor::concreteOperatorSize( std::string_view operatorName ) const
{
  using namespace std::string_view_literals;

  struct OpSize
  {
    std::string_view name;
    int32_t size;
  };

  static constexpr OpSize tab[] = {
    { "address"sv, 2 },
    { "size"sv, 2 }
  };


  for ( OpSize const& op : tab )
  {
    if ( op.name == operatorName )
    {
      return op.size;
    }
  }

  throw Ex{} << "Unknown named operator " << operatorName;
}

Value DependentDataSegmentDescriptor::concreteOperatorValue( nga::ILinkingContext & ctx, Segment* segment, std::string_view operatorName ) const
{
  INTERNAL_ERROR;
}

DependentSegmentChunk::DependentSegmentChunk( std::unique_ptr<ISegmentFilter> segmentFilter ) : mSegmentFilter{ std::move( segmentFilter ) }
{
}

uint32_t DependentSegmentChunk::size( nga::ILinkingContext& lc ) const
{
  return mSegmentFilter->size( lc );
}

void DependentSegmentChunk::emit( nga::ILinkingContext& lc ) const
{
  return mSegmentFilter->emit( lc );
}

void DependentSegmentChunk::setPosCpu( Pos pos, CpuType cpu )
{
  mPos = pos;
}

Pos DependentSegmentChunk::getPos() const
{
  return mPos;
}

std::tuple<bool, std::string_view, nga::Pos> DependentSegmentChunk::listingLine() const
{
  return {};
}

}
