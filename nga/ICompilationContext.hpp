#pragma once

namespace assembler
{
class Generator;
struct LabelDecl;
struct MacroInv;
struct Seg;
struct Ends;
struct Local;
struct Endl;
struct Proc;
struct Endp;
struct Zone;
struct Endz;
struct End;
struct Equ;
struct Equr;
struct Cpu;
struct Icl;
struct MacroDef;
struct NSName;
}

namespace nga
{

class ICompilationContext
{
public:

  virtual ~ICompilationContext() = default;

  template <typename T, typename... Args>
  std::shared_ptr<T> create( Args&&... args )
  {
    return std::make_shared<T>( std::forward<Args>( args )... );
  }

  virtual std::shared_ptr<void> process( std::shared_ptr<assembler::Generator> generator ) = 0;
  virtual std::shared_ptr<void> process( std::shared_ptr<assembler::LabelDecl> label ) = 0;
  virtual std::shared_ptr<void> process( std::shared_ptr<assembler::MacroInv> macroInv ) = 0;

  virtual std::shared_ptr<void> process( std::shared_ptr<assembler::Seg> segment ) = 0;
  virtual std::shared_ptr<void> process( std::shared_ptr<assembler::Ends> endseg ) = 0;
  virtual std::shared_ptr<void> process( std::shared_ptr<assembler::Local> local ) = 0;
  virtual std::shared_ptr<void> process( std::shared_ptr<assembler::Endl> endl ) = 0;
  virtual std::shared_ptr<void> process( std::shared_ptr<assembler::Proc> proc ) = 0;
  virtual std::shared_ptr<void> process( std::shared_ptr<assembler::Endp> endp ) = 0;
  virtual std::shared_ptr<void> process( std::shared_ptr<assembler::Zone> zone ) = 0;
  virtual std::shared_ptr<void> process( std::shared_ptr<assembler::Endz> endz ) = 0;
  virtual std::shared_ptr<void> process( std::shared_ptr<assembler::End> end ) = 0;
  virtual std::shared_ptr<void> process( std::shared_ptr<assembler::Equ> equ ) = 0;
  virtual std::shared_ptr<void> process( std::shared_ptr<assembler::Equr> equ ) = 0;
  virtual std::shared_ptr<void> process( std::shared_ptr<assembler::Cpu> cpu ) = 0;
  virtual std::shared_ptr<void> process( std::shared_ptr<assembler::Icl> file ) = 0;
  virtual std::shared_ptr<void> process( std::shared_ptr<assembler::MacroDef> macroDef ) = 0;

  virtual std::string_view context() const = 0;
  virtual std::optional<int> regMapping( std::string_view name ) const = 0;
  virtual void registerReference( assembler::NSName const& reference ) = 0;

};

}
