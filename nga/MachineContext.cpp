#include "pch.hpp"
#include "MachineContext.hpp"
#include "SegmentDescriptor.hpp"
#include "Input.hpp"
#include "Project.hpp"
#include "Machine.hpp"
#include "Output.hpp"
#include "Ex.hpp"

namespace nga
{

MachineContext::MachineContext() : mSegmentDescriptors{}, mInputs{}, mSymbols{}, mGlobalProgress{}
{
}

std::shared_ptr<SegmentDescriptor> MachineContext::segmentDescriptor( std::string_view name ) const
{
  auto it = mSegmentDescriptors.find( name );
  if ( it != mSegmentDescriptors.end() )
  {
    return it->second;
  }

  return {};
}

void MachineContext::addSegmentDescriptor( std::string name, std::shared_ptr<SegmentDescriptor> desc )
{
  if ( name.empty() )
    throw Ex{ "SegmentDescriptor must have a name" };

  assert( desc );

  desc->setName( name );

  auto [it,success] = mSegmentDescriptors.insert( std::pair<std::string, std::shared_ptr<SegmentDescriptor>>( std::move( name ), desc ) );
  if ( !success )
    throw Ex{ "Duplicated segment descriptor" };

  if ( auto output = desc->output() )
  {
    addOutput( std::move( output ) );
  }
}

void MachineContext::addInput( std::shared_ptr<Input> input )
{
  input->setMachineContext( this );
  mInputs.push_back( std::move( input ) );
}

void MachineContext::addOutput( std::shared_ptr<Output> output )
{
  if ( std::ranges::find( mOutputs, output ) == mOutputs.cend() )
    mOutputs.push_back( std::move( output ) );
}

void MachineContext::commit()
{
  if ( mOutputs.empty() )
    throw Ex{ "No outputs defined" };

  for ( auto& output : mOutputs )
  {
    output->commit();
  }
}

void MachineContext::populate( sol::table const & tab )
{
  for ( auto kv : tab )
  {
    sol::object const& value = kv.second;
    sol::object const& key = kv.first;
    auto vtype = value.get_type();

    switch ( vtype )
    {
    case sol::type::string:
      if ( key.is<std::string>() )
      {
        throw Ex{ "Unsupported string parameter in MachineContext argument table" };
      }
      break;
    case sol::type::userdata:
      if ( value.is<std::shared_ptr<Input>>() )
      {
        addInput( value.as<std::shared_ptr<Input>>() );
      }
      else
      {
        throw Ex{ "Unsupported type in MachineContext argument table" };
      }
      break;
    default:
      throw Ex{ "Unexpected type in MachineContext array" };
      break;
    }
  }
}

std::shared_ptr<ISymbol const> MachineContext::findSymbol( std::string_view ctx, std::string_view name ) const
{
  return mSymbols.find( ctx, name );
}

void MachineContext::addSymbol( std::string key, std::shared_ptr<ISymbol const> symbol )
{
  mSymbols.add( std::move( key ), std::move( symbol ) );
}

void MachineContext::registerLUA( sol::state & lua )
{
  lua["Segments"] = []( sol::table const& tab )
  {
    auto ctx = std::make_shared<MachineContext>();
    ctx->populate( tab );
    project().machine()->setContext( std::move( ctx ) );
  };
}

void MachineContext::collectInputs()
{
  for ( auto& input : mInputs )
  {
    for ( auto const& s : input->symbols() )
    {
      addSymbol( s.first, s.second );
    }
  }
}

void MachineContext::advanceProgress()
{
  mGlobalProgress += 1;
}

int64_t MachineContext::getProgress() const
{
  return mGlobalProgress;
}

}


