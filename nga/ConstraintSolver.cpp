#include "pch.hpp"
#include "ConstraintSolver.hpp"
#include "Segment.hpp"
#include "SegmentDescriptor.hpp"
#include "Log.hpp"
#include "Ex.hpp"

namespace nga
{


ConstraintSolver::ConstraintSolver() : mModel{}, mMap{}
{
}

ConstraintSolver::~ConstraintSolver()
{
}

void ConstraintSolver::addSegment( Segment* segment )
{
  if ( mMap.find( segment ) != mMap.cend() )
    return;

  auto& desc = segment->segmentDescriptor();
  if ( desc.emptyDomain() )
    return;

  auto const& flattenedIntervals = desc.domain().FlattenedIntervals();
  std::vector<ort::ClosedInterval> sizedIntervals;

  for ( auto const& interval : desc.domain() )
  {
    std::pair<int64_t, int64_t> intv{ interval.start, interval.end - (int64_t)segment->size() };
    if ( intv.first <= intv.second )
    {
      sizedIntervals.emplace_back( intv.first, intv.second );
    }
  }

  mMap.insert( { segment, mModel.NewIntVar( ort::Domain::FromIntervals( std::span<ort::ClosedInterval>{ sizedIntervals.data(), sizedIntervals.size() } ) ) } );
}

void ConstraintSolver::addEdge( Segment* src, Segment* dst )
{
  auto pSrcVar = mMap.find( src );
  auto pDstVar = mMap.find( dst );

  if ( pSrcVar == mMap.end() || pDstVar == mMap.end() )
    throw Ex{ "Added edge to undeclared segment" };

  std::array< sat::BoolVar, 2 > alt;

  alt[0] = mModel.NewBoolVar();
  alt[1] = alt[0].Not();

  mModel.AddLessOrEqual( sat::LinearExpr{ pSrcVar->second }.AddConstant( src->size() ), pDstVar->second ).OnlyEnforceIf( alt[0] );
  mModel.AddLessOrEqual( sat::LinearExpr{ pDstVar->second }.AddConstant( dst->size() ), pSrcVar->second ).OnlyEnforceIf( alt[1] );
}

void ConstraintSolver::process()
{
  const sat::CpSolverResponse response = sat::Solve( mModel.Build() );
  L_TRACE << sat::CpSolverResponseStats( response );

  if ( response.status() == sat::CpSolverStatus::OPTIMAL || response.status() == sat::CpSolverStatus::FEASIBLE )
  {
    for ( auto const& pair : mMap )
    {
      pair.first->setAddress( (int32_t)sat::SolutionIntegerValue( response, pair.second ) );
    }
  }
  else
  {
    throw Ex{ "Unsuccessfull constraint solving" };
  }
}

}
