#include "pch.hpp"
#include "SegmentDescriptorClass.hpp"
#include "SegmentDescriptor.hpp"
#include "SegmentProcessor.hpp"
#include "Segment.hpp"
#include "ConstraintSolver.hpp"
#include "Ex.hpp"

namespace nga
{

SegmentDescriptorClass::SegmentDescriptorClass() : mDomain{}, mProcessors{}, mSolver{ std::make_unique<ConstraintSolver>() }
{
}

SegmentDescriptorClass::~SegmentDescriptorClass()
{
}

bool SegmentDescriptorClass::overlaps( ort::Domain const& d )
{
  return !mDomain.IntersectionWith( d ).IsEmpty();
}

void SegmentDescriptorClass::addProcessor( std::shared_ptr<SegmentProcessor> proc )
{
  if ( mSolver )
  {
    mDomain = mDomain.UnionWith( proc->processedSegment().segmentDescriptor().domain() );
    mProcessors.push_back( proc );
  }
  else
  {
    throw Ex{ "Inconsistent segment network" };
  }
}

std::shared_ptr<SegmentProcessor> SegmentDescriptorClass::allocateSegments()
{
  if ( mSolver )
  {
    for ( auto const& proc : mProcessors )
    {
      if ( (int)proc->phase() < (int)SegmentProcessor::Phase::EMITING )
        return proc;
    }

    std::vector<Segment*> absoluteSegments;
    for ( auto const& proc : mProcessors )
    {
      auto& segment = proc->processedSegment();
      auto& segmentDescriptor = segment.segmentDescriptor();

      if ( segmentDescriptor.emptyDomain() )
      {
        INTERNAL_ERROR; //IN PROGRESS
      }
      else
      {
        mSolver->addSegment( &segment );
        absoluteSegments.push_back( &segment );
      }
    }

    //currently implementing only full clique
    for ( size_t i = 1; i < absoluteSegments.size(); ++i )
    {
      for ( size_t j = 0; j < i; ++j )
      {
        assert( absoluteSegments[i] != absoluteSegments[j] );
        mSolver->addEdge( absoluteSegments[i], absoluteSegments[j] );
      }
    }

    mSolver->process();
    mSolver.reset();
  }

  return {};
}

}

