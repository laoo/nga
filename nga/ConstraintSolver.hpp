#pragma once

namespace nga
{
class Segment;

class ConstraintSolver
{
public:
  ConstraintSolver();
  ~ConstraintSolver();

  void addSegment( Segment* node );
  void addEdge( Segment* src, Segment* dst );
  void process();

private:

  sat::CpModelBuilder mModel;
  std::unordered_map<Segment*, sat::IntVar> mMap;
};

}
