#pragma once

#include "Output.hpp"
#include "Value.hpp"

namespace nga::output
{

class XEX : public Output
{
public:
  XEX( sol::table const& tab );
  ~XEX() override = default;

  static void registerLUA( sol::state& lua );

  void setEntry( Value v ) override;
  void addSegment( Segment* segment ) override;
  void commit() override;

private:
  struct Block
  {
    int32_t address;
    std::span<uint8_t const> data;
  };

private:
  std::filesystem::path mPath;
  std::optional<Value> mEntry;
  std::vector<Segment*> mSegments;
};

}

