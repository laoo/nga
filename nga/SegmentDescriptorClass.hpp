#pragma once

namespace nga
{

class SegmentDescriptor;
class SegmentProcessor;
class ConstraintSolver;

class SegmentDescriptorClass
{
public:

  SegmentDescriptorClass();
  ~SegmentDescriptorClass();

  bool overlaps( ort::Domain const& d );
  void addProcessor( std::shared_ptr<SegmentProcessor> proc );
  std::shared_ptr<SegmentProcessor> allocateSegments();

private:

  ort::Domain mDomain;
  std::vector<std::shared_ptr<SegmentProcessor>> mProcessors;
  std::unique_ptr<ConstraintSolver> mSolver;
};

}
