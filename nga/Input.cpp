#include "pch.hpp"
#include "Input.hpp"
#include "Ex.hpp"
#include "Assembler.hpp"
#include "Vasm68k.hpp"
#include "GCC.hpp"

namespace nga
{

void Input::registerLUA( sol::state & lua )
{
  lua["Assembler"] = []( sol::table const& tab ) -> std::shared_ptr<Input>
  {
    return std::make_shared<input::Assembler>( tab );
  };

  lua["Vasm68k"] = []( sol::table const& tab ) -> std::shared_ptr<Input>
  {
    return std::make_shared<input::Vasm68k>( tab );
  };

  lua["GCC"] = []( sol::table const& tab ) -> std::shared_ptr<Input>
  {
    return std::make_shared<input::GCC>( tab );
  };
}

Input::Input()
{
}

Input::~Input()
{
}

}
