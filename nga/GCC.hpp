#pragma once

#include "Input.hpp"

namespace nga::input
{

class GCC : public Input
{
public:
  GCC( sol::table const& tab );
  ~GCC() override = default;

private:
  std::filesystem::path processC( std::filesystem::path path );
  void processAsm( std::filesystem::path path );

private:
  std::filesystem::path mBasePath;
  std::regex mNamePattern;
};

}

