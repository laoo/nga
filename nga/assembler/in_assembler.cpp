#include "pch.hpp"
#include "in_assembler.hpp"
#include "Ex.hpp"
#include "CompilationContext.hpp"
#include "SegmentStore.hpp"
#include "Pos.hpp"
#include "Project.hpp"

namespace assembler
{

void assemble( std::filesystem::path asmSrc, nga::SegmentStore & segmentStore )
{
  CompilationContext cu{ segmentStore };
  cu.parseFile( asmSrc );
}

}

