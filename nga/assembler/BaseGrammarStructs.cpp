#include "pch.hpp"
#include "BaseGrammarStructs.hpp"
#include "Project.hpp"
#include "Segment.hpp"
#include "Ex.hpp"
#include "MachineContext.hpp"
#include "ILinkingContext.hpp"

namespace assembler
{
LitValue::LitValue( int32_t v ) : v{ v }
{
}
uint32_t LitValue::size( nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  return cpu.valueSize( v );
}

nga::Value LitValue::value( nga::ILinkingContext& lc ) const
{
  return nga::Value::create( v );
}

uint32_t CurrentPC::size( nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  return lc.processedSegmentAddressSize();
}

nga::Value CurrentPC::value( nga::ILinkingContext& lc ) const
{
  return lc.processedChunkAddress();
}


AnonLabelRef::AnonLabelRef( std::string_view ctx, int distance ) : ctx{ ctx }, distance{ distance }
{
}

uint32_t AnonLabelRef::size( nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  return lc.processedSegmentAddressSize();
}

nga::Value AnonLabelRef::value( nga::ILinkingContext& lc ) const
{
  if ( auto opt = lc.anonymousLabelAddressFromCurrentChunk( distance ) )
    return std::move( *opt );
  else if ( distance > 0 )
  {
    if ( distance == 1 )
      throw Ex{ "Forward anonymous label not found" };
    else
      throw Ex{} << "Anonymous label past " << distance - 1 << " labels forward not found";
  }
  else
  {
    if ( distance == -1 )
      throw Ex{ "Backward anonymous label not found" };
    else
      throw Ex{} << "Anonymous label past " << -distance - 1 << " labels backward not found";
  }
}

NSName::NSName( std::string_view ctx, std::string_view n ) : ctx{ ctx }, name{ n }, kind{ nga::SRK_DEFAULT }
{
}

uint32_t NSName::size( nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  return lc.symbolSize( ctx, name, cpu );
}

nga::Value NSName::value( nga::ILinkingContext& lc ) const
{
  return lc.symbolValue( ctx, name );
}

void NSName::visit( nga::ILinkingContext& lc ) const
{
  lc.visit( ctx, name, kind );
}

void NSName::visit( nga::ICompilationContext& cc ) const
{
  cc.registerReference( *this );
}

void NSName::setSegmentReferenceKind( nga::SegmentReferenceKind k )
{
  kind |= k;
}

ExprUnary_::ExprUnary_( std::shared_ptr<Expression> unary ) : expr{ std::move( unary ) }
{
}

uint32_t ExprUnary_::size( nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  return expr->size( lc, cpu );
}

nga::Value ExprUnary_::value( nga::ILinkingContext& lc ) const
{
  return -expr->value( lc );
}

void ExprUnary_::visit( nga::ICompilationContext& cc ) const
{
  expr->visit( cc );
}

ExprMul_::ExprMul_( std::shared_ptr<Expression> exprMul, char op, std::shared_ptr<Expression> exprUnary ) : exprMul{ std::move( exprMul ) }, exprUnary{ std::move( exprUnary ) }, op{ op }
{
}

uint32_t ExprMul_::size( nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  return std::max( exprUnary->size( lc, cpu ), exprMul->size( lc, cpu ) );
}

nga::Value ExprMul_::value( nga::ILinkingContext& lc ) const
{
  switch ( op )
  {
  case '*':
    return exprMul->value( lc ) * exprUnary->value( lc );
  case '/':
    return exprMul->value( lc ) / exprUnary->value( lc );
  case '%':
    return exprMul->value( lc ) % exprUnary->value( lc );
  default:
    INTERNAL_ERROR;
  }
}

void ExprMul_::visit( nga::ICompilationContext& cc ) const
{
  exprMul->visit( cc );
  exprUnary->visit( cc );
}

ExprShift_::ExprShift_( std::shared_ptr<ExprShift> exprShift, char op, std::shared_ptr<ExprAdd> exprAdd ) : exprShift{ std::move( exprShift ) }, exprAdd{ std::move( exprAdd ) }, op{ op }
{
}

uint32_t ExprShift_::size( nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  return std::max( exprShift->size( lc, cpu ), exprAdd->size( lc, cpu ) );
}

nga::Value ExprShift_::value( nga::ILinkingContext& lc ) const
{
  switch ( op )
  {
  case 'L':
    return exprShift->value( lc ) << exprAdd->value( lc );
  case 'R':
    return exprShift->value( lc ) >> exprAdd->value( lc );
  default:
    INTERNAL_ERROR;
  }
}

void ExprShift_::visit( nga::ICompilationContext& cc ) const
{
  exprShift->visit( cc );
  exprAdd->visit( cc );
}

ExprAdd_::ExprAdd_( std::shared_ptr<Expression> exprAdd, char op, std::shared_ptr<Expression> exprMul ) : exprAdd{ std::move( exprAdd ) }, exprMul{ std::move( exprMul ) }, op{ op }
{
}

uint32_t ExprAdd_::size( nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  return std::max( exprMul->size( lc, cpu ), exprAdd->size( lc, cpu ) );
}

nga::Value ExprAdd_::value( nga::ILinkingContext& lc ) const
{
  switch ( op )
  {
  case '+':
    return exprAdd->value( lc ) + exprMul->value( lc );
  case '-':
    return exprAdd->value( lc ) - exprMul->value( lc );
  default:
    INTERNAL_ERROR;
  }
}

void ExprAdd_::visit( nga::ICompilationContext& cc ) const
{
  exprAdd->visit( cc );
  exprMul->visit( cc );
}

ExprAnd_::ExprAnd_( std::shared_ptr<Expression> exprAnd, std::shared_ptr<Expression> exprShift ) : exprAnd{ std::move( exprAnd ) }, exprShift{ std::move( exprShift ) }
{
}

uint32_t ExprAnd_::size( nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  return std::max( exprAnd->size( lc, cpu ), exprShift->size( lc, cpu ) );
}

nga::Value ExprAnd_::value( nga::ILinkingContext& lc ) const
{
  return exprAnd->value( lc ) & exprShift->value( lc );
}

void ExprAnd_::visit( nga::ICompilationContext& cc ) const
{
  exprAnd->visit( cc );
  exprShift->visit( cc );
}

ExprOr_::ExprOr_( std::shared_ptr<Expression> exprOr, std::shared_ptr<Expression> exprAnd ) : exprOr{ std::move( exprOr ) }, exprAnd{ std::move( exprAnd ) }
{
}

uint32_t ExprOr_::size( nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  return std::max( exprOr->size( lc, cpu ), exprAnd->size( lc, cpu ) );
}

nga::Value ExprOr_::value( nga::ILinkingContext& lc ) const
{
  return exprOr->value( lc ) | exprAnd->value( lc );
}

void ExprOr_::visit( nga::ICompilationContext& cc ) const
{
  exprOr->visit( cc );
  exprAnd->visit( cc );
}

Mnemonic::Mnemonic( int opcode ) : opcode{ opcode } {}

Generator::Generator() : pos{}, cpu{}
{
}

void Generator::emit( nga::ILinkingContext& lc ) const
{
  try
  {
    emit( lc, cpu );
  }
  catch ( Ex& ex )
  {
    ex.addStackTrace( nga::project().posString( pos ) );
    throw;
  }
}

uint32_t Generator::size( nga::ILinkingContext& lc ) const
{
  try
  {
    return size( lc, cpu );
  }
  catch ( Ex& ex )
  {
    ex.addStackTrace( nga::project().posString( pos ) );
    throw;
  }
}

void Generator::setPosCpu( nga::Pos pos, nga::CpuType cpu )
{
  this->pos = pos;
  this->cpu = cpu;
}

nga::Pos Generator::getPos() const
{
  return pos;
}

std::tuple<bool, std::string_view, nga::Pos> Generator::listingLine() const
{
  return { false, nga::project().posLine( pos ), pos };
}

Opcode::Opcode( std::shared_ptr<Mnemonic> m, std::shared_ptr<Addressing> a ) : Generator{},
  mnemonic{ std::move( m ) }, addressing{ std::move( a ) }
{
}

uint32_t Opcode::size( nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  return addressing->size( *mnemonic, lc, cpu );
}

void Opcode::emit( nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  return addressing->emit( *mnemonic, lc, cpu );
}

void Opcode::visit( nga::ICompilationContext& cc )
{
  addressing->visit( *mnemonic, cc );
}

DS::DS( std::shared_ptr<Expression> expression, uint16_t size ) : Generator{},
  expression{ std::move( expression ) }, multiplier{ 1 }
{
  switch ( size )
  {
  case 0:
    [[fallthrough]];
  case 1:
    multiplier = 1;
    break;
  case 2:
    multiplier = 2;
    break;
  case 3:
    multiplier = 4;
    break;
  default:
    INTERNAL_ERROR;
  }
}

uint32_t DS::size( nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  return expression->value( lc ).evaluate( lc ) * multiplier;
}

void DS::emit( nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  auto value = expression->value( lc ).evaluate( lc );
  lc.reserve( value * multiplier );
}

uint32_t ExprList::size( nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  uint32_t size{};
  for ( auto const& expr : list )
  {
    size += expr->size( lc, cpu );
  }
  return size;
}

void ExprList::emit( int size, nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  for ( auto const& elem : list )
  {
    auto v = elem->value( lc );
    switch ( size )
    {
    case 1:
      lc.emitI8( v, cpu );
      break;
    default:
      throw Ex{} << "Unexpected size " << size;
    }
  }
}

DataByte::DataByte( std::shared_ptr<ExprList> exprList ) : Generator{},
  exprList{ std::move( exprList ) }, string{}
{
}

DataByte::DataByte( std::string_view string ) : Generator{},
  exprList{}, string{ string.substr( 1, string.size() - 2 ) }
{
}

uint32_t DataByte::size( nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  if ( exprList )
    return exprList->size( lc, cpu );
  else
    return (uint32_t)string.size();
}

void DataByte::emit( nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  if ( exprList )
    exprList->emit( 1, lc, cpu );
  else
  {
    for ( char c : string )
    {
      lc.emitI8( c );
    }
  }
}

MacroInvParams::MacroInvParams() : params{} {}

NamedOperator::NamedOperator( std::string operatorName, std::shared_ptr<NSName> symbolName ) : operatorName{ std::move( operatorName ) }, arg{}, symbolName{ std::move( symbolName ) }
{
}

NamedOperator::NamedOperator( std::string operatorName, std::string arg, std::shared_ptr<NSName> symbolName ) : operatorName{ std::move( operatorName ) }, arg{ std::move( arg ) }, symbolName{ std::move( symbolName ) }
{
}


uint32_t NamedOperator::size( nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  return lc.namedOperatorSize( symbolName->ctx, symbolName->name, cpu, operatorName, arg );
}

nga::Value NamedOperator::value( nga::ILinkingContext& lc ) const
{
  return lc.namedOperatorValue( symbolName->ctx, symbolName->name, operatorName, arg );
}

void NamedOperator::visit( nga::ILinkingContext& lc ) const
{
  if ( !arg.empty() )
  {
    throw Ex{} << "Constructor " << operatorName << " can't be referenced through a symbol";
  }
}

void NamedOperator::visit( nga::ICompilationContext& cc ) const
{
  if ( !arg.empty() )
    symbolName->setSegmentReferenceKind( nga::SRK_CTOR );

  symbolName->visit( cc );
}

}
