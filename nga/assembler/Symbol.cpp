#include "pch.hpp"
#include "Symbol.hpp"
#include "MachineContext.hpp"
#include "BaseGrammarStructs.hpp"
#include "Project.hpp"
#include "ILinkingContext.hpp"
#include "Ex.hpp"

namespace assembler
{

Symbol::Symbol( nga::Pos pos, std::shared_ptr<Expression> expr ) : mPos{ pos }, mVar{ std::move( expr ) }
{
}

Symbol::Symbol( nga::Pos pos, nga::Segment::ChunkOffset offset ) : mPos{ pos }, mVar{ std::move( offset ) }
{
}

Symbol::~Symbol()
{
}

uint32_t Symbol::size( nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  struct Visitor
  {
    nga::ILinkingContext & lc;
    nga::CpuType cpu;

    uint32_t operator()( std::shared_ptr<Expression> const& expr )
    {
      return expr->size( lc, cpu );
    }

    uint32_t operator()( nga::Segment::ChunkOffset const& co )
    {
      return co.seg->addressSize();
    }

  } visitor{ lc, cpu };

  return std::visit( visitor, mVar );
}

nga::Value Symbol::value( nga::ILinkingContext & lc ) const
{
  struct Visitor
  {
    nga::ILinkingContext& lc;

    nga::Value operator()( std::shared_ptr<Expression> const& expr )
    {
      return expr->value( lc );
    }

    nga::Value operator()( nga::Segment::ChunkOffset const& co )
    {
      return co.seg->chunkOffsetValue( lc, co.off );
    }

  } visitor{ lc };

  return std::visit( visitor, mVar );
}

nga::Segment* Symbol::segment() const
{
  struct Visitor
  {
    nga::Segment* operator()( std::shared_ptr<Expression> const& expr )
    {
      return {};
    }

    nga::Segment* operator()( nga::Segment::ChunkOffset const& co )
    {
      return co.seg;
    }

  } visitor{};

  return std::visit( visitor, mVar );
}

void Symbol::visit( nga::ILinkingContext& lc, nga::SegmentReferenceKind kind ) const
{
  struct Visitor
  {
    nga::ILinkingContext& lc;
    nga::SegmentReferenceKind kind;

    void operator()( std::shared_ptr<Expression> const& expr )
    {
      expr->visit( lc );
    }

    void operator()( nga::Segment::ChunkOffset const& co )
    {
      nga::project().addReference( &lc.processedSegment(), co.seg, kind );
      co.seg->visit();
    }

  } visitor{ lc, kind };

  try
  {
    return std::visit( visitor, mVar );
  }
  catch ( Ex& ex )
  {
    ex.addStackTrace( nga::project().posString( mPos ) );
    throw;
  }
}

}
