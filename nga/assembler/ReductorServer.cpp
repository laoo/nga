#include "pch.hpp"
#include "ReductorServer.hpp"
#include "CompilationContext.hpp"
#include "grm/base.grm.hpp"
#include "CpuMOS.hpp"
#include "grm/mos.grm.hpp"
#include "CpuM68K.hpp"
#include "grm/m68k.grm.hpp"
#include "CpuJRISC.hpp"
#include "grm/jrisc.grm.hpp"
#include "Ex.hpp"

namespace assembler
{

std::shared_ptr<nga::Reductor> getReductor( nga::ICompilationContext* ctx, nga::CpuType cpuType )
{
  switch ( cpuType )
  {
  case nga::CpuType::NO_CPU:
    return std::make_shared<base::Reductor>( ctx );
  case nga::CpuType::MOS6502:
  case nga::CpuType::G65SC02:
  case nga::CpuType::WDC65C02:
    return std::make_shared<mos::Reductor>( ctx );
  case nga::CpuType::M68000:
    return std::make_shared<m68k::Reductor>( ctx );
  case nga::CpuType::TOM:
  case nga::CpuType::JERRY:
    return std::make_shared<jrisc::Reductor>( ctx );
  default:
    INTERNAL_ERROR;
  }
}

}
