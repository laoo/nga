#pragma once

#include "Ex.hpp"
#include "ICompilationContext.hpp"
#include "BaseGrammarStructs.hpp"

namespace nga
{
class ILinkingContext;
}

namespace assembler
{
namespace cpuJRISC
{
enum struct Op
{
  ADD,
  ADDC,
  ADDQ,
  ADDQT,
  SUB,
  SUBC,
  SUBQ,
  SUBQT,
  NEG,
  AND,
  OR,
  XOR,
  NOT,
  BTST,
  BSET,
  BCLR,
  MULT,
  IMULT,
  IMULTN,
  RESMAC,
  IMACN,
  DIV,
  ABS,
  SH,
  SHLQ,
  SHRQ,
  SHA,
  SHARQ,
  ROLQ,
  ROR,
  RORQ,
  CMP,
  CMPQ,
  SAT8,
  SUBQMOD,
  SAT16,
  SAT16S,
  MOVE,
  MOVEQ,
  MOVETA,
  MOVEFA,
  MOVEI,
  LOADB,
  LOADW,
  LOAD,
  LOADP,
  SAT32S,
  STOREB,
  STOREW,
  STORE,
  STOREP,
  MIRROR,
  JUMP,
  JR,
  MMULT,
  MTOI,
  NORMI,
  NOP,
  SAT24,
  PACK,
  ADDQMOD,
  UNPACK,

  _SIZE
};

int getReg( nga::ICompilationContext* ctx, std::string_view ident );
std::optional<int> getRegOpt( nga::ICompilationContext* ctx, std::string_view ident );
std::optional<int> getRegOpt( nga::ICompilationContext* ctx, std::shared_ptr<Expression> expr );

struct BaseIdxDisp
{
  std::optional<int> baseReg;
  std::optional<int> idxReg;
  std::shared_ptr<Expression> disp;
} getBaseIdxDisp( nga::ICompilationContext* ctx, std::shared_ptr<Expression> expr );

}

struct CCJRISC
{
  static constexpr int T    = 0b00000; //always
  static constexpr int NZ   = 0b00001; //if zero flag is clear
  static constexpr int Z    = 0b00010; //if zero flag is set
  static constexpr int NC   = 0b00100; //if carry flag is clear
  static constexpr int NC_NZ= 0b00101; //if carry flag is clear and zero flag is clear
  static constexpr int NC_Z = 0b00110; //if carry flag is clear and zero flag is set
  static constexpr int C    = 0b01000; //if carry flag is set
  static constexpr int C_NZ = 0b01001; //if carry flag is set and zero flag is clear
  static constexpr int C_Z  = 0b01010; //if carry flag is set and zero flag is set
  static constexpr int NN   = 0b10100; //if negative flag is clear
  static constexpr int NN_NZ= 0b10101; //if negative flag is clear and zero flag is clear
  static constexpr int NN_Z = 0b10110; //if negative flag is clear and zero flag is set
  static constexpr int N    = 0b11000; //if negative flag is set
  static constexpr int N_NZ = 0b11001; //if negative flag is set and zero flag is clear
  static constexpr int N_Z  = 0b11010; //if negative flag is set and zero flag is set
  static constexpr int F    = 0b11111; //never

  int cc;
};

struct MnemonicJRISC : public Mnemonic
{
  MnemonicJRISC( cpuJRISC::Op opcode, int cc = CCJRISC::T );

  int cc;
};


class AddressingJRISC : public Addressing
{
public:
  ~AddressingJRISC() override = default;

  struct Single
  {
    int dst;
  };

  struct Double
  {
    int src;
    int dst;
  };

  struct Immediate
  {
    std::shared_ptr<Expression> expr;
    int dst;
  };

  struct Jump
  {
    int cc;
    int dst;
  };

  struct Branch
  {
    int cc;
    std::shared_ptr<Expression> dest;
  };

  struct Load
  {
    int sourceAddress;
    int destinationRegister;
  };

  struct LoadWithDisplacement
  {
    int baseSourceAddress;
    std::shared_ptr<Expression> displacement;
    int destinationRegister;
  };

  struct LoadWithIndex
  {
    int baseSourceAddress;
    int index;
    int destinationRegister;
  };

  struct Store
  {
    int sourceRegister;
    int destinationAddress;
  };

  struct StoreWithDisplacement
  {
    int sourceRegister;
    int baseDestinationAddress;
    std::shared_ptr<Expression> displacement;
  };

  struct StoreWithIndex
  {
    int sourceRegister;
    int baseDestinationAddress;
    int index;
  };

  struct Nil
  {
  };

  struct Bad
  {
    enum Reason
    {
      DOUBLE_PARENTHIZED,
      NON_PARENTHIZED,
      REGISTER_EXPECTED,
      INDIRECTION,
      LABEL_EXPECTED
    } reason;
  };

  std::variant<
    Single, Double, Immediate, Jump, Branch, Load, LoadWithDisplacement, LoadWithIndex, Store, StoreWithDisplacement, StoreWithIndex, Nil, Bad
  > mVar;

  AddressingJRISC( Single const& arg );
  AddressingJRISC( Double const& arg );
  AddressingJRISC( Immediate const& arg );
  AddressingJRISC( Jump const& arg );
  AddressingJRISC( Branch const& arg );
  AddressingJRISC( Load const& arg );
  AddressingJRISC( LoadWithDisplacement const& arg );
  AddressingJRISC( LoadWithIndex const& arg );
  AddressingJRISC( Store const& arg );
  AddressingJRISC( StoreWithDisplacement const& arg );
  AddressingJRISC( StoreWithIndex const& arg );
  AddressingJRISC( Nil const& arg );
  AddressingJRISC( Bad const& arg );


  uint32_t size( Mnemonic const& mnemonic, nga::ILinkingContext & lc, nga::CpuType cpu ) const override;
  void emit( Mnemonic const& mnemonic, nga::ILinkingContext & lc, nga::CpuType cpu ) const override;
  void visit( Mnemonic const& mnemonic, nga::ICompilationContext& cc ) override {};

};

}
