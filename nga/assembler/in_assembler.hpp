#pragma once

namespace nga
{
class SegmentStore;
}

namespace assembler
{

void assemble( std::filesystem::path asmSrc, nga::SegmentStore & segmentStore );

}
