#pragma once

#include "ICompilationContext.hpp"
#include "CpuType.hpp"

namespace nga
{
class Reductor;
}

namespace assembler
{

std::shared_ptr<nga::Reductor> getReductor( nga::ICompilationContext* ctx, nga::CpuType cpuType );

}

