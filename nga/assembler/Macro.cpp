#include "pch.hpp"
#include "Macro.hpp"
#include "Ex.hpp"
#include "Project.hpp"

namespace assembler
{

using namespace std::string_view_literals;

Macro::Macro( nga::Pos pos, std::string name, size_t namedParams ) : mPos{ pos }, mName { std::move( name ) }, mParams{}
{
  mParams.reserve( namedParams );
}

void Macro::addParamName( std::string param )
{
  mParams.push_back( std::move( param ) );
}

bool Macro::addLine( std::pair<nga::Pos, std::string_view> pair )
{
  if ( check( pair.second, ".endm"sv ) )
    return true;
  if ( check( pair.second, ".macro"sv ) )
    throw Ex{} << "Macro definitions cant' be nested";

  auto comment = pair.second.find_first_of( ';' );
  if ( comment == std::string_view::npos )
    mLines.push_back( pair );
  else
  {
    auto line = pair.second.substr( 0, comment );
    mLines.push_back( { pair.first, line } );
  }
  return false;
}

nga::Pos Macro::pos() const
{
  return mPos;
}

std::shared_ptr<nga::IInputTextSource> Macro::invocate( std::vector<std::string> params, size_t number, std::span<char> tempBuffer )
{
  return std::make_shared<Incovation>( *this, std::move( params ), number, tempBuffer );
}

Macro::~Macro()
{
}

bool Macro::check( std::string_view line, std::string_view pattern )
{
  auto it = line.cbegin();

  while ( it != line.cend() && std::isspace( *it ) )
  {
    ++it;
  }

  if ( it == line.cend() || *it != '.' )
    return false;
 
  if ( (size_t)std::distance( it, line.cend() ) < pattern.size() )
    return false;

  auto pair = std::mismatch( pattern.cbegin(), pattern.cend(), it, []( char left, char right )
  {
    return std::tolower( left ) == right;
  } );

  if ( pair.first != pattern.cend() )
    return false;

  return pair.second == line.cend() || std::isspace( *pair.second );
}

std::optional<size_t> Macro::findParam( std::string_view name )
{
  for ( size_t i = 0; i < mParams.size(); ++i )
  {
    if ( name == mParams[i] )
      return i;
  }

  return {};
}


Macro::Incovation::Incovation( Macro& macro, std::vector<std::string> params, size_t number, std::span<char> tempBuffer ) : mMacro{ macro }, mParams{ std::move( params ) }, mTempBuffer{ tempBuffer }, mNumber{ number }, mDstIdx{}, mCurrentLine{}
{
}

std::pair<nga::Pos, std::string_view> Macro::Incovation::nextLine()
{
  if ( mCurrentLine >= mMacro.mLines.size() )
    return {};

  auto pair = mMacro.mLines[mCurrentLine++];
  auto line = pair.second;

  mDstIdx = mTempBuffer.size();

  for ( auto it = line.rbegin(); it != line.rend(); ++it )
  {
    process( *it );
  }

  return { pair.first, newLine() };
}

std::pair<nga::Pos, std::string_view> Macro::Incovation::currentLine() const
{
  if ( mCurrentLine > mMacro.mLines.size() || mCurrentLine == 0 )
    return {};
  else
    return mMacro.mLines[mCurrentLine - 1];
}

nga::Pos Macro::Incovation::currentPos() const
{
  if ( mCurrentLine > mMacro.mLines.size() || mCurrentLine == 0 )
    return {};
  else
    return mMacro.mLines[mCurrentLine-1].first;
}

std::filesystem::path const& Macro::Incovation::path() const
{
  return nga::project().posPath( mMacro.pos() );
}

void Macro::Incovation::process( char c )
{
  if ( c != ':' )
  {
    if ( mDstIdx > 0 )
      mTempBuffer[--mDstIdx] = c;
    else
      raportOverflow();
  }
  else
  {
    std::string_view view{ mTempBuffer.data() + mDstIdx, mTempBuffer.size() - mDstIdx };
    int token = idenityToken( view );
    if ( token == std::numeric_limits<int>::max() )
      throw Ex{} << "Undefined macro parameter :" << view;
    std::span<char> dest{ mTempBuffer.data(), mDstIdx + view.size() };
    if ( token == -1 )
      mDstIdx -= expandNumber( mNumber, dest ) - 1;
    else if ( token == 0 )
      mDstIdx -= expandNumber( mParams.size(), dest ) - 1;
    else if ( token - 1 < mParams.size() )
      mDstIdx -= expandText( mParams[token-1], dest ) - 1;
    else
      throw Ex{} << "Undefined macro parameter :" << view;
  }
}

std::string_view Macro::Incovation::newLine()
{
  mMacro.mExpandedLines.emplace_back( mTempBuffer.data() + mDstIdx, mTempBuffer.size() - mDstIdx );

  return { mMacro.mExpandedLines.back().data(), mMacro.mExpandedLines.back().size() };
}

int Macro::Incovation::idenityToken( std::string_view & text )
{
  if ( text[0] == '@' )
  {
    text = { text.data(), 1 };
    return -1;
  }

  int value;
  auto [ptr, ec] = std::from_chars( text.data(), text.data() + text.size(), value );

  if ( ec == std::errc{} )
  {
    text = { text.data(), (size_t)( ptr - text.data() ) };
    return value;
  }

  size_t i = 0;
  for ( ; i < text.size(); ++i )
  {
    if ( !std::isalnum( text[i] ) )
      break;
  }

  std::string_view name{ text.data(), i };
  if ( name.empty() )
  {
    text = { text.data(), 1 };
    return ~0;
  }

  text = name;

  if ( auto opt = mMacro.findParam( name ) )
  {
    return (int)*opt + 1;
  }

  return std::numeric_limits<int>::max();
}

size_t Macro::Incovation::expandText( std::string_view src, std::span<char> dest )
{
  if ( src.size() > dest.size() )
    raportOverflow();

  std::copy( src.cbegin(), src.cend(), dest.data() + dest.size() - src.size() );

  return src.size();
}

size_t Macro::Incovation::expandNumber( size_t value, std::span<char> dest )
{
  std::array<char, 64> buf;
  auto [it,size] = std::format_to_n( buf.data(), buf.size(), "{}", value );
  return expandText( { buf.data(), (size_t)size }, dest );
}

void Macro::Incovation::raportOverflow()
{
  throw Ex{} << "Macro expansion exceeds " << mTempBuffer.size() << " characters. Specify desired size in Config.MaxMacroExpansion variable";
}

}
