#include "pch.hpp"
#include "assembler/CompilationContext.hpp"
#include "assembler/CpuM68K.hpp"
#include "m68k.grm.hpp"

namespace assembler
{

// <DS> ::= .ds <SizeM68K> <Expression>
std::shared_ptr<void> reduceDS( nga::ICompilationContext* ctx, Tu002eds t0, std::shared_ptr<SizeM68K> n1, std::shared_ptr<Expression> n2 )
{
  return ctx->create<DS>( std::move( n2 ), (uint16_t)n1->size );
}

// <Opcode> ::= <MnemonicM68K> <AddressingM68K>
std::shared_ptr<void> reduceOpcode( nga::ICompilationContext* ctx, std::shared_ptr<MnemonicM68K> n0, std::shared_ptr<AddressingM68K> n1 )
{
  return ctx->create<Opcode>( std::move( n0 ), std::move( n1 ) );
}

// <SizeM68K> ::= .b
std::shared_ptr<void> reduceSizeM68K( nga::ICompilationContext* ctx, Tu002ebu0020 t0 )
{
  return ctx->create<SizeM68K>( SizeM68K::SIZE_BYTE );
}

// <SizeM68K> ::= .w
std::shared_ptr<void> reduceSizeM68K( nga::ICompilationContext* ctx, Tu002ewu0020 t0 )
{
  return ctx->create<SizeM68K>( SizeM68K::SIZE_WORD );
}

// <SizeM68K> ::= .l
std::shared_ptr<void> reduceSizeM68K( nga::ICompilationContext* ctx, Tu002elu0020 t0 )
{
  return ctx->create<SizeM68K>( SizeM68K::SIZE_LONG );
}

// <SizeM68K> ::= <>
std::shared_ptr<void> reduceSizeM68K( nga::ICompilationContext* ctx )
{
  return ctx->create<SizeM68K>( SizeM68K::SIZE_NONE );
}

// <SizeM68KBranch> ::= .b
std::shared_ptr<void> reduceSizeM68KBranch( nga::ICompilationContext* ctx, Tu002ebu0020 t0 )
{
  return ctx->create<SizeM68K>( SizeM68K::SIZE_BYTE );
}

// <SizeM68KBranch> ::= .s 
std::shared_ptr<void> reduceSizeM68KBranch( nga::ICompilationContext* ctx, Tu002esu0020 t0 )
{
  return ctx->create<SizeM68K>( SizeM68K::SIZE_BYTE );
}

// <SizeM68KBranch> ::= .w
std::shared_ptr<void> reduceSizeM68KBranch( nga::ICompilationContext* ctx, Tu002ewu0020 t0 )
{
  return ctx->create<SizeM68K>( SizeM68K::SIZE_WORD );
}

// <SizeM68KBranch> ::= <>
std::shared_ptr<void> reduceSizeM68KBranch( nga::ICompilationContext* ctx )
{
  return ctx->create<SizeM68K>( SizeM68K::SIZE_NONE );
}

// <MnemonicM68K> ::= ABCD <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TABCD t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::ABCD, n1->size );
}

// <MnemonicM68K> ::= ADD <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TADD t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::ADD, n1->size );
}

// <MnemonicM68K> ::= ADDA <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TADDA t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::ADDA, n1->size );
}

// <MnemonicM68K> ::= ADDI <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TADDI t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::ADDI, n1->size );
}

// <MnemonicM68K> ::= ADDQ <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TADDQ t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::ADDQ, n1->size );
}

// <MnemonicM68K> ::= ADDX <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TADDX t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::ADDX, n1->size );
}

// <MnemonicM68K> ::= AND <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TAND t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::AND, n1->size );
}

// <MnemonicM68K> ::= ANDI <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TANDI t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::ANDI, n1->size );
}

// <MnemonicM68K> ::= ASL <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TASL t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::ASL, n1->size );
}

// <MnemonicM68K> ::= ASR <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TASR t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::ASR, n1->size );
}

// <MnemonicM68K> ::= BCC <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TBCC t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::BCC, n1->size );
}

// <MnemonicM68K> ::= BCS <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TBCS t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::BCS, n1->size );
}

// <MnemonicM68K> ::= BEQ <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TBEQ t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::BEQ, n1->size );
}

// <MnemonicM68K> ::= BGE <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TBGE t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::BGE, n1->size );
}

// <MnemonicM68K> ::= BGT <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TBGT t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::BGT, n1->size );
}

// <MnemonicM68K> ::= BHI <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TBHI t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::BHI, n1->size );
}

// <MnemonicM68K> ::= BLE <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TBLE t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::BLE, n1->size );
}

// <MnemonicM68K> ::= BLS <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TBLS t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::BLS, n1->size );
}

// <MnemonicM68K> ::= BLT <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TBLT t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::BLT, n1->size );
}

// <MnemonicM68K> ::= BMI <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TBMI t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::BMI, n1->size );
}

// <MnemonicM68K> ::= BNE <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TBNE t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::BNE, n1->size );
}

// <MnemonicM68K> ::= BPL <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TBPL t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::BPL, n1->size );
}

// <MnemonicM68K> ::= BRA <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TBRA t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::BRA, n1->size );
}

// <MnemonicM68K> ::= BSR <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TBSR t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::BSR, n1->size );
}

// <MnemonicM68K> ::= BVC <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TBVC t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::BVC, n1->size );
}

// <MnemonicM68K> ::= BVS <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TBVS t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::BVS, n1->size );
}

// <MnemonicM68K> ::= BCHG <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TBCHG t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::BCHG, n1->size );
}

// <MnemonicM68K> ::= BCLR <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TBCLR t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::BCLR, n1->size );
}

// <MnemonicM68K> ::= BSET <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TBSET t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::BSET, n1->size );
}

// <MnemonicM68K> ::= BTST <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TBTST t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::BTST, n1->size );
}

// <MnemonicM68K> ::= CHK <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TCHK t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::CHK, n1->size );
}

// <MnemonicM68K> ::= CLR <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TCLR t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::CLR, n1->size );
}

// <MnemonicM68K> ::= CMP <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TCMP t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::CMP, n1->size );
}

// <MnemonicM68K> ::= CMPA <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TCMPA t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::CMPA, n1->size );
}

// <MnemonicM68K> ::= CMPI <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TCMPI t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::CMPI, n1->size );
}

// <MnemonicM68K> ::= CMPM <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TCMPM t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::CMPM, n1->size );
}

// <MnemonicM68K> ::= DBCC <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TDBCC t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::DBCC, n1->size );
}

// <MnemonicM68K> ::= DBCS <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TDBCS t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::DBCS, n1->size );
}

// <MnemonicM68K> ::= DBEQ <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TDBEQ t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::DBEQ, n1->size );
}

// <MnemonicM68K> ::= DBF <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TDBF t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::DBF, n1->size );
}

// <MnemonicM68K> ::= DBRA <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TDBRA t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::DBF, n1->size );
}

// <MnemonicM68K> ::= DBGE <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TDBGE t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::DBGE, n1->size );
}

// <MnemonicM68K> ::= DBGT <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TDBGT t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::DBGT, n1->size );
}

// <MnemonicM68K> ::= DBHI <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TDBHI t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::DBHI, n1->size );
}

// <MnemonicM68K> ::= DBLE <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TDBLE t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::DBLE, n1->size );
}

// <MnemonicM68K> ::= DBLS <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TDBLS t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::DBLS, n1->size );
}

// <MnemonicM68K> ::= DBLT <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TDBLT t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::DBLT, n1->size );
}

// <MnemonicM68K> ::= DBMI <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TDBMI t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::DBMI, n1->size );
}

// <MnemonicM68K> ::= DBNE <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TDBNE t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::DBNE, n1->size );
}

// <MnemonicM68K> ::= DBPL <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TDBPL t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::DBPL, n1->size );
}

// <MnemonicM68K> ::= DBT <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TDBT t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::DBT, n1->size );
}

// <MnemonicM68K> ::= DBVC <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TDBVC t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::DBVC, n1->size );
}

// <MnemonicM68K> ::= DBVS <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TDBVS t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::DBVS, n1->size );
}

// <MnemonicM68K> ::= DIVS <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TDIVS t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::DIVS, n1->size );
}

// <MnemonicM68K> ::= DIVU <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TDIVU t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::DIVU, n1->size );
}

// <MnemonicM68K> ::= EOR <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TEOR t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::EOR, n1->size );
}

// <MnemonicM68K> ::= EORI <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TEORI t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::EORI, n1->size );
}

// <MnemonicM68K> ::= EXG <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TEXG t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::EXG, n1->size );
}

// <MnemonicM68K> ::= EXT <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TEXT t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::EXT, n1->size );
}

// <MnemonicM68K> ::= ILLEGAL
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TILLEGAL t0 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::ILLEGAL );
}

// <MnemonicM68K> ::= JMP
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TJMP t0 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::JMP );
}

// <MnemonicM68K> ::= JSR
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TJSR t0 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::JSR );
}

// <MnemonicM68K> ::= LEA <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TLEA t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::LEA, n1->size );
}

// <MnemonicM68K> ::= LINK <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TLINK t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::LINK, n1->size );
}

// <MnemonicM68K> ::= LSL <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TLSL t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::LSL, n1->size );
}

// <MnemonicM68K> ::= LSR <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TLSR t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::LSR, n1->size );
}

// <MnemonicM68K> ::= MOVE <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TMOVE t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::MOVE, n1->size );
}

// <MnemonicM68K> ::= MOVEA <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TMOVEA t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::MOVEA, n1->size );
}

// <MnemonicM68K> ::= MOVEM <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TMOVEM t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::MOVEM, n1->size );
}

// <MnemonicM68K> ::= MOVEP <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TMOVEP t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::MOVEP, n1->size );
}

// <MnemonicM68K> ::= MOVEQ <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TMOVEQ t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::MOVEQ, n1->size );
}

// <MnemonicM68K> ::= MULS <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TMULS t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::MULS, n1->size );
}

// <MnemonicM68K> ::= MULU <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TMULU t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::MULU, n1->size );
}

// <MnemonicM68K> ::= NBCD <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TNBCD t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::NBCD, n1->size );
}

// <MnemonicM68K> ::= NEG <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TNEG t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::NEG, n1->size );
}

// <MnemonicM68K> ::= NEGX <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TNEGX t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::NEGX, n1->size );
}

// <MnemonicM68K> ::= NOP
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TNOP t0 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::NOP );
}

// <MnemonicM68K> ::= NOT <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TNOT t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::NOT, n1->size );
}

// <MnemonicM68K> ::= OR <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TOR t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::OR, n1->size );
}

// <MnemonicM68K> ::= ORI <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TORI t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::ORI, n1->size );
}

// <MnemonicM68K> ::= PEA <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TPEA t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::PEA, n1->size );
}

// <MnemonicM68K> ::= RESET
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TRESET t0 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::RESET );
}

// <MnemonicM68K> ::= ROL <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TROL t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::ROL, n1->size );
}

// <MnemonicM68K> ::= ROR <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TROR t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::ROR, n1->size );
}

// <MnemonicM68K> ::= ROXL <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TROXL t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::ROXL, n1->size );
}

// <MnemonicM68K> ::= ROXR <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TROXR t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::ROXR, n1->size );
}

// <MnemonicM68K> ::= RTE
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TRTE t0 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::RTE );
}

// <MnemonicM68K> ::= RTR
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TRTR t0 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::RTR );
}

// <MnemonicM68K> ::= RTS
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TRTS t0 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::RTS );
}

// <MnemonicM68K> ::= SBCD <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TSBCD t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::SBCD, n1->size );
}

// <MnemonicM68K> ::= SCC <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TSCC t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::SCC, n1->size );
}

// <MnemonicM68K> ::= SCS <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TSCS t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::SCS, n1->size );
}

// <MnemonicM68K> ::= SEQ <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TSEQ t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::SEQ, n1->size );
}

// <MnemonicM68K> ::= SF <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TSF t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::SF, n1->size );
}

// <MnemonicM68K> ::= SGE <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TSGE t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::SGE, n1->size );
}

// <MnemonicM68K> ::= SGT <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TSGT t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::SGT, n1->size );
}

// <MnemonicM68K> ::= SHI <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TSHI t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::SHI, n1->size );
}

// <MnemonicM68K> ::= SLE <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TSLE t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::SLE, n1->size );
}

// <MnemonicM68K> ::= SLS <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TSLS t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::SLS, n1->size );
}

// <MnemonicM68K> ::= SLT <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TSLT t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::SLT, n1->size );
}

// <MnemonicM68K> ::= SMI <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TSMI t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::SMI, n1->size );
}

// <MnemonicM68K> ::= SNE <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TSNE t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::SNE, n1->size );
}

// <MnemonicM68K> ::= SPL <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TSPL t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::SPL, n1->size );
}

// <MnemonicM68K> ::= ST <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TST t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::ST, n1->size );
}

// <MnemonicM68K> ::= SVC <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TSVC t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::SVC, n1->size );
}

// <MnemonicM68K> ::= SVS <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TSVS t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::SVS, n1->size );
}

// <MnemonicM68K> ::= STOP
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TSTOP t0 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::STOP );
}

// <MnemonicM68K> ::= SUB <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TSUB t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::SUB, n1->size );
}

// <MnemonicM68K> ::= SUBA <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TSUBA t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::SUBA, n1->size );
}

// <MnemonicM68K> ::= SUBI <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TSUBI t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::SUBI, n1->size );
}

// <MnemonicM68K> ::= SUBQ <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TSUBQ t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::SUBQ, n1->size );
}

// <MnemonicM68K> ::= SUBX <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TSUBX t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::SUBX, n1->size );
}

// <MnemonicM68K> ::= SWAP <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TSWAP t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::SWAP, n1->size );
}

// <MnemonicM68K> ::= TAS <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TTAS t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::TAS, n1->size );
}

// <MnemonicM68K> ::= TRAP
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TTRAP t0 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::TRAP );
}

// <MnemonicM68K> ::= TRAPV
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TTRAPV t0 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::TRAPV );
}

// <MnemonicM68K> ::= TST <SizeM68K>
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TTST t0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::TST, n1->size );
}

// <MnemonicM68K> ::= UNLK
std::shared_ptr<void> reduceMnemonicM68K( nga::ICompilationContext* ctx, TUNLK t0 )
{
  return ctx->create<MnemonicM68K>( (int)cpuM68K::Op::UNLK );
}

// <M68KXn> ::= AReg
std::shared_ptr<void> reduceM68KXn( nga::ICompilationContext* ctx, TAReg t0 )
{
  return ctx->create<M68KXn>( M68KXn::RegType::ADDRESS, t0[1] - '0' );
}

// <M68KXn> ::= DReg
std::shared_ptr<void> reduceM68KXn( nga::ICompilationContext* ctx, TDReg t0 )
{
  return ctx->create<M68KXn>( M68KXn::RegType::DATA, t0[1] - '0' );
}

// <M68KXnSized> ::= <M68KXn> <SizeM68K>
std::shared_ptr<void> reduceM68KXnSized( nga::ICompilationContext* ctx, std::shared_ptr<M68KXn> n0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<M68KXnSized>( *n0, n1->size );
}

// <M68KRegList> ::= <M68KRegList> / <M68KRegRange>
std::shared_ptr<void> reduceM68KRegList( nga::ICompilationContext* ctx, std::shared_ptr<M68KRegList> n0, Tu002f t1, std::shared_ptr<M68KRegRange> n2 )
{
  n0->add( *n2 );
  return n0;
}

// <M68KRegList> ::= <M68KRegList> / <M68KXn>
std::shared_ptr<void> reduceM68KRegList( nga::ICompilationContext* ctx, std::shared_ptr<M68KRegList> n0, Tu002f t1, std::shared_ptr<M68KXn> n2 )
{
  n0->add( *n2 );
  return n0;
}

// <M68KRegList> ::= <M68KRegRange>
std::shared_ptr<void> reduceM68KRegList( nga::ICompilationContext* ctx, std::shared_ptr<M68KRegRange> n0 )
{
  return ctx->create<M68KRegList>( *n0 );
}

// <M68KRegList> ::= <M68KXn>
std::shared_ptr<void> reduceM68KRegList( nga::ICompilationContext* ctx, std::shared_ptr<M68KXn> n0 )
{
  return ctx->create<M68KRegList>( *n0 );
}

// <M68KRegRange> ::= <M68KXn> - <M68KXn>
std::shared_ptr<void> reduceM68KRegRange( nga::ICompilationContext* ctx, std::shared_ptr<M68KXn> n0, Tu002d t1, std::shared_ptr<M68KXn> n2 )
{
  return ctx->create<M68KRegRange>( *n0, *n2 );
}

// <ImpliedAddressM68K> ::= <>
std::shared_ptr<void> reduceImpliedAddressM68K( nga::ICompilationContext* ctx )
{
  return ctx->create<ImpliedAddressM68K>();
}

// <SingleAddressM68K> ::= <M68KRegList>
std::shared_ptr<void> reduceSingleAddressM68K( nga::ICompilationContext* ctx, std::shared_ptr<M68KRegList> n0 )
{
  return ctx->create<SingleAddressM68K>( *n0 );
}

// <SingleAddressM68K> ::= ( AReg )
std::shared_ptr<void> reduceSingleAddressM68K( nga::ICompilationContext* ctx, Tu0028 t0, TAReg t1, Tu0029 t2 )
{
  return ctx->create<SingleAddressM68K>( cpuM68K::ADR_IND, M68KRegList{ M68KXn{ M68KXn::RegType::ADDRESS, t1[1] - '0' } }  );
}

// <SingleAddressM68K> ::= ( sp )
std::shared_ptr<void> reduceSingleAddressM68K( nga::ICompilationContext* ctx, Tu0028 t0, Tsp t1, Tu0029 t2 )
{
  return ctx->create<SingleAddressM68K>( cpuM68K::ADR_IND, M68KRegList{ M68KXn{ M68KXn::RegType::ADDRESS, 7 } }  );
}

// <SingleAddressM68K> ::= ( AReg ) +
std::shared_ptr<void> reduceSingleAddressM68K( nga::ICompilationContext* ctx, Tu0028 t0, TAReg t1, Tu0029 t2, Tu002b t3 )
{
  return ctx->create<SingleAddressM68K>( cpuM68K::ADR_IND_POST, M68KRegList{ M68KXn{ M68KXn::RegType::ADDRESS, t1[1] - '0' } } );
}

// <SingleAddressM68K> ::= ( sp ) +
std::shared_ptr<void> reduceSingleAddressM68K( nga::ICompilationContext* ctx, Tu0028 t0, Tsp t1, Tu0029 t2, Tu002b t3 )
{
  return ctx->create<SingleAddressM68K>( cpuM68K::ADR_IND_POST, M68KRegList{ M68KXn{ M68KXn::RegType::ADDRESS, 7 } } );
}

// <SingleAddressM68K> ::= - ( AReg )
std::shared_ptr<void> reduceSingleAddressM68K( nga::ICompilationContext* ctx, Tu002d t0, Tu0028 t1, TAReg t2, Tu0029 t3 )
{
  return ctx->create<SingleAddressM68K>( cpuM68K::ADR_IND_PRE, M68KRegList{ M68KXn{ M68KXn::RegType::ADDRESS, t2[1] - '0' } } );
}

// <SingleAddressM68K> ::= - ( sp )
std::shared_ptr<void> reduceSingleAddressM68K( nga::ICompilationContext* ctx, Tu002d t0, Tu0028 t1, Tsp t2, Tu0029 t3 )
{
  return ctx->create<SingleAddressM68K>( cpuM68K::ADR_IND_PRE, M68KRegList{ M68KXn{ M68KXn::RegType::ADDRESS, 7 } } );
}

// <SingleAddressM68K> ::= <Expression> ( AReg )
std::shared_ptr<void> reduceSingleAddressM68K( nga::ICompilationContext* ctx, std::shared_ptr<Expression> n0, Tu0028 t1, TAReg t2, Tu0029 t3 )
{
  return ctx->create<SingleAddressM68K>( cpuM68K::ADR_IND_DISP, std::move( n0 ), M68KRegList{ M68KXn{ M68KXn::RegType::ADDRESS, t2[1] - '0' } } );
}

// <SingleAddressM68K> ::= <Expression> ( AReg , <M68KXnSized> )
std::shared_ptr<void> reduceSingleAddressM68K( nga::ICompilationContext* ctx, std::shared_ptr<Expression> n0, Tu0028 t1, TAReg t2, Tu002c t3, std::shared_ptr<M68KXnSized> n4, Tu0029 t5 )
{
  return ctx->create<SingleAddressM68K>( cpuM68K::ADR_IND_IDX, std::move( n0 ), M68KRegList{ M68KXn{ M68KXn::RegType::ADDRESS, t2[1] - '0' } }, *n4 );
}

// <SingleAddressM68K> ::= <Expression> ( pc )
std::shared_ptr<void> reduceSingleAddressM68K( nga::ICompilationContext* ctx, std::shared_ptr<Expression> n0, Tu0028 t1, Tpc t2, Tu0029 t3 )
{
  return ctx->create<SingleAddressM68K>( cpuM68K::ADR_PCIND_DISP, std::move( n0 ) );
}

// <SingleAddressM68K> ::= <Expression> ( pc , <M68KXnSized> )
std::shared_ptr<void> reduceSingleAddressM68K( nga::ICompilationContext* ctx, std::shared_ptr<Expression> n0, Tu0028 t1, Tpc t2, Tu002c t3, std::shared_ptr<M68KXnSized> n4, Tu0029 t5 )
{
  return ctx->create<SingleAddressM68K>( cpuM68K::ADR_PCIND_IDX, std::move( n0 ), *n4 );
}

// <SingleAddressM68K> ::= + ( AReg )
std::shared_ptr<void> reduceSingleAddressM68K( nga::ICompilationContext* ctx, Tu002b t0, Tu0028 t1, TAReg t2, Tu0029 t3 )
{
  return ctx->create<SingleAddressM68K>( cpuM68K::ADR_IND_DISP, std::make_shared<LitValue>(), M68KRegList{ M68KXn{ M68KXn::RegType::ADDRESS, t2[1] - '0' } } );
}

// <SingleAddressM68K> ::= ( AReg , <M68KXnSized> )
std::shared_ptr<void> reduceSingleAddressM68K( nga::ICompilationContext* ctx, Tu0028 t0, TAReg t1, Tu002c t2, std::shared_ptr<M68KXnSized> n3, Tu0029 t4 )
{
  return ctx->create<SingleAddressM68K>( cpuM68K::ADR_IND_IDX, std::make_shared<LitValue>(), M68KRegList{ M68KXn{ M68KXn::RegType::ADDRESS, t1[1] - '0' } }, *n3 );
}

// <SingleAddressM68K> ::= ( pc )
std::shared_ptr<void> reduceSingleAddressM68K( nga::ICompilationContext* ctx, Tu0028 t0, Tpc t1, Tu0029 t2 )
{
  return ctx->create<SingleAddressM68K>( cpuM68K::ADR_PCIND_DISP, std::make_shared<LitValue>() );
}

// <SingleAddressM68K> ::= ( pc , <M68KXnSized> )
std::shared_ptr<void> reduceSingleAddressM68K( nga::ICompilationContext* ctx, Tu0028 t0, Tpc t1, Tu002c t2, std::shared_ptr<M68KXnSized> n3, Tu0029 t4 )
{
  return ctx->create<SingleAddressM68K>( cpuM68K::ADR_PCIND_IDX, std::make_shared<LitValue>(), *n3 );
}

// <SingleAddressM68K> ::= <Expression> <SizeM68K>
std::shared_ptr<void> reduceSingleAddressM68K( nga::ICompilationContext* ctx, std::shared_ptr<Expression> n0, std::shared_ptr<SizeM68K> n1 )
{
  return ctx->create<SingleAddressM68K>( std::move( n0 ), *n1 );
}

// <SingleAddressM68K> ::= # <Expression>
std::shared_ptr<void> reduceSingleAddressM68K( nga::ICompilationContext* ctx, Tu0023 t0, std::shared_ptr<Expression> n1 )
{
  return ctx->create<SingleAddressM68K>( cpuM68K::ADR_IMM, std::move( n1 ) );
}

// <SingleAddressM68K> ::= ccr
std::shared_ptr<void> reduceSingleAddressM68K( nga::ICompilationContext* ctx, Tccr t0 )
{
  return ctx->create<SingleAddressM68K>( cpuM68K::ADR_CCR );
}

// <SingleAddressM68K> ::= sr
std::shared_ptr<void> reduceSingleAddressM68K( nga::ICompilationContext* ctx, Tsr t0 )
{
  return ctx->create<SingleAddressM68K>( cpuM68K::ADR_SR );
}

// <SingleAddressM68K> ::= usp
std::shared_ptr<void> reduceSingleAddressM68K( nga::ICompilationContext* ctx, Tusp t0 )
{
  return ctx->create<SingleAddressM68K>( cpuM68K::ADR_USP );
}

// <SingleAddressM68K> ::= sp
std::shared_ptr<void> reduceSingleAddressM68K( nga::ICompilationContext* ctx, Tsp t0 )
{
  return ctx->create<SingleAddressM68K>( M68KRegList{ M68KXn{ M68KXn::RegType::ADDRESS, 7 } } );
}

// <DoubleAddressM68K> ::= <SingleAddressM68K> , <SingleAddressM68K>
std::shared_ptr<void> reduceDoubleAddressM68K( nga::ICompilationContext* ctx, std::shared_ptr<SingleAddressM68K> n0, Tu002c t1, std::shared_ptr<SingleAddressM68K> n2 )
{
  return ctx->create<DoubleAddressM68K>( std::move( n0 ), std::move( n2 ) );
}

// <AddressingM68K> ::= <SingleAddressM68K>
std::shared_ptr<void> reduceAddressingM68K( nga::ICompilationContext* ctx, std::shared_ptr<SingleAddressM68K> n0 )
{
  return n0;
}

// <AddressingM68K> ::= <DoubleAddressM68K>
std::shared_ptr<void> reduceAddressingM68K( nga::ICompilationContext* ctx, std::shared_ptr<DoubleAddressM68K> n0 )
{
  return n0;
}

// <AddressingM68K> ::= <ImpliedAddressM68K>
std::shared_ptr<void> reduceAddressingM68K( nga::ICompilationContext* ctx, std::shared_ptr<ImpliedAddressM68K> n0 )
{
  return n0;
}


}
