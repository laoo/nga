"Case Sensitive"= False 
"Start Symbol"  = <Statement>

{IdHead}        = {Letter} + [_]
{IdTail}        = {IdHead} + {Digit}

Id              = {IdHead}{IdTail}*
Label           @= { Source = Virtual }
MacroInvLit     @= { Source = Virtual }
MacroInvParam   @= { Source = Virtual }

!-- Sign            = [+-]

{HexDigit}      = {Digit} + [abcdef]
{BinDigit}      = [01]

{StringChar}    = {Printable} - ["]

String          = '"' {StringChar}* '"'

BinLiteral      = '%'{BinDigit}+
HexLiteral      = '$'{HexDigit}+
DecLiteral      = {Digit}+
AnonLabel       = '@'
AnonLabelRefLit = '@' [+-] {Digit}*

Comment Line    = ';'

<Statement>     ::= <LabelDecl>
                | <LabelOpt> <Generator>
                | <LabelOpt> <MacroInv>
                | <Declarator>
                |

<LabelOpt>      ::= <LabelDecl>
                |

<LabelDecl>     ::= Label
                | AnonLabel

<Declarator>    ::= <Seg>
                | <Ends>
                | <Local>
                | <Endl>
                | <Proc>
                | <Endp>
                | <Zone>
                | <Endz>
                | <End>
                | <Equ>
                | <Cpu>
                | <Icl>
                | <MacroDef>

<Seg>           ::= '.seg' Id

<Ends>          ::= '.ends'

<Local>         ::= '.local' Id

<Endl>          ::= '.endl'

<Proc>          ::= '.proc' Id

<Endp>          ::= '.endp'

<Zone>          ::= '.zone'

<Endz>          ::= '.endz'

<End>           ::= '.end' Id

<Equ>           ::= Label '=' <Expression>

<Icl>           ::= '.icl' String

<MacroDef>      ::= '.macro' Id <ArgList>
                | '.macro' Id

<ArgList>       ::= <ArgList> ',' Id
                | Id

<Generator>     ::= <DataByte>
                | <DS>

<MacroInv>      ::= MacroInvLit <MacroInvParams>
                | MacroInvLit

<MacroInvParams> ::= <MacroInvParams> MacroInvParam
                | MacroInvParam

<DataByte>      ::= '.by' <ExprList>
                | '.by' String

<ExprList>      ::= <ExprList> ',' <Expression>
                | <Expression>

<DS>            ::= '.ds' <Expression>

<Expression>    ::= <ExprOr>

<ExprOr>       ::= <ExprOr> '|' <ExprAnd>
                | <ExprAnd>

<ExprAnd>       ::= <ExprAnd> '&' <ExprShift>
                | <ExprShift>

<ExprShift>     ::= <ExprShift> '<<' <ExprAdd>
                | <ExprShift> '>>' <ExprAdd>
                | <ExprAdd>

<ExprAdd>       ::= <ExprAdd> '+' <ExprMul>
                | <ExprAdd> '-' <ExprMul>
                | <ExprMul>

<ExprMul>       ::= <ExprMul> '*' <ExprUnary>
                | <ExprMul> '/' <ExprUnary>
                | <ExprMul> '%' <ExprUnary>
                | <ExprUnary>

<ExprUnary>     ::= '+' <ExprUnary>
                |  '-' <ExprUnary>
                | <ExprPrimary>

<ExprPrimary>   ::= '(' <Expression> ')'
                | <LitValue>
                | <NSName>
                | <AnonLabelRef>
                | <CurrentPC>
                | <NamedOperator>

<AnonLabelRef>  ::= AnonLabelRefLit

<CurrentPC>     ::= '*'

<NSName>        ::= <NSName> '.' Id
                | Id

<LitValue>      ::= DecLiteral
                | HexLiteral
                | BinLiteral

<NamedOperator> ::= '[' Id ']' <NSName>
                | '[' Id '(' Id ')' ']' <NSName>

!------------

<Cpu>           ::= '.6502'
                | '.65sc02'
                | '.65c02'
                | '.68000'
                | '.gpu'
                | '.dsp'
                | '.tom'
                | '.jerry'
