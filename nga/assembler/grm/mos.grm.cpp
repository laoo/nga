#include "pch.hpp"
#include "assembler/CompilationContext.hpp"
#include "assembler/CpuMOS.hpp"
#include "mos.grm.hpp"

namespace assembler
{

// <Opcode> ::= <MnemonicMOS> <AddressingMOS>
std::shared_ptr<void> reduceOpcode( nga::ICompilationContext* ctx, std::shared_ptr<MnemonicMOS> n0, std::shared_ptr<AddressingMOS> n1 )
{
  return ctx->create<Opcode>( std::move( n0 ), std::move( n1 ) );
}

// <MnemonicMOS> ::= ADC
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TADC t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::ADC );
}

// <MnemonicMOS> ::= AND
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TAND t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::AND );
}

// <MnemonicMOS> ::= ASL
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TASL t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::ASL );
}

// <MnemonicMOS> ::= BBR0
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TBBR0 t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::BBR0 );
}

// <MnemonicMOS> ::= BBR1
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TBBR1 t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::BBR1 );
}

// <MnemonicMOS> ::= BBR2
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TBBR2 t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::BBR2 );
}

// <MnemonicMOS> ::= BBR3
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TBBR3 t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::BBR3 );
}

// <MnemonicMOS> ::= BBR4
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TBBR4 t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::BBR4 );
}

// <MnemonicMOS> ::= BBR5
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TBBR5 t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::BBR5 );
}

// <MnemonicMOS> ::= BBR6
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TBBR6 t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::BBR6 );
}

// <MnemonicMOS> ::= BBR7
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TBBR7 t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::BBR7 );
}

// <MnemonicMOS> ::= BBS0
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TBBS0 t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::BBS0 );
}

// <MnemonicMOS> ::= BBS1
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TBBS1 t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::BBS1 );
}

// <MnemonicMOS> ::= BBS2
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TBBS2 t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::BBS2 );
}

// <MnemonicMOS> ::= BBS3
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TBBS3 t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::BBS3 );
}

// <MnemonicMOS> ::= BBS4
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TBBS4 t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::BBS4 );
}

// <MnemonicMOS> ::= BBS5
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TBBS5 t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::BBS5 );
}

// <MnemonicMOS> ::= BBS6
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TBBS6 t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::BBS6 );
}

// <MnemonicMOS> ::= BBS7
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TBBS7 t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::BBS7 );
}

// <MnemonicMOS> ::= BCC
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TBCC t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::BCC );
}

// <MnemonicMOS> ::= BCS
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TBCS t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::BCS );
}

// <MnemonicMOS> ::= BEQ
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TBEQ t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::BEQ );
}

// <MnemonicMOS> ::= BIT
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TBIT t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::BIT );
}

// <MnemonicMOS> ::= BMI
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TBMI t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::BMI );
}

// <MnemonicMOS> ::= BNE
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TBNE t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::BNE );
}

// <MnemonicMOS> ::= BPL
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TBPL t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::BPL );
}

// <MnemonicMOS> ::= BRA
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TBRA t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::BRA );
}

// <MnemonicMOS> ::= BRK
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TBRK t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::BRK );
}

// <MnemonicMOS> ::= BVC
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TBVC t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::BVC );
}

// <MnemonicMOS> ::= BVS
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TBVS t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::BVS );
}

// <MnemonicMOS> ::= CLC
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TCLC t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::CLC );
}

// <MnemonicMOS> ::= CLD
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TCLD t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::CLD );
}

// <MnemonicMOS> ::= CLI
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TCLI t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::CLI );
}

// <MnemonicMOS> ::= CLV
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TCLV t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::CLV );
}

// <MnemonicMOS> ::= CMP
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TCMP t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::CMP );
}

// <MnemonicMOS> ::= CPX
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TCPX t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::CPX );
}

// <MnemonicMOS> ::= CPY
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TCPY t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::CPY );
}

// <MnemonicMOS> ::= DEC
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TDEC t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::DEC );
}

// <MnemonicMOS> ::= DEX
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TDEX t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::DEX );
}

// <MnemonicMOS> ::= DEY
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TDEY t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::DEY );
}

// <MnemonicMOS> ::= EOR
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TEOR t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::EOR );
}

// <MnemonicMOS> ::= INC
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TINC t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::INC );
}

// <MnemonicMOS> ::= INX
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TINX t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::INX );
}

// <MnemonicMOS> ::= INY
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TINY t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::INY );
}

// <MnemonicMOS> ::= JMP
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TJMP t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::JMP );
}

// <MnemonicMOS> ::= JSR
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TJSR t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::JSR );
}

// <MnemonicMOS> ::= LDA
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TLDA t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::LDA );
}

// <MnemonicMOS> ::= LDX
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TLDX t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::LDX );
}

// <MnemonicMOS> ::= LDY
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TLDY t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::LDY );
}

// <MnemonicMOS> ::= LSR
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TLSR t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::LSR );
}

// <MnemonicMOS> ::= NOP
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TNOP t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::NOP );
}

// <MnemonicMOS> ::= ORA
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TORA t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::ORA );
}

// <MnemonicMOS> ::= PHA
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TPHA t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::PHA );
}

// <MnemonicMOS> ::= PHP
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TPHP t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::PHP );
}

// <MnemonicMOS> ::= PHX
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TPHX t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::PHX );
}

// <MnemonicMOS> ::= PHY
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TPHY t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::PHY );
}

// <MnemonicMOS> ::= PLA
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TPLA t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::PLA );
}

// <MnemonicMOS> ::= PLP
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TPLP t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::PLP );
}

// <MnemonicMOS> ::= PLX
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TPLX t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::PLX );
}

// <MnemonicMOS> ::= PLY
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TPLY t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::PLY );
}

// <MnemonicMOS> ::= RMB0
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TRMB0 t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::RMB0 );
}

// <MnemonicMOS> ::= RMB1
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TRMB1 t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::RMB1 );
}

// <MnemonicMOS> ::= RMB2
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TRMB2 t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::RMB2 );
}

// <MnemonicMOS> ::= RMB3
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TRMB3 t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::RMB3 );
}

// <MnemonicMOS> ::= RMB4
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TRMB4 t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::RMB4 );
}

// <MnemonicMOS> ::= RMB5
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TRMB5 t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::RMB5 );
}

// <MnemonicMOS> ::= RMB6
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TRMB6 t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::RMB6 );
}

// <MnemonicMOS> ::= RMB7
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TRMB7 t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::RMB7 );
}

// <MnemonicMOS> ::= ROL
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TROL t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::ROL );
}

// <MnemonicMOS> ::= ROR
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TROR t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::ROR );
}

// <MnemonicMOS> ::= RTI
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TRTI t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::RTI );
}

// <MnemonicMOS> ::= RTS
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TRTS t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::RTS );
}

// <MnemonicMOS> ::= SBC
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TSBC t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::SBC );
}

// <MnemonicMOS> ::= SEC
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TSEC t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::SEC );
}

// <MnemonicMOS> ::= SED
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TSED t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::SED );
}

// <MnemonicMOS> ::= SEI
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TSEI t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::SEI );
}

// <MnemonicMOS> ::= SMB0
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TSMB0 t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::SMB0 );
}

// <MnemonicMOS> ::= SMB1
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TSMB1 t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::SMB1 );
}

// <MnemonicMOS> ::= SMB2
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TSMB2 t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::SMB2 );
}

// <MnemonicMOS> ::= SMB3
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TSMB3 t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::SMB3 );
}

// <MnemonicMOS> ::= SMB4
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TSMB4 t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::SMB4 );
}

// <MnemonicMOS> ::= SMB5
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TSMB5 t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::SMB5 );
}

// <MnemonicMOS> ::= SMB6
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TSMB6 t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::SMB6 );
}

// <MnemonicMOS> ::= SMB7
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TSMB7 t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::SMB7 );
}

// <MnemonicMOS> ::= STA
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TSTA t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::STA );
}

// <MnemonicMOS> ::= STP
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TSTP t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::STP );
}

// <MnemonicMOS> ::= STX
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TSTX t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::STX );
}

// <MnemonicMOS> ::= STY
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TSTY t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::STY );
}

// <MnemonicMOS> ::= STZ
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TSTZ t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::STZ );
}

// <MnemonicMOS> ::= TAX
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TTAX t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::TAX );
}

// <MnemonicMOS> ::= TAY
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TTAY t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::TAY );
}

// <MnemonicMOS> ::= TRB
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TTRB t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::TRB );
}

// <MnemonicMOS> ::= TSB
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TTSB t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::TSB );
}

// <MnemonicMOS> ::= TSX
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TTSX t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::TSX );
}

// <MnemonicMOS> ::= TXA
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TTXA t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::TXA );
}

// <MnemonicMOS> ::= TXS
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TTXS t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::TXS );
}

// <MnemonicMOS> ::= TYA
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TTYA t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::TYA );
}

// <MnemonicMOS> ::= WAI
std::shared_ptr<void> reduceMnemonicMOS( nga::ICompilationContext* ctx, TWAI t0 )
{
  return ctx->create<MnemonicMOS>( (int)cpuMOS::Op::WAI );
}

// <AddressingMOS> ::= <Expression>
std::shared_ptr<void> reduceAddressingMOS( nga::ICompilationContext* ctx, std::shared_ptr<Expression> n0 )
{
  return ctx->create<MosAbsolute>( std::move( n0 ) );
}

// <AddressingMOS> ::= <Expression> , <Expression>
std::shared_ptr<void> reduceAddressingMOS( nga::ICompilationContext* ctx, std::shared_ptr<Expression> n0, Tu002c t1, std::shared_ptr<Expression> n2 )
{
  return ctx->create<MosAbsoluteRelative>( std::move( n0 ), std::move( n2 ) );
}

// <AddressingMOS> ::= ( <Expression> , X )
std::shared_ptr<void> reduceAddressingMOS( nga::ICompilationContext* ctx, Tu0028 t0, std::shared_ptr<Expression> n1, Tu002c t2, TX t3, Tu0029 t4 )
{
  return ctx->create<MosIndexedXIndirect>( std::move( n1 ) );
}

// <AddressingMOS> ::= <Expression> , X
std::shared_ptr<void> reduceAddressingMOS( nga::ICompilationContext* ctx, std::shared_ptr<Expression> n0, Tu002c t1, TX t2 )
{
  return ctx->create<MosIndexedX>( std::move( n0 ) );
}

// <AddressingMOS> ::= <Expression> , Y
std::shared_ptr<void> reduceAddressingMOS( nga::ICompilationContext* ctx, std::shared_ptr<Expression> n0, Tu002c t1, TY t2 )
{
  return ctx->create<MosIndexedY>( std::move( n0 ) );
}

// <AddressingMOS> ::= # <Expression>
std::shared_ptr<void> reduceAddressingMOS( nga::ICompilationContext* ctx, Tu0023 t0, std::shared_ptr<Expression> n1 )
{
  return ctx->create<MosImmediate>( std::move( n1 ), '\0' );
}

// <AddressingMOS> ::= # < <Expression>
std::shared_ptr<void> reduceAddressingMOS( nga::ICompilationContext* ctx, Tu0023 t0, Tu003c t1, std::shared_ptr<Expression> n2 )
{
  return ctx->create<MosImmediate>( std::move( n2 ), '<' );
}

// <AddressingMOS> ::= # > <Expression>
std::shared_ptr<void> reduceAddressingMOS( nga::ICompilationContext* ctx, Tu0023 t0, Tu003e t1, std::shared_ptr<Expression> n2 )
{
  return ctx->create<MosImmediate>( std::move( n2 ), '>' );
}

// <AddressingMOS> ::= <>
std::shared_ptr<void> reduceAddressingMOS( nga::ICompilationContext* ctx )
{
  return ctx->create<MosImplied>();
}


}
