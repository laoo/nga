#include "pch.hpp"
#include "assembler/CompilationContext.hpp"
#include "base.grm.hpp"

namespace assembler
{

// <Statement> ::= <LabelDecl>
std::shared_ptr<void> reduceStatement( nga::ICompilationContext* ctx, std::shared_ptr<LabelDecl> n0 )
{
  return ctx->process( std::move( n0 ) );
}

// <Statement> ::= <LabelOpt> <Generator>
std::shared_ptr<void> reduceStatement( nga::ICompilationContext* ctx, std::shared_ptr<LabelOpt> n0, std::shared_ptr<Generator> n1 )
{
  return ctx->process( std::move( n1 ) );
}

// <Statement> ::= <LabelOpt> <MacroInv>
std::shared_ptr<void> reduceStatement( nga::ICompilationContext* ctx, std::shared_ptr<LabelOpt> n0, std::shared_ptr<MacroInv> n1 )
{
  return ctx->process( std::move( n1 ) );
}

// <Statement> ::= <Declarator>
std::shared_ptr<void> reduceStatement( nga::ICompilationContext* ctx, std::shared_ptr<Declarator> n0 )
{
  return {};
}

// <Statement> ::= <>
std::shared_ptr<void> reduceStatement( nga::ICompilationContext* ctx )
{
  return {};
}

// <LabelOpt> ::= <LabelDecl>
std::shared_ptr<void> reduceLabelOpt( nga::ICompilationContext* ctx, std::shared_ptr<LabelDecl> n0 )
{
  return ctx->process( std::move( n0 ) );
}

// <LabelOpt> ::= <>
std::shared_ptr<void> reduceLabelOpt( nga::ICompilationContext* ctx )
{
  return {};
}

// <LabelDecl> ::= Label
std::shared_ptr<void> reduceLabelDecl( nga::ICompilationContext* ctx, TLabel t0 )
{
  return ctx->create<LabelDecl>( std::string{ t0 } );
}

// <LabelDecl> ::= AnonLabel
std::shared_ptr<void> reduceLabelDecl( nga::ICompilationContext* ctx, TAnonLabel t0 )
{
  return ctx->create<LabelDecl>();
}

// <Declarator> ::= <Seg>
std::shared_ptr<void> reduceDeclarator( nga::ICompilationContext* ctx, std::shared_ptr<Seg> n0 )
{
  return ctx->process( std::move( n0 ) );
}

// <Declarator> ::= <Ends>
std::shared_ptr<void> reduceDeclarator( nga::ICompilationContext* ctx, std::shared_ptr<Ends> n0 )
{
  return ctx->process( std::move( n0 ) );
}

// <Declarator> ::= <Local>
std::shared_ptr<void> reduceDeclarator( nga::ICompilationContext* ctx, std::shared_ptr<Local> n0 )
{
  return ctx->process( std::move( n0 ) );
}

// <Declarator> ::= <Endl>
std::shared_ptr<void> reduceDeclarator( nga::ICompilationContext* ctx, std::shared_ptr<Endl> n0 )
{
  return ctx->process( std::move( n0 ) );
}

// <Declarator> ::= <Proc>
std::shared_ptr<void> reduceDeclarator( nga::ICompilationContext* ctx, std::shared_ptr<Proc> n0 )
{
  return ctx->process( std::move( n0 ) );
}

// <Declarator> ::= <Endp>
std::shared_ptr<void> reduceDeclarator( nga::ICompilationContext* ctx, std::shared_ptr<Endp> n0 )
{
  return ctx->process( std::move( n0 ) );
}

// <Declarator> ::= <Zone>
std::shared_ptr<void> reduceDeclarator( nga::ICompilationContext* ctx, std::shared_ptr<Zone> n0 )
{
  return ctx->process( std::move( n0 ) );
}

// <Declarator> ::= <Endz>
std::shared_ptr<void> reduceDeclarator( nga::ICompilationContext* ctx, std::shared_ptr<Endz> n0 )
{
  return ctx->process( std::move( n0 ) );
}


// <Declarator> ::= <End>
std::shared_ptr<void> reduceDeclarator( nga::ICompilationContext* ctx, std::shared_ptr<End> n0 )
{
  return ctx->process( std::move( n0 ) );
}

// <Declarator> ::= <Equ>
std::shared_ptr<void> reduceDeclarator( nga::ICompilationContext* ctx, std::shared_ptr<Equ> n0 )
{
  return ctx->process( std::move( n0 ) );
}

// <Declarator> ::= <Cpu>
std::shared_ptr<void> reduceDeclarator( nga::ICompilationContext* ctx, std::shared_ptr<Cpu> n0 )
{
  return ctx->process( std::move( n0 ) );
}

// <Declarator> ::= <Icl>
std::shared_ptr<void> reduceDeclarator( nga::ICompilationContext* ctx, std::shared_ptr<Icl> n0 )
{
  return ctx->process( std::move( n0 ) );
}

// <Declarator> ::= <MacroDef>
std::shared_ptr<void> reduceDeclarator( nga::ICompilationContext* ctx, std::shared_ptr<MacroDef> n0 )
{
  return ctx->process( std::move( n0 ) );
}

// <Seg> ::= .seg Id
std::shared_ptr<void> reduceSeg( nga::ICompilationContext* ctx, Tu002eseg t0, TId t1 )
{
  return ctx->create<Seg>( std::string{ t1 } );
}

// <Ends> ::= .ends
std::shared_ptr<void> reduceEnds( nga::ICompilationContext* ctx, Tu002eends t0 )
{
  return {}; //only type is important
}

// <Local> ::= .local Id
std::shared_ptr<void> reduceLocal( nga::ICompilationContext* ctx, Tu002elocal t0, TId t1 )
{
  return ctx->create<Local>( std::string{ t1 } );
}

// <Endl> ::= .endl
std::shared_ptr<void> reduceEndl( nga::ICompilationContext* ctx, Tu002eendl t0 )
{
  return {}; //only type is important
}

// <Proc> ::= .proc Id
std::shared_ptr<void> reduceProc( nga::ICompilationContext* ctx, Tu002eproc t0, TId t1 )
{
  return ctx->create<Proc>( std::string{ t1 } );
}

// <Endp> ::= .endp
std::shared_ptr<void> reduceEndp( nga::ICompilationContext* ctx, Tu002eendp t0 )
{
  return {}; //only type is important
}

// <Zone> ::= .zone
std::shared_ptr<void> reduceZone( nga::ICompilationContext* ctx, Tu002ezone t0 )
{
  return {}; //only type is important
}

// <Endz> ::= .endz
std::shared_ptr<void> reduceEndz( nga::ICompilationContext* ctx, Tu002eendz t0 )
{
  return {}; //only type is important
}

// <End> ::= .end Id
std::shared_ptr<void> reduceEnd( nga::ICompilationContext* ctx, Tu002eend t0, TId t1 )
{
  return ctx->create<End>( std::string{ t1 } );
}

// <Equ> ::= Label = <Expression>
std::shared_ptr<void> reduceEqu( nga::ICompilationContext* ctx, TLabel t0, Tu003d t1, std::shared_ptr<Expression> n2 )
{
  return ctx->create<Equ>( std::string{ t0 }, std::move( n2 ) );
}

// <Cpu> ::= .6502
std::shared_ptr<void> reduceCpu( nga::ICompilationContext* ctx, Tu002e6502 t0 )
{
  return ctx->create<Cpu>( nga::CpuType::MOS6502 );
}

// <Cpu> ::= .65sc02
std::shared_ptr<void> reduceCpu( nga::ICompilationContext* ctx, Tu002e65sc02 t0 )
{
  return ctx->create<Cpu>( nga::CpuType::G65SC02 );
}

// <Cpu> ::= .65c02
std::shared_ptr<void> reduceCpu( nga::ICompilationContext* ctx, Tu002e65c02 t0 )
{
  return ctx->create<Cpu>( nga::CpuType::WDC65C02 );
}

// <Cpu> ::= .68000
std::shared_ptr<void> reduceCpu( nga::ICompilationContext* ctx, Tu002e68000 t0 )
{
  return ctx->create<Cpu>( nga::CpuType::M68000 );
}

// <Cpu> ::= .gpu
std::shared_ptr<void> reduceCpu( nga::ICompilationContext* ctx, Tu002egpu t0 )
{
  return ctx->create<Cpu>( nga::CpuType::TOM );
}

// <Cpu> ::= .dsp
std::shared_ptr<void> reduceCpu( nga::ICompilationContext* ctx, Tu002edsp t0 )
{
  return ctx->create<Cpu>( nga::CpuType::JERRY );
}

// <Cpu> ::= .tom
std::shared_ptr<void> reduceCpu( nga::ICompilationContext* ctx, Tu002etom t0 )
{
  return ctx->create<Cpu>( nga::CpuType::TOM );
}

// <Cpu> ::= .jerry
std::shared_ptr<void> reduceCpu( nga::ICompilationContext* ctx, Tu002ejerry t0 )
{
  return ctx->create<Cpu>( nga::CpuType::JERRY );
}


// <Icl> ::= .icl String
std::shared_ptr<void> reduceIcl( nga::ICompilationContext* ctx, Tu002eicl t0, TString t1 )
{
  return ctx->create<Icl>( std::string{ t1.substr( 1, t1.size() - 2 ) } );
}

// <MacroDef> ::= .macro Id <ArgList>
std::shared_ptr<void> reduceMacroDef( nga::ICompilationContext* ctx, Tu002emacro t0, TId t1, std::shared_ptr<ArgList> n2 )
{
  return ctx->create<MacroDef>( std::string{ t1 }, std::move( n2 ) );
}

// <MacroDef> ::= .macro Id
std::shared_ptr<void> reduceMacroDef( nga::ICompilationContext* ctx, Tu002emacro t0, TId t1 )
{
  return ctx->create<MacroDef>( std::string{ t1 } );
}

// <ArgList> ::= <ArgList> , Id
std::shared_ptr<void> reduceArgList( nga::ICompilationContext* ctx, std::shared_ptr<ArgList> n0, Tu002c t1, TId t2 )
{
  n0->push_back( std::string{ t2 } );
  return n0;
}

// <ArgList> ::= Id
std::shared_ptr<void> reduceArgList( nga::ICompilationContext* ctx, TId t0 )
{
  auto arglist = ctx->create<ArgList>();
  arglist->push_back( std::string{ t0 } );
  return arglist;
}

// <Generator> ::= <Opcode>
std::shared_ptr<void> reduceGenerator( nga::ICompilationContext* ctx, std::shared_ptr<Opcode> n0 )
{
  n0->visit( *ctx );
  return n0;
}

// <Generator> ::= <DataByte>
std::shared_ptr<void> reduceGenerator( nga::ICompilationContext* ctx, std::shared_ptr<DataByte> n0 )
{
  return n0;
}

// <Generator> ::= <DS>
std::shared_ptr<void> reduceGenerator( nga::ICompilationContext* ctx, std::shared_ptr<DS> n0 )
{
  return n0;
}

// <MacroInv> ::= MacroInvLit <MacroInvParams>
std::shared_ptr<void> reduceMacroInv( nga::ICompilationContext* ctx, TMacroInvLit t0, std::shared_ptr<MacroInvParams> n1 )
{
  return ctx->create<MacroInv>( std::string{ t0 }, std::move( n1->params ) );
}

// <MacroInv> ::= MacroInvLit
std::shared_ptr<void> reduceMacroInv( nga::ICompilationContext* ctx, TMacroInvLit t0 )
{
  return ctx->create<MacroInv>( std::string{ t0 } );
}

// <MacroInvParams> ::= <MacroInvParams> MacroInvParam
std::shared_ptr<void> reduceMacroInvParams( nga::ICompilationContext* ctx, std::shared_ptr<MacroInvParams> n0, TMacroInvParam t1 )
{
  n0->params.push_back( std::string{ t1 } );
  return n0;
}

// <MacroInvParams> ::= MacroInvParam
std::shared_ptr<void> reduceMacroInvParams( nga::ICompilationContext* ctx, TMacroInvParam t0 )
{
  auto params = ctx->create<MacroInvParams>();
  params->params.push_back( std::string{ t0 } );
  return params;
}


// <DataByte> ::= .by <ExprList>
std::shared_ptr<void> reduceDataByte( nga::ICompilationContext* ctx, Tu002eby t0, std::shared_ptr<ExprList> n1 )
{
  return ctx->create<DataByte>( std::move( n1 ) );
}

// <DataByte> ::= .by String
std::shared_ptr<void> reduceDataByte( nga::ICompilationContext* ctx, Tu002eby t0, TString t1 )
{
  return ctx->create<DataByte>( t1 );
}


// <ExprList> ::= <ExprList> , <Expression>
std::shared_ptr<void> reduceExprList( nga::ICompilationContext* ctx, std::shared_ptr<ExprList> n0, Tu002c t1, std::shared_ptr<Expression> n2 )
{
  n0->list.push_back( std::move( n2 ) );
  return n0;
}

// <ExprList> ::= <Expression>
std::shared_ptr<void> reduceExprList( nga::ICompilationContext* ctx, std::shared_ptr<Expression> n0 )
{
  auto exprList = ctx->create<ExprList>();
  exprList->list.push_back( std::move( n0 ) );
  return exprList;
}

// <DS> ::= .ds <Expression>
std::shared_ptr<void> reduceDS( nga::ICompilationContext* ctx, Tu002eds t0, std::shared_ptr<Expression> n1 )
{
  return ctx->create<DS>( std::move( n1 ) );
}

// <Expression> ::= <ExprOr>
std::shared_ptr<void> reduceExpression( nga::ICompilationContext* ctx, std::shared_ptr<ExprOr> n0 )
{
  return n0;
}

// <ExprOr> ::= <ExprOr> | <ExprAnd>
std::shared_ptr<void> reduceExprOr( nga::ICompilationContext* ctx, std::shared_ptr<ExprOr> n0, Tu007c t1, std::shared_ptr<ExprAnd> n2 )
{
  return ctx->create<ExprOr_>( std::move( n0 ), std::move( n2 ) );
}

// <ExprOr> ::= <ExprAnd>
std::shared_ptr<void> reduceExprOr( nga::ICompilationContext* ctx, std::shared_ptr<ExprAnd> n0 )
{
  return n0;
}

// <ExprAnd> ::= <ExprAnd> & <ExprShift>
std::shared_ptr<void> reduceExprAnd( nga::ICompilationContext* ctx, std::shared_ptr<ExprAnd> n0, Tu0026 t1, std::shared_ptr<ExprShift> n2 )
{
  return ctx->create<ExprAnd_>( std::move( n0 ), std::move( n2 ) );
}

// <ExprAnd> ::= <ExprShift>
std::shared_ptr<void> reduceExprAnd( nga::ICompilationContext* ctx, std::shared_ptr<ExprShift> n0 )
{
  return n0;
}

// <ExprShift> ::= <ExprShift> << <ExprAdd>
std::shared_ptr<void> reduceExprShift( nga::ICompilationContext* ctx, std::shared_ptr<ExprShift> n0, Tu003cu003c t1, std::shared_ptr<ExprAdd> n2 )
{
  return ctx->create<ExprShift_>( std::move( n0 ), 'L', std::move( n2 ) );
}

// <ExprShift> ::= <ExprShift> >> <ExprAdd>
std::shared_ptr<void> reduceExprShift( nga::ICompilationContext* ctx, std::shared_ptr<ExprShift> n0, Tu003eu003e t1, std::shared_ptr<ExprAdd> n2 )
{
  return ctx->create<ExprShift_>( std::move( n0 ), 'R', std::move( n2 ) );
}

// <ExprShift> ::= <ExprAdd>
std::shared_ptr<void> reduceExprShift( nga::ICompilationContext* ctx, std::shared_ptr<ExprAdd> n0 )
{
  return n0;
}

// <ExprAdd> ::= <ExprAdd> + <ExprMul>
std::shared_ptr<void> reduceExprAdd( nga::ICompilationContext* ctx, std::shared_ptr<ExprAdd> n0, Tu002b t1, std::shared_ptr<ExprMul> n2 )
{
  return ctx->create<ExprAdd_>( std::move( n0 ), '+', std::move( n2 ) );
}

// <ExprAdd> ::= <ExprAdd> - <ExprMul>
std::shared_ptr<void> reduceExprAdd( nga::ICompilationContext* ctx, std::shared_ptr<ExprAdd> n0, Tu002d t1, std::shared_ptr<ExprMul> n2 )
{
  return ctx->create<ExprAdd_>( std::move( n0 ), '-', std::move( n2 ) );
}

// <ExprAdd> ::= <ExprMul>
std::shared_ptr<void> reduceExprAdd( nga::ICompilationContext* ctx, std::shared_ptr<ExprMul> n0 )
{
  return n0;
}

// <ExprMul> ::= <ExprMul> * <ExprUnary>
std::shared_ptr<void> reduceExprMul( nga::ICompilationContext* ctx, std::shared_ptr<ExprMul> n0, Tu002a t1, std::shared_ptr<ExprUnary> n2 )
{
  return ctx->create<ExprMul_>( std::move( n0 ), '*', std::move( n2 ) );
}

// <ExprMul> ::= <ExprMul> / <ExprUnary>
std::shared_ptr<void> reduceExprMul( nga::ICompilationContext* ctx, std::shared_ptr<ExprMul> n0, Tu002f t1, std::shared_ptr<ExprUnary> n2 )
{
  return ctx->create<ExprMul_>( std::move( n0 ), '/', std::move( n2 ) );
}

// <ExprMul> ::= <ExprMul> % <ExprUnary>
std::shared_ptr<void> reduceExprMul( nga::ICompilationContext* ctx, std::shared_ptr<ExprMul> n0, Tu0025 t1, std::shared_ptr<ExprUnary> n2 )
{
  return ctx->create<ExprMul_>( std::move( n0 ), '%', std::move( n2 ) );
}

// <ExprMul> ::= <ExprUnary>
std::shared_ptr<void> reduceExprMul( nga::ICompilationContext* ctx, std::shared_ptr<ExprUnary> n0 )
{
  return n0;
}

// <ExprUnary> ::= + <ExprUnary>
std::shared_ptr<void> reduceExprUnary( nga::ICompilationContext* ctx, Tu002b t0, std::shared_ptr<ExprUnary> n1 )
{
  return n1;
}

// <ExprUnary> ::= - <ExprUnary>
std::shared_ptr<void> reduceExprUnary( nga::ICompilationContext* ctx, Tu002d t0, std::shared_ptr<ExprUnary> n1 )
{
  return ctx->create<ExprUnary_>( std::move( n1 ) );
}

// <ExprUnary> ::= <ExprPrimary>
std::shared_ptr<void> reduceExprUnary( nga::ICompilationContext* ctx, std::shared_ptr<ExprPrimary> n0 )
{
  return n0;
}

// <ExprPrimary> ::= ( <Expression> )
std::shared_ptr<void> reduceExprPrimary( nga::ICompilationContext* ctx, Tu0028 t0, std::shared_ptr<Expression> n1, Tu0029 t2 )
{
  n1->parenthized = true;
  return n1;
}

// <ExprPrimary> ::= <LitValue>
std::shared_ptr<void> reduceExprPrimary( nga::ICompilationContext* ctx, std::shared_ptr<LitValue> n0 )
{
  return n0;
}

// <ExprPrimary> ::= <NSName>
std::shared_ptr<void> reduceExprPrimary( nga::ICompilationContext* ctx, std::shared_ptr<NSName> n0 )
{
  return n0;
}

// <ExprPrimary> ::= <AnonLabelRef>
std::shared_ptr<void> reduceExprPrimary( nga::ICompilationContext* ctx, std::shared_ptr<AnonLabelRef> n0 )
{
  return n0;
}

// <ExprPrimary> ::= <CurrentPC>
std::shared_ptr<void> reduceExprPrimary( nga::ICompilationContext* ctx, std::shared_ptr<CurrentPC> n0 )
{
  return n0;
}

// <ExprPrimary> ::= <NamedOperator>
std::shared_ptr<void> reduceExprPrimary( nga::ICompilationContext* ctx, std::shared_ptr<NamedOperator> n0 )
{
  return n0;
}

// <AnonLabelRef> ::= AnonLabelRefLit
std::shared_ptr<void> reduceAnonLabelRef( nga::ICompilationContext* ctx, TAnonLabelRefLit t0 )
{
  assert( t0[0] == '@' );
  assert( t0[1] == '+' || t0[1] == '-' );

  int sign = t0[1] == '+' ? 1 : -1;

  if ( t0.size() > 2 )
  {
    int number = 0;
    for ( size_t i = 2; i < t0.size(); ++i )
    {
      assert( isdigit( t0[i] ) );
      number = number * 10 + t0[i] - '0';
    }
    return ctx->create<AnonLabelRef>( ctx->context(), sign * ( number + 1 ) );
  }
  else
  {
    return ctx->create<AnonLabelRef>( ctx->context(), sign );
  }
}

// <CurrentPC> ::= *
std::shared_ptr<void> reduceCurrentPC( nga::ICompilationContext* ctx, Tu002a t0 )
{
  return std::shared_ptr<CurrentPC>();  //empty pointer on purpose, as only the type is important (structure does not have and fields)
}

// <NSName> ::= <NSName> . Id
std::shared_ptr<void> reduceNSName( nga::ICompilationContext* ctx, std::shared_ptr<NSName> n0, Tu002e t1, TId t2 )
{
  n0->name.append( "." ).append( t2 );
  return n0;
}

// <NSName> ::= Id
std::shared_ptr<void> reduceNSName( nga::ICompilationContext* ctx, TId t0 )
{
  auto result = ctx->create<NSName>( ctx->context(), t0 );
  return result;
}

// <LitValue> ::= DecLiteral
std::shared_ptr<void> reduceLitValue( nga::ICompilationContext* ctx, TDecLiteral t0 )
{
  int number = 0;
  for ( char c : t0 )
  {
    if ( isdigit( c ) )
    {
      number = number * 10 + c - '0';
    }
  }

  auto litValue = ctx->create<LitValue>();
  litValue->v = number;
  return litValue;
}

// <LitValue> ::= HexLiteral
std::shared_ptr<void> reduceLitValue( nga::ICompilationContext* ctx, THexLiteral t0 )
{
  int number = 0;
  for ( auto it = t0.cbegin() + 1; it != t0.cend(); ++it )
  {
    char const c = *it;
    if ( isdigit( c ) )
    {
      number = number * 16 + c - '0';
    }
    else
    {
      number = number * 16 + c - ( isupper( c ) ? 'A' : 'a' ) + 10;
    }
  }

  auto litValue = ctx->create<LitValue>();
  litValue->v = number;
  return litValue;
}

// <LitValue> ::= BinLiteral
std::shared_ptr<void> reduceLitValue( nga::ICompilationContext* ctx, TBinLiteral t0 )
{
  int number = 0;
  for ( auto it = t0.cbegin() + 1; it != t0.cend(); ++it )
  {
    number = number * 2 + *it - '0';
  }

  auto litValue = ctx->create<LitValue>();
  litValue->v = number;
  return litValue;
}

// <NamedOperator> ::= [ Id ] <NSName>
std::shared_ptr<void> reduceNamedOperator( nga::ICompilationContext* ctx, Tu005b t0, TId t1, Tu005d t2, std::shared_ptr<NSName> n3 )
{
  return ctx->create<NamedOperator>( std::string{ t1 }, std::move( n3 ) );
}

// <NamedOperator> ::= [ Id ( Id ) ] <NSName>
std::shared_ptr<void> reduceNamedOperator( nga::ICompilationContext* ctx, Tu005b t0, TId t1, Tu0028 t2, TId t3, Tu0029 t4, Tu005d t5, std::shared_ptr<NSName> n6 )
{
  return ctx->create<NamedOperator>( std::string{ t1 }, std::string{ t3 }, std::move( n6 ) );
}

}
