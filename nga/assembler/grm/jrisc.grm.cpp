#include "pch.hpp"
#include "assembler/CompilationContext.hpp"
#include "assembler/CpuJRISC.hpp"
#include "jrisc.grm.hpp"

namespace assembler
{

// <Declarator> ::= <Equr>
std::shared_ptr<void> reduceDeclarator( nga::ICompilationContext* ctx, std::shared_ptr<Equr> n0 )
{
  return ctx->process( std::move( n0 ) );
}

// <Equr> ::= Label .equr Id
std::shared_ptr<void> reduceEqur( nga::ICompilationContext* ctx, TLabel t0, Tu002eequr t1, TId t2 )
{
  return ctx->create<Equr>( std::string{ t0 }, cpuJRISC::getReg( ctx, t2 ) );
}

// <Opcode> ::= <MnemonicJRISC> <AddressingJRISC>
std::shared_ptr<void> reduceOpcode( nga::ICompilationContext* ctx, std::shared_ptr<MnemonicJRISC> n0, std::shared_ptr<AddressingJRISC> n1 )
{
  return ctx->create<Opcode>( std::move( n0 ), std::move( n1 ) );
}

// <MnemonicJRISC> ::= ABS
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TABS t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::ABS );
}

// <MnemonicJRISC> ::= ADD
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TADD t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::ADD );
}

// <MnemonicJRISC> ::= ADDC
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TADDC t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::ADDC );
}

// <MnemonicJRISC> ::= ADDQ
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TADDQ t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::ADDQ );
}

// <MnemonicJRISC> ::= ADDQMOD
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TADDQMOD t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::ADDQMOD );
}

// <MnemonicJRISC> ::= ADDQT
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TADDQT t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::ADDQT );
}

// <MnemonicJRISC> ::= AND
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TAND t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::AND );
}

// <MnemonicJRISC> ::= BCC
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TBCC t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::JR, CCJRISC::NC );
}

// <MnemonicJRISC> ::= BCLR
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TBCLR t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::BCLR );
}

// <MnemonicJRISC> ::= BCS
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TBCS t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::JR, CCJRISC::C );
}

// <MnemonicJRISC> ::= BEQ
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TBEQ t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::JR, CCJRISC::Z );
}

// <MnemonicJRISC> ::= BMI
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TBMI t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::JR, CCJRISC::N );
}

// <MnemonicJRISC> ::= BNE
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TBNE t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::JR, CCJRISC::NZ );
}

// <MnemonicJRISC> ::= BRA
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TBRA t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::JR );
}

// <MnemonicJRISC> ::= BPL
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TBPL t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::JR, CCJRISC::NN );
}

// <MnemonicJRISC> ::= BSET
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TBSET t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::BSET );
}

// <MnemonicJRISC> ::= BTST
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TBTST t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::BTST );
}

// <MnemonicJRISC> ::= CMP
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TCMP t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::CMP );
}

// <MnemonicJRISC> ::= CMPQ
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TCMPQ t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::CMPQ );
}

// <MnemonicJRISC> ::= DIV
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TDIV t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::DIV );
}

// <MnemonicJRISC> ::= IMACN
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TIMACN t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::IMACN );
}

// <MnemonicJRISC> ::= IMULT
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TIMULT t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::IMULT );
}

// <MnemonicJRISC> ::= IMULTN
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TIMULTN t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::IMULTN );
}

// <MnemonicJRISC> ::= JCC
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TJCC t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::JUMP, CCJRISC::NC );
}

// <MnemonicJRISC> ::= JCS
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TJCS t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::JUMP, CCJRISC::C );
}

// <MnemonicJRISC> ::= JEQ
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TJEQ t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::JUMP, CCJRISC::Z );
}

// <MnemonicJRISC> ::= JMI
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TJMI t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::JUMP, CCJRISC::N );
}

// <MnemonicJRISC> ::= JNE
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TJNE t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::JUMP, CCJRISC::NZ );
}

// <MnemonicJRISC> ::= JPL
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TJPL t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::JUMP, CCJRISC::NN );
}

// <MnemonicJRISC> ::= JR
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TJR t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::JR );
}

// <MnemonicJRISC> ::= JUMP
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TJUMP t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::JUMP );
}

// <MnemonicJRISC> ::= LOAD
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TLOAD t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::LOAD );
}

// <MnemonicJRISC> ::= LOADB
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TLOADB t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::LOADB );
}

// <MnemonicJRISC> ::= LOADW
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TLOADW t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::LOADW );
}

// <MnemonicJRISC> ::= LOADP
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TLOADP t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::LOADP );
}

// <MnemonicJRISC> ::= MIRROR
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TMIRROR t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::MIRROR );
}

// <MnemonicJRISC> ::= MMULT
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TMMULT t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::MMULT );
}

// <MnemonicJRISC> ::= MOVE
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TMOVE t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::MOVE );
}

// <MnemonicJRISC> ::= MOVEFA
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TMOVEFA t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::MOVEFA );
}

// <MnemonicJRISC> ::= MOVEI
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TMOVEI t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::MOVEI );
}

// <MnemonicJRISC> ::= MOVEQ
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TMOVEQ t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::MOVEQ );
}

// <MnemonicJRISC> ::= MOVETA
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TMOVETA t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::MOVETA );
}

// <MnemonicJRISC> ::= MTOI
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TMTOI t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::MTOI );
}

// <MnemonicJRISC> ::= MULT
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TMULT t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::MULT );
}

// <MnemonicJRISC> ::= NEG
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TNEG t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::NEG );
}

// <MnemonicJRISC> ::= NOP
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TNOP t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::NOP );
}

// <MnemonicJRISC> ::= NORMI
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TNORMI t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::NORMI );
}

// <MnemonicJRISC> ::= NOT
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TNOT t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::NOT );
}

// <MnemonicJRISC> ::= OR
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TOR t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::OR );
}

// <MnemonicJRISC> ::= PACK
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TPACK t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::PACK );
}

// <MnemonicJRISC> ::= RESMAC
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TRESMAC t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::RESMAC );
}

// <MnemonicJRISC> ::= ROLQ
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TROLQ t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::ROLQ );
}

// <MnemonicJRISC> ::= ROR
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TROR t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::ROR );
}

// <MnemonicJRISC> ::= RORQ
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TRORQ t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::RORQ );
}

// <MnemonicJRISC> ::= SAT8
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TSAT8 t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::SAT8 );
}

// <MnemonicJRISC> ::= SAT16
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TSAT16 t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::SAT16 );
}

// <MnemonicJRISC> ::= SAT16S
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TSAT16S t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::SAT16S );
}

// <MnemonicJRISC> ::= SAT24
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TSAT24 t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::SAT24 );
}

// <MnemonicJRISC> ::= SAT32S
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TSAT32S t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::SAT32S );
}

// <MnemonicJRISC> ::= SH
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TSH t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::SH );
}

// <MnemonicJRISC> ::= SHA
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TSHA t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::SHA );
}

// <MnemonicJRISC> ::= SHARQ
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TSHARQ t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::SHARQ );
}

// <MnemonicJRISC> ::= SHLQ
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TSHLQ t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::SHLQ );
}

// <MnemonicJRISC> ::= SHRQ
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TSHRQ t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::SHRQ );
}

// <MnemonicJRISC> ::= STORE
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TSTORE t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::STORE );
}

// <MnemonicJRISC> ::= STOREB
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TSTOREB t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::STOREB );
}

// <MnemonicJRISC> ::= STOREW
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TSTOREW t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::STOREW );
}

// <MnemonicJRISC> ::= STOREP
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TSTOREP t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::STOREP );
}

// <MnemonicJRISC> ::= SUB
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TSUB t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::SUB );
}

// <MnemonicJRISC> ::= SUBC
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TSUBC t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::SUBC );
}

// <MnemonicJRISC> ::= SUBQ
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TSUBQ t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::SUBQ );
}

// <MnemonicJRISC> ::= SUBQMOD
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TSUBQMOD t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::SUBQMOD );
}

// <MnemonicJRISC> ::= SUBQT
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TSUBQT t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::SUBQT );
}

// <MnemonicJRISC> ::= UNPACK
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TUNPACK t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::UNPACK );
}

// <MnemonicJRISC> ::= XOR
std::shared_ptr<void> reduceMnemonicJRISC( nga::ICompilationContext* ctx, TXOR t0 )
{
  return ctx->create<MnemonicJRISC>( cpuJRISC::Op::XOR );
}

// <CCJRISC> ::= NZ
std::shared_ptr<void> reduceCCJRISC( nga::ICompilationContext* ctx, TNZ t0 )
{
  return ctx->create<CCJRISC>( CCJRISC::NZ );
}

// <CCJRISC> ::= Z
std::shared_ptr<void> reduceCCJRISC( nga::ICompilationContext* ctx, TZ t0 )
{
  return ctx->create<CCJRISC>( CCJRISC::Z );
}

// <CCJRISC> ::= NC
std::shared_ptr<void> reduceCCJRISC( nga::ICompilationContext* ctx, TNC t0 )
{
  return ctx->create<CCJRISC>( CCJRISC::NC );
}

// <CCJRISC> ::= NC NZ
std::shared_ptr<void> reduceCCJRISC( nga::ICompilationContext* ctx, TNC t0, TNZ t1 )
{
  return ctx->create<CCJRISC>( CCJRISC::NC_NZ );
}

// <CCJRISC> ::= NC Z
std::shared_ptr<void> reduceCCJRISC( nga::ICompilationContext* ctx, TNC t0, TZ t1 )
{
  return ctx->create<CCJRISC>( CCJRISC::NC_Z );
}

// <CCJRISC> ::= C
std::shared_ptr<void> reduceCCJRISC( nga::ICompilationContext* ctx, TC t0 )
{
  return ctx->create<CCJRISC>( CCJRISC::C );
}

// <CCJRISC> ::= C NZ
std::shared_ptr<void> reduceCCJRISC( nga::ICompilationContext* ctx, TC t0, TNZ t1 )
{
  return ctx->create<CCJRISC>( CCJRISC::C_NZ );
}

// <CCJRISC> ::= C Z
std::shared_ptr<void> reduceCCJRISC( nga::ICompilationContext* ctx, TC t0, TZ t1 )
{
  return ctx->create<CCJRISC>( CCJRISC::C_Z );
}

// <CCJRISC> ::= NN
std::shared_ptr<void> reduceCCJRISC( nga::ICompilationContext* ctx, TNN t0 )
{
  return ctx->create<CCJRISC>( CCJRISC::NN );
}

// <CCJRISC> ::= NN NZ
std::shared_ptr<void> reduceCCJRISC( nga::ICompilationContext* ctx, TNN t0, TNZ t1 )
{
  return ctx->create<CCJRISC>( CCJRISC::NN_NZ );
}

// <CCJRISC> ::= NN Z
std::shared_ptr<void> reduceCCJRISC( nga::ICompilationContext* ctx, TNN t0, TZ t1 )
{
  return ctx->create<CCJRISC>( CCJRISC::NN_Z );
}

// <CCJRISC> ::= N
std::shared_ptr<void> reduceCCJRISC( nga::ICompilationContext* ctx, TN t0 )
{
  return ctx->create<CCJRISC>( CCJRISC::N );
}

// <CCJRISC> ::= N NZ
std::shared_ptr<void> reduceCCJRISC( nga::ICompilationContext* ctx, TN t0, TNZ t1 )
{
  return ctx->create<CCJRISC>( CCJRISC::N_NZ );
}

// <CCJRISC> ::= N Z
std::shared_ptr<void> reduceCCJRISC( nga::ICompilationContext* ctx, TN t0, TZ t1 )
{
  return ctx->create<CCJRISC>( CCJRISC::N_Z );
}

// <CCJRISC> ::= NE
std::shared_ptr<void> reduceCCJRISC( nga::ICompilationContext* ctx, TNE t0 )
{
  return ctx->create<CCJRISC>( CCJRISC::NZ );
}

// <CCJRISC> ::= EQ
std::shared_ptr<void> reduceCCJRISC( nga::ICompilationContext* ctx, TEQ t0 )
{
  return ctx->create<CCJRISC>( CCJRISC::Z );
}

// <CCJRISC> ::= CC
std::shared_ptr<void> reduceCCJRISC( nga::ICompilationContext* ctx, TCC t0 )
{
  return ctx->create<CCJRISC>( CCJRISC::NC );
}

// <CCJRISC> ::= CS
std::shared_ptr<void> reduceCCJRISC( nga::ICompilationContext* ctx, TCS t0 )
{
  return ctx->create<CCJRISC>( CCJRISC::C );
}

// <CCJRISC> ::= PL
std::shared_ptr<void> reduceCCJRISC( nga::ICompilationContext* ctx, TPL t0 )
{
  return ctx->create<CCJRISC>( CCJRISC::NN );
}

// <CCJRISC> ::= MI
std::shared_ptr<void> reduceCCJRISC( nga::ICompilationContext* ctx, TMI t0 )
{
  return ctx->create<CCJRISC>( CCJRISC::N );
}

// <AddressingJRISC> ::= <Expression>
std::shared_ptr<void> reduceAddressingJRISC( nga::ICompilationContext* ctx, std::shared_ptr<Expression> n0 )
{
  bool par = n0->parenthized;
  auto opt = cpuJRISC::getRegOpt( ctx, n0 );

  if ( opt )
  {
    if ( par )
      return ctx->create<AddressingJRISC>( AddressingJRISC::Jump{ CCJRISC::T, *opt } );
    else
      return ctx->create<AddressingJRISC>( AddressingJRISC::Single{ *opt } );
  }
  else
  {
    if ( par )
      return ctx->create<AddressingJRISC>( AddressingJRISC::Bad{ AddressingJRISC::Bad::INDIRECTION } );
    else
      return ctx->create<AddressingJRISC>( AddressingJRISC::Branch{ CCJRISC::T, n0 } );
  }
}

// <AddressingJRISC> ::= <Expression> , <Expression>
std::shared_ptr<void> reduceAddressingJRISC( nga::ICompilationContext* ctx, std::shared_ptr<Expression> n0, Tu002c t1, std::shared_ptr<Expression> n2 )
{
  bool srcPar = n0->parenthized;
  bool dstPar = n2->parenthized;
  auto srcOpt = cpuJRISC::getRegOpt( ctx, n0 );
  auto dstOpt = cpuJRISC::getRegOpt( ctx, n2 );

  if ( srcPar )
  {
    if ( dstPar )
    {
      // (src),(dst)
      return ctx->create<AddressingJRISC>( AddressingJRISC::Bad{ AddressingJRISC::Bad::DOUBLE_PARENTHIZED } );
    }
    else
    {
      // (src),dst
      if ( !dstOpt )
        return ctx->create<AddressingJRISC>( AddressingJRISC::Bad{ AddressingJRISC::Bad::REGISTER_EXPECTED } );

      if ( srcOpt )
        return ctx->create<AddressingJRISC>( AddressingJRISC::Load{ *srcOpt, *dstOpt } );
      else
      {
        auto bid = cpuJRISC::getBaseIdxDisp( ctx, n0 );
        if ( !bid.baseReg )
          return ctx->create<AddressingJRISC>( AddressingJRISC::Bad{ AddressingJRISC::Bad::REGISTER_EXPECTED } );
        if ( bid.idxReg )
          return ctx->create<AddressingJRISC>( AddressingJRISC::LoadWithIndex{ *bid.baseReg, *bid.idxReg, *dstOpt } );
        else
          return ctx->create<AddressingJRISC>( AddressingJRISC::LoadWithDisplacement{ *bid.baseReg, bid.disp, *dstOpt } );
      }
    }
  }
  else
  {
    if ( dstPar )
    {
      // src,(dst)
      if ( !srcOpt )
        return ctx->create<AddressingJRISC>( AddressingJRISC::Bad{ AddressingJRISC::Bad::REGISTER_EXPECTED } );

      if ( dstOpt )
        return ctx->create<AddressingJRISC>( AddressingJRISC::Store{ *srcOpt, *dstOpt } );
      else
      {
        auto bid = cpuJRISC::getBaseIdxDisp( ctx, n2 );
        if ( !bid.baseReg )
          return ctx->create<AddressingJRISC>( AddressingJRISC::Bad{ AddressingJRISC::Bad::REGISTER_EXPECTED } );
        if ( bid.idxReg )
          return ctx->create<AddressingJRISC>( AddressingJRISC::StoreWithIndex{ *srcOpt, *bid.baseReg, *bid.idxReg } );
        else
          return ctx->create<AddressingJRISC>( AddressingJRISC::StoreWithDisplacement{ *srcOpt , *bid.baseReg, bid.disp } );
      }
    }
    else
    {
      // src,dst
      if ( srcOpt && dstOpt )
        return ctx->create<AddressingJRISC>( AddressingJRISC::Double{ *srcOpt, *dstOpt } );
      else
        return ctx->create<AddressingJRISC>( AddressingJRISC::Bad{ AddressingJRISC::Bad::REGISTER_EXPECTED } );
    }
  }
}

// <AddressingJRISC> ::= PC , <Expression>
std::shared_ptr<void> reduceAddressingJRISC( nga::ICompilationContext* ctx, TPC t0, Tu002c t1, std::shared_ptr<Expression> n2 )
{
  bool dstPar = n2->parenthized;
  auto dstOpt = cpuJRISC::getRegOpt( ctx, n2 );

  if ( dstPar )
    return ctx->create<AddressingJRISC>( AddressingJRISC::Bad{ AddressingJRISC::Bad::INDIRECTION } );
  if ( !dstOpt )
    return ctx->create<AddressingJRISC>( AddressingJRISC::Bad{ AddressingJRISC::Bad::REGISTER_EXPECTED } );

  return ctx->create<AddressingJRISC>( AddressingJRISC::Single{ *dstOpt } );
}

// <AddressingJRISC> ::= # <Expression> , <Expression>
std::shared_ptr<void> reduceAddressingJRISC( nga::ICompilationContext* ctx, Tu0023 t0, std::shared_ptr<Expression> n1, Tu002c t2, std::shared_ptr<Expression> n3 )
{
  bool dstPar = n3->parenthized;
  auto dstOpt = cpuJRISC::getRegOpt( ctx, n3 );

  if ( dstPar )
    return ctx->create<AddressingJRISC>( AddressingJRISC::Bad{ AddressingJRISC::Bad::INDIRECTION } );
  if ( !dstOpt )
    return ctx->create<AddressingJRISC>( AddressingJRISC::Bad{ AddressingJRISC::Bad::REGISTER_EXPECTED } );

  return ctx->create<AddressingJRISC>( AddressingJRISC::Immediate{ n1, *dstOpt } );
}

// <AddressingJRISC> ::= <CCJRISC> , <Expression>
std::shared_ptr<void> reduceAddressingJRISC( nga::ICompilationContext* ctx, std::shared_ptr<CCJRISC> n0, Tu002c t1, std::shared_ptr<Expression> n2 )
{
  bool dstPar = n2->parenthized;
  auto dstOpt = cpuJRISC::getRegOpt( ctx, n2 );

  if ( dstPar )
  {
    if ( !dstOpt )
      return ctx->create<AddressingJRISC>( AddressingJRISC::Bad{ AddressingJRISC::Bad::REGISTER_EXPECTED } );
    else
      return ctx->create<AddressingJRISC>( AddressingJRISC::Jump{ n0->cc, *dstOpt } );
  }
  else
  {
    if ( !dstOpt )
      return ctx->create<AddressingJRISC>( AddressingJRISC::Branch{ n0->cc, n2 } );
    else
      return ctx->create<AddressingJRISC>( AddressingJRISC::Bad{ AddressingJRISC::Bad::LABEL_EXPECTED } );
  }
}

// <AddressingJRISC> ::= <>
std::shared_ptr<void> reduceAddressingJRISC( nga::ICompilationContext* ctx )
{
  return ctx->create<AddressingJRISC>( AddressingJRISC::Nil{} );
}

}
