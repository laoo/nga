#include "pch.hpp"
#include "CpuM68K.hpp"
#include "ILinkingContext.hpp"
#include "Log.hpp"

namespace assembler
{
namespace cpuM68K
{

std::string_view opName( Op op )
{
  static constexpr std::array<std::string_view, ( ( size_t )Op::_SIZE >> 16 )> name{
  "abcd",
  "add",
  "adda",
  "addi",
  "addq",
  "addx",
  "and",
  "andi",
  "asl",
  "asr",
  "bcc",
  "bcs",
  "beq",
  "bge",
  "bgt",
  "bhi",
  "ble",
  "bls",
  "blt",
  "bmi",
  "bne",
  "bpl",
  "bra",
  "bsr",
  "bvc",
  "bvs",
  "bchg",
  "bclr",
  "bset",
  "btst",
  "chk",
  "clr",
  "cmp",
  "cmpa",
  "cmpi",
  "cmpm",
  "dbcc",
  "dbcs",
  "dbeq",
  "dbf",
  "dbge",
  "dbgt",
  "dbhi",
  "dble",
  "dbls",
  "dblt",
  "dbmi",
  "dbne",
  "dbpl",
  "dbt",
  "dbvc",
  "dbvs",
  "divs",
  "divu",
  "eor",
  "eori",
  "exg",
  "ext",
  "illegal",
  "jmp",
  "jsr",
  "lea",
  "link",
  "lsl",
  "lsr",
  "move",
  "movea",
  "movem",
  "movep",
  "moveq",
  "muls",
  "mulu",
  "nbcd",
  "neg",
  "negx",
  "nop",
  "not",
  "or",
  "ori",
  "pea",
  "reset",
  "rol",
  "ror",
  "roxl",
  "roxr",
  "rte",
  "rtr",
  "rts",
  "sbcd",
  "scc",
  "scs",
  "seq",
  "sf",
  "sge",
  "sgt",
  "shi",
  "sle",
  "sls",
  "slt",
  "smi",
  "sne",
  "spl",
  "st",
  "svc",
  "svs",
  "stop",
  "sub",
  "suba",
  "subi",
  "subq",
  "subx",
  "swap",
  "tas",
  "trap",
  "trapv",
  "tst",
  "unlk"
  };

  size_t idx = ( size_t )op >> 16;
  assert( idx < name.size() );
  return name[idx];
}

class OpEx : public Ex
{
public:
  OpEx( Op op ) : Ex{}
  {
    *this << "Opcode '" << opName( op ) << "': ";
  }

  OpEx( Mnemonic mnem ) : Ex{}
  {
    *this << "Opcode '" << opName( (Op)mnem.opcode ) << "': ";
  }
};


template<typename T>
static void assertDistance( int distance, Op op )
{
  if ( distance < std::numeric_limits<T>::min() )
  {
    throw OpEx{ op } << "displacement too far by " << ( std::numeric_limits<T>::min() - distance );
  }
  else if ( distance > std::numeric_limits<T>::max() )
  {
    throw OpEx{ op } << "displacement too far by " << ( distance - std::numeric_limits<T>::max() );
  }
}

static void assertB( const MnemonicM68K& mnemonic )
{
  if ( mnemonic.size != SizeM68K::SIZE_BYTE )
    throw OpEx{ mnemonic } << "requires byte size";
}

static void assertW( const MnemonicM68K& mnemonic )
{
  if ( mnemonic.size != SizeM68K::SIZE_WORD )
    throw OpEx{ mnemonic } << "requires word size";
}

static void assertWN( const MnemonicM68K& mnemonic )
{
  if ( mnemonic.size != SizeM68K::SIZE_WORD && mnemonic.size != SizeM68K::SIZE_NONE )
    throw OpEx{ mnemonic } << "requires word size";
}

static void assertBW( const MnemonicM68K& mnemonic )
{
  if ( mnemonic.size != SizeM68K::SIZE_BYTE && mnemonic.size != SizeM68K::SIZE_WORD )
    throw OpEx{ mnemonic } << "requires byte or word size";
}

static void assertL( const MnemonicM68K& mnemonic )
{
  if ( mnemonic.size != SizeM68K::SIZE_LONG )
    throw OpEx{ mnemonic } << "requires long size";
}

static void assertLN( const MnemonicM68K& mnemonic )
{
  if ( mnemonic.size != SizeM68K::SIZE_LONG && mnemonic.size != SizeM68K::SIZE_NONE )
    throw OpEx{ mnemonic } << "requires long size";
}

static void assertBL( const MnemonicM68K& mnemonic )
{
  if ( mnemonic.size != SizeM68K::SIZE_BYTE && mnemonic.size != SizeM68K::SIZE_LONG )
    throw OpEx{ mnemonic } << "requires byte or long size";
}

static void assertWL( const MnemonicM68K& mnemonic )
{
  if ( mnemonic.size != SizeM68K::SIZE_WORD && mnemonic.size != SizeM68K::SIZE_LONG )
    throw OpEx{ mnemonic } << "requires word or long size";
}

static void assertBWL( const MnemonicM68K& mnemonic )
{
  if ( mnemonic.size == SizeM68K::SIZE_NONE )
    throw OpEx{ mnemonic } << "requires explicit size";
}

static Op immedateOp( Op op )
{
  switch ( op )
  {
  case Op::ADD:
    return Op::ADDI;
  case Op::AND:
    return Op::ANDI;
  case Op::CMP:
    return Op::CMPI;
  case Op::EOR:
    return Op::EORI;
  case Op::OR:
    return Op::ORI;
  case Op::SUB:
    return Op::SUBI;
  default:
    INTERNAL_ERROR;
  }
}

}

using namespace cpuM68K;

MnemonicM68K::MnemonicM68K( int opcode, SizeM68K::Size size ) : Mnemonic{ opcode }, size{ size } {}

int MnemonicM68K::sizeValue() const
{
  return ( (int)size - 1 ) << 6;
}

int MnemonicM68K::sizeWordLong() const
{
  return ( size & 1 ) << 8;
}

ImpliedAddressM68K::ImpliedAddressM68K() {}

uint32_t ImpliedAddressM68K::size( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  auto op = (Op)mnemonic.opcode;

  if ( cpu != nga::CpuType::M68000 )
    throw OpEx{ op } << "unsupported processor";

  switch ( op )
  {
  case Op::ILLEGAL:
  case Op::NOP:
  case Op::RESET:
  case Op::RTE:
  case Op::RTR:
  case Op::RTS:
  case Op::TRAPV:
    return 2;
  default:
    throw OpEx{ op } << "does not have implied addressing mode";
  }
}

void ImpliedAddressM68K::emit( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  auto op = (cpuM68K::Op)mnemonic.opcode;
  switch ( op )
  {
  case Op::ILLEGAL:
  case Op::NOP:
  case Op::RESET:
  case Op::RTE:
  case Op::RTR:
  case Op::RTS:
  case Op::TRAPV:
    lc.emitBE16( (uint16_t)op );
    break;
  default:
    throw OpEx{ op } << "unsupported addressing mode";
  }
}

M68KXn::M68KXn( RegType type, int number ) : regType{ type }, regNumber{ (uint8_t)number }
{
}

uint16_t M68KXn::rangeBit() const
{
  switch ( regType )
  {
  case RegType::DATA:
    return 1 << regNumber;
  case RegType::ADDRESS:
    return 0b100000000 << regNumber;
  default:
    return 0;
  }
}

M68KXnSized::M68KXnSized( M68KXn src, SizeM68K::Size size ) : M68KXn{ src }, size{ size }
{
}

uint16_t M68KXnSized::briefExtensionWord( int8_t displacement ) const
{
  return ( ( regType == RegType::ADDRESS ) ? 0x8000 : 0x0000 ) |
    ( (uint16_t)regNumber << 12 ) |
    ( ( size == SizeM68K::SIZE_LONG ) ? 0x0800 : 0x0000 ) |
    (uint8_t)displacement;
}

M68KRegRange::M68KRegRange( M68KXn const& from, M68KXn const& to ) : data{}
{
  auto r1 = from.rangeBit();
  auto r2 = to.rangeBit();

  auto lower = std::min( r1, r2 );
  auto upper = std::max( r1, r2 );

  if ( lower == 0 )
    throw Ex{ "bad register" };

  data = lower;
  while ( lower != upper )
  {
    lower <<= 1;
    data |= lower;
  }
}

M68KRegList::M68KRegList() : M68KXn{}, data1{}
{
}

M68KRegList::M68KRegList( M68KXn const& reg ) : M68KXn{ reg }, data1{ rangeBit() }
{
}

M68KRegList::M68KRegList( M68KRegRange const& range ) : M68KXn{}, data1{ range.data }
{
}

void M68KRegList::add( M68KXn const& reg )
{
  data1 |= reg.rangeBit();
}

void M68KRegList::add( M68KRegRange const& range )
{
  data1 |= range.data;
}

bool M68KRegList::isList() const
{
  return data1 != 0;
}

SingleAddressM68K::SingleAddressM68K( cpuM68K::AdrType type ) :
  mType{ type },
  mRegList{},
  mIndex{},
  mExpr{},
  mExprSize{}
{
}

SingleAddressM68K::SingleAddressM68K( M68KRegList list ) :
  mType{},
  mRegList{ list },
  mIndex{},
  mExpr{},
  mExprSize{}
{
  switch ( mRegList.regType )
  {
  case M68KXn::RegType::NONE:
    mType = cpuM68K::ADR_NONE;
    break;
  case M68KXn::RegType::DATA:
    mType = cpuM68K::ADR_DATA;
    break;
  case M68KXn::RegType::ADDRESS:
    mType = cpuM68K::ADR_ADDRESS;
    break;
  }
}

SingleAddressM68K::SingleAddressM68K( cpuM68K::AdrType type, M68KRegList reg ) :
  mType{ type },
  mRegList{ reg },
  mIndex{},
  mExpr{},
  mExprSize{}
{
}

SingleAddressM68K::SingleAddressM68K( cpuM68K::AdrType type, std::shared_ptr<Expression> expr ) :
  mType{ type },
  mRegList{},
  mIndex{},
  mExpr{ std::move( expr ) },
  mExprSize{}
{
}

SingleAddressM68K::SingleAddressM68K( cpuM68K::AdrType type, std::shared_ptr<Expression> expr, M68KRegList list ) :
  mType{ type },
  mRegList{ list },
  mIndex{},
  mExpr{ std::move( expr ) },
  mExprSize{}
{
}

SingleAddressM68K::SingleAddressM68K( cpuM68K::AdrType type, std::shared_ptr<Expression> expr, M68KRegList list, M68KXnSized index ) :
  mType{ type },
  mRegList{ list },
  mIndex{ index },
  mExpr{ std::move( expr ) },
  mExprSize{}
{
}

SingleAddressM68K::SingleAddressM68K( cpuM68K::AdrType type, std::shared_ptr<Expression> expr, M68KXnSized index ) :
  mType{ type },
  mRegList{},
  mIndex{ index },
  mExpr{ std::move( expr ) },
  mExprSize{}
{
}

SingleAddressM68K::SingleAddressM68K( std::shared_ptr<Expression> expr, SizeM68K exprSize ) :
  mType{ cpuM68K::ADR_ABS },
  mRegList{},
  mIndex{},
  mExpr{ std::move( expr ) },
  mExprSize{ exprSize }
{
}

uint32_t SingleAddressM68K::size( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  using namespace cpuM68K;
  auto m68k = (MnemonicM68K const&)mnemonic;
  auto op = (Op)mnemonic.opcode;

  if ( cpu != nga::CpuType::M68000 )
    throw OpEx{ op } << "unsupported processor";

  switch ( op )
  {
  case Op::ASL:
  case Op::ASR:
  case Op::LSL:
  case Op::LSR:
  case Op::ROL:
  case Op::ROR:
  case Op::ROXL:
  case Op::ROXR:
    if ( auto s = maskedSize( lc, ADR_IND | ADR_IND_POST | ADR_IND_PRE | ADR_IND_DISP | ADR_IND_IDX | ADR_ABS ) )
      return s;
    break;
  case Op::BCC:
  case Op::BCS:
  case Op::BEQ:
  case Op::BGE:
  case Op::BGT:
  case Op::BHI:
  case Op::BLE:
  case Op::BLS:
  case Op::BLT:
  case Op::BMI:
  case Op::BNE:
  case Op::BPL:
  case Op::BRA:
  case Op::BSR:
  case Op::BVC:
  case Op::BVS:
    if ( isType( ADR_ABS ) )
    {
      switch ( m68k.size )
      {
      case SizeM68K::SIZE_BYTE:
        return 2;
      case SizeM68K::SIZE_WORD:
        return 4;
      default:
        break;
      }
    }
    break;
  case Op::CLR:
  case Op::NBCD:
  case Op::NEG:
  case Op::NEGX:
  case Op::NOT:
  case Op::SCC:
  case Op::SCS:
  case Op::SEQ:
  case Op::SF:
  case Op::SGE:
  case Op::SGT:
  case Op::SHI:
  case Op::SLE:
  case Op::SLS:
  case Op::SLT:
  case Op::SMI:
  case Op::SNE:
  case Op::SPL:
  case Op::ST:
  case Op::SVC:
  case Op::SVS:
  case Op::TAS:
  case Op::TST:
    if ( auto s = maskedSize( lc, ADR_DATA | ADR_IND | ADR_IND_POST | ADR_IND_PRE | ADR_IND_DISP | ADR_IND_IDX | ADR_ABS ) )
      return s;
    break;
  case Op::EXT:
    if ( isType( ADR_DATA ) )
      return 2;
    break;
  case Op::JMP:
    if ( auto s = maskedSize( lc, ADR_IND | ADR_IND_DISP | ADR_IND_IDX | ADR_PCIND_DISP | ADR_PCIND_IDX | ADR_ABS ) )
      return s;
    break;
  case Op::JSR:
    if ( auto s = maskedSize( lc, ADR_IND | ADR_IND_DISP | ADR_IND_IDX | ADR_PCIND_DISP | ADR_PCIND_IDX | ADR_ABS ) )
      return s;
    break;
  case Op::PEA:
    if ( auto s = maskedSize( lc, ADR_IND | ADR_IND_DISP | ADR_IND_IDX | ADR_PCIND_DISP | ADR_PCIND_IDX | ADR_ABS ) )
      return s;
    break;
  case Op::SWAP:
    if ( isType( ADR_DATA ) )
      return 2;
    break;
  case Op::TRAP:
    if ( isType( ADR_IMM ) )
      return 2;
    break;
  case Op::STOP:
    if ( isType( ADR_IMM ) )
      return 4;
    break;
  case Op::UNLK:
    switch ( mExprSize.size )
    {
    case SizeM68K::SIZE_NONE:
      if ( isType( ADR_ADDRESS ) )
        return 2;
      break;
    default:
      break;
    }
    break;
  default:
    break;
  }

  throw OpEx{ op } << "unsupported addressing mode";
}

void SingleAddressM68K::emit( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  auto m68k = (MnemonicM68K const&)mnemonic;
  auto op = (Op)mnemonic.opcode;
  switch ( op )
  {
  case Op::ASL:
  case Op::ASR:
  case Op::LSL:
  case Op::LSR:
  case Op::ROL:
  case Op::ROR:
  case Op::ROXL:
  case Op::ROXR:
    assertW( m68k );
    lc.emitBE16( (uint16_t)( mnemonic.opcode | ( 0b11 << 6 ) | memoryAddressing( lc ) ) );
    return emitExtensionWord( m68k, lc, cpu, 2 );
  case Op::BCC:
  case Op::BCS:
  case Op::BEQ:
  case Op::BGE:
  case Op::BGT:
  case Op::BHI:
  case Op::BLE:
  case Op::BLS:
  case Op::BLT:
  case Op::BMI:
  case Op::BNE:
  case Op::BPL:
  case Op::BRA:
  case Op::BSR:
  case Op::BVC:
  case Op::BVS:
    return emitBranch( (MnemonicM68K const&)mnemonic, lc );
  case Op::NEGX:
  case Op::CLR:
  case Op::NEG:
  case Op::NOT:
  case Op::TST:
    assertBWL( m68k );
    lc.emitBE16( (uint16_t)( m68k.opcode | m68k.sizeValue() | memoryAddressing( lc ) ) );
    return emitExtensionWord( m68k, lc, cpu, 2 );
  case Op::NBCD:
  case Op::TAS:
    assertB( m68k );
    lc.emitBE16( (uint16_t)( m68k.opcode | memoryAddressing( lc ) ) );
    return emitExtensionWord( m68k, lc, cpu, 2 );
  case Op::PEA:
    assertL( m68k );
    lc.emitBE16( (uint16_t)( m68k.opcode | memoryAddressing( lc ) ) );
    return emitExtensionWord( m68k, lc, cpu, 2 );
  case Op::SCC:
  case Op::SCS:
  case Op::SEQ:
  case Op::SF:
  case Op::SGE:
  case Op::SGT:
  case Op::SHI:
  case Op::SLE:
  case Op::SLS:
  case Op::SLT:
  case Op::SMI:
  case Op::SNE:
  case Op::SPL:
  case Op::ST:
  case Op::SVC:
  case Op::SVS:
    assertB( m68k );
    lc.emitBE16( (uint16_t)( m68k.opcode | memoryAddressing( lc ) ) );
    return emitExtensionWord( m68k, lc, cpu, 2 );
  case Op::EXT:
    assertWL( m68k );
    {
      int a = ( m68k.sizeValue() >> 1 ) & 0x40;
      int b = regValue();
      int c = m68k.opcode | a | b;
      return lc.emitBE16( (uint16_t)( m68k.opcode | ( ( m68k.sizeValue() >> 1 ) & 0x40 ) | regValue() ) );
    }
  case Op::JMP:
  case Op::JSR:
    lc.emitBE16( (uint16_t)( m68k.opcode | memoryAddressing( lc ) ) );
    return emitExtensionWord( m68k, lc, cpu, 2 );
  case Op::SWAP:
    assertWN( m68k );
    return lc.emitBE16( (uint16_t)( m68k.opcode | regValue() ) );
  case Op::TRAP:
    return emitTrap( (MnemonicM68K const&)mnemonic, lc );
  case Op::STOP:
    return emitStop( (MnemonicM68K const&)mnemonic, lc );
  case Op::UNLK:
    return lc.emitBE16( (uint16_t)( m68k.opcode | mRegList.regNumber ) );
  default:
    break;
  }

  throw OpEx{ op } << "error";
}

void SingleAddressM68K::visit( Mnemonic const& mnemonic, nga::ICompilationContext& cc )
{
  auto op = (Op)mnemonic.opcode;
  if ( op == Op::JSR || op == Op::BSR )
  {
    if ( mExpr )
    {
      mExpr->setSegmentReferenceKind( nga::SRK_JSR );
    }
    else
    {
      L_WARNING << "TODO: call through register - make sure there is a declaration of segment where call is made";
    }
  }
  
  if ( mExpr )
  {
    mExpr->visit( cc );
  }
}

bool SingleAddressM68K::isType( cpuM68K::AdrType t ) const
{
  return ( mType & t ) != 0;
}

uint32_t SingleAddressM68K::exprSize( nga::ILinkingContext& lc ) const
{
  switch ( mExprSize.size )
  {
  case SizeM68K::SIZE_WORD:
    return 2;
  case SizeM68K::SIZE_LONG:
    return 4;
  default:
    return mExpr->size( lc, nga::CpuType::M68000 );
    break;
  }
}

int SingleAddressM68K::qData( MnemonicM68K const& mnemonic, nga::ILinkingContext& lc ) const
{
  auto value = mExpr->value( lc );
  auto imm = value.evaluate();
  if ( value.isOffset() || *imm <= 0 || *imm > 8 )
    throw OpEx{ mnemonic } << "only immediate value in range 1-8 allowed";

  return ( *imm & 7 ) << 9;
}

uint32_t SingleAddressM68K::maskedSize( nga::ILinkingContext& lc, cpuM68K::AdrType mask, MnemonicM68K const* mnem ) const
{
  if ( isType( mask & ( ADR_DATA | ADR_ADDRESS | ADR_IND | ADR_IND_PRE | ADR_IND_POST ) ) )
    return 2;
  if ( isType( mask & ( ADR_IND_DISP | ADR_IND_IDX | ADR_PCIND_DISP | ADR_PCIND_IDX ) ) )
    return 4;
  if ( isType( mask & ADR_ABS ) )
    return 2 + std::max( 2u, exprSize( lc ) );
  if ( mnem && isType( mask & ADR_IMM ) )
  {
    switch ( mnem->size )
    {
    case SizeM68K::SIZE_BYTE:
    case SizeM68K::SIZE_WORD:
      return 4;
    case SizeM68K::SIZE_LONG:
      return 6;
    default:
      break;
    }
  }
  return 0;
}

void SingleAddressM68K::emitTrap( MnemonicM68K const& mnemonic, nga::ILinkingContext& lc ) const
{
  auto value = mExpr->value( lc );
  if ( auto imm = value.evaluate() )
  {
    if ( *imm >= 0 && *imm < 16 )
      return lc.emitBE16( (uint16_t)( mnemonic.opcode | *imm ) );
  }

  throw OpEx{ mnemonic } << "only immediate value in range 0-15 allowed";
}

void SingleAddressM68K::emitStop( MnemonicM68K const& mnemonic, nga::ILinkingContext& lc ) const
{
  auto value = mExpr->value( lc );
  if ( auto imm = value.evaluate() )
  {
    if ( *imm >= 0 || *imm < std::numeric_limits<uint16_t>::max() )
    {
      lc.emitBE16( (uint16_t)( mnemonic.opcode ) );
      return lc.emitBE16( (uint16_t)( *imm ) );
    }
  }

  throw OpEx{ mnemonic } << "only immediate 16-bit value allowed";
}

void SingleAddressM68K::emitBranch( MnemonicM68K const& mnemonic, nga::ILinkingContext& lc ) const
{
  assertBW( mnemonic );

  if ( mExprSize.size == SizeM68K::SIZE_BYTE && mExpr->size( lc, nga::CpuType::M68000 ) > 1 )
    throw OpEx{ mnemonic } << "expression size does not fit in a byte";
  if ( mExprSize.size == SizeM68K::SIZE_WORD && mExpr->size( lc, nga::CpuType::M68000 ) > 2 )
    throw OpEx{ mnemonic } << "expression size does not fit in a word";

  auto value = mExpr->value( lc );
  if ( !value.isOffset() )
    throw OpEx{ mnemonic } << "destination must be a label";

  auto v = lc.getRel( value, 2 );

  if ( mnemonic.size == SizeM68K::SIZE_BYTE )
  {
    assertDistance<int8_t>( v, (Op)mnemonic.opcode );
    lc.emitBE16( (uint16_t)( mnemonic.opcode | (uint8_t)v ) );
  }
  else
  {
    assertDistance<int16_t>( v, (Op)mnemonic.opcode );
    lc.emitBE16( (uint16_t)( mnemonic.opcode ) );
    lc.emitBE16( (int16_t)v );
  }
}

void SingleAddressM68K::emitExtensionWord( MnemonicM68K const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu, int extensionWordOffset ) const
{
  auto op = (Op)mnemonic.opcode;

  switch ( mType )
  {
  case ADR_IND_DISP:
    emitIndirectDisplacement( lc, op, std::nullopt );
    break;
  case ADR_IND_IDX:
    emitIndirectIndex( lc, op, std::nullopt );
    break;
  case ADR_PCIND_DISP:
    emitIndirectDisplacement( lc, op, extensionWordOffset );
    break;
  case ADR_PCIND_IDX:
    emitIndirectIndex( lc, op, extensionWordOffset );
    break;
  case ADR_ABS:
    emitAbsolute( mnemonic, lc, cpu );
    break;
  case ADR_IMM:
    emitImmediate( mnemonic, lc, cpu );
    break;
  default:
    break;
  }
}

std::shared_ptr<Expression> SingleAddressM68K::expression() const
{
  return mExpr;
}

int SingleAddressM68K::extraRegValue() const
{
  return (int)mRegList.regNumber << 9;
}

int SingleAddressM68K::regValue() const
{
  return (int)mRegList.regNumber;
}

cpuM68K::AdrType SingleAddressM68K::type() const
{
  return mType;
}

M68KRegList SingleAddressM68K::regList() const
{
  return mRegList;
}

void SingleAddressM68K::emitIndirectIndex( nga::ILinkingContext& lc, assembler::cpuM68K::Op op, std::optional<int> extensionWordOffset ) const
{
  auto value = mExpr->value( lc );

  if ( extensionWordOffset )
  {
    emitIndirectIndex( lc.getRel( value, *extensionWordOffset ), op, lc );
  }
  else
  {
    auto distance = value.evaluate( lc );
    emitIndirectIndex( distance, op, lc );
  }
}


void SingleAddressM68K::emitIndirectIndex( int distance, cpuM68K::Op op, nga::ILinkingContext& lc ) const
{
  cpuM68K::assertDistance<int8_t>( distance, op );
  lc.emitBE16( mIndex.briefExtensionWord( (int8_t)distance ) );
}

void SingleAddressM68K::emitIndirectDisplacement( nga::ILinkingContext& lc, assembler::cpuM68K::Op op, std::optional<int> extensionWordOffset ) const
{
  auto value = mExpr->value( lc );

  if ( extensionWordOffset )
  {
    emitIndirectDisplacement( lc.getRel( value, *extensionWordOffset ), op, lc );
  }
  else
  {
    auto distance = value.evaluate( lc );
    emitIndirectDisplacement( distance, op, lc );
  }
}

void SingleAddressM68K::emitAbsolute( MnemonicM68K const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  auto op = (Op)mnemonic.opcode;

  switch ( mExprSize.size )
  {
  case SizeM68K::SIZE_BYTE:
    throw OpEx{ op } << "only word or long addressing allowed";
  case SizeM68K::SIZE_WORD:
    if ( mExpr->size( lc, nga::CpuType::M68000 ) > 2 )
      throw OpEx{ op } << "Expression size does not fit to word";
    else
      return lc.emitBE16( mExpr->value( lc ), cpu );
  case SizeM68K::SIZE_LONG:
    return lc.emitBE32( mExpr->value( lc ) );
  case SizeM68K::SIZE_NONE:
    switch ( mExpr->size( lc, nga::CpuType::M68000 ) )
    {
    case 1:
    case 2:
      return lc.emitBE16( mExpr->value( lc ), cpu );
    case 4:
      return lc.emitBE32( mExpr->value( lc ) );
    default:
      INTERNAL_ERROR;
    }
  }
}

int SingleAddressM68K::emitImmediate( MnemonicM68K const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  auto size = mExpr->size( lc, nga::CpuType::M68000 );

  switch ( mnemonic.size )
  {
  case SizeM68K::SIZE_BYTE:
    if ( size > 1 )
      throw OpEx{ mnemonic } << "Immediate value does not fit to byte";
    else
    {
      lc.emitBE16( mExpr->value( lc ), cpu, 0xff );
      return 2;
    }
  case SizeM68K::SIZE_WORD:
    if ( size > 2 )
      throw OpEx{ mnemonic } << "Immediate value does not fit to word";
    else
    {
      lc.emitBE16( mExpr->value( lc ), cpu );
      return 2;
    }
  case SizeM68K::SIZE_LONG:
    lc.emitBE32( mExpr->value( lc ) );
    return 4;
  default:
    INTERNAL_ERROR;
  }
}

M68KXn::RegType SingleAddressM68K::indexType() const
{
  return mIndex.regType;
}

void SingleAddressM68K::emitIndirectDisplacement( int distance, cpuM68K::Op op, nga::ILinkingContext& lc ) const
{
  cpuM68K::assertDistance<int16_t>( distance, op );
  lc.emitBE16( (int16_t)distance );
}

uint16_t SingleAddressM68K::memoryAddressing( nga::ILinkingContext& lc ) const
{
  auto [mem, reg] = memoryDescriptor( lc );
  return (uint16_t)( ( mem << 3 ) | reg );
}

std::pair<int, int> SingleAddressM68K::memoryDescriptor( nga::ILinkingContext& lc ) const
{
  switch ( mType )
  {
  case ADR_DATA:
    assert( mRegList.regType == M68KXn::RegType::DATA );
    return { 0b000, regValue() };
  case ADR_ADDRESS:
    assert( mRegList.regType == M68KXn::RegType::ADDRESS );
    return { 0b001, regValue() };
  case ADR_IND:
    assert( mRegList.regType == M68KXn::RegType::ADDRESS );
    return { 0b010, regValue() };
  case ADR_IND_POST:
    assert( mRegList.regType == M68KXn::RegType::ADDRESS );
    return { 0b011, regValue() };
  case ADR_IND_PRE:
    assert( mRegList.regType == M68KXn::RegType::ADDRESS );
    return { 0b100, regValue() };
  case ADR_IND_DISP:
    assert( mRegList.regType == M68KXn::RegType::ADDRESS );
    return { 0b101, regValue() };
  case ADR_IND_IDX:
    assert( mRegList.regType == M68KXn::RegType::ADDRESS );
    return { 0b110, regValue() };
  case ADR_PCIND_DISP:
    return { 0b111, 0b010 };
  case ADR_PCIND_IDX:
    return { 0b111, 0b011 };
  case ADR_ABS:
    return { 0b111, exprSize( lc ) == 4 ? 1 : 0 };
  case ADR_IMM:
    return { 0b111, 0b100 };
  default:
    INTERNAL_ERROR;
  }
}

DoubleAddressM68K::DoubleAddressM68K( std::shared_ptr<SingleAddressM68K> src, std::shared_ptr<SingleAddressM68K> dst ) : src{ std::move( src ) }, dst{ std::move( dst ) }
{
}

uint32_t DoubleAddressM68K::size( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  using namespace cpuM68K;
  auto m68k = (MnemonicM68K const&)mnemonic;
  auto op = (Op)mnemonic.opcode;

  if ( cpu != nga::CpuType::M68000 )
    throw OpEx{ op } << "unsupported processor";

  switch ( op )
  {
  case Op::ASL:
  case Op::ASR:
  case Op::LSL:
  case Op::LSR:
  case Op::ROL:
  case Op::ROR:
  case Op::ROXL:
  case Op::ROXR:
    if ( src->isType( ADR_DATA | ADR_IMM ) && dst->isType( ADR_DATA ) )
      return 2;
    break;
  case Op::ABCD:
  case Op::ADDX:
  case Op::SBCD:
  case Op::SUBX:
    if ( src->isType( ADR_DATA ) && dst->isType( ADR_DATA ) )
      return 2;
    if ( src->isType( ADR_IND_PRE ) && dst->isType( ADR_IND_PRE ) )
      return 2;
    break;
  case Op::ADD:
  case Op::SUB:
    if ( dst->isType( ADR_ADDRESS ) )
    {
      return size( MnemonicM68K{ op == Op::ADD ? (int)Op::ADDA : (int)Op::SUBA, m68k.size }, lc, cpu );
    }
    else if ( dst->isType( ADR_DATA ) )
    {
      if ( src->isType( ADR_ADDRESS ) && ( m68k.size == SizeM68K::SIZE_WORD || m68k.size == SizeM68K::SIZE_LONG ) )
        return 2;
      if ( auto s = src->maskedSize( lc, ADR_DATA | ADR_IND | ADR_IND_PRE | ADR_IND_POST | ADR_IND_DISP | ADR_IND_IDX | ADR_PCIND_DISP | ADR_PCIND_IDX | ADR_ABS | ADR_IMM, &m68k ) )
        return s;
    }
    else if ( src->isType( ADR_DATA ) )
    {
      if ( auto s = dst->maskedSize( lc, ADR_IND | ADR_IND_PRE | ADR_IND_POST | ADR_IND_DISP | ADR_IND_IDX | ADR_ABS ) )
        return s;
    }
    else if ( src->isType( ADR_IMM ) )
    {
      return size( MnemonicM68K{ op == Op::ADD ? (int)Op::ADDI : (int)Op::SUBI, m68k.size }, lc, cpu );
    }
    break;
  case Op::ADDA:
  case Op::CMPA:
  case Op::SUBA:
    if ( dst->isType( ADR_ADDRESS ) && ( m68k.size == SizeM68K::SIZE_WORD || m68k.size == SizeM68K::SIZE_LONG ) )
    {
      if ( auto s = src->maskedSize( lc, ADR_DATA | ADR_ADDRESS | ADR_IND | ADR_IND_PRE | ADR_IND_POST | ADR_IND_DISP | ADR_IND_IDX | ADR_PCIND_DISP | ADR_PCIND_IDX | ADR_ABS | ADR_IMM, &m68k ) )
        return s;
    }
    break;
  case Op::ADDI:
  case Op::SUBI:
    if ( src->isType( ADR_IMM ) )
    {
      if ( auto s = dst->maskedSize( lc, ADR_DATA | ADR_IND | ADR_IND_PRE | ADR_IND_POST | ADR_IND_DISP | ADR_IND_IDX | ADR_ABS ) )
        return s + ( m68k.size == SizeM68K::SIZE_LONG ? 4 : 2 );
    }
    break;
  case Op::ADDQ:
  case Op::SUBQ:
    if ( src->isType( ADR_IMM ) )
    {
      if ( auto s = dst->maskedSize( lc, ADR_DATA | ADR_ADDRESS | ADR_IND | ADR_IND_PRE | ADR_IND_POST | ADR_IND_DISP | ADR_IND_IDX | ADR_ABS ) )
        return s;
    }
    break;
  case Op::AND:
  case Op::OR:
    if ( dst->isType( ADR_DATA ) )
    {
      if ( auto s = src->maskedSize( lc, ADR_DATA | ADR_IND | ADR_IND_PRE | ADR_IND_POST | ADR_IND_DISP | ADR_IND_IDX | ADR_PCIND_DISP | ADR_PCIND_IDX | ADR_ABS | ADR_IMM, &m68k ) )
        return s;
    }
    else if ( src->isType( ADR_DATA ) )
    {
      if ( auto s = dst->maskedSize( lc, ADR_IND | ADR_IND_PRE | ADR_IND_POST | ADR_IND_DISP | ADR_IND_IDX | ADR_ABS ) )
        return s;
    }
    else if ( src->isType( ADR_IMM ) )
    {
      return size( MnemonicM68K{ op == Op::AND ? (int)Op::ANDI : (int)Op::ORI, m68k.size }, lc, cpu );
    }
    break;
  case Op::EOR:
    if ( src->isType( ADR_DATA ) )
    {
      if ( auto s = dst->maskedSize( lc, ADR_DATA | ADR_IND | ADR_IND_PRE | ADR_IND_POST | ADR_IND_DISP | ADR_IND_IDX | ADR_ABS ) )
        return s;
    }
    else if ( src->isType( ADR_IMM ) )
    {
      return size( MnemonicM68K{ (int)Op::EORI, m68k.size }, lc, cpu );
    }
    break;
  case Op::ANDI:
  case Op::EORI:
  case Op::ORI:
    if ( src->isType( ADR_IMM ) )
    {
      if ( dst->isType( ADR_CCR | ADR_SR ) )
      {
        return 4;
      }
      if ( auto s = dst->maskedSize( lc, ADR_DATA | ADR_IND | ADR_IND_PRE | ADR_IND_POST | ADR_IND_DISP | ADR_IND_IDX | ADR_ABS ) )
        return s + ( m68k.size == SizeM68K::SIZE_LONG ? 4 : 2 );
    }
    break;
  case Op::BCHG:
  case Op::BCLR:
  case Op::BSET:
    if ( src->isType( ADR_DATA | ADR_IMM ) )
    {
      if ( auto s = dst->maskedSize( lc, ADR_DATA | ADR_IND | ADR_IND_PRE | ADR_IND_POST | ADR_IND_DISP | ADR_IND_IDX | ADR_ABS ) )
        return s + ( src->isType( ADR_IMM ) ? 2 : 0 );
    }
    break;
  case Op::BTST:
    if ( src->isType( ADR_DATA | ADR_IMM ) )
    {
      if ( auto s = dst->maskedSize( lc, ADR_DATA | ADR_IND | ADR_IND_PRE | ADR_IND_POST | ADR_IND_DISP | ADR_IND_IDX | ADR_PCIND_DISP | ADR_PCIND_IDX | ADR_ABS | ADR_IMM, &m68k ) )
        return s + ( src->isType( ADR_IMM ) ? 2 : 0 );
    }
    break;
  case Op::CHK:
    if ( dst->isType( ADR_DATA ) && m68k.size == SizeM68K::SIZE_WORD )
    {
      if ( auto s = src->maskedSize( lc, ADR_DATA | ADR_IND | ADR_IND_PRE | ADR_IND_POST | ADR_IND_DISP | ADR_IND_IDX | ADR_PCIND_DISP | ADR_PCIND_IDX | ADR_ABS | ADR_IMM, &m68k ) )
        return s;
    }
    break;
  case Op::CMP:
    if ( dst->isType( ADR_ADDRESS ) )
    {
      return size( MnemonicM68K{ (int)Op::CMPA, m68k.size }, lc, cpu );
    }
    else if ( src->isType( ADR_IMM ) && !dst->isType( ADR_DATA ) )
    {
      return size( MnemonicM68K{ (int)Op::CMPI, m68k.size }, lc, cpu );
    }
    else if ( dst->isType( ADR_DATA ) )
    {
      if ( src->isType( ADR_ADDRESS ) && ( m68k.size == SizeM68K::SIZE_WORD || m68k.size == SizeM68K::SIZE_LONG ) )
        return 2;
      if ( auto s = src->maskedSize( lc, ADR_DATA | ADR_IND | ADR_IND_PRE | ADR_IND_POST | ADR_IND_DISP | ADR_IND_IDX | ADR_PCIND_DISP | ADR_PCIND_IDX | ADR_ABS | ADR_IMM, &m68k ) )
        return s;
    }
    break;
  case Op::CMPI:
    if ( src->isType( ADR_IMM ) )
    {
      if ( auto s = dst->maskedSize( lc, ADR_DATA | ADR_IND | ADR_IND_PRE | ADR_IND_POST | ADR_IND_DISP | ADR_IND_IDX | ADR_ABS ) )
        return s + ( m68k.size == SizeM68K::SIZE_LONG ? 4 : 2 );
    }
    break;
  case Op::CMPM:
    if ( src->isType( ADR_IND_POST ) && dst->isType( ADR_IND_POST ) )
    {
      return 2;
    }
    break;
  case Op::DBCC:
  case Op::DBCS:
  case Op::DBEQ:
  case Op::DBF:
  case Op::DBGE:
  case Op::DBGT:
  case Op::DBHI:
  case Op::DBLE:
  case Op::DBLS:
  case Op::DBLT:
  case Op::DBMI:
  case Op::DBNE:
  case Op::DBPL:
  case Op::DBT:
  case Op::DBVC:
  case Op::DBVS:
    if ( src->isType( ADR_DATA ) && dst->isType( ADR_ABS ) )
    {
      return 4;
    }
    break;
  case Op::DIVS:
  case Op::DIVU:
  case Op::MULS:
  case Op::MULU:
    if ( dst->isType( ADR_DATA ) )
    {
      if ( auto s = src->maskedSize( lc, ADR_DATA | ADR_IND | ADR_IND_PRE | ADR_IND_POST | ADR_IND_DISP | ADR_IND_IDX | ADR_PCIND_DISP | ADR_PCIND_IDX | ADR_ABS | ADR_IMM, &m68k ) )
        return s;
    }
    break;
  case Op::EXG:
    if ( src->isType( ADR_DATA ) && ( dst->isType( ADR_DATA | ADR_ADDRESS ) ) )
      return 2;
    if ( src->isType( ADR_ADDRESS ) && dst->isType( ADR_DATA | ADR_ADDRESS ) )
      return 2;
    break;
  case Op::LEA:
    if ( dst->isType( ADR_ADDRESS ) )
    {
      if ( auto s = src->maskedSize( lc, ADR_IND | ADR_IND_DISP | ADR_IND_IDX | ADR_PCIND_DISP | ADR_PCIND_IDX | ADR_ABS ) )
        return s;
    }
    break;
  case Op::LINK:
    if ( src->isType( ADR_ADDRESS ) && dst->isType( ADR_IMM ) )
    {
      return 4;
    }
    break;
  case Op::MOVE:
  {
    if ( src->isType( ADR_SR ) )
    {
      if ( auto s = dst->maskedSize( lc, ADR_DATA | ADR_IND | ADR_IND_PRE | ADR_IND_POST | ADR_IND_DISP | ADR_IND_IDX | ADR_ABS ) )
        return s;
    }
    else if ( dst->isType( ADR_SR | ADR_CCR ) )
    {
      if ( auto s = src->maskedSize( lc, ADR_DATA | ADR_IND | ADR_IND_PRE | ADR_IND_POST | ADR_IND_DISP | ADR_IND_IDX | ADR_ABS | ADR_IMM, &m68k ) )
        return s;
    }
    else if ( src->isType( ADR_USP ) && dst->isType( ADR_ADDRESS ) )
    {
      return 2;
    }
    else if ( src->isType( ADR_ADDRESS ) && dst->isType( ADR_USP ) )
    {
      return 2;
    }
    else
    {
      const int srcSize = src->maskedSize( lc, ADR_DATA | ADR_ADDRESS | ADR_IND | ADR_IND_PRE | ADR_IND_POST | ADR_IND_DISP | ADR_IND_IDX | ADR_PCIND_DISP | ADR_PCIND_IDX | ADR_ABS | ADR_IMM, &m68k ) - 2;
      if ( auto s = dst->maskedSize( lc, ADR_DATA | ADR_ADDRESS /* as a movea shortcut */ | ADR_IND | ADR_IND_PRE | ADR_IND_POST | ADR_IND_DISP | ADR_IND_IDX | ADR_ABS ) )
        return s + srcSize;
    }
  }
  case Op::MOVEA:
    if ( dst->isType( ADR_ADDRESS ) )
    {
      if ( auto s = src->maskedSize( lc, ADR_DATA | ADR_ADDRESS | ADR_IND | ADR_IND_PRE | ADR_IND_POST | ADR_IND_DISP | ADR_IND_IDX | ADR_PCIND_DISP | ADR_PCIND_IDX | ADR_ABS | ADR_IMM, &m68k ) )
        return s;
    }
    break;
  case Op::MOVEM:
    if ( ( src->regList().data1 != 0 && !src->isType( ADR_IND | ADR_IND_POST | ADR_IND_PRE | ADR_IND_DISP | ADR_IND_IDX ) ) || src->isType( ADR_IMM ) )
    {
      if ( auto s = dst->maskedSize( lc, ADR_IND | ADR_IND_PRE | ADR_IND_DISP | ADR_IND_IDX | ADR_ABS ) )
        return s + 2;
    }
    if ( ( dst->regList().data1 != 0 && !dst->isType( ADR_IND | ADR_IND_POST | ADR_IND_PRE | ADR_IND_DISP | ADR_IND_IDX ) ) || dst->isType( ADR_IMM ) )
    {
      if ( auto s = src->maskedSize( lc, ADR_IND | ADR_IND_POST | ADR_IND_DISP | ADR_IND_IDX | ADR_PCIND_DISP | ADR_PCIND_IDX | ADR_ABS ) )
        return s + 2;
    }
    break;
  case Op::MOVEP:
    if ( src->isType( ADR_DATA ) && dst->isType( ADR_IND_DISP ) )
    {
      return 4;
    }
    else if ( src->isType( ADR_IND_DISP ) && dst->isType( ADR_DATA ) )
    {
      return 4;
    }
    break;
  case Op::MOVEQ:
    if ( src->isType( ADR_IMM ) && dst->isType( ADR_DATA ) )
    {
      return 2;
    }
    break;
  default:
    break;
  }

  throw OpEx{ op } << "unsupported addressing mode";
}

void DoubleAddressM68K::emit( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  auto m68k = (MnemonicM68K const&)mnemonic;

  auto op = (Op)mnemonic.opcode;
  switch ( op )
  {
  case Op::ASL:
  case Op::ASR:
  case Op::LSL:
  case Op::LSR:
  case Op::ROL:
  case Op::ROR:
  case Op::ROXL:
  case Op::ROXR:
    return emitRotation( m68k, lc );
  case Op::ABCD:
  case Op::SBCD:
    assertB( m68k );
    return lc.emitBE16( (uint16_t)( mnemonic.opcode | dst->extraRegValue() | ( src->isType( ADR_DATA ) ? 0x0 : 0x8 ) | src->regValue() ) );
  case Op::ADDX:
  case Op::SUBX:
    assertB( m68k );
    return lc.emitBE16( (uint16_t)( m68k.opcode | dst->extraRegValue() | m68k.sizeValue() | ( src->isType( ADR_DATA ) ? 0x0 : 0x8 ) | src->regValue() ) );
  case Op::ADD:
  case Op::AND:
  case Op::CMP:
  case Op::OR:
  case Op::SUB:
    if ( dst->isType( ADR_ADDRESS ) )
    {
      return emit( MnemonicM68K{ op == Op::ADD ? (int)Op::ADDA : ( op == Op::CMP ? (int)Op::CMPA : (int)Op::SUBA ), m68k.size }, lc, cpu );
    }
    else if ( src->isType( ADR_IMM ) && !dst->isType( ADR_DATA ) )
    {
      return emit( MnemonicM68K{ (int)immedateOp( op ), m68k.size }, lc, cpu );
    }
    else if ( dst->isType( ADR_DATA ) )
    {
      lc.emitBE16( (uint16_t)( m68k.opcode | dst->extraRegValue() | m68k.sizeValue() | src->memoryAddressing( lc ) ) );
      return src->emitExtensionWord( m68k, lc, cpu, 2 );
    }
    else
    {
      lc.emitBE16( (uint16_t)( m68k.opcode | src->extraRegValue() | 0x100 | m68k.sizeValue() | dst->memoryAddressing( lc ) ) );
      return dst->emitExtensionWord( m68k, lc, cpu, 2 );
    }
  case Op::EOR:
    if ( src->isType( ADR_IMM ) )
    {
      return emit( MnemonicM68K{ (int)Op::EORI, m68k.size }, lc, cpu );
    }
    else if ( src->isType( ADR_DATA ) )
    {
      lc.emitBE16( (uint16_t)( m68k.opcode | src->extraRegValue() | m68k.sizeValue() | dst->memoryAddressing( lc ) ) );
      return dst->emitExtensionWord( m68k, lc, cpu, 2 );
    }
    break;
  case Op::ADDA:
  case Op::SUBA:
  case Op::CMPA:
    assertWL( m68k );
    lc.emitBE16( (uint16_t)( m68k.opcode | dst->extraRegValue() | m68k.sizeWordLong() | src->memoryAddressing( lc ) ) );
    return src->emitExtensionWord( m68k, lc, cpu, 2 );
  case Op::ADDI:
  case Op::ANDI:
  case Op::CMPI:
  case Op::EORI:
  case Op::ORI:
  case Op::SUBI:
    return emitImmediate( m68k, lc, cpu );
  case Op::ADDQ:
  case Op::SUBQ:
    if ( dst->isType( ADR_ADDRESS ) )
      assertWL( m68k );
    else
      assertBWL( m68k );
    lc.emitBE16( (uint16_t)( m68k.opcode | dst->memoryAddressing( lc ) | m68k.sizeValue() | src->qData( m68k, lc ) ) );
    return dst->emitExtensionWord( m68k, lc, cpu, 2 );
  case Op::BCHG:
  case Op::BCLR:
  case Op::BSET:
  case Op::BTST:
    return emitBRMW( m68k, lc, cpu );
    break;
  case Op::CHK:
    assertW( m68k );
    lc.emitBE16( (uint16_t)( m68k.opcode | dst->extraRegValue() | src->memoryAddressing( lc ) ) );
    return src->emitExtensionWord( m68k, lc, cpu, 2 );
  case Op::CMPM:
    assertBWL( m68k );
    return lc.emitBE16( (uint16_t)( m68k.opcode | dst->extraRegValue() | m68k.sizeValue() | src->regValue() ) );
  case Op::DBCC:
  case Op::DBCS:
  case Op::DBEQ:
  case Op::DBF:
  case Op::DBGE:
  case Op::DBGT:
  case Op::DBHI:
  case Op::DBLE:
  case Op::DBLS:
  case Op::DBLT:
  case Op::DBMI:
  case Op::DBNE:
  case Op::DBPL:
  case Op::DBT:
  case Op::DBVC:
  case Op::DBVS:
    return emitBranch( m68k, lc, false );
  case Op::LINK:
    return emitBranch( m68k, lc, true );
  case Op::DIVS:
  case Op::DIVU:
  case Op::MULS:
  case Op::MULU:
    assertW( m68k );
    lc.emitBE16( (uint16_t)( m68k.opcode | dst->extraRegValue() | m68k.sizeValue() | src->memoryAddressing( lc ) ) );
    return src->emitExtensionWord( m68k, lc, cpu, 2 );
  case Op::EXG:
    assertL( m68k );
    if ( src->isType( ADR_DATA ) )
      return lc.emitBE16( (uint16_t)( mnemonic.opcode | src->extraRegValue() | ( dst->isType( ADR_DATA ) ? 0b01000000 : 0b10001000 ) | dst->regValue() ) );
    else
      return lc.emitBE16( (uint16_t)( mnemonic.opcode | dst->extraRegValue() | ( dst->isType( ADR_DATA ) ? 0b10001000 : 0b01001000 ) | src->regValue() ) );
  case Op::LEA:
    assertLN( m68k );
    lc.emitBE16( (uint16_t)( m68k.opcode | dst->extraRegValue() | src->memoryAddressing( lc ) ) );
    return src->emitExtensionWord( m68k, lc, cpu, 2 );
  case Op::MOVE:
    return emitMove( m68k, lc, cpu );
  case Op::MOVEA:
    assertWL( m68k );
    return emitMove( m68k, lc, cpu );
  case Op::MOVEM:
    return emitMovem( m68k, lc, cpu );
  case Op::MOVEP:
    return emitMovep( m68k, lc, cpu );
  case Op::MOVEQ:
    return emitMoveq( m68k, lc );
  default:
    break;
  }

  throw OpEx{ op } << "error";

}

void DoubleAddressM68K::visit( Mnemonic const& mnemonic, nga::ICompilationContext& cc )
{
  if ( auto expr = src->expression() )
  {
    expr->visit( cc );
  }
  if ( auto expr = dst->expression() )
  {
    expr->visit( cc );
  }
}

void DoubleAddressM68K::emitImmediate( MnemonicM68K const& m68k, nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  assertBWL( m68k );

  int opcode = m68k.opcode;

  switch ( dst->type() )
  {
  case ADR_CCR:
    assertB( m68k );
    opcode |= 0b00'111100;
    break;
  case ADR_SR:
    assertW( m68k );
    opcode |= 0b01'111100;
    break;
  default:
    opcode |= m68k.sizeValue() | dst->memoryAddressing( lc );
    break;
  }

  lc.emitBE16( (uint16_t)opcode );
  int offset = src->emitImmediate( m68k, lc, cpu );
  dst->emitExtensionWord( m68k, lc, cpu, 2 + offset );
}

void DoubleAddressM68K::emitRotation( MnemonicM68K const& m68k, nga::ILinkingContext& lc ) const
{
  assertBWL( m68k );

  int opcode = m68k.opcode | m68k.sizeValue() | ( src->isType( ADR_DATA ) ? 0x20 : 0x00 ) | dst->regValue();

  switch ( (Op)m68k.opcode )
  {
  case Op::ASL:
  case Op::ASR:
    break;
  case Op::LSL:
  case Op::LSR:
    opcode |= 0b01000;
    break;
  case Op::ROXL:
  case Op::ROXR:
    opcode |= 0b10000;
    break;
  case Op::ROL:
  case Op::ROR:
    opcode |= 0b11000;
    break;
  default:
    INTERNAL_ERROR;
  }

  opcode &= 0b1111'000'111'111'111;

  if ( src->isType( ADR_DATA ) )
  {
    opcode |= src->extraRegValue();
  }
  else
  {
    opcode |= src->qData( m68k, lc );
  }

  lc.emitBE16( (uint16_t)opcode );
}

void DoubleAddressM68K::emitBRMW( MnemonicM68K const& m68k, nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  if ( dst->isType( ADR_DATA ) )
    assertL( m68k );
  else
    assertB( m68k );

  int opcode = m68k.opcode | dst->memoryAddressing( lc );

  if ( src->isType( ADR_DATA ) )
  {
    opcode |= ( 0b1 << 8 ) | src->extraRegValue();
  }
  else
  {
    opcode |= 0b1000 << 8;
  }

  lc.emitBE16( (uint16_t)opcode );

  int offset = 2;

  if ( src->isType( ADR_IMM ) )
  {
    if ( dst->isType( ADR_IMM ) )
      throw OpEx{ m68k } << "Can't test immediate bit within immediate value";

    auto expr = src->expression();
    auto size = expr->size( lc, nga::CpuType::M68000 );

    if ( size > 1 )
      throw OpEx{ m68k } << "Immediate value does not fit to byte";

    lc.emitBE16( expr->value( lc ), cpu, 0xff );
    offset += 2;
  }

  dst->emitExtensionWord( m68k, lc, cpu, offset );
}

void DoubleAddressM68K::emitBranch( MnemonicM68K const& mnemonic, nga::ILinkingContext& lc, bool link ) const
{
  assertWN( mnemonic );
  auto value = dst->expression()->value( lc );

  int v = 0;
  if ( link )
  {
    if ( value.isOffset() )
      throw OpEx{ mnemonic } << "destination must be an offset value";

    v = value.evaluate( lc );
  }
  else
  {
    if ( !value.isOffset() )
      throw OpEx{ mnemonic } << "destination must be a label";

    v = lc.getRel( value, 2 );
  }

  assertDistance<int16_t>( v, (Op)mnemonic.opcode );
  lc.emitBE16( (uint16_t)( mnemonic.opcode | src->regValue() ) );
  lc.emitBE16( (int16_t)v );
}

void DoubleAddressM68K::emitMove( MnemonicM68K const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  if ( src->isType( ADR_CCR ) )
  {
    assertW( mnemonic );
    lc.emitBE16( (uint16_t)( 0b0100'001'011'000'000 | dst->memoryAddressing( lc ) ) );
    return dst->emitExtensionWord( mnemonic, lc, cpu, 2 );
  }
  else if ( dst->isType( ADR_CCR ) )
  {
    assertW( mnemonic );
    lc.emitBE16( (uint16_t)( 0b0100'010'011'000'000 | src->memoryAddressing( lc ) ) );
    return src->emitExtensionWord( mnemonic, lc, cpu, 2 );
  }
  else if ( src->isType( ADR_SR ) )
  {
    assertW( mnemonic );
    lc.emitBE16( (uint16_t)( 0b0100'000'011'000'000 | dst->memoryAddressing( lc ) ) );
    return dst->emitExtensionWord( mnemonic, lc, cpu, 2 );
  }
  else if ( dst->isType( ADR_SR ) )
  {
    assertW( mnemonic );
    lc.emitBE16( (uint16_t)( 0b0100'011'011'000'000 | src->memoryAddressing( lc ) ) );
    return src->emitExtensionWord( mnemonic, lc, cpu, 2 );
  }
  else if ( src->isType( ADR_USP ) )
  {
    assertL( mnemonic );
    return lc.emitBE16( (uint16_t)( 0b0100'111'001'101'000 | dst->regValue() ) );
  }
  else if ( dst->isType( ADR_USP ) )
  {
    assertL( mnemonic );
    return lc.emitBE16( (uint16_t)( 0b0100'111'001'100'000 | src->regValue() ) );
  }

  if ( src->isType( ADR_ADDRESS ) || dst->isType( ADR_ADDRESS ) )
    assertWL( mnemonic );

  auto [srcmem, srcreg] = src->memoryDescriptor( lc );
  auto [dstmem, dstreg] = dst->memoryDescriptor( lc );

  int size = 0;
  switch ( mnemonic.size )
  {
  case  SizeM68K::SIZE_BYTE:
    size = 0b01 << 12;
    break;
  case  SizeM68K::SIZE_WORD:
    size = 0b11 << 12;
    break;
  case  SizeM68K::SIZE_LONG:
    size = 0b10 << 12;
    break;
  default:
    INTERNAL_ERROR;
  }

  lc.emitBE16( (uint16_t)( size | ( dstreg << 9 ) | ( dstmem << 6 ) | ( srcmem << 3 ) | srcreg ) );
  src->emitExtensionWord( mnemonic, lc, cpu, 2 );
  dst->emitExtensionWord( mnemonic, lc, cpu, 2 );
}

void DoubleAddressM68K::emitMovem( MnemonicM68K const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  assertWL( mnemonic );

  if ( ( src->regList().data1 != 0 && !src->isType( ADR_IND | ADR_IND_POST | ADR_IND_PRE | ADR_IND_DISP | ADR_IND_IDX ) ) || src->isType( ADR_IMM ) )
  {
    lc.emitBE16( (uint16_t)( mnemonic.opcode | ( ( mnemonic.sizeValue() >> 1 ) & 0x40 ) ) | dst->memoryAddressing( lc ) );

    uint16_t regList = src->regList().data1;

    if ( src->isType( ADR_IMM ) )
    {
      auto size = src->expression()->size( lc, nga::CpuType::M68000 );
      auto v = src->expression()->value( lc ).evaluate();

      if ( size > 2 || !v.has_value() || *v < 0 )
        throw OpEx{ mnemonic } << "register list immediate must by 16-bit word";

      regList = *v;
    }

    if ( dst->isType( ADR_IND_PRE ) && !src->isType( ADR_IMM ) )
    {
      uint64_t blo = regList & 0xff;
      uint64_t bhi = ( regList & 0xff00 ) >> 8;

      uint64_t rlo = ( blo * 0x0202020202ULL & 0x010884422010ULL ) % 1023;
      uint64_t rhi = ( bhi * 0x0202020202ULL & 0x010884422010ULL ) % 1023;

      //https://graphics.stanford.edu/~seander/bithacks.html#ReverseByteWith64BitsDiv
      lc.emitI8( (uint8_t)( rlo ) );
      lc.emitI8( (uint8_t)( rhi ) );
    }
    else
    {
      lc.emitBE16( regList );
    }
    dst->emitExtensionWord( mnemonic, lc, cpu, 4 );

  }
  else
  {
    uint16_t regList = dst->regList().data1;
    if ( dst->isType( ADR_IMM ) )
    {
      auto size = dst->expression()->size( lc, nga::CpuType::M68000 );
      auto v = dst->expression()->value( lc ).evaluate();

      if ( size > 2 || !v.has_value() || *v < 0 )
        throw OpEx{ mnemonic } << "register list immediate must by 16-bit word";

      regList = *v;
    }

    lc.emitBE16( (uint16_t)( mnemonic.opcode | 0x400 | ( ( mnemonic.sizeValue() >> 1 ) & 0x40 ) | src->memoryAddressing( lc ) ) );
    lc.emitBE16( regList );
    src->emitExtensionWord( mnemonic, lc, cpu, 4 );
  }
}

void DoubleAddressM68K::emitMovep( MnemonicM68K const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  assertWL( mnemonic );

  int size = ( mnemonic.sizeValue() >> 1 ) & 0x40;
  if ( src->isType( ADR_DATA ) )
  {
    lc.emitBE16( (uint16_t)( mnemonic.opcode | src->extraRegValue() | 0x080 | ( ( mnemonic.sizeValue() >> 1 ) & 0x40 ) ) | dst->regValue() );
    dst->emitExtensionWord( mnemonic, lc, cpu, 2 );
  }
  else
  {
    lc.emitBE16( (uint16_t)( mnemonic.opcode | dst->extraRegValue() | ( ( mnemonic.sizeValue() >> 1 ) & 0x40 ) ) | src->regValue() );
    src->emitExtensionWord( mnemonic, lc, cpu, 2 );
  }
}

void DoubleAddressM68K::emitMoveq( MnemonicM68K const& mnemonic, nga::ILinkingContext& lc ) const
{
  assertLN( mnemonic );

  auto value = src->expression()->value( lc );
  if ( auto imm = value.evaluate() )
  {
    if ( *imm >= std::numeric_limits<int8_t>::min() && *imm <= std::numeric_limits<uint8_t>::max() )
      return lc.emitBE16( (uint16_t)( mnemonic.opcode | dst->extraRegValue() | (uint8_t)*imm ) );
  }

  throw OpEx{ mnemonic } << "only immediate byte value allowed";
}

}


