#include "pch.hpp"
#include "CpuMOS.hpp"
#include "ILinkingContext.hpp"
#include "CpuType.hpp"

namespace assembler
{
namespace cpuMOS
{

std::string_view opName( Op op )
{
  static constexpr std::array<std::string_view, ( size_t )Op::_SIZE> name{
    "adc",
    "and",
    "asl",
    "bbr0",
    "bbr1",
    "bbr2",
    "bbr3",
    "bbr4",
    "bbr5",
    "bbr6",
    "bbr7",
    "bbs0",
    "bbs1",
    "bbs2",
    "bbs3",
    "bbs4",
    "bbs5",
    "bbs6",
    "bbs7",
    "bcc",
    "bcs",
    "beq",
    "bit",
    "bmi",
    "bne",
    "bpl",
    "bra",
    "brk",
    "bvc",
    "bvs",
    "clc",
    "cld",
    "cli",
    "clv",
    "cmp",
    "cpx",
    "cpy",
    "dec",
    "dex",
    "dey",
    "eor",
    "inc",
    "inx",
    "iny",
    "jmp",
    "jsr",
    "lda",
    "ldx",
    "ldy",
    "lsr",
    "nop",
    "ora",
    "pha",
    "php",
    "phx",
    "phy",
    "pla",
    "plp",
    "plx",
    "ply",
    "rmb0",
    "rmb1",
    "rmb2",
    "rmb3",
    "rmb4",
    "rmb5",
    "rmb6",
    "rmb7",
    "rol",
    "ror",
    "rti",
    "rts",
    "sbc",
    "sec",
    "sed",
    "sei",
    "smb0",
    "smb1",
    "smb2",
    "smb3",
    "smb4",
    "smb5",
    "smb6",
    "smb7",
    "sta",
    "stp",
    "stx",
    "sty",
    "stz",
    "tax",
    "tay",
    "trb",
    "tsb",
    "tsx",
    "txa",
    "txs",
    "tya",
    "wai"
  };

  size_t idx = ( size_t )op;
  assert( idx < name.size() );
  return name[idx];
}

uint32_t sizeMosRelative( Op op, nga::CpuType cpu )
{
  switch ( cpu )
  {
  case nga::CpuType::MOS6502:
    switch ( op )
    {
    case Op::BCC:
    case Op::BCS:
    case Op::BEQ:
    case Op::BMI:
    case Op::BNE:
    case Op::BPL:
    case Op::BRA:
    case Op::BVC:
    case Op::BVS:
      return 2;
    default:
      return 0;
    }
    break;
  case nga::CpuType::G65SC02:
    switch ( op )
    {
    case Op::BCC:
    case Op::BCS:
    case Op::BEQ:
    case Op::BMI:
    case Op::BNE:
    case Op::BPL:
    case Op::BRA:
    case Op::BVC:
    case Op::BVS:
    case Op::RMB0:
    case Op::RMB1:
    case Op::RMB2:
    case Op::RMB3:
    case Op::RMB4:
    case Op::RMB5:
    case Op::RMB6:
    case Op::RMB7:
    case Op::SMB0:
    case Op::SMB1:
    case Op::SMB2:
    case Op::SMB3:
    case Op::SMB4:
    case Op::SMB5:
    case Op::SMB6:
    case Op::SMB7:
      return 2;
    default:
      return 0;
    }
    break;
  case nga::CpuType::WDC65C02:
    switch ( op )
    {
    case Op::BCC:
    case Op::BCS:
    case Op::BEQ:
    case Op::BMI:
    case Op::BNE:
    case Op::BPL:
    case Op::BRA:
    case Op::BVC:
    case Op::BVS:
      return 2;
    default:
      return 0;
    }
    break;
  default:
    throw Ex{} << "Unsupported processor " << cpu.name();
  }
}

uint32_t sizeMosAbsolute( Op op, uint32_t exprSize, nga::CpuType cpu )
{
  switch ( cpu )
  {
  case nga::CpuType::MOS6502:
    switch ( op )
    {
    case Op::ADC:
    case Op::AND:
    case Op::ASL:
    case Op::BIT:
    case Op::CMP:
    case Op::CPX:
    case Op::CPY:
    case Op::DEC:
    case Op::EOR:
    case Op::INC:
    case Op::JMP:
    case Op::JSR:
    case Op::LDA:
    case Op::LDX:
    case Op::LDY:
    case Op::LSR:
    case Op::ORA:
    case Op::ROL:
    case Op::ROR:
    case Op::SBC:
    case Op::STA:
    case Op::STX:
    case Op::STY:
      return 1 + exprSize;
    default:
      throw Ex{} << "Opcode '" << opName( op ) << "' does not have " << ( exprSize < 2 ? "zero-page addressing mode" : "absolute addressing mode" );
    }
    break;
  case nga::CpuType::G65SC02:
    switch ( op )
    {
    case Op::ADC:
    case Op::AND:
    case Op::ASL:
    case Op::BIT:
    case Op::CMP:
    case Op::CPX:
    case Op::CPY:
    case Op::DEC:
    case Op::EOR:
    case Op::INC:
    case Op::JMP:
    case Op::JSR:
    case Op::LDA:
    case Op::LDX:
    case Op::LDY:
    case Op::LSR:
    case Op::ORA:
    case Op::ROL:
    case Op::ROR:
    case Op::SBC:
    case Op::STA:
    case Op::STX:
    case Op::STY:
    case Op::STZ:
    case Op::TRB:
    case Op::TSB:
      return 1 + exprSize;
    default:
      throw Ex{} << "Opcode '" << opName( op ) << "' does not have " << ( exprSize < 2 ? "zero-page addressing mode" : "absolute addressing mode" );
    }
    break;
  case nga::CpuType::WDC65C02:
    switch ( op )
    {
    case Op::ADC:
    case Op::AND:
    case Op::ASL:
    case Op::BIT:
    case Op::CMP:
    case Op::CPX:
    case Op::CPY:
    case Op::DEC:
    case Op::EOR:
    case Op::INC:
    case Op::JMP:
    case Op::JSR:
    case Op::LDA:
    case Op::LDX:
    case Op::LDY:
    case Op::LSR:
    case Op::ORA:
    case Op::ROL:
    case Op::ROR:
    case Op::SBC:
    case Op::STA:
    case Op::STX:
    case Op::STY:
    case Op::STZ:
    case Op::TRB:
    case Op::TSB:
      return 1 + exprSize;
    default:
      throw Ex{} << "Opcode '" << opName( op ) << "' does not have " << ( exprSize < 2 ? "zero-page addressing mode" : "absolute addressing mode" );
    }
    break;
  default:
    throw Ex{} << "Unsupported processor " << cpu.name();
  }
}

uint32_t sizeMosAbsoluteRelative( Op op, nga::CpuType cpu )
{
  switch ( cpu )
  {
  case nga::CpuType::G65SC02:
    return 3;
  default:
    throw Ex{} << "Unsupported processor " << cpu.name();
  }

}

uint32_t sizeMosIndexedXIndirect( Op op, uint32_t exprSize, nga::CpuType cpu )
{
  switch ( cpu )
  {
  case nga::CpuType::MOS6502:
    switch ( op )
    {
    case Op::ADC:
    case Op::AND:
    case Op::CMP:
    case Op::EOR:
    case Op::LDA:
    case Op::ORA:
    case Op::SBC:
    case Op::STA:
      return 2;
    default:
      throw Ex{} << "Unsupported addressing mode " << opName( op ) << ( exprSize < 2 ? " ($nn,x)" : " ($nnnn,x)" );
    }
    break;
  case nga::CpuType::G65SC02:
    [[fallthrough]];
  case nga::CpuType::WDC65C02:
    switch ( op )
    {
    case Op::ADC:
    case Op::AND:
    case Op::CMP:
    case Op::EOR:
    case Op::LDA:
    case Op::ORA:
    case Op::SBC:
    case Op::STA:
      return 2;
    case Op::JMP:
      return 3;
    default:
      throw Ex{} << "Unsupported addressing mode " << opName( op ) << ( exprSize < 2 ? " ($nn,x)" : " ($nnnn,x)" );
    }
    break;
  default:
    throw Ex{} << "Unsupported processor " << cpu.name();
  }

}

uint32_t sizeMosIndexedX( Op op, uint32_t exprSize, nga::CpuType cpu )
{
  switch ( cpu )
  {
  case nga::CpuType::MOS6502:
    switch ( op )
    {
    case Op::ADC:
    case Op::AND:
    case Op::ASL:
    case Op::BIT:
    case Op::CMP:
    case Op::DEC:
    case Op::EOR:
    case Op::INC:
    case Op::LDA:
    case Op::LDY:
    case Op::LSR:
    case Op::ORA:
    case Op::ROL:
    case Op::ROR:
    case Op::SBC:
    case Op::STA:
      return 1 + exprSize;
    case Op::STY:
      return 2;
    default:
      throw Ex{} << "Unsupported addressing mode " << opName( op ) << ( exprSize < 2 ? " $nn,x" : " $nnnn,x" );
    }
    break;
  case nga::CpuType::G65SC02:
  case nga::CpuType::WDC65C02:
    switch ( op )
    {
    case Op::ADC:
    case Op::AND:
    case Op::ASL:
    case Op::BIT:
    case Op::CMP:
    case Op::DEC:
    case Op::EOR:
    case Op::INC:
    case Op::LDA:
    case Op::LDY:
    case Op::LSR:
    case Op::ORA:
    case Op::ROL:
    case Op::ROR:
    case Op::SBC:
    case Op::STA:
    case Op::STZ:
      return 1 + exprSize;
    case Op::STY:
      return 2;
    default:
      throw Ex{} << "Unsupported addressing mode " << opName( op ) << ( exprSize < 2 ? " $nn,x" : " $nnnn,x" );
    }
  default:
    throw Ex{} << "Unsupported processor " << cpu.name();
  }
}

uint32_t sizeMosIndexedY( Op op, uint32_t exprSize, nga::CpuType cpu )
{
  switch ( cpu )
  {
  case nga::CpuType::MOS6502:
  case nga::CpuType::G65SC02:
  case nga::CpuType::WDC65C02:
    switch ( op )
    {
    case Op::ADC:
    case Op::AND:
    case Op::CMP:
    case Op::EOR:
    case Op::LDA:
    case Op::ORA:
    case Op::SBC:
    case Op::STA:
      return 3;
    case Op::LDX:
      return 1 + exprSize;
    case Op::STX:
      return 2;
    default:
      throw Ex{} << "Unsupported addressing mode " << opName( op ) << ( exprSize < 2 ? " $nn,y" : " $nnnn,y" );
    }
    break;
  default:
    throw Ex{} << "Unsupported processor " << cpu.name();
  }
}

uint32_t sizeMosIndirect( Op op, uint32_t exprSize, nga::CpuType cpu )
{
  switch ( cpu )
  {
  case nga::CpuType::MOS6502:
  case nga::CpuType::G65SC02:
  case nga::CpuType::WDC65C02:
    switch ( op )
    {
    case Op::ADC:
    case Op::AND:
    case Op::CMP:
    case Op::EOR:
    case Op::LDA:
    case Op::ORA:
    case Op::SBC:
    case Op::STA:
      return 2;
    case Op::JMP:
      return 3;
    default:
      throw Ex{} << "Opcode '" << opName( op ) << "' does not have " << ( exprSize < 2 ? "indirect zero-page addressing mode" : "indirect absolute addressing mode" );
    }
    break;
  default:
    throw Ex{} << "Unsupported processor " << cpu.name();
  }

}

uint32_t sizeMosIndirectIndexedY( Op op, nga::CpuType cpu )
{
  switch ( cpu )
  {
  case nga::CpuType::MOS6502:
  case nga::CpuType::G65SC02:
  case nga::CpuType::WDC65C02:
    switch ( op )
    {
    case Op::ADC:
    case Op::AND:
    case Op::CMP:
    case Op::EOR:
    case Op::LDA:
    case Op::ORA:
    case Op::SBC:
    case Op::STA:
      return 2;
    default:
      throw Ex{} << "Unsupported addressing mode " << opName( op ) << " ($nn,y)";
    }
    break;
  default:
    throw Ex{} << "Unsupported processor " << cpu.name();
  }

}

uint32_t sizeMosImmediate( Op op, nga::CpuType cpu )
{
  switch ( cpu )
  {
  case nga::CpuType::MOS6502:
  case nga::CpuType::G65SC02:
  case nga::CpuType::WDC65C02:
    switch ( op )
    {
    case Op::ADC:
    case Op::AND:
    case Op::BIT:
    case Op::CMP:
    case Op::CPX:
    case Op::CPY:
    case Op::EOR:
    case Op::LDA:
    case Op::LDX:
    case Op::LDY:
    case Op::ORA:
    case Op::SBC:
      return 2;
    default:
      throw Ex{} << "Unsupported addressing mode " << opName( op ) << " #$nn";
    }
    break;
  default:
    throw Ex{} << "Unsupported processor " << cpu.name();
  }

}

uint32_t sizeMosImplied( Op op, nga::CpuType cpu )
{
  switch ( cpu )
  {
  case nga::CpuType::MOS6502:
    switch ( op )
    {
    case Op::ASL:
    case Op::BRK:
    case Op::CLC:
    case Op::CLD:
    case Op::CLI:
    case Op::CLV:
    case Op::DEC:
    case Op::DEX:
    case Op::DEY:
    case Op::INX:
    case Op::INY:
    case Op::LSR:
    case Op::NOP:
    case Op::PHA:
    case Op::PHP:
    case Op::PLA:
    case Op::PLP:
    case Op::ROL:
    case Op::ROR:
    case Op::RTI:
    case Op::RTS:
    case Op::SEC:
    case Op::SED:
    case Op::SEI:
    case Op::TAX:
    case Op::TAY:
    case Op::TSX:
    case Op::TXA:
    case Op::TXS:
    case Op::TYA:
      return 1;
    default:
      throw Ex{} << "Opcode '"<< opName( op ) << "' does not have implied addressing mode";
    }
    break;
  case nga::CpuType::G65SC02:
    switch ( op )
    {
    case Op::ASL:
    case Op::BRK:
    case Op::CLC:
    case Op::CLD:
    case Op::CLI:
    case Op::CLV:
    case Op::DEC:
    case Op::DEX:
    case Op::DEY:
    case Op::INC:
    case Op::INX:
    case Op::INY:
    case Op::LSR:
    case Op::NOP:
    case Op::PHA:
    case Op::PHP:
    case Op::PHX:
    case Op::PHY:
    case Op::PLA:
    case Op::PLP:
    case Op::PLX:
    case Op::PLY:
    case Op::ROL:
    case Op::ROR:
    case Op::RTI:
    case Op::RTS:
    case Op::SEC:
    case Op::SED:
    case Op::SEI:
    case Op::TAX:
    case Op::TAY:
    case Op::TSX:
    case Op::TXA:
    case Op::TXS:
    case Op::TYA:
      return 1;
    default:
      throw Ex{} << "Unsupported addressing mode " << opName( op );
    }
    break;
  case nga::CpuType::WDC65C02:
    switch ( op )
    {
    case Op::ASL:
    case Op::BRK:
    case Op::CLC:
    case Op::CLD:
    case Op::CLI:
    case Op::CLV:
    case Op::DEC:
    case Op::DEX:
    case Op::DEY:
    case Op::INC:
    case Op::INX:
    case Op::INY:
    case Op::LSR:
    case Op::NOP:
    case Op::PHA:
    case Op::PHP:
    case Op::PHX:
    case Op::PHY:
    case Op::PLA:
    case Op::PLP:
    case Op::PLX:
    case Op::PLY:
    case Op::ROL:
    case Op::ROR:
    case Op::RTI:
    case Op::RTS:
    case Op::SEC:
    case Op::SED:
    case Op::SEI:
    case Op::STP:
    case Op::TAX:
    case Op::TAY:
    case Op::TSX:
    case Op::TXA:
    case Op::TXS:
    case Op::TYA:
    case Op::WAI:
      return 1;
    default:
      throw Ex{} << "Unsupported addressing mode " << opName( op );
    }
    break;
  default:
    throw Ex{} << "Unsupported processor " << cpu.name();
  }
}

int emitMosAbsolute( nga::ILinkingContext & lc, Op op, uint32_t exprSize )
{
  if ( exprSize == 1 )
  {
    switch ( op )
    {
    case Op::ADC:
      lc.emitI8( 0x65 );
      return 1;
    case Op::AND:
      lc.emitI8( 0x25 );
      return 1;
    case Op::ASL:
      lc.emitI8( 0x06 );
      return 1;
    case Op::BCC:
      lc.emitI8( 0x90 );
      return 1;
    case Op::BCS:
      lc.emitI8( 0xb0 );
      return 1;
    case Op::BEQ:
      lc.emitI8( 0xf0 );
      return 1;
    case Op::BIT:
      lc.emitI8( 0x24 );
      return 1;
    case Op::BMI:
      lc.emitI8( 0x30 );
      return 1;
    case Op::BNE:
      lc.emitI8( 0xd0 );
      return 1;
    case Op::BPL:
      lc.emitI8( 0x10 );
      return 1;
    case Op::BRA:
      lc.emitI8( 0x80 );
      return 1;
    case Op::BVC:
      lc.emitI8( 0x50 );
      return 1;
    case Op::BVS:
      lc.emitI8( 0x70 );
      return 1;
    case Op::CMP:
      lc.emitI8( 0xc5 );
      return 1;
    case Op::CPX:
      lc.emitI8( 0xe4 );
      return 1;
    case Op::CPY:
      lc.emitI8( 0xc4 );
      return 1;
    case Op::DEC:
      lc.emitI8( 0xc6 );
      return 1;
    case Op::EOR:
      lc.emitI8( 0x45 );
      return 1;
    case Op::INC:
      lc.emitI8( 0xe6 );
      return 1;
    case Op::JMP:
      lc.emitI8( 0x4c );
      return 2;
    case Op::JSR:
      lc.emitI8( 0x20 );
      return 2;
    case Op::LDA:
      lc.emitI8( 0xa5 );
      return 1;
    case Op::LDX:
      lc.emitI8( 0xa6 );
      return 1;
    case Op::LDY:
      lc.emitI8( 0xa4 );
      return 1;
    case Op::LSR:
      lc.emitI8( 0x46 );
      return 1;
    case Op::ORA:
      lc.emitI8( 0x05 );
      return 1;
    case Op::RMB0:
      lc.emitI8( 0x07 );
      return 1;
    case Op::RMB1:
      lc.emitI8( 0x17 );
      return 1;
    case Op::RMB2:
      lc.emitI8( 0x27 );
      return 1;
    case Op::RMB3:
      lc.emitI8( 0x37 );
      return 1;
    case Op::RMB4:
      lc.emitI8( 0x47 );
      return 1;
    case Op::RMB5:
      lc.emitI8( 0x57 );
      return 1;
    case Op::RMB6:
      lc.emitI8( 0x67 );
      return 1;
    case Op::RMB7:
      lc.emitI8( 0x77 );
      return 1;
    case Op::ROL:
      lc.emitI8( 0x26 );
      return 1;
    case Op::ROR:
      lc.emitI8( 0x66 );
      return 1;
    case Op::SBC:
      lc.emitI8( 0xe5 );
      return 1;
    case Op::SMB0:
      lc.emitI8( 0x87 );
      return 1;
    case Op::SMB1:
      lc.emitI8( 0x97 );
      return 1;
    case Op::SMB2:
      lc.emitI8( 0xa7 );
      return 1;
    case Op::SMB3:
      lc.emitI8( 0xb7 );
      return 1;
    case Op::SMB4:
      lc.emitI8( 0xc7 );
      return 1;
    case Op::SMB5:
      lc.emitI8( 0xd7 );
      return 1;
    case Op::SMB6:
      lc.emitI8( 0xe7 );
      return 1;
    case Op::SMB7:
      lc.emitI8( 0xf7 );
      return 1;
    case Op::STA:
      lc.emitI8( 0x85 );
      return 1;
    case Op::STX:
      lc.emitI8( 0x86 );
      return 1;
    case Op::STY:
      lc.emitI8( 0x84 );
      return 1;
    case Op::STZ:
      lc.emitI8( 0x64 );
      return 1;
    case Op::TRB:
      lc.emitI8( 0x14 );
      return 1;
    case Op::TSB:
      lc.emitI8( 0x04 );
      return 1;
    default:
      INTERNAL_ERROR;
      return 0;
    }
  }
  else if ( exprSize == 2 )
  {
    switch ( op )
    {
    case Op::ADC:
      lc.emitI8( 0x6d );
      return 2;
    case Op::AND:
      lc.emitI8( 0x2d );
      return 2;
    case Op::ASL:
      lc.emitI8( 0x0e );
      return 2;
    case Op::BCC:
    case Op::BCS:
    case Op::BEQ:
      return 0;
    case Op::BIT:
      lc.emitI8( 0x2c );
      return 2;
    case Op::BMI:
    case Op::BNE:
    case Op::BPL:
    case Op::BRA:
    case Op::BVC:
    case Op::BVS:
      return 0;
    case Op::CMP:
      lc.emitI8( 0xcd );
      return 2;
    case Op::CPX:
      lc.emitI8( 0xec );
      return 2;
    case Op::CPY:
      lc.emitI8( 0xcc );
      return 2;
    case Op::DEC:
      lc.emitI8( 0xce );
      return 2;
    case Op::EOR:
      lc.emitI8( 0x4d );
      return 2;
    case Op::INC:
      lc.emitI8( 0xee );
      return 2;
    case Op::JMP:
      lc.emitI8( 0x4c );
      return 2;
    case Op::JSR:
      lc.emitI8( 0x20 );
      return 2;
    case Op::LDA:
      lc.emitI8( 0xad );
      return 2;
    case Op::LDX:
      lc.emitI8( 0xae );
      return 2;
    case Op::LDY:
      lc.emitI8( 0xac );
      return 2;
    case Op::LSR:
      lc.emitI8( 0x4e );
      return 2;
    case Op::ORA:
      lc.emitI8( 0x0d );
      return 2;
    case Op::RMB0:
    case Op::RMB1:
    case Op::RMB2:
    case Op::RMB3:
    case Op::RMB4:
    case Op::RMB5:
    case Op::RMB6:
    case Op::RMB7:
      return 0;
    case Op::ROL:
      lc.emitI8( 0x2e );
      return 2;
    case Op::ROR:
      lc.emitI8( 0x6e );
      return 2;
    case Op::SBC:
      lc.emitI8( 0xed );
      return 2;
    case Op::SMB0:
    case Op::SMB1:
    case Op::SMB2:
    case Op::SMB3:
    case Op::SMB4:
    case Op::SMB5:
    case Op::SMB6:
    case Op::SMB7:
      return 0;
    case Op::STA:
      lc.emitI8( 0x8d );
      return 2;
    case Op::STX:
      lc.emitI8( 0x8e );
      return 2;
    case Op::STY:
      lc.emitI8( 0x8c );
      return 2;
    case Op::STZ:
      lc.emitI8( 0x9c );
      return 2;
    case Op::TRB:
      lc.emitI8( 0x1c );
      return 2;
    case Op::TSB:
      lc.emitI8( 0x0c );
      return 2;
    default:
      INTERNAL_ERROR;
      return 0;
    }
  }
  else
  {
    assert( false );
    return 0;
  }

}

int emitMosAbsoluteRelative( nga::ILinkingContext & lc, Op op )
{
  switch ( op )
  {
  case Op::BBR0:
    lc.emitI8( 0x0f );
    return 2;
  case Op::BBR1:
    lc.emitI8( 0x1f );
    return 2;
  case Op::BBR2:
    lc.emitI8( 0x2f );
    return 2;
  case Op::BBR3:
    lc.emitI8( 0x3f );
    return 2;
  case Op::BBR4:
    lc.emitI8( 0x4f );
    return 2;
  case Op::BBR5:
    lc.emitI8( 0x5f );
    return 2;
  case Op::BBR6:
    lc.emitI8( 0x6f );
    return 2;
  case Op::BBR7:
    lc.emitI8( 0x7f );
    return 2;
  case Op::BBS0:
    lc.emitI8( 0x8f );
    return 2;
  case Op::BBS1:
    lc.emitI8( 0x9f );
    return 2;
  case Op::BBS2:
    lc.emitI8( 0xaf );
    return 2;
  case Op::BBS3:
    lc.emitI8( 0xbf );
    return 2;
  case Op::BBS4:
    lc.emitI8( 0xcf );
    return 2;
  case Op::BBS5:
    lc.emitI8( 0xdf );
    return 2;
  case Op::BBS6:
    lc.emitI8( 0xef );
    return 2;
  case Op::BBS7:
    lc.emitI8( 0xff );
    return 2;
  default:
    assert( false );
    return 0;
  }
}

int emitMosIndexedXIndirect( nga::ILinkingContext & lc, Op op, uint32_t exprSize )
{
  if ( exprSize == 1 )
  {
    switch ( op )
    {
    case Op::ADC:
      lc.emitI8( 0x61 );
      return 1;
    case Op::AND:
      lc.emitI8( 0x21 );
      return 1;
    case Op::CMP:
      lc.emitI8( 0xc1 );
      return 1;
    case Op::EOR:
      lc.emitI8( 0x41 );
      return 1;
    case Op::JMP:
      lc.emitI8( 0x7c );
      return 2;
    case Op::LDA:
      lc.emitI8( 0xa1 );
      return 1;
    case Op::ORA:
      lc.emitI8( 0x01 );
      return 1;
    case Op::SBC:
      lc.emitI8( 0xe1 );
      return 1;
    case Op::STA:
      lc.emitI8( 0x81 );
      return 1;
    default:
      INTERNAL_ERROR;
      return 0;
    }
  }
  else if ( exprSize == 2 )
  {
    switch ( op )
    {
    case Op::ADC:
    case Op::AND:
    case Op::CMP:
    case Op::EOR:
    case Op::LDA:
    case Op::ORA:
    case Op::SBC:
    case Op::STA:
      return 0;
    case Op::JMP:
      lc.emitI8( 0x7c );
      return 2;
    default:
      INTERNAL_ERROR;
      return 0;
    }
  }
  else
  {
    INTERNAL_ERROR;
    return 0;
  }
}

int emitMosIndexedX( nga::ILinkingContext & lc, Op op, uint32_t exprSize )
{
  if ( exprSize == 1 )
  {
    switch ( op )
    {
    case Op::ADC:
      lc.emitI8( 0x75 );
      return 1;
    case Op::AND:
      lc.emitI8( 0x35 );
      return 1;
    case Op::ASL:
      lc.emitI8( 0x16 );
      return 1;
    case Op::BIT:
      lc.emitI8( 0x34 );
      return 1;
    case Op::CMP:
      lc.emitI8( 0xd5 );
      return 1;
    case Op::DEC:
      lc.emitI8( 0xd6 );
      return 1;
    case Op::EOR:
      lc.emitI8( 0x55 );
      return 1;
    case Op::INC:
      lc.emitI8( 0xf6 );
      return 1;
    case Op::LDA:
      lc.emitI8( 0xb5 );
      return 1;
    case Op::LDY:
      lc.emitI8( 0xb4 );
      return 1;
    case Op::LSR:
      lc.emitI8( 0x56 );
      return 1;
    case Op::ORA:
      lc.emitI8( 0x15 );
      return 1;
    case Op::ROL:
      lc.emitI8( 0x36 );
      return 1;
    case Op::ROR:
      lc.emitI8( 0x76 );
      return 1;
    case Op::SBC:
      lc.emitI8( 0xf5 );
      return 1;
    case Op::STA:
      lc.emitI8( 0x95 );
      return 1;
    case Op::STY:
      lc.emitI8( 0x94 );
      return 1;
    case Op::STZ:
      lc.emitI8( 0x74 );
      return 1;
    default:
      INTERNAL_ERROR;
      return 0;
    }
  }
  else if ( exprSize == 2 )
  {
    switch ( op )
    {
    case Op::ADC:
      lc.emitI8( 0x7d );
      return 2;
    case Op::AND:
      lc.emitI8( 0x3d );
      return 2;
    case Op::ASL:
      lc.emitI8( 0x1e );
      return 2;
    case Op::BIT:
      lc.emitI8( 0x3c );
      return 2;
    case Op::CMP:
      lc.emitI8( 0xdd );
      return 2;
    case Op::DEC:
      lc.emitI8( 0xde );
      return 2;
    case Op::EOR:
      lc.emitI8( 0x5d );
      return 2;
    case Op::INC:
      lc.emitI8( 0xfe );
      return 2;
    case Op::LDA:
      lc.emitI8( 0xbd );
      return 2;
    case Op::LDY:
      lc.emitI8( 0xbc );
      return 2;
    case Op::LSR:
      lc.emitI8( 0x5e );
      return 2;
    case Op::ORA:
      lc.emitI8( 0x1d );
      return 2;
    case Op::ROL:
      lc.emitI8( 0x3e );
      return 2;
    case Op::ROR:
      lc.emitI8( 0x7e );
      return 2;
    case Op::SBC:
      lc.emitI8( 0xfd );
      return 2;
    case Op::STA:
      lc.emitI8( 0x9d );
      return 2;
    case Op::STY:
      return 0;
    case Op::STZ:
      lc.emitI8( 0x9e );
      return 2;
    default:
      INTERNAL_ERROR;
      return 0;
    }
  }
  else
  {
    INTERNAL_ERROR;
    return 0;
  }
}

int emitMosIndexedY( nga::ILinkingContext & lc, Op op, uint32_t exprSize )
{
  if ( exprSize == 1 )
  {
    switch ( op )
    {
    case Op::ADC:
      lc.emitI8( 0x79 );
      return 2;
    case Op::AND:
      lc.emitI8( 0x39 );
      return 2;
    case Op::CMP:
      lc.emitI8( 0xd9 );
      return 2;
    case Op::EOR:
      lc.emitI8( 0x59 );
      return 2;
    case Op::LDA:
      lc.emitI8( 0xb9 );
      return 2;
    case Op::LDX:
      lc.emitI8( 0xb6 );
      return 1;
    case Op::ORA:
      lc.emitI8( 0x19 );
      return 2;
    case Op::SBC:
      lc.emitI8( 0xf9 );
      return 2;
    case Op::STA:
      lc.emitI8( 0x99 );
      return 2;
    case Op::STX:
      lc.emitI8( 0x96 );
      return 1;
    default:
      INTERNAL_ERROR;
      return 0;
    }
  }
  else if ( exprSize == 2 )
  {
    switch ( op )
    {
    case Op::ADC:
      lc.emitI8( 0x79 );
      return 2;
    case Op::AND:
      lc.emitI8( 0x39 );
      return 2;
    case Op::CMP:
      lc.emitI8( 0xd9 );
      return 2;
    case Op::EOR:
      lc.emitI8( 0x59 );
      return 2;
    case Op::LDA:
      lc.emitI8( 0xb9 );
      return 2;
    case Op::LDX:
      lc.emitI8( 0xbe );
      return 2;
    case Op::ORA:
      lc.emitI8( 0x19 );
      return 2;
    case Op::SBC:
      lc.emitI8( 0xf9 );
      return 2;
    case Op::STA:
      lc.emitI8( 0x99 );
      return 2;
    case Op::STX:
      return 0;
    default:
      INTERNAL_ERROR;
      return 0;
    }
  }
  else
  {
    INTERNAL_ERROR;
    return 0;
  }

}

int emitMosIndirect( nga::ILinkingContext & lc, Op op )
{
  switch ( op )
  {
  case Op::ADC:
    lc.emitI8( 0x72 );
    return 1;
  case Op::AND:
    lc.emitI8( 0x32 );
    return 1;
  case Op::CMP:
    lc.emitI8( 0xd2 );
    return 1;
  case Op::EOR:
    lc.emitI8( 0x52 );
    return 1;
  case Op::JMP:
    lc.emitI8( 0x6c );
    return 2;
  case Op::LDA:
    lc.emitI8( 0xb2 );
    return 1;
  case Op::ORA:
    lc.emitI8( 0x12 );
    return 1;
  case Op::SBC:
    lc.emitI8( 0xf2 );
    return 1;
  case Op::STA:
    lc.emitI8( 0x92 );
    return 1;
  default:
    INTERNAL_ERROR;
    return 0;
  }
}

int emitMosIndirectIndexedY( nga::ILinkingContext & lc, Op op )
{
  switch ( op )
  {
  case Op::ADC:
    lc.emitI8( 0x71 );
    return 1;
  case Op::AND:
    lc.emitI8( 0x31 );
    return 1;
  case Op::CMP:
    lc.emitI8( 0xd1 );
    return 1;
  case Op::EOR:
    lc.emitI8( 0x51 );
    return 1;
  case Op::LDA:
    lc.emitI8( 0xb1 );
    return 1;
  case Op::ORA:
    lc.emitI8( 0x11 );
    return 1;
  case Op::SBC:
    lc.emitI8( 0xf1 );
    return 1;
  case Op::STA:
    lc.emitI8( 0x91 );
    return 1;
  default:
    INTERNAL_ERROR;
    return 0;
  }
}

int emitMosImmediate( nga::ILinkingContext & lc, Op op )
{
  switch ( op )
  {
  case Op::ADC:
    lc.emitI8( 0x69 );
    return 1;
  case Op::AND:
    lc.emitI8( 0x29 );
    return 1;
  case Op::BIT:
    lc.emitI8( 0x89 );
    return 1;
  case Op::CMP:
    lc.emitI8( 0xc9 );
    return 1;
  case Op::CPX:
    lc.emitI8( 0xe0 );
    return 1;
  case Op::CPY:
    lc.emitI8( 0xc0 );
    return 1;
  case Op::EOR:
    lc.emitI8( 0x49 );
    return 1;
  case Op::LDA:
    lc.emitI8( 0xa9 );
    return 1;
  case Op::LDX:
    lc.emitI8( 0xa2 );
    return 1;
  case Op::LDY:
    lc.emitI8( 0xa0 );
    return 1;
  case Op::ORA:
    lc.emitI8( 0x09 );
    return 1;
  case Op::SBC:
    lc.emitI8( 0xe9 );
    return 1;
  default:
    INTERNAL_ERROR;
    return 0;
  }
}

int emitMosImplied( nga::ILinkingContext & lc, Op op )
{
  switch ( op )
  {
  case Op::ASL:
    lc.emitI8( 0x0a );
    return 0;
  case Op::BRK:
    lc.emitI8( 0x00 );
    return 0;
  case Op::CLC:
    lc.emitI8( 0x18 );
    return 0;
  case Op::CLD:
    lc.emitI8( 0xd8 );
    return 0;
  case Op::CLI:
    lc.emitI8( 0x58 );
    return 0;
  case Op::CLV:
    lc.emitI8( 0xb8 );
    return 0;
  case Op::DEC:
    lc.emitI8( 0x3a );
    return 0;
  case Op::DEX:
    lc.emitI8( 0xca );
    return 0;
  case Op::DEY:
    lc.emitI8( 0x88 );
    return 0;
  case Op::INC:
    lc.emitI8( 0x1a );
    return 0;
  case Op::INX:
    lc.emitI8( 0xe8 );
    return 0;
  case Op::INY:
    lc.emitI8( 0xc8 );
    return 0;
  case Op::LSR:
    lc.emitI8( 0x4a );
    return 0;
  case Op::NOP:
    lc.emitI8( 0xea );
    return 0;
  case Op::PHA:
    lc.emitI8( 0x48 );
    return 0;
  case Op::PHP:
    lc.emitI8( 0x08 );
    return 0;
  case Op::PHX:
    lc.emitI8( 0xda );
    return 0;
  case Op::PHY:
    lc.emitI8( 0x5a );
    return 0;
  case Op::PLA:
    lc.emitI8( 0x68 );
    return 0;
  case Op::PLP:
    lc.emitI8( 0x28 );
    return 0;
  case Op::PLX:
    lc.emitI8( 0xfa );
    return 0;
  case Op::PLY:
    lc.emitI8( 0x7a );
    return 0;
  case Op::ROL:
    lc.emitI8( 0x2a );
    return 0;
  case Op::ROR:
    lc.emitI8( 0x6a );
    return 0;
  case Op::RTI:
    lc.emitI8( 0x40 );
    return 0;
  case Op::RTS:
    lc.emitI8( 0x60 );
    return 0;
  case Op::SEC:
    lc.emitI8( 0x38 );
    return 0;
  case Op::SED:
    lc.emitI8( 0xf8 );
    return 0;
  case Op::SEI:
    lc.emitI8( 0x78 );
    return 0;
  case Op::STP:
    lc.emitI8( 0xdb );
    return 0;
  case Op::TAX:
    lc.emitI8( 0xaa );
    return 0;
  case Op::TAY:
    lc.emitI8( 0xa8 );
    return 0;
  case Op::TSX:
    lc.emitI8( 0xba );
    return 0;
  case Op::TXA:
    lc.emitI8( 0x8a );
    return 0;
  case Op::TXS:
    lc.emitI8( 0x9a );
    return 0;
  case Op::TYA:
    lc.emitI8( 0x98 );
    return 0;
  case Op::WAI:
    lc.emitI8( 0xcb );
    return 0;
  default:
    INTERNAL_ERROR;
    return 0;
  }
 
}

int emitMosRelative( nga::ILinkingContext& lc, Op op )
{
  switch ( op )
  {
  case Op::BCC:
    lc.emitI8( 0x90 );
    return 1;
  case Op::BCS:
    lc.emitI8( 0xb0 );
    return 1;
  case Op::BEQ:
    lc.emitI8( 0xf0 );
    return 1;
  case Op::BMI:
    lc.emitI8( 0x30 );
    return 1;
  case Op::BNE:
    lc.emitI8( 0xd0 );
    return 1;
  case Op::BPL:
    lc.emitI8( 0x10 );
    return 1;
  case Op::BRA:
    lc.emitI8( 0x80 );
    return 1;
  case Op::BVC:
    lc.emitI8( 0x50 );
    return 1;
  case Op::BVS:
    lc.emitI8( 0x70 );
    return 1;
  default:
    return 0;
  }
}

}

MnemonicMOS::MnemonicMOS( int opcode ) : Mnemonic{ opcode } {}

MosAbsolute::MosAbsolute( std::shared_ptr<Expression> expression ) : expression{ std::move( expression ) } {}

uint32_t MosAbsolute::size( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  auto op = (cpuMOS::Op)mnemonic.opcode;

  if ( auto size = cpuMOS::sizeMosRelative( op, cpu ) )
  {
    if ( expression->parenthized )
      throw Ex{} << "Opcode '" << cpuMOS::opName( op ) << "': Unsupported addressing mode";
    return size;
  }
  else if ( expression->parenthized )
  {
    return cpuMOS::sizeMosIndirect( op, expression->size( lc, cpu ), cpu );
  }
  else
  {
    return cpuMOS::sizeMosAbsolute( op, expression->size( lc, cpu ), cpu );
  }
}

void MosAbsolute::emit( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  auto op = (cpuMOS::Op)mnemonic.opcode;
  auto value = expression->value( lc );

  if ( cpuMOS::emitMosRelative( lc, op ) )
  {
    if ( auto distance = lc.emitRel8( value, 2 ) )
    {
      throw Ex{} << "Opcode '" << cpuMOS::opName( op ) << "': branch too far by " << distance;
    }
  }
  else
  {
    switch ( expression->parenthized ? cpuMOS::emitMosIndirect( lc, op ) : cpuMOS::emitMosAbsolute( lc, op, value.size( cpu ) ) )
    {
    case 0:
      throw Ex{} << "Opcode '" << cpuMOS::opName( op ) << "': Unsupported addressing mode";
    case 1:
      lc.emitI8( value, cpu );
      break;
    case 2:
      lc.emitLE16( value, cpu );
      break;
    default:
      INTERNAL_ERROR;
      break;
    }
  }
}

void MosAbsolute::visit( Mnemonic const& mnemonic, nga::ICompilationContext& cc )
{
  if ( (cpuMOS::Op)mnemonic.opcode == cpuMOS::Op::JSR )
    expression->setSegmentReferenceKind( nga::SRK_JSR );

  expression->visit( cc );
}

MosAbsoluteRelative::MosAbsoluteRelative( std::shared_ptr<Expression> absolute, std::shared_ptr<Expression> relative ) : absolute{ std::move( absolute ) }, relative{ std::move( relative ) } {}

uint32_t MosAbsoluteRelative::size( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  auto op = (cpuMOS::Op)mnemonic.opcode;

  return cpuMOS::sizeMosAbsoluteRelative( op, cpu );
}

void MosAbsoluteRelative::emit( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  auto op = (cpuMOS::Op)mnemonic.opcode;
  auto abs = absolute->value( lc );
  auto rel = relative->value( lc );

  switch ( cpuMOS::emitMosAbsoluteRelative( lc, op ) )
  {
  case 0:
    throw Ex{ "Unsupported addressing mode" };
  case 2:
    lc.emitI8( abs, cpu );
    if ( auto distance = lc.emitRel8( rel, 3 ) )
    {
      throw Ex{} << "Opcode '" << cpuMOS::opName( op ) << "': branch too far by " << distance;
    }
    break;
  default:
    INTERNAL_ERROR;
    break;
  }
}

void MosAbsoluteRelative::visit( Mnemonic const& mnemonic, nga::ICompilationContext& cc )
{
  absolute->visit( cc );
  relative->visit( cc );
}

MosIndexedXIndirect::MosIndexedXIndirect( std::shared_ptr<Expression> expression ) : expression{ std::move( expression ) } {}

uint32_t MosIndexedXIndirect::size( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  auto op = (cpuMOS::Op)mnemonic.opcode;

  return cpuMOS::sizeMosIndexedXIndirect( op, expression->size( lc, cpu ), cpu );
}

void MosIndexedXIndirect::emit( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  auto op = (cpuMOS::Op)mnemonic.opcode;
  auto value = expression->value( lc );

  switch ( cpuMOS::emitMosIndexedXIndirect( lc, op, value.size( cpu ) ) )
  {
  case 0:
    throw Ex{ "Unsupported addressing mode" };
  case 1:
    lc.emitI8( value, cpu );
    break;
  case 2:
    lc.emitLE16( value, cpu );
    break;
  default:
    INTERNAL_ERROR;
    break;
  }
}

void MosIndexedXIndirect::visit( Mnemonic const& mnemonic, nga::ICompilationContext& cc )
{
  expression->visit( cc );
}

MosIndexedX::MosIndexedX( std::shared_ptr<Expression> expression ) : expression{ std::move( expression ) } {}

uint32_t MosIndexedX::size( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  auto op = (cpuMOS::Op)mnemonic.opcode;
  return cpuMOS::sizeMosIndexedX( op, expression->size( lc, cpu ), cpu );
}

void MosIndexedX::emit( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  auto op = (cpuMOS::Op)mnemonic.opcode;
  auto value = expression->value( lc );

  switch ( cpuMOS::emitMosIndexedX( lc, op, value.size( cpu ) ) )
  {
  case 0:
    throw Ex{ "Unsupported addressing mode" };
  case 1:
    lc.emitI8( value, cpu );
    break;
  case 2:
    lc.emitLE16( value, cpu );
    break;
  default:
    INTERNAL_ERROR;
    break;
  }
}

void MosIndexedX::visit( Mnemonic const& mnemonic, nga::ICompilationContext& cc )
{
  expression->visit( cc );
}

MosIndexedY::MosIndexedY( std::shared_ptr<Expression> expression ) : expression{ std::move( expression ) } {}

uint32_t MosIndexedY::size( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  auto op = (cpuMOS::Op)mnemonic.opcode;

  if ( expression->parenthized )
  {
    return cpuMOS::sizeMosIndirectIndexedY( op, cpu );
  }
  else
  {
    return cpuMOS::sizeMosIndexedY( op, expression->size( lc, cpu ), cpu );
  }
}

void MosIndexedY::emit( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  auto op = (cpuMOS::Op)mnemonic.opcode;
  auto value = expression->value( lc );

  if ( expression->parenthized )
  {

    switch ( cpuMOS::emitMosIndirectIndexedY( lc, op ) )
    {
    case 0:
      throw Ex{ "Unsupported addressing mode" };
    case 1:
      lc.emitI8( value, cpu );
      break;
    default:
      INTERNAL_ERROR;
      break;
    }
  }
  else
  {
    switch ( cpuMOS::emitMosIndexedY( lc, op, value.size( cpu ) ) )
    {
    case 0:
      throw Ex{ "Unsupported addressing mode" };
    case 1:
      lc.emitI8( value, cpu );
      break;
    case 2:
      lc.emitLE16( value, cpu );
      break;
    default:
      INTERNAL_ERROR;
      break;
    }
  }
}

void MosIndexedY::visit( Mnemonic const& mnemonic, nga::ICompilationContext& cc )
{
  expression->visit( cc );
}

MosImmediate::MosImmediate( std::shared_ptr<Expression> expression, char op ) : expression{ std::move( expression ) }, op{ op } {}

uint32_t MosImmediate::size( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  auto op = (cpuMOS::Op)mnemonic.opcode;

  return cpuMOS::sizeMosImmediate( op, cpu );
}

void MosImmediate::emit( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  auto opcode = (cpuMOS::Op)mnemonic.opcode;
  auto value = expression->value( lc );

  switch ( cpuMOS::emitMosImmediate( lc, opcode ) )
  {
  case 0:
    throw Ex{ "Unsupported addressing mode" };
  case 1:
    switch ( op )
    {
    case '<':
      lc.emitI8( value.evaluate( lc ) & 0xff );
      break;
    case '>':
      lc.emitI8( ( value.evaluate( lc ) & 0xff00 ) >> 8 );
      break;
    default:
      lc.emitI8( value, cpu );
      break;
    }
    break;
  default:
    INTERNAL_ERROR;
    break;
  }
}

void MosImmediate::visit( Mnemonic const& mnemonic, nga::ICompilationContext& cc )
{
  expression->visit( cc );
}

MosImplied::MosImplied() {}

uint32_t MosImplied::size( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  auto op = (cpuMOS::Op)mnemonic.opcode;
  return cpuMOS::sizeMosImplied( op, cpu );
}

void MosImplied::emit( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const
{
  auto op = (cpuMOS::Op)mnemonic.opcode;
  cpuMOS::emitMosImplied( lc, op );
}

void MosImplied::visit( Mnemonic const& mnemonic, nga::ICompilationContext& cc )
{
}

}


