#include "pch.hpp"
#include "CpuJRISC.hpp"
#include "ILinkingContext.hpp"

namespace assembler
{
namespace cpuJRISC
{

std::string_view opName( Op op )
{
  static constexpr std::array<std::string_view, ( ( size_t )Op::_SIZE )> name{
    "add",
    "addc",
    "addq",
    "addqt",
    "sub",
    "subc",
    "subq",
    "subqt",
    "neg",
    "and",
    "or",
    "xor",
    "not",
    "btst",
    "bset",
    "bclr",
    "mult",
    "imult",
    "imultn",
    "resmac",
    "imacn",
    "div",
    "abs",
    "sh",
    "shlq",
    "shrq",
    "sha",
    "sharq",
    "rolq",
    "ror",
    "rorq",
    "cmp",
    "cmpq",
    "sat8",
    "subqmod",
    "sat16",
    "sat16s",
    "move",
    "moveq",
    "moveta",
    "movefa",
    "movei",
    "loadb",
    "loadw",
    "load",
    "loadp",
    "sat32s",
    "storeb",
    "storew",
    "store",
    "storep",
    "mirror",
    "jump",
    "jr",
    "mmult",
    "mtoi",
    "normi",
    "nop",
    "sat24",
    "pack",
    "addqmod",
    "unpack"
  };

  size_t idx = ( size_t )op;
  assert( idx < name.size() );
  return name[idx];
}

class OpEx : public Ex
{
public:
  OpEx( Op op ) : Ex{}
  {
    *this << "Opcode '" << opName( op ) << "': ";
  }

  OpEx( Mnemonic mnem ) : Ex{}
  {
    *this << "Opcode '" << opName( (Op)mnem.opcode ) << "': ";
  }
};

std::optional<int> getRegOpt( std::string_view ident )
{
  if ( std::tolower( ident[0] ) == 'r' )
  {
    int value;
    auto [ptr, ec] = std::from_chars( ident.data() + 1, ident.data() + 1 + ident.size(), value );

    if ( ec == std::errc{} )
    {
      return value;
    }
  }

  return {};
}

std::optional<int> getRegOpt( nga::ICompilationContext* ctx, std::string_view ident )
{
  if ( auto opt = getRegOpt( ident ) )
  {
    return opt;
  }
  if ( auto opt = ctx->regMapping( ident ) )
  {
    return opt;
  }
  return {};
}

BaseIdxDisp getBaseIdxDisp( nga::ICompilationContext * ctx, std::shared_ptr<Expression> expr )
{
  auto exprAdd = std::dynamic_pointer_cast<ExprAdd_>( expr );
  if ( !exprAdd )
    return {};

  BaseIdxDisp result{ getRegOpt( ctx, exprAdd->exprAdd ) };
  if ( !result.baseReg )
    return {};

  result.idxReg = getRegOpt( ctx, exprAdd->exprMul );
  if ( !result.idxReg )
  {
    result.disp = exprAdd->exprMul;
  }

  return result;
}

std::optional<int> getRegOpt( nga::ICompilationContext* ctx, std::shared_ptr<Expression> expr )
{
  if ( auto name = std::dynamic_pointer_cast<NSName>( expr ) )
    return getRegOpt( ctx, name->name );
  return {};
}

int getReg( nga::ICompilationContext* ctx, std::string_view ident )
{
  if ( auto opt = getRegOpt( ctx, ident ) )
  {
    return *opt;
  }

  throw Ex{ "register expected or undefined register mapping" };
}

template<typename T>
T const& assertType( AddressingJRISC const& addr )
{
  auto t = std::get_if<T>( &addr.mVar );
  if ( !t )
    throw Ex{ "Bad addressing mode" };

  return *t;
}

static uint16_t compose( int opcode, int reg1, int reg2 )
{
  return ( uint16_t)( ( opcode << 10 ) | ( reg1 << 5 ) | reg2 );
}

static void emitDouble( AddressingJRISC const& addr, nga::ILinkingContext & lc, int opcode )
{
  auto dbl = assertType<AddressingJRISC::Double>( addr );

  lc.emitBE16( compose( opcode, dbl.src, dbl.dst ) );
}

static void emitSingle( AddressingJRISC const& addr, nga::ILinkingContext & lc, int opcode, int src = 0 )
{
  auto sng = assertType<AddressingJRISC::Single>( addr );

  lc.emitBE16( compose( opcode, src, sng.dst ) );
}

template<typename F>
void emitQ( AddressingJRISC const& addr, nga::ILinkingContext & lc, int opcode, int minVal, int maxVal, F f )
{
  auto imm = assertType<AddressingJRISC::Immediate>( addr );

  auto value = imm.expr->value( lc ).evaluate( lc );
  if ( value >= minVal && value <= maxVal )
    lc.emitBE16( compose( opcode, f( value ), imm.dst ) );
  else
    throw Ex{} << "only immediate vale " << minVal << "-" << maxVal << " allowed, " << value << " given";
}

static void emitADD( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitDouble( addr, lc, 0 );
}

static void emitADDC( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitDouble( addr, lc, 1 );
}

static void emitADDQ( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitQ( addr, lc, 2, 1, 32, []( int v ) { return v & 31; } );
}

static void emitADDQT( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitQ( addr, lc, 3, 1, 32, []( int v ) { return v & 31; } );
}

static void emitSUB( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitDouble( addr, lc, 4 );
}

static void emitSUBC( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitDouble( addr, lc, 5 );
}

static void emitSUBQ( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitQ( addr, lc, 6, 1, 32, []( int v ) { return v & 31; } );
}

static void emitSUBQT( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitQ( addr, lc, 7, 1, 32, []( int v ) { return v & 31; } );
}

static void emitNEG( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitSingle( addr, lc, 8 );
}

static void emitAND( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitDouble( addr, lc, 9 );
}

static void emitOR( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitDouble( addr, lc, 10 );
}

static void emitXOR( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitDouble( addr, lc, 11 );
}

static void emitNOT( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitSingle( addr, lc, 12 );
}

static void emitBTST( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitQ( addr, lc, 13, 0, 31, []( int v ) { return v; } );
}

static void emitBSET( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitQ( addr, lc, 14, 0, 31, []( int v ) { return v; } );
}

static void emitBCLR( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitQ( addr, lc, 15, 0, 31, []( int v ) { return v; } );
}

static void emitMULT( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitDouble( addr, lc, 16 );
}

static void emitIMULT( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitDouble( addr, lc, 17 );
}

static void emitIMULTN( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitDouble( addr, lc, 18 );
}

static void emitRESMAC( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitSingle( addr, lc, 19 );
}

static void emitIMACN( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitDouble( addr, lc, 20 );
}

static void emitDIV( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitDouble( addr, lc, 21 );
}

static void emitABS( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitSingle( addr, lc, 22 );
}

static void emitSH( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitDouble( addr, lc, 23 );
}

static void emitSHLQ( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitQ( addr, lc, 24, 1, 32, []( int v ) { return 32 - v; } );
}

static void emitSHRQ( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitQ( addr, lc, 25, 1, 32, []( int v ) { return 32 - v; } );
}

static void emitSHA( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitDouble( addr, lc, 26 );
}

static void emitSHARQ( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitQ( addr, lc, 27, 1, 32, []( int v ) { return 32 - v; } );
}

static void emitROLQ( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitQ( addr, lc, 29, 1, 32, []( int v ) { return 32 - v; } );
}

static void emitROR( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitDouble( addr, lc, 28 );
}

static void emitRORQ( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitQ( addr, lc, 29, 1, 32, []( int v ) { return v & 31; } );
}

static void emitCMP( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitDouble( addr, lc, 30 );
}

static void emitCMPQ( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitQ( addr, lc, 31, -16, 15, []( int v ) { return v & 31; } );
}

static void emitSAT8( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  if ( cpu == nga::CpuType::JERRY )
    throw Ex{} << "Not supported on DSP";

  emitSingle( addr, lc, 32 );
}

static void emitSUBQMOD( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  if ( cpu == nga::CpuType::TOM )
    throw Ex{} << "Not supported on GPU";

  emitQ( addr, lc, 32, 1, 32, []( int v ) { return v & 31; } );
}

static void emitSAT16( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  if ( cpu == nga::CpuType::JERRY )
    throw Ex{} << "Not supported on DSP";

  emitSingle( addr, lc, 33 );
}

static void emitSAT16S( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  if ( cpu == nga::CpuType::TOM )
    throw Ex{} << "Not supported on GPU";

  emitSingle( addr, lc, 33 );
}

static void emitMOVE( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  if ( auto dbl = std::get_if<AddressingJRISC::Double>( &addr.mVar ) )
  {
    return lc.emitBE16( compose( 34, dbl->src, dbl->dst ) );
  }
  if ( auto sng = std::get_if<AddressingJRISC::Single>( &addr.mVar ) )
  {
    return lc.emitBE16( compose( 51, 0, sng->dst ) );
  }

  throw Ex{ "Bad addressing mode" };
}

static void emitMOVEQ( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitQ( addr, lc, 35, 0, 31, []( int v ) { return v; } );
}

static void emitMOVETA( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitDouble( addr, lc, 36 );
}

static void emitMOVEFA( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitDouble( addr, lc, 37 );
}

static void emitMOVEI( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  auto imm = assertType<AddressingJRISC::Immediate>( addr );

  auto value = imm.expr->value( lc ).evaluate( lc );
  lc.emitBE16( compose( 38, 0, imm.dst ) );
  lc.emitBE16( (uint16_t)( value & 0xffff ) );
  lc.emitBE16( (uint16_t)( value >> 16 ) );
}

static void emitLOADB( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  auto load = assertType<AddressingJRISC::Load>( addr );
  lc.emitBE16( compose( 39, load.sourceAddress, load.destinationRegister ) );
}

static void emitLOADW( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  auto load = assertType<AddressingJRISC::Load>( addr );
  lc.emitBE16( compose( 40, load.sourceAddress, load.destinationRegister ) );
}

static void emitLOAD( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  if ( auto load = std::get_if<AddressingJRISC::Load>( &addr.mVar ) )
  {
    return lc.emitBE16( compose( 41, load->sourceAddress, load->destinationRegister ) );
  }
  else if ( auto idx = std::get_if<AddressingJRISC::LoadWithIndex>( &addr.mVar ) )
  {
    if ( int opcode = idx->baseSourceAddress == 14 ? 58 : ( idx->baseSourceAddress == 15 ? 59 : 0 ) )
      return lc.emitBE16( compose( opcode, idx->index, idx->destinationRegister ) );
    else
      throw Ex{ "Load long indexed needs base register r14 or r15" };
  }
  else if ( auto disp = std::get_if<AddressingJRISC::LoadWithDisplacement>( &addr.mVar ) )
  {
    if ( int opcode = disp->baseSourceAddress == 14 ? 43 : ( disp->baseSourceAddress == 15 ? 44 : 0 ) )
    {
      auto offset = disp->displacement->value( lc ).evaluate( lc );

      if ( offset >= 1 && offset <= 32 )
        return lc.emitBE16( compose( opcode, offset & 31, disp->destinationRegister ) );
      else
        throw Ex{} << "Load long with displacement needs offset in range 1-32, " << offset << " supplied";
    }
    else
      throw Ex{ "Load long with displacement needs base register r14 or r15" };
  }

  throw Ex{ "Bad addressing mode" };
}

static void emitLOADP( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  if ( cpu == nga::CpuType::JERRY )
    throw Ex{} << "Not supported on DSP";

  auto load = assertType<AddressingJRISC::Load>( addr );
  lc.emitBE16( compose( 42, load.sourceAddress, load.destinationRegister ) );
}

static void emitSAT32S( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  if ( cpu == nga::CpuType::TOM )
    throw Ex{} << "Not supported on GPU";

  emitSingle( addr, lc, 42 );
}

static void emitSTOREB( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  auto store = assertType<AddressingJRISC::Store>( addr );
  lc.emitBE16( compose( 45, store.destinationAddress, store.sourceRegister ) );
}

static void emitSTOREW( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  auto store = assertType<AddressingJRISC::Store>( addr );
  lc.emitBE16( compose( 46, store.destinationAddress, store.sourceRegister ) );
}

static void emitSTORE( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  if ( auto store = std::get_if<AddressingJRISC::Store>( &addr.mVar ) )
  {
    return lc.emitBE16( compose( 47, store->destinationAddress, store->sourceRegister ) );
  }
  else if ( auto idx = std::get_if<AddressingJRISC::StoreWithIndex>( &addr.mVar ) )
  {
    if ( int opcode = idx->baseDestinationAddress == 14 ? 60 : ( idx->baseDestinationAddress == 15 ? 61 : 0 ) )
      return lc.emitBE16( compose( opcode, idx->index, idx->sourceRegister ) );
    else
      throw Ex{ "Store long indexed needs base register r14 or r15" };
  }
  else if ( auto disp = std::get_if<AddressingJRISC::StoreWithDisplacement>( &addr.mVar ) )
  {
    if ( int opcode = disp->baseDestinationAddress == 14 ? 49 : ( disp->baseDestinationAddress == 15 ? 50 : 0 ) )
    {
      auto offset = disp->displacement->value( lc ).evaluate( lc );

      if ( offset >= 1 && offset <= 32 )
        return lc.emitBE16( compose( opcode, offset & 31, disp->sourceRegister ) );
      else
        throw Ex{} << "Store long with displacement needs offset in range 1-32, " << offset << " supplied";
    };
  }
  else
    throw Ex{ "Store long with displacement needs base register r14 or r15" };

  throw Ex{ "Bad addressing mode" };
}

static void emitSTOREP( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  if ( cpu == nga::CpuType::JERRY )
    throw Ex{} << "Not supported on DSP";

  auto store = assertType<AddressingJRISC::Store>( addr );
  lc.emitBE16( compose( 46, store.destinationAddress, store.sourceRegister ) );
}

static void emitMIRROR( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  if ( cpu == nga::CpuType::TOM )
    throw Ex{} << "Not supported on GPU";

  emitSingle( addr, lc, 48 );
}

static int assertCC( int mnCC, int adrCC )
{
  if ( mnCC == adrCC )
    return mnCC;

  if ( mnCC == CCJRISC::T )
    return adrCC;

  if ( adrCC == CCJRISC::T )
    return mnCC;

  throw Ex{ "Bad conditional code combination" };
}

static void emitJUMP( MnemonicJRISC const& mnemonic, AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  auto jump = assertType<AddressingJRISC::Jump>( addr );

  int cc = assertCC( mnemonic.cc, jump.cc );

  lc.emitBE16( compose( 52, jump.dst, cc ) );
}

static void emitJR( MnemonicJRISC const& mnemonic, AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  auto branch = assertType<AddressingJRISC::Branch>( addr );

  int cc = assertCC( mnemonic.cc, branch.cc );

  auto value = branch.dest->value( lc );
  auto off = lc.getRel( value, 2 );

  if ( off & 1 )
    throw Ex{} << "Jump relative by odd offset " << off;

  off /= 2;

  if ( off >= -16 && off <= 15 )
    return lc.emitBE16( compose( 53, off & 31, cc ) );
  else
    throw Ex{} << "Jump relative ouf of range -16..15 words: " << off;
}

static void emitMMULT( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitDouble( addr, lc, 54 );
}

static void emitMTOI( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitDouble( addr, lc, 55 );
}

static void emitNORMI( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  emitDouble( addr, lc, 56 );
}

static void emitNOP( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  auto nil = assertType<AddressingJRISC::Nil>( addr );
  lc.emitBE16( compose( 57, 0, 0 ) );
}

static void emitSAT24( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  if ( cpu == nga::CpuType::JERRY )
    throw Ex{} << "Not supported on DSP";

  emitSingle( addr, lc, 62 );
}

static void emitPACK( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  if ( cpu == nga::CpuType::JERRY )
    throw Ex{} << "Not supported on DSP";

  emitSingle( addr, lc, 63, 1 ); // or 0 ??
}

static void emitADDQMOD( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  if ( cpu == nga::CpuType::TOM )
    throw Ex{} << "Not supported on GPU";

  emitQ( addr, lc, 63, 1, 32, []( int v ) { return v & 31; } );
}

static void emitUNPACK( AddressingJRISC const& addr, nga::ILinkingContext & lc, nga::CpuType cpu )
{
  if ( cpu == nga::CpuType::JERRY )
    throw Ex{} << "Not supported on DSP";

  emitSingle( addr, lc, 63, 0 ); // or 1 ??
}

}

using namespace cpuJRISC;

MnemonicJRISC::MnemonicJRISC( cpuJRISC::Op opcode, int cc ) : Mnemonic{ (int)opcode }, cc{ cc }
{
}

AddressingJRISC::AddressingJRISC( Single const & arg ) : mVar{ arg }
{
}

AddressingJRISC::AddressingJRISC( Double const & arg ) : mVar{ arg }
{
}

AddressingJRISC::AddressingJRISC( Immediate const & arg ) : mVar{ arg }
{
}

AddressingJRISC::AddressingJRISC( Jump const & arg ) : mVar{ arg }
{
}

AddressingJRISC::AddressingJRISC( Branch const & arg ) : mVar{ arg }
{
}

AddressingJRISC::AddressingJRISC( Load const & arg ) : mVar{ arg }
{
}

AddressingJRISC::AddressingJRISC( LoadWithDisplacement const & arg ) : mVar{ arg }
{
}

AddressingJRISC::AddressingJRISC( LoadWithIndex const & arg ) : mVar{ arg }
{
}

AddressingJRISC::AddressingJRISC( Store const & arg ) : mVar{ arg }
{
}

AddressingJRISC::AddressingJRISC( StoreWithDisplacement const & arg ) : mVar{ arg }
{
}

AddressingJRISC::AddressingJRISC( StoreWithIndex const & arg ) : mVar{ arg }
{
}

AddressingJRISC::AddressingJRISC( Nil const & arg ) : mVar{ arg }
{
}

AddressingJRISC::AddressingJRISC( Bad const & arg ) : mVar{ arg }
{
}

uint32_t AddressingJRISC::size( Mnemonic const& mnemonic, nga::ILinkingContext & lc, nga::CpuType cpu ) const
{
  auto op = ( Op )mnemonic.opcode;

  if ( op == Op::MOVEI )
    return 6;
  else
    return 2;
}

void AddressingJRISC::emit( Mnemonic const& mnemonic, nga::ILinkingContext & lc, nga::CpuType cpu ) const
{
  auto op = (Op)mnemonic.opcode;
  auto mjrisc = (MnemonicJRISC const&)mnemonic;


  try
  {
    switch ( op )
    {
    case Op::ADD:
      return emitADD( *this, lc, cpu );
    case Op::ADDC:
      return emitADDC( *this, lc, cpu );
    case Op::ADDQ:
      return emitADDQ( *this, lc, cpu );
    case Op::ADDQT:
      return emitADDQT( *this, lc, cpu );
    case Op::SUB:
      return emitSUB( *this, lc, cpu );
    case Op::SUBC:
      return emitSUBC( *this, lc, cpu );
    case Op::SUBQ:
      return emitSUBQ( *this, lc, cpu );
    case Op::SUBQT:
      return emitSUBQT( *this, lc, cpu );
    case Op::NEG:
      return emitNEG( *this, lc, cpu );
    case Op::AND:
      return emitAND( *this, lc, cpu );
    case Op::OR:
      return emitOR( *this, lc, cpu );
    case Op::XOR:
      return emitXOR( *this, lc, cpu );
    case Op::NOT:
      return emitNOT( *this, lc, cpu );
    case Op::BTST:
      return emitBTST( *this, lc, cpu );
    case Op::BSET:
      return emitBSET( *this, lc, cpu );
    case Op::BCLR:
      return emitBCLR( *this, lc, cpu );
    case Op::MULT:
      return emitMULT( *this, lc, cpu );
    case Op::IMULT:
      return emitIMULT( *this, lc, cpu );
    case Op::IMULTN:
      return emitIMULTN( *this, lc, cpu );
    case Op::RESMAC:
      return emitRESMAC( *this, lc, cpu );
    case Op::IMACN:
      return emitIMACN( *this, lc, cpu );
    case Op::DIV:
      return emitDIV( *this, lc, cpu );
    case Op::ABS:
      return emitABS( *this, lc, cpu );
    case Op::SH:
      return emitSH( *this, lc, cpu );
    case Op::SHLQ:
      return emitSHLQ( *this, lc, cpu );
    case Op::SHRQ:
      return emitSHRQ( *this, lc, cpu );
    case Op::SHA:
      return emitSHA( *this, lc, cpu );
    case Op::SHARQ:
      return emitSHARQ( *this, lc, cpu );
    case Op::ROLQ:
      return emitROLQ( *this, lc, cpu );
    case Op::ROR:
      return emitROR( *this, lc, cpu );
    case Op::RORQ:
      return emitRORQ( *this, lc, cpu );
    case Op::CMP:
      return emitCMP( *this, lc, cpu );
    case Op::CMPQ:
      return emitCMPQ( *this, lc, cpu );
    case Op::SAT8:
      return emitSAT8( *this, lc, cpu );
    case Op::SUBQMOD:
      return emitSUBQMOD( *this, lc, cpu );
    case Op::SAT16:
      return emitSAT16( *this, lc, cpu );
    case Op::SAT16S:
      return emitSAT16S( *this, lc, cpu );
    case Op::MOVE:
      return emitMOVE( *this, lc, cpu );
    case Op::MOVEQ:
      return emitMOVEQ( *this, lc, cpu );
    case Op::MOVETA:
      return emitMOVETA( *this, lc, cpu );
    case Op::MOVEFA:
      return emitMOVEFA( *this, lc, cpu );
    case Op::MOVEI:
      return emitMOVEI( *this, lc, cpu );
    case Op::LOADB:
      return emitLOADB( *this, lc, cpu );
    case Op::LOADW:
      return emitLOADW( *this, lc, cpu );
    case Op::LOAD:
      return emitLOAD( *this, lc, cpu );
    case Op::LOADP:
      return emitLOADP( *this, lc, cpu );
    case Op::SAT32S:
      return emitSAT32S( *this, lc, cpu );
    case Op::STOREB:
      return emitSTOREB( *this, lc, cpu );
    case Op::STOREW:
      return emitSTOREW( *this, lc, cpu );
    case Op::STORE:
      return emitSTORE( *this, lc, cpu );
    case Op::STOREP:
      return emitSTOREP( *this, lc, cpu );
    case Op::MIRROR:
      return emitMIRROR( *this, lc, cpu );
    case Op::JUMP:
      return emitJUMP( mjrisc, *this, lc, cpu );
    case Op::JR:
      return emitJR( mjrisc, *this, lc, cpu );
    case Op::MMULT:
      return emitMMULT( *this, lc, cpu );
    case Op::MTOI:
      return emitMTOI( *this, lc, cpu );
    case Op::NORMI:
      return emitNORMI( *this, lc, cpu );
    case Op::NOP:
      return emitNOP( *this, lc, cpu );
    case Op::SAT24:
      return emitSAT24( *this, lc, cpu );
    case Op::PACK:
      return emitPACK( *this, lc, cpu );
    case Op::ADDQMOD:
      return emitADDQMOD( *this, lc, cpu );
    case Op::UNPACK:
      return emitUNPACK( *this, lc, cpu );
    default:
      INTERNAL_ERROR;
    }
  }
  catch ( Ex const& ex )
  {
    throw Ex{} << "Opcode '" << opName( op ) << "': " << ex.what();
  }
}

}


