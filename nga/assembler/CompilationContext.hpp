#pragma once

#include "ICompilationContext.hpp"
#include "BaseGrammarStructs.hpp"
#include "CpuType.hpp"
#include "Pos.hpp"
#include "GrammarStructs.hpp"
#include "Dictionary.hpp"
#include "ScopeStack.hpp"
#include "Lexer.hpp"
#include <filesystem>

namespace nga
{
class SegmentStore;
class Segment;
class Reductor;
class IInputTextSource;
}

namespace assembler
{

struct Pos;
class ScopeStack;
class Macro;

class CompilationContext : public nga::ICompilationContext
{
public:
  CompilationContext( nga::SegmentStore & segmentStore );
  ~CompilationContext() override = default;

  std::shared_ptr<void> process( std::shared_ptr<Generator> generator ) override;
  std::shared_ptr<void> process( std::shared_ptr<LabelDecl> label ) override;
  std::shared_ptr<void> process( std::shared_ptr<MacroInv> macroInv ) override;

  std::shared_ptr<void> process( std::shared_ptr<Seg> segment ) override;
  std::shared_ptr<void> process( std::shared_ptr<Ends> endseg ) override;
  std::shared_ptr<void> process( std::shared_ptr<Local> local ) override;
  std::shared_ptr<void> process( std::shared_ptr<Endl> endl ) override;
  std::shared_ptr<void> process( std::shared_ptr<Proc> proc ) override;
  std::shared_ptr<void> process( std::shared_ptr<Endp> endp ) override;
  std::shared_ptr<void> process( std::shared_ptr<End> end ) override;
  std::shared_ptr<void> process( std::shared_ptr<Zone> zone ) override;
  std::shared_ptr<void> process( std::shared_ptr<Endz> endz ) override;
  std::shared_ptr<void> process( std::shared_ptr<Equ> equ ) override;
  std::shared_ptr<void> process( std::shared_ptr<Equr> equ ) override;
  std::shared_ptr<void> process( std::shared_ptr<Cpu> cpu ) override;
  std::shared_ptr<void> process( std::shared_ptr<Icl> file ) override;
  std::shared_ptr<void> process( std::shared_ptr<MacroDef> macroDef ) override;

  std::string_view context() const override;
  std::optional<int> regMapping( std::string_view name ) const override;
  void registerReference( assembler::NSName const& reference ) override;

  void parseFile( std::filesystem::path const& asmSrc );

private:

  struct Ctx
  {
    enum class Action
    {
      None,
      Accept,
      Shift,
      Reduce
    } action;
    int currentLALR;
  };

  void pushInputSource( std::shared_ptr<nga::IInputTextSource> src );
  std::pair<nga::Pos, std::string_view> nextLine();
  Ctx parseLALR( std::vector<nga::Token>& stack, Ctx ctx, nga::Lexeme & lexeme ) const;
  void parse( std::string_view text );
  static nga::LALRState::Action const* action( nga::LALRState const& state, size_t symbolIdx );
  nga::LALRState::Action const* currentAction( Ctx const& ctx, nga::Lexeme & lexeme ) const;
  std::span<char> macroBuffer();
  nga::Pos currentPos() const;
  void printStackTrace( Ex & ex );
 
private:
  nga::CpuType mCpu;
  nga::CpuType mNewCpu;
  std::filesystem::path mBasePath;
  nga::SegmentStore & mSegmentStore;
  std::shared_ptr<nga::Reductor> mReductor;
  std::unique_ptr<nga::Lexer> mLexer;
  std::unique_ptr<ScopeStack> mScope;
  std::vector<std::shared_ptr<nga::IInputTextSource>> mSourceStack;
  std::vector<nga::Segment*> mSegmentStack;
  nga::Dictionary<Macro> mMacros;
  std::vector<char> mMarcoBuffer;
  std::vector<std::shared_ptr<Equr>> mEqurs;
  size_t mMacroInvocationCount;

};

}