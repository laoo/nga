#pragma once

#include "Ex.hpp"
#include "ICompilationContext.hpp"
#include "BaseGrammarStructs.hpp"

namespace nga
{
class ILinkingContext;
}

namespace assembler
{
namespace cpuMOS
{
enum struct Op
{
  ADC,
  AND,
  ASL,
  BBR0,
  BBR1,
  BBR2,
  BBR3,
  BBR4,
  BBR5,
  BBR6,
  BBR7,
  BBS0,
  BBS1,
  BBS2,
  BBS3,
  BBS4,
  BBS5,
  BBS6,
  BBS7,
  BCC,
  BCS,
  BEQ,
  BIT,
  BMI,
  BNE,
  BPL,
  BRA,
  BRK,
  BVC,
  BVS,
  CLC,
  CLD,
  CLI,
  CLV,
  CMP,
  CPX,
  CPY,
  DEC,
  DEX,
  DEY,
  EOR,
  INC,
  INX,
  INY,
  JMP,
  JSR,
  LDA,
  LDX,
  LDY,
  LSR,
  NOP,
  ORA,
  PHA,
  PHP,
  PHX,
  PHY,
  PLA,
  PLP,
  PLX,
  PLY,
  RMB0,
  RMB1,
  RMB2,
  RMB3,
  RMB4,
  RMB5,
  RMB6,
  RMB7,
  ROL,
  ROR,
  RTI,
  RTS,
  SBC,
  SEC,
  SED,
  SEI,
  SMB0,
  SMB1,
  SMB2,
  SMB3,
  SMB4,
  SMB5,
  SMB6,
  SMB7,
  STA,
  STP,
  STX,
  STY,
  STZ,
  TAX,
  TAY,
  TRB,
  TSB,
  TSX,
  TXA,
  TXS,
  TYA,
  WAI,

  _SIZE
};

}

struct MnemonicMOS : public Mnemonic
{
  MnemonicMOS( int opcode );
};

class AddressingMOS : public Addressing
{
public:
  virtual ~AddressingMOS() = default;
};

class MosAbsolute : public AddressingMOS
{
  std::shared_ptr<Expression> expression;

public:

  MosAbsolute( std::shared_ptr<Expression> expression );
  ~MosAbsolute() override = default;


  uint32_t size( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const override;
  void emit( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const override;
  void visit( Mnemonic const& mnemonic, nga::ICompilationContext& cc ) override;
};

class MosAbsoluteRelative : public AddressingMOS
{
  std::shared_ptr<Expression> absolute;
  std::shared_ptr<Expression> relative;

public:

  MosAbsoluteRelative( std::shared_ptr<Expression> absolute, std::shared_ptr<Expression> relative );
  ~MosAbsoluteRelative() override = default;


  uint32_t size( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const override;
  void emit( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const override;
  void visit( Mnemonic const& mnemonic, nga::ICompilationContext& cc ) override;
};

class MosIndexedXIndirect : public AddressingMOS
{
  std::shared_ptr<Expression> expression;

public:

  MosIndexedXIndirect( std::shared_ptr<Expression> expression );
  ~MosIndexedXIndirect() override = default;

  uint32_t size( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const;
  void emit( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const;
  void visit( Mnemonic const& mnemonic, nga::ICompilationContext& cc ) override;
};

class MosIndexedX : public AddressingMOS
{
  std::shared_ptr<Expression> expression;

public:

  MosIndexedX( std::shared_ptr<Expression> expression );
  ~MosIndexedX() override = default;


  uint32_t size( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const override;
  void emit( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const override;
  void visit( Mnemonic const& mnemonic, nga::ICompilationContext& cc ) override;
};

class MosIndexedY : public AddressingMOS
{
  std::shared_ptr<Expression> expression;

public:

  MosIndexedY( std::shared_ptr<Expression> expression );
  ~MosIndexedY() override = default;


  uint32_t size( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const override;
  void emit( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const override;
  void visit( Mnemonic const& mnemonic, nga::ICompilationContext& cc ) override;
};

class MosImmediate : public AddressingMOS
{
  std::shared_ptr<Expression> expression;
  char op;

public:

  MosImmediate( std::shared_ptr<Expression> expression, char op );
  ~MosImmediate() override = default;

  uint32_t size( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const override;
  void emit( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const override;
  void visit( Mnemonic const& mnemonic, nga::ICompilationContext& cc ) override;
};

class MosImplied : public AddressingMOS
{
public:
  MosImplied();
  ~MosImplied() override = default;

  uint32_t size( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const override;
  void emit( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const override;
  void visit( Mnemonic const& mnemonic, nga::ICompilationContext& cc ) override;
};

}
