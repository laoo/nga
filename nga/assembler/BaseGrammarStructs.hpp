#pragma once

#include "IChunk.hpp"
#include "Value.hpp"
#include "CpuType.hpp"
#include "SegmentReferenceKind.hpp"


namespace nga
{
class ILinkingContext;
class ICompilationContext;
}

namespace assembler
{

struct Expression
{
  bool parenthized = false;

  virtual ~Expression() = default;

  virtual uint32_t size( nga::ILinkingContext& lc, nga::CpuType cpu ) const = 0;
  virtual nga::Value value( nga::ILinkingContext& lc ) const = 0;
  virtual void visit( nga::ILinkingContext& lc ) const {};
  virtual void visit( nga::ICompilationContext& cc ) const {};
  virtual void setSegmentReferenceKind( nga::SegmentReferenceKind kind ) {}
};


struct LitValue : public Expression
{
  LitValue( int32_t v = 0 );
  ~LitValue() override = default;

  int32_t v;

  uint32_t size( nga::ILinkingContext& lc, nga::CpuType cpu ) const override;
  nga::Value value( nga::ILinkingContext& lc ) const override;
};

struct CurrentPC : public Expression
{
  ~CurrentPC() override = default;

  uint32_t size( nga::ILinkingContext& lc, nga::CpuType cpu ) const override;
  nga::Value value( nga::ILinkingContext& lc ) const override;
};

struct AnonLabelRef : public Expression
{
  AnonLabelRef( std::string_view ctx, int distance );
  ~AnonLabelRef() override = default;

  std::string_view ctx;
  int distance;

  uint32_t size( nga::ILinkingContext& lc, nga::CpuType cpu ) const override;
  nga::Value value( nga::ILinkingContext& lc ) const override;
};

struct NSName : public Expression
{
  NSName( std::string_view ctx, std::string_view n );
  ~NSName() override = default;

  std::string_view ctx;
  std::string name;
  nga::SegmentReferenceKind kind;

  uint32_t size( nga::ILinkingContext& lc, nga::CpuType cpu ) const override;
  nga::Value value( nga::ILinkingContext& lc ) const override;
  void visit( nga::ILinkingContext& lc ) const override;
  void visit( nga::ICompilationContext& cc ) const override;
  void setSegmentReferenceKind( nga::SegmentReferenceKind kind ) override;

};

struct NamedOperator : public Expression
{
  NamedOperator( std::string operatorName, std::shared_ptr<NSName> symbolName );
  NamedOperator( std::string operatorName, std::string arg, std::shared_ptr<NSName> symbolName );
  ~NamedOperator() override = default;

  std::string operatorName;
  std::string arg;
  std::shared_ptr<NSName> symbolName;

  uint32_t size( nga::ILinkingContext& lc, nga::CpuType cpu ) const override;
  nga::Value value( nga::ILinkingContext& lc ) const override;
  void visit( nga::ILinkingContext& lc ) const override;
  void visit( nga::ICompilationContext& cc ) const override;
};

using ExprPrimary = Expression;
using ExprUnary = Expression;

struct ExprUnary_ : public Expression
{
  ExprUnary_( std::shared_ptr<Expression> unary );
  ~ExprUnary_() override = default;

  std::shared_ptr<Expression> expr;

  uint32_t size( nga::ILinkingContext& lc, nga::CpuType cpu ) const override;
  nga::Value value( nga::ILinkingContext& lc ) const override;
  void visit( nga::ICompilationContext& cc ) const override;
};

using ExprMul = Expression;

struct ExprMul_ : public Expression
{
  ExprMul_( std::shared_ptr<Expression> exprMul, char op, std::shared_ptr<Expression> exprUnary );
  ~ExprMul_() override = default;

  std::shared_ptr<Expression> exprMul;
  std::shared_ptr<Expression> exprUnary;
  char op;

  uint32_t size( nga::ILinkingContext& lc, nga::CpuType cpu ) const override;
  nga::Value value( nga::ILinkingContext& lc ) const override;
  void visit( nga::ICompilationContext& cc ) const override;
};

using ExprAdd = Expression;

struct ExprAdd_ : public Expression
{
  ExprAdd_( std::shared_ptr<Expression> exprAdd, char op, std::shared_ptr<Expression> exprMul );
  ~ExprAdd_() override = default;

  std::shared_ptr<Expression> exprAdd;
  std::shared_ptr<Expression> exprMul;
  char op;

  uint32_t size( nga::ILinkingContext& lc, nga::CpuType cpu ) const override;
  nga::Value value( nga::ILinkingContext& lc ) const override;
  void visit( nga::ICompilationContext& cc ) const override;
};

using ExprShift = Expression;


struct ExprShift_ : public Expression
{
  ExprShift_( std::shared_ptr<ExprShift> exprShift, char op, std::shared_ptr<ExprAdd> exprAdd );
  ~ExprShift_() override = default;

  std::shared_ptr<ExprShift> exprShift;
  std::shared_ptr<ExprAdd> exprAdd;
  char op;

  uint32_t size( nga::ILinkingContext& lc, nga::CpuType cpu ) const override;
  nga::Value value( nga::ILinkingContext& lc ) const override;
  void visit( nga::ICompilationContext& cc ) const override;
};

using ExprAnd = Expression;

struct ExprAnd_ : public Expression
{
  ExprAnd_( std::shared_ptr<Expression> exprAnd, std::shared_ptr<Expression> exprShift );
  ~ExprAnd_() override = default;

  std::shared_ptr<Expression> exprAnd;
  std::shared_ptr<Expression> exprShift;

  uint32_t size( nga::ILinkingContext& lc, nga::CpuType cpu ) const override;
  nga::Value value( nga::ILinkingContext& lc ) const override;
  void visit( nga::ICompilationContext& cc ) const override;
};

using ExprOr = Expression;

struct ExprOr_ : public Expression
{
  ExprOr_( std::shared_ptr<Expression> exprOr, std::shared_ptr<Expression> exprAnd );
  ~ExprOr_() override = default;

  std::shared_ptr<Expression> exprOr;
  std::shared_ptr<Expression> exprAnd;

  uint32_t size( nga::ILinkingContext& lc, nga::CpuType cpu ) const override;
  nga::Value value( nga::ILinkingContext& lc ) const override;
  void visit( nga::ICompilationContext& cc ) const override;
};

struct Mnemonic
{
  int opcode;

  Mnemonic( int opcode );
};

class Addressing
{
public:
  virtual ~Addressing() = default;

  virtual uint32_t size( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const = 0;
  virtual void emit( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const = 0;
  virtual void visit( Mnemonic const& mnemonic, nga::ICompilationContext& cc ) = 0;
};

class Generator : public nga::IChunk
{
  nga::Pos pos;
  nga::CpuType cpu;

public:

  Generator();
  ~Generator() override = default;

  uint32_t size( nga::ILinkingContext& lc ) const override;
  void emit( nga::ILinkingContext& lc ) const;
  void setPosCpu( nga::Pos pos, nga::CpuType cpu ) override;
  nga::Pos getPos() const override;
  std::tuple<bool, std::string_view, nga::Pos> listingLine() const override;

protected:
  virtual uint32_t size( nga::ILinkingContext& lc, nga::CpuType cpu ) const = 0;
  virtual void emit( nga::ILinkingContext& lc, nga::CpuType cpu ) const = 0;

};

class Opcode : public Generator
{
  std::shared_ptr<Mnemonic> mnemonic;
  std::shared_ptr<Addressing> addressing;

public:
  Opcode( std::shared_ptr<Mnemonic> mnemonic, std::shared_ptr<Addressing> addressing );
  ~Opcode() override = default;

  uint32_t size( nga::ILinkingContext& lc, nga::CpuType cpu ) const override;
  void emit( nga::ILinkingContext& lc, nga::CpuType cpu ) const override;
  void visit( nga::ICompilationContext& cc );
};

class DS : public Generator
{
  std::shared_ptr<Expression> expression;
  uint16_t multiplier;
public:
  DS( std::shared_ptr<Expression> expression, uint16_t size = 0 );
  ~DS() override = default;

  uint32_t size( nga::ILinkingContext& lc, nga::CpuType cpu ) const override;
  void emit( nga::ILinkingContext& lc, nga::CpuType cpu ) const override;
};

struct ExprList
{
  std::vector<std::shared_ptr<Expression>> list;

  uint32_t size( nga::ILinkingContext& lc, nga::CpuType cpu ) const;
  void emit( int size, nga::ILinkingContext& lc, nga::CpuType cpu ) const;
};

class DataByte : public Generator
{
  std::shared_ptr<ExprList> exprList;
  std::string string;
public:
  DataByte( std::shared_ptr<ExprList> exprList );
  DataByte( std::string_view string );
  ~DataByte() override = default;

  uint32_t size( nga::ILinkingContext& lc, nga::CpuType cpu ) const override;
  void emit( nga::ILinkingContext& lc, nga::CpuType cpu ) const override;
};


struct ArgList : public std::vector<std::string>
{
};


struct MacroDef
{
  std::string name{};
  std::shared_ptr<ArgList> argList{};
};

struct LabelDecl
{
  std::string id; //empty string denotes anonymous label
};

struct LabelOpt
{
};

struct Icl
{
  std::string icl;
};

struct Cpu
{
  nga::CpuType type;
};

struct Equ
{
  std::string label;
  std::shared_ptr<Expression> expression;
};

struct Equr
{
  std::string label;
  int reg;
};

struct End
{
  std::string id;
};

struct Endz
{
};

struct Zone
{
};

struct Endp
{
};

struct Proc
{
  std::string id;
};

struct Endl
{
};

struct Local
{
  std::string id;
};

struct Ends
{
};

struct Seg
{
  std::string id;
};

struct MacroInvParams
{
  MacroInvParams();

  std::vector<std::string> params;
};

struct MacroInv
{
  std::string name;
  std::vector<std::string> params;
};

struct Declarator
{
};

}
