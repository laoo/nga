#pragma once

#include "Ex.hpp"
#include "SegmentReferenceKind.hpp"
#include "ICompilationContext.hpp"
#include "BaseGrammarStructs.hpp"

namespace nga
{
class ILinkingContext;
}

namespace assembler
{
namespace cpuM68K
{
enum struct Op
{
  ABCD =  0x0000'0000 | 0b1100'000'100'000'000,
  ADD =   0x0001'0000 | 0b1101'000'000'000'000,
  ADDA =  0x0002'0000 | 0b1101'000'011'000'000,
  ADDI =  0x0003'0000 | 0b0000'011'000'000'000,
  ADDQ =  0x0004'0000 | 0b0101'000'000'000'000,
  ADDX =  0x0005'0000 | 0b1101'000'100'000'000,
  AND =   0x0006'0000 | 0b1100'000'000'000'000,
  ANDI =  0x0007'0000 | 0b0000'001'000'000'000,
  ASL =   0x0008'0000 | 0b1110'000'100'000'000,
  ASR =   0x0009'0000 | 0b1110'000'000'000'000,
  BCC =   0x000a'0000 | 0b0110'010'000'000'000,
  BCS =   0x000b'0000 | 0b0110'010'100'000'000,
  BEQ =   0x000c'0000 | 0b0110'011'100'000'000,
  BGE =   0x000d'0000 | 0b0110'110'000'000'000,
  BGT =   0x000e'0000 | 0b0110'111'000'000'000,
  BHI =   0x000f'0000 | 0b0110'001'000'000'000,
  BLE =   0x0010'0000 | 0b0110'111'100'000'000,
  BLS =   0x0011'0000 | 0b0110'001'100'000'000,
  BLT =   0x0012'0000 | 0b0110'110'100'000'000,
  BMI =   0x0013'0000 | 0b0110'101'100'000'000,
  BNE =   0x0014'0000 | 0b0110'011'000'000'000,
  BPL =   0x0015'0000 | 0b0110'101'000'000'000,
  BRA =   0x0016'0000 | 0b0110'000'000'000'000,
  BSR =   0x0017'0000 | 0b0110'000'100'000'000,
  BVC =   0x0018'0000 | 0b0110'100'000'000'000,
  BVS =   0x0019'0000 | 0b0110'100'100'000'000,
  BCHG =  0x001a'0000 | 0b0000'000'001'000'000,
  BCLR =  0x001b'0000 | 0b0000'000'010'000'000,
  BSET =  0x001c'0000 | 0b0000'000'011'000'000,
  BTST =  0x001d'0000 | 0b0000'000'000'000'000,
  CHK =   0x001e'0000 | 0b0100'000'110'000'000,
  CLR =   0x001f'0000 | 0b0100'001'000'000'000,
  CMP =   0x0020'0000 | 0b1011'000'000'000'000,
  CMPA =  0x0021'0000 | 0b1011'000'011'000'000,
  CMPI =  0x0022'0000 | 0b0000'110'000'000'000,
  CMPM =  0x0023'0000 | 0b1011'000'100'001'000,
  DBCC =  0x0024'0000 | 0b0101'010'011'001'000,
  DBCS =  0x0025'0000 | 0b0101'010'111'001'000,
  DBEQ =  0x0026'0000 | 0b0101'011'111'001'000,
  DBF =   0x0027'0000 | 0b0101'000'111'001'000,
  DBGE =  0x0028'0000 | 0b0101'110'011'001'000,
  DBGT =  0x0029'0000 | 0b0101'111'011'001'000,
  DBHI =  0x002a'0000 | 0b0101'001'011'001'000,
  DBLE =  0x002b'0000 | 0b0101'111'111'001'000,
  DBLS =  0x002c'0000 | 0b0101'001'111'001'000,
  DBLT =  0x002d'0000 | 0b0101'110'111'001'000,
  DBMI =  0x002e'0000 | 0b0101'101'111'001'000,
  DBNE =  0x002f'0000 | 0b0101'011'011'001'000,
  DBPL =  0x0030'0000 | 0b0101'101'011'001'000,
  DBT =   0x0031'0000 | 0b0101'000'011'001'000,
  DBVC =  0x0032'0000 | 0b0101'100'011'001'000,
  DBVS =  0x0033'0000 | 0b0101'100'111'001'000,
  DIVS =  0x0034'0000 | 0b1000'000'111'000'000,
  DIVU =  0x0035'0000 | 0b1000'000'011'000'000,
  EOR =   0x0036'0000 | 0b1011'000'100'000'000,
  EORI =  0x0037'0000 | 0b0000'101'000'000'000,
  EXG =   0x0038'0000 | 0b1100'000'100'000'000,
  EXT =   0x0039'0000 | 0b0100'100'010'000'000,
  ILLEGAL=0x003a'0000 | 0b0100'101'011'111'100,
  JMP =   0x003b'0000 | 0b0100'111'011'000'000,
  JSR =   0x003c'0000 | 0b0100'111'010'000'000,
  LEA =   0x003d'0000 | 0b0100'000'111'000'000,
  LINK =  0x003e'0000 | 0b0100'111'001'010'000,
  LSL =   0x003f'0000 | 0b1110'001'100'000'000,
  LSR =   0x0040'0000 | 0b1110'001'000'000'000,
  MOVE =  0x0041'0000 | 0b0000'000'000'000'000,
  MOVEA = 0x0042'0000 | 0b0000'000'001'000'000,
  MOVEM = 0x0043'0000 | 0b0100'100'010'000'000,
  MOVEP = 0x0044'0000 | 0b0000'000'100'001'000,
  MOVEQ = 0x0045'0000 | 0b0111'000'000'000'000,
  MULS =  0x0046'0000 | 0b1100'000'111'000'000,
  MULU =  0x0047'0000 | 0b1100'000'011'000'000,
  NBCD =  0x0048'0000 | 0b0100'100'000'000'000,
  NEG =   0x0049'0000 | 0b0100'010'000'000'000,
  NEGX =  0x004a'0000 | 0b0100'000'000'000'000,
  NOP =   0x004b'0000 | 0b0100'111'001'110'001,
  NOT =   0x004c'0000 | 0b0100'011'000'000'000,
  OR =    0x004d'0000 | 0b1000'000'000'000'000,
  ORI =   0x004e'0000 | 0b0000'000'000'000'000,
  PEA =   0x004f'0000 | 0b0100'100'001'000'000,
  RESET = 0x0050'0000 | 0b0100'111'001'110'000,
  ROL =   0x0051'0000 | 0b1110'011'100'000'000,
  ROR =   0x0052'0000 | 0b1110'011'000'000'000,
  ROXL =  0x0053'0000 | 0b1110'010'100'000'000,
  ROXR =  0x0054'0000 | 0b1110'010'000'000'000,
  RTE =   0x0055'0000 | 0b0100'111'001'110'011,
  RTR =   0x0056'0000 | 0b0100'111'001'110'111,
  RTS =   0x0057'0000 | 0b0100'111'001'110'101,
  SBCD =  0x0058'0000 | 0b1000'000'100'000'000,
  SCC =   0x0059'0000 | 0b0101'010'011'000'000,
  SCS =   0x005a'0000 | 0b0101'010'111'000'000,
  SEQ =   0x005b'0000 | 0b0101'011'111'000'000,
  SF =    0x005c'0000 | 0b0101'000'111'000'000,
  SGE =   0x005d'0000 | 0b0101'110'011'000'000,
  SGT =   0x005e'0000 | 0b0101'111'011'000'000,
  SHI =   0x005f'0000 | 0b0101'001'011'000'000,
  SLE =   0x0060'0000 | 0b0101'111'111'000'000,
  SLS =   0x0061'0000 | 0b0101'001'111'000'000,
  SLT =   0x0062'0000 | 0b0101'110'111'000'000,
  SMI =   0x0063'0000 | 0b0101'101'111'000'000,
  SNE =   0x0064'0000 | 0b0101'011'011'000'000,
  SPL =   0x0065'0000 | 0b0101'101'011'000'000,
  ST =    0x0066'0000 | 0b0101'000'011'000'000,
  SVC =   0x0067'0000 | 0b0101'100'011'000'000,
  SVS =   0x0068'0000 | 0b0101'100'111'000'000,
  STOP =  0x0069'0000 | 0b0100'111'001'110'010,
  SUB =   0x006a'0000 | 0b1001'000'000'000'000,
  SUBA =  0x006b'0000 | 0b1001'000'011'000'000,
  SUBI =  0x006c'0000 | 0b0000'010'000'000'000,
  SUBQ =  0x006d'0000 | 0b0101'000'100'000'000,
  SUBX =  0x006e'0000 | 0b1001'000'100'000'000,
  SWAP =  0x006f'0000 | 0b0100'100'001'000'000,
  TAS =   0x0070'0000 | 0b0100'101'011'000'000,
  TRAP =  0x0071'0000 | 0b0100'111'001'000'000,
  TRAPV = 0x0072'0000 | 0b0100'111'001'110'110,
  TST =   0x0073'0000 | 0b0100'101'000'000'000,
  UNLK =  0x0074'0000 | 0b0100'111'001'011'000,
   
  _SIZE = 0x0075'0000
};

using AdrType = uint16_t;

static constexpr AdrType ADR_NONE = 0x0000;
static constexpr AdrType ADR_DATA = 0x0001;
static constexpr AdrType ADR_ADDRESS = 0x0002;
static constexpr AdrType ADR_CCR = 0x0004;
static constexpr AdrType ADR_SR = 0x0008;
static constexpr AdrType ADR_USP = 0x0010;
static constexpr AdrType ADR_IND = 0x0020;
static constexpr AdrType ADR_IND_POST = 0x0040;
static constexpr AdrType ADR_IND_PRE = 0x0080;
static constexpr AdrType ADR_IND_DISP = 0x0100;
static constexpr AdrType ADR_IND_IDX = 0x0200;
static constexpr AdrType ADR_PCIND_DISP = 0x0400;
static constexpr AdrType ADR_PCIND_IDX = 0x0800;
static constexpr AdrType ADR_ABS = 0x1000;
static constexpr AdrType ADR_IMM = 0x2000;

}

struct SizeM68K
{
  enum Size : uint16_t
  {
    SIZE_NONE = 0b00,
    SIZE_BYTE = 0b01,
    SIZE_WORD = 0b10,
    SIZE_LONG = 0b11,
  } size;
};

using SizeM68KBranch = SizeM68K;
  
struct MnemonicM68K : public Mnemonic
{
  SizeM68K::Size size;

  MnemonicM68K( int opcode, SizeM68K::Size size = SizeM68K::SIZE_NONE );

  int sizeValue() const;
  int sizeWordLong() const;
};

struct M68KXn
{
  enum struct RegType : uint8_t
  {
    NONE,
    DATA,
    ADDRESS,
  } regType;

  uint8_t regNumber;

  M68KXn( RegType type = RegType::NONE, int number = 0 );

  uint16_t rangeBit() const;
};


struct M68KXnSized : public M68KXn
{
  SizeM68K::Size size;

  M68KXnSized( M68KXn src = {}, SizeM68K::Size size = SizeM68K::SIZE_NONE );

  uint16_t briefExtensionWord( int8_t displacement ) const;
};

struct M68KRegRange
{
  //lsb: d0
  //msb: a7
  //all bits between must be set
  uint16_t data;
  M68KRegRange( M68KXn const& from, M68KXn const& to );
};

struct M68KRegList : M68KXn
{
  //lsb: d0
  //msb: a7
  uint16_t data1;

  M68KRegList();
  M68KRegList( M68KXn const& reg );
  M68KRegList( M68KRegRange const& range );

  void add( M68KXn const& reg );
  void add( M68KRegRange const& range );

  bool isList() const;
};


class AddressingM68K : public Addressing
{
public:
  virtual ~AddressingM68K() = default;
};


class SingleAddressM68K : public AddressingM68K
{
public:

  SingleAddressM68K( cpuM68K::AdrType type );
  SingleAddressM68K( M68KRegList list );
  SingleAddressM68K( cpuM68K::AdrType type, M68KRegList reg );
  SingleAddressM68K( cpuM68K::AdrType type, std::shared_ptr<Expression> expr );
  SingleAddressM68K( cpuM68K::AdrType type, std::shared_ptr<Expression> expr, M68KRegList list );
  SingleAddressM68K( cpuM68K::AdrType type, std::shared_ptr<Expression> expr, M68KRegList list, M68KXnSized index );
  SingleAddressM68K( cpuM68K::AdrType type, std::shared_ptr<Expression> expr, M68KXnSized index );
  SingleAddressM68K( std::shared_ptr<Expression> expr, SizeM68K exprSize );
  ~SingleAddressM68K() override = default;

  uint32_t size( Mnemonic const& mnemonic, nga::ILinkingContext & lc, nga::CpuType cpu ) const override;
  void emit( Mnemonic const& mnemonic, nga::ILinkingContext & lc, nga::CpuType cpu ) const override;
  void visit( Mnemonic const& mnemonic, nga::ICompilationContext& cc ) override;

  bool isType( cpuM68K::AdrType t ) const;
  uint32_t exprSize( nga::ILinkingContext& lc ) const;
  int qData( MnemonicM68K const& mnemonic, nga::ILinkingContext& lc ) const;
  uint32_t maskedSize( nga::ILinkingContext& lc, cpuM68K::AdrType mask, MnemonicM68K const* mnem = nullptr ) const;
  uint16_t memoryAddressing( nga::ILinkingContext& lc ) const;
  std::pair<int,int> memoryDescriptor( nga::ILinkingContext& lc ) const;
  void emitExtensionWord( MnemonicM68K const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu, int extensionWordOffset ) const;
  std::shared_ptr<Expression> expression() const;
  int extraRegValue() const;
  int regValue() const;
  cpuM68K::AdrType type() const;
  M68KRegList regList() const;
  int emitImmediate( MnemonicM68K const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const;
  M68KXn::RegType indexType() const;

private:
  void emitTrap( MnemonicM68K const& mnemonic, nga::ILinkingContext& lc ) const;
  void emitStop( MnemonicM68K const& mnemonic, nga::ILinkingContext& lc ) const;
  void emitBranch( MnemonicM68K const& mnemonic, nga::ILinkingContext& lc ) const;
  void emitIndirectIndex( nga::ILinkingContext& lc, cpuM68K::Op op, std::optional<int> extensionWordOffset ) const;
  void emitIndirectIndex( int distance, cpuM68K::Op op, nga::ILinkingContext& lc ) const;
  void emitIndirectDisplacement( nga::ILinkingContext& lc, cpuM68K::Op op, std::optional<int> extensionWordOffset ) const;
  void emitAbsolute( MnemonicM68K const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const;
  void emitIndirectDisplacement( int distance, assembler::cpuM68K::Op op, nga::ILinkingContext& lc ) const;

private:
  cpuM68K::AdrType mType;
  M68KRegList mRegList;
  M68KXnSized mIndex;  //Xn
  std::shared_ptr<Expression> mExpr;
  SizeM68K mExprSize;
};

class DoubleAddressM68K : public AddressingM68K
{
  std::shared_ptr<SingleAddressM68K> src;
  std::shared_ptr<SingleAddressM68K> dst;

public:
  DoubleAddressM68K( std::shared_ptr<SingleAddressM68K> src, std::shared_ptr<SingleAddressM68K> dst );
  ~DoubleAddressM68K() override = default;

  uint32_t size( Mnemonic const& mnemonic, nga::ILinkingContext & lc, nga::CpuType cpu ) const override;
  void emit( Mnemonic const& mnemonic, nga::ILinkingContext & lc, nga::CpuType cpu ) const override;
  void visit( Mnemonic const& mnemonic, nga::ICompilationContext& cc ) override;

private:
  void emitImmediate( MnemonicM68K const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const;
  void emitRotation( MnemonicM68K const& mnemonic, nga::ILinkingContext& lc ) const;
  void emitBRMW( MnemonicM68K const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const;
  void emitBranch( MnemonicM68K const& mnemonic, nga::ILinkingContext& lc, bool link ) const;
  void emitMove( MnemonicM68K const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const;
  void emitMovem( MnemonicM68K const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const;
  void emitMovep( MnemonicM68K const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const;
  void emitMoveq( MnemonicM68K const& mnemonic, nga::ILinkingContext& lc ) const;
};

class ImpliedAddressM68K : public AddressingM68K
{
public:
  ImpliedAddressM68K();
  ~ImpliedAddressM68K() override = default;

  uint32_t size( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const override;
  void emit( Mnemonic const& mnemonic, nga::ILinkingContext& lc, nga::CpuType cpu ) const override;
  void visit( Mnemonic const& mnemonic, nga::ICompilationContext& cc ) override {};
};

}
