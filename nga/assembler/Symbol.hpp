#pragma once

#include "ISymbol.hpp"
#include "Segment.hpp"
#include "Pos.hpp"

namespace assembler
{
struct Expression;

class Symbol : public nga::ISymbol
{
public:
  Symbol( nga::Pos pos, std::shared_ptr<Expression> expr );
  Symbol( nga::Pos pos, nga::Segment::ChunkOffset offset );

  ~Symbol() override;

  uint32_t size( nga::ILinkingContext & lc, nga::CpuType cpu ) const override;
  nga::Value value( nga::ILinkingContext & lc ) const override;
  nga::Segment* segment() const override;
  void visit( nga::ILinkingContext& lc, nga::SegmentReferenceKind kind ) const override;

private:
  nga::Pos mPos;
  std::variant<
    std::shared_ptr<Expression>,
    nga::Segment::ChunkOffset
  > mVar;

};

}