#include "pch.hpp"
#include "CompilationContext.hpp"
#include "SegmentStore.hpp"
#include "Segment.hpp"
#include "Ex.hpp"
#include "ScopeStack.hpp"
#include "Symbol.hpp"
#include "GrammarStructs.hpp"
#include "SourceTextFile.hpp"
#include "Macro.hpp"
#include "Lexer.hpp"
#include "Project.hpp"
#include "ReductorServer.hpp"
#include "SegmentReferenceKind.hpp"

namespace assembler
{

CompilationContext::CompilationContext( nga::SegmentStore & segmentStore ) :
  mCpu{ nga::CpuType::UNDEFINED },
  mNewCpu{ nga::CpuType::NO_CPU },
  mBasePath{},
  mSegmentStore{ segmentStore },
  mReductor{},
  mLexer{},
  mScope{ std::make_unique<ScopeStack>() },
  mSourceStack{},
  mSegmentStack{},
  mMacros{},
  mMarcoBuffer{},
  mEqurs{},
  mMacroInvocationCount{}
{
  mMarcoBuffer.resize( nga::project().config().getMaxMacroExpansion() );
}

std::shared_ptr<void> CompilationContext::process( std::shared_ptr<Generator> generator )
{
  if ( mSegmentStack.empty() )
  {
    throw Ex{ "Can't place code or data outside of a segment" };
  }

  auto const& seg = mSegmentStack.back();

  generator->setPosCpu( currentPos(), mCpu );
  seg->append( std::move( generator ) );

  return {};
}

std::shared_ptr<void> CompilationContext::process( std::shared_ptr<LabelDecl> label )
{
  if ( mSegmentStack.empty() )
  {
    throw Ex{ "Can't place labels outside of a segment" };
  }

  auto const& seg = mSegmentStack.back();

  if ( label->id.empty() )
  {
    seg->addAnonymousLabel();
  }
  else
  {
    mSegmentStore.declareSymbol( mScope->nsName( label->id ), std::make_shared<Symbol>( currentPos(), seg->chunkOffset() ) );
  }

  return {};
}

std::shared_ptr<void> CompilationContext::process( std::shared_ptr<Proc> proc )
{
  if ( mSegmentStack.empty() )
  {
    throw Ex{ "Can't place proc outside of a segment" };
  }

  auto const& seg = mSegmentStack.back();

  mSegmentStore.declareSymbol( mScope->nsName( proc->id ), std::make_shared<Symbol>( currentPos(), seg->chunkOffset() ) );
  mScope->push( NSType::PROC, proc->id, currentPos().pos );
  return {};
}

std::shared_ptr<void> CompilationContext::process( std::shared_ptr<Equ> equ )
{
  mSegmentStore.declareSymbol( mScope->nsName( equ->label ), std::make_shared<Symbol>( currentPos(), std::move( equ->expression ) ) );
  return {};
}

std::shared_ptr<void> CompilationContext::process( std::shared_ptr<Equr> equr )
{
  auto it = std::ranges::find_if( mEqurs, [&]( auto const& e ) { return e->label == equr->label; } );
  if ( it == mEqurs.end() )
  {
    mEqurs.push_back( std::move( equr ) );
  }
  else
  {
    ( *it )->reg = equr->reg;
  }
  return {};
}

std::shared_ptr<void> CompilationContext::process( std::shared_ptr<MacroInv> macroInv )
{
  auto ctx = mScope->context();

  auto pMacro = mMacros.find( ctx, macroInv->name );

  if ( !pMacro )
    throw Ex{} << "Macro '" << macroInv->name << "' not found in context '" << ctx << "'";

  pushInputSource( pMacro->invocate( std::move( macroInv->params ), mMacroInvocationCount++, mMarcoBuffer ) );
  return {};
}

std::shared_ptr<void> CompilationContext::process( std::shared_ptr<Seg> segment )
{
  auto pSegment = mSegmentStore.newSegment();
  assert( pSegment );
  pSegment->setSegmentDescriptorName( segment->id );

  mSegmentStack.push_back( pSegment );
  return {};
}

std::shared_ptr<void> CompilationContext::process( std::shared_ptr<Ends> ends )
{
  if ( mSegmentStack.empty() )
  {
    throw Ex{ ".ends without matching .seg" };
  }
  else
  {
    mSegmentStack.pop_back();
  }
  return {};
}

std::shared_ptr<void> CompilationContext::process( std::shared_ptr<Local> local )
{
  mScope->push( NSType::LOCAL, local->id, currentPos().pos );
  return {};
}

std::shared_ptr<void> CompilationContext::process( std::shared_ptr<Zone> zone )
{
  if ( mSegmentStack.empty() )
    throw Ex{ ".zone allowed only inside of a segment" };

  auto z = mSegmentStack.back()->newZone();

  std::string id( (char const*)&z, 1 );

  mScope->push( NSType::ZONE, std::move( id ), currentPos().pos );
  return {};
}


std::shared_ptr<void> CompilationContext::process( std::shared_ptr<End> end )
{
  auto [success, entry] = mScope->pop( end->id );
  if ( entry )
  {
    Ex ex{};
    ex << "Unmatched scope. Expected ";
    switch ( entry->type )
    {
    case NSType::LOCAL:
      ex << "local ";
      break;
    case NSType::PROC:
      ex << "proc ";
      break;
    case NSType::ZONE:
      ex << "zone ";
      break;
    default:
      ex << "UNEXPECTED ";
      INTERNAL_ERROR;
      break;
    }
    ex << entry->name << " opened at line " << entry->openingRow;
    throw ex;
  }
  else if ( !success )
  {
    throw Ex{ "No scope to close" };
  }
  return {};
}

std::shared_ptr<void> CompilationContext::process( std::shared_ptr<Endl> end )
{
  auto [success, entry] = mScope->pop( NSType::LOCAL );
  if ( entry )
  {
    Ex ex{};
    ex << "Unmatched endl. Expected ";
    switch ( entry->type )
    {
    case NSType::PROC:
      ex << ".endp ";
      break;
    case NSType::ZONE:
      ex << ".endz ";
      break;
    default:
      ex << "UNEXPECTED ";
      INTERNAL_ERROR;
      break;
    }
    ex << entry->name << " opened at line " << entry->openingRow;
    throw ex;
  }
  else if ( !success )
  {
    throw Ex{ "No scope to close" };
  }
  return {};
}

std::shared_ptr<void> CompilationContext::process( std::shared_ptr<Endz> end )
{
  auto [success, entry] = mScope->pop( NSType::ZONE );
  if ( entry )
  {
    Ex ex{};
    ex << "Unmatched endz. Expected ";
    switch ( entry->type )
    {
    case NSType::PROC:
      ex << ".endp ";
      break;
    case NSType::LOCAL:
      ex << ".endl ";
      break;
    default:
      ex << "UNEXPECTED ";
      INTERNAL_ERROR;
      break;
    }
    ex << entry->name << " opened at line " << entry->openingRow;
    throw ex;
  }
  else if ( !success )
  {
    throw Ex{ "No scope to close" };
  }
  return {};
}

std::shared_ptr<void> CompilationContext::process( std::shared_ptr<Endp> end )
{
  auto [success, entry] = mScope->pop( NSType::PROC );
  if ( entry )
  {
    Ex ex{};
    ex << "Unmatched endp. Expected ";
    switch ( entry->type )
    {
    case NSType::LOCAL:
      ex << ".endl ";
      break;
    case NSType::ZONE:
      ex << ".endz ";
      break;
    default:
      ex << "UNEXPECTED ";
      INTERNAL_ERROR;
      break;
    }
    ex << entry->name << " opened at line " << entry->openingRow;
    throw ex;
  }
  else if ( !success )
  {
    throw Ex{ "No scope to close" };
  }
  return {};
}


std::shared_ptr<void> CompilationContext::process( std::shared_ptr<Cpu> cpu )
{
  mNewCpu = cpu->type;
  return {};
}

std::shared_ptr<void> CompilationContext::process( std::shared_ptr<Icl> icl )
{
  auto path = mBasePath / std::filesystem::path{ icl->icl };
  pushInputSource( std::make_shared<nga::SourceTextFile>( std::filesystem::relative( path ) ) );
  return {};
}

std::shared_ptr<void> CompilationContext::process( std::shared_ptr<MacroDef> macroDef )
{
  auto name = mScope->nsName( macroDef->name );

  if ( auto macro = mMacros.find( name ) )
    throw Ex{} << "Macro " << name << " redefinition. Previously defined at " << nga::project().posString( macro->pos() );

  auto pMacro = std::make_shared<Macro>( currentPos(), name, macroDef->argList ? macroDef->argList->size() : 0 );
  if ( macroDef->argList )
  {
    for ( auto& param : *macroDef->argList )
    {
      pMacro->addParamName( std::move( param ) );
    }
  }

  for ( ;; )
  {
    auto pair = mSourceStack.back()->nextLine();
    if ( pair.first )
    {
      if ( pMacro->addLine( pair ) )
        break;
    }
    else
      throw Ex{ "No .endm found" };
  }

  mMacros.add( name, std::move( pMacro ) );
  
  return {};
}

std::string_view CompilationContext::context() const
{
  return mScope->context();
}

std::optional<int> CompilationContext::regMapping( std::string_view name ) const
{
  auto it = std::ranges::find_if( mEqurs, [=]( auto const& e ) { return e->label == name; } );
  if ( it != mEqurs.cend() )
  {
    return ( *it )->reg;
  }
  else
  {
    return {};
  }
}

void CompilationContext::registerReference( assembler::NSName const& reference )
{
  if ( !mSegmentStack.empty() )
  {
    size_t nameSize = reference.name.size();
    std::string str( nameSize + reference.ctx.size() + 1, '#' );
    std::copy_n( reference.name.begin(), nameSize, str.begin() );
    std::copy_n( reference.ctx.begin(), reference.ctx.size(), str.begin() + nameSize + 1 );

    mSegmentStack.back()->registerReference( std::move( str ), reference.kind );
  }
}

void CompilationContext::parseFile( std::filesystem::path const& asmSrc )
{
  mBasePath = std::filesystem::absolute( asmSrc ).parent_path();

  pushInputSource( std::make_shared<nga::SourceTextFile>( asmSrc ) );

  try
  {
    for ( ;; )
    {
      auto pair = nextLine();
      if ( !pair.first )
        break;
      parse( pair.second );
    }
  }
  catch ( Ex& ex )
  {
    printStackTrace( ex );
    throw;
  }
}

void CompilationContext::pushInputSource( std::shared_ptr<nga::IInputTextSource> src )
{
  nga::project().registerSource( src );
  mSourceStack.push_back( src );
}

std::pair<nga::Pos, std::string_view> CompilationContext::nextLine()
{
  while ( !mSourceStack.empty() )
  {
    auto next = mSourceStack.back()->nextLine();

    if ( next.first )
    {
      return next;
    }
    else
    {
      mSourceStack.pop_back();
    }
  }

  return {};
}

nga::LALRState::Action const* CompilationContext::currentAction( Ctx const& ctx, nga::Lexeme & lexeme ) const
{
  if ( nga::LALRState::Action const* pAction = action( mReductor->lalrTable[ctx.currentLALR], lexeme.symbol->idx ) )
    return pAction;
  else if ( mLexer->setMacroMode( lexeme ) )
  {
    return action( mReductor->lalrTable[ctx.currentLALR], lexeme.symbol->idx );
  }
  else
  {
    return nullptr;
  }
}

std::span<char> CompilationContext::macroBuffer()
{
  return std::span<char>( mMarcoBuffer.data(), mMarcoBuffer.size() );
}

nga::Pos CompilationContext::currentPos() const
{
  if ( mSourceStack.empty() )
    return {};
  else
    return mSourceStack.back()->currentPos();
}

void CompilationContext::printStackTrace( Ex& ex )
{
  while ( !mSourceStack.empty() )
  {
    auto currentLine = mSourceStack.back()->currentLine();
    ex.addStackTrace( nga::project().posString( currentLine.first ), std::string{ currentLine.second } );
    mSourceStack.pop_back();
  }
}


CompilationContext::Ctx CompilationContext::parseLALR( std::vector<nga::Token>& stack, Ctx ctx, nga::Lexeme & lexeme ) const
{
  nga::LALRState::Action const* pAction = currentAction( ctx, lexeme );

  if ( !pAction )
  {
    throw Ex{} << "Unexpected token '" << lexeme.content << "'";
  }

  switch ( pAction->type )
  {
  case nga::LALRState::Action::Shift:
  {
    stack.push_back( nga::Token{ lexeme, ctx.currentLALR = pAction->targetIndex, {} } );
    ctx.action = Ctx::Action::Shift;
    return ctx;
  }
  case nga::LALRState::Action::Reduce:
  {
    nga::Production const& p = mReductor->productionTable[pAction->targetIndex];
    auto node = mReductor->reduce( stack, pAction->targetIndex );
    int state = stack.empty() ? mReductor->initialLALRState : stack.back().state;
    pAction = action( mReductor->lalrTable[state], p.nonterminal );
    if ( !pAction )
    {
      throw Ex{ "ParseError" };
    }
    nga::Lexeme l{ &mReductor->symbolTable[p.nonterminal], std::string_view{} };
    stack.push_back( nga::Token{ l, ctx.currentLALR = pAction->targetIndex, std::move( node ) } );
    ctx.action = Ctx::Action::Reduce;
    return ctx;
  }
  case nga::LALRState::Action::Accept:
    ctx.action = Ctx::Action::Accept;
    return ctx;
  default:
    throw Ex{ "ParseError" };
  }
}

void CompilationContext::parse( std::string_view text )
{
  if ( mCpu != mNewCpu )
  {
    mCpu = mNewCpu;
    mReductor = getReductor( this, mCpu );
    mLexer = std::make_unique<nga::Lexer>( mReductor );
  }

  Ctx ctx{ Ctx::Action::None, mReductor->initialLALRState };
  std::vector<nga::Token> stack;
  mLexer->resetText( text );

  while ( ctx.action != Ctx::Action::Accept )
  {
    if ( auto lexeme = mLexer->produceLexeme(); lexeme.symbol->type != nga::Symbol::Type::Noise )
    {
      do
      {
        ctx = parseLALR( stack, std::move( ctx ), lexeme );
      } while ( ctx.action == Ctx::Action::Reduce );
    }
  }
}

nga::LALRState::Action const* CompilationContext::action( nga::LALRState const& state, size_t symbolIdx )
{
  auto it = std::find_if( state.begin, state.end, [symbolIdx]( nga::LALRState::Action const& a )
  {
    return a.symbolIndex == symbolIdx;
  } );

  if ( it != state.end )
  {
    return &*it;
  }
  else
  {
    return nullptr;
  }
}

}
