#include "pch.hpp"
#include "ScopeStack.hpp"
#include "Segment.hpp"
#include "Project.hpp"

namespace assembler
{

ScopeStack::ScopeStack() : mNameContext{}, mStack {}
{
}

ScopeStack::~ScopeStack()
{
}

void ScopeStack::push( NSType type, std::string node, int row )
{
  mStack.push_back( NSNode{ type, std::move( node ), row } );
  updateName();
}

std::pair<bool,ScopeStack::NSNode const*> ScopeStack::pop( std::string const & node )
{
  if ( mStack.empty() )
  {
    return { false, nullptr };
  }
  else if ( mStack.back().name == node )
  {
    mStack.pop_back();
    updateName();
    return { true, nullptr };
  }
  else
  {
    return { false, &mStack.back() };
  }
}

std::pair<bool,ScopeStack::NSNode const*> ScopeStack::pop( NSType type )
{
  if ( mStack.empty() )
  {
    return { false, nullptr };
  } 
  else if ( mStack.back().type == type )
  {
    mStack.pop_back();
    updateName();
    return { true, nullptr };
  }
  else
  {
    return { false, &mStack.back() };
  }
}

std::string_view ScopeStack::context() const
{
  return mNameContext;
}

std::string ScopeStack::nsName( std::string const& name ) const
{
  size_t strSize = !mNameContext.empty() ? mNameContext.size() + 1 + name.size() : name.size();
  std::string str( strSize, '.' );
  if ( !mNameContext.empty() )
  {
    std::copy( name.cbegin(), name.cend(), std::copy( mNameContext.cbegin(), mNameContext.cend(), str.begin() ) + 1 );
  }
  else
  {
    std::copy( name.cbegin(), name.cend(), str.begin() );
  }

  return str;
}

void ScopeStack::updateName()
{
  //zone information is encoded into the context name
  //AFTER (i.e. at ctx.data() + ctx.size() ) the name there is a sequence of bytes with zones stack ended with 0

  std::string path;

  auto noZone = std::views::filter( mStack, []( NSNode const& n ) { return n.type != NSType::ZONE; } );

  auto it = noZone.begin();
  if ( it != noZone.end() )
  {
    path = it->name;
    for ( ++it; it != noZone.end(); ++it )
    {
      path += '.' + it->name;
    }
  }

  size_t size = path.size();
  
  for ( auto const& n : std::views::reverse( mStack ) | std::views::filter( []( NSNode const& n ) { return n.type == NSType::ZONE; } ) )
  {
    path.push_back( n.name[0] );
  }

  path.push_back( 0 );

  auto fullView = nga::project().strPool( path );

  mNameContext = std::string_view{ fullView.data(), size };
}

}
