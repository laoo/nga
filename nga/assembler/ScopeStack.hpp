#pragma once

#include <string>
#include <optional>
#include <vector>

namespace assembler
{

enum struct NSType
{
  LOCAL,
  PROC,
  ZONE
};

class ScopeStack
{
public:

  struct NSNode
  {
    NSType type;
    std::string name;
    int openingRow;
  };

  ScopeStack();
  ~ScopeStack();

  void push( NSType type, std::string node, int row );
  //Success is silent. Returns entry on error.
  std::pair<bool,NSNode const*> pop( std::string const& node );
  std::pair<bool,NSNode const*> pop( NSType type );

  std::string_view context() const;
  std::string nsName( std::string const& name ) const;

private:
  void updateName();

private:

  std::string_view mNameContext;
  std::vector<NSNode> mStack;
};

}
