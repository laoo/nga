#pragma once

#include "IInputTextSource.hpp"

namespace assembler
{

/*
  :name - named parameter
  :0    - number of parameters
  :n    - n'th parameter
  :@    - global invocation number

*/

class Macro
{
public:

  Macro( nga::Pos pos, std::string name, size_t namedParams );
  ~Macro();

  void addParamName( std::string param );
  bool addLine( std::pair<nga::Pos, std::string_view> pair );
  nga::Pos pos() const;
  std::shared_ptr<nga::IInputTextSource> invocate( std::vector<std::string> params, size_t number, std::span<char> tempBuffer );

private:

  class Incovation : public nga::IInputTextSource
  {
  public:
    Incovation( Macro & macro, std::vector<std::string> params, size_t number, std::span<char> tempBuffer );
    ~Incovation() override = default;

    std::pair<nga::Pos, std::string_view> nextLine() override;
    std::pair<nga::Pos, std::string_view> currentLine() const override;
    nga::Pos currentPos() const override;
    std::filesystem::path const& path() const override;

  private:

    void process( char c );
    std::string_view newLine();
    /*
    * ~0 - error
    * -1 - global invocation number
    *  0 - number of parameters
    *  n - nth parameter
    *  modifies string view to reflect token
    */
    int idenityToken( std::string_view & text );
    size_t expandText( std::string_view src, std::span<char> dest );
    size_t expandNumber( size_t value, std::span<char> dest );
    [[noreturn]] void raportOverflow();

  private:
    Macro & mMacro;
    std::vector<std::string> mParams;
    std::span<char> mTempBuffer;
    size_t mNumber;
    size_t mDstIdx;
    uint32_t mCurrentLine;
  };

  bool check( std::string_view line, std::string_view pattern );
  std::optional<size_t> findParam( std::string_view name );

private:
  nga::Pos mPos;
  std::string mName;
  std::vector<std::string> mParams;

  std::vector<std::pair<nga::Pos, std::string_view>> mLines;
  std::vector<std::string> mExpandedLines;
};

}
