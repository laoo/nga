#pragma once

#include "Value.hpp"
#include "SegmentReferenceKind.hpp"
#include "CpuType.hpp"

namespace nga
{

class ILinkingContext
{
public:
  virtual ~ILinkingContext() = default;

  virtual Segment& processedSegment() = 0;
  virtual void setProcessedChunkIndex( size_t idx ) = 0;
  virtual int32_t processedSegmentAddressSize() const = 0;
  virtual Value processedChunkAddress() = 0;
  virtual std::optional<Value> anonymousLabelAddressFromCurrentChunk( int distance ) = 0;
  virtual void visit( std::string_view ctx, std::string_view name, nga::SegmentReferenceKind kind ) = 0;
  virtual int32_t symbolSize( std::string_view ctx, std::string_view name, nga::CpuType cpu ) = 0;
  virtual Value symbolValue( std::string_view ctx, std::string_view name ) = 0;
  virtual int32_t segmentAddress() = 0;
  virtual int32_t segmentAddress( Segment* segment ) = 0;
  virtual int32_t segmentSize( Segment* segment ) = 0;
  virtual std::span<uint8_t const> segmentData( Segment* segment ) = 0;
  virtual int32_t namedOperatorSize( std::string_view ctx, std::string_view name, nga::CpuType cpu, std::string_view operatorName, std::string_view arg ) = 0;
  virtual Value namedOperatorValue( std::string_view ctx, std::string_view name, std::string_view operatorName, std::string_view arg ) = 0;


  virtual void emitI8( int v ) = 0;
  virtual void emitBE16( int v ) = 0;
  virtual void emitI8( Value const& v, nga::CpuType cpu ) = 0;
  virtual int emitRel8( Value const& dest, Value const& src, int offset ) = 0;
  virtual void emitLE16( Value const& v, nga::CpuType cpu ) = 0;
  virtual void emitBE16( Value const& v, nga::CpuType cpu, uint16_t mask = 0xffff ) = 0;
  virtual void emitBE32( Value const& v ) = 0;
  virtual void reserve( size_t size ) = 0;
  virtual void emitBlock( std::span<uint8_t const> ) = 0;
  virtual int getRel( Value const& dest, Value const& src, int offset ) = 0;

  virtual void addRelocation( int32_t off ) = 0;

  int emitRel8( Value const& dest, int offset = 0 )
  {
    return emitRel8( dest, processedChunkAddress(), offset );
  }

  int getRel( Value const& dest, int offset )
  {
    return getRel( dest, processedChunkAddress(), offset );
  }
};

}
