#pragma once

#include "Machine6502.hpp"

namespace nga::machine
{

class MachineC64 : public Machine6502
{
public:

  MachineC64();
  ~MachineC64() override = default;

  static void registerLUA( sol::state& lua );

};

}

