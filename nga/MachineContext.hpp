#pragma once

#include "Dictionary.hpp"

namespace nga
{

class ISymbol;
class SegmentDescriptor;
class Input;
class Output;
class Machine;

class MachineContext
{
public:

  MachineContext();
  virtual ~MachineContext() = default;

  std::shared_ptr<SegmentDescriptor> segmentDescriptor( std::string_view name ) const;


  void addSegmentDescriptor( std::string name, std::shared_ptr<SegmentDescriptor> desc );
  void addInput( std::shared_ptr<Input> input );
  void addOutput( std::shared_ptr<Output> output );
  void commit();

  void populate( sol::table const& tab );
  std::shared_ptr<ISymbol const> findSymbol( std::string_view ctx, std::string_view name ) const;
  void addSymbol( std::string key, std::shared_ptr<ISymbol const> symbol );

  static void registerLUA( sol::state & lua );

  void collectInputs();

  void advanceProgress();
  int64_t getProgress() const;


private:

  std::map<std::string,std::shared_ptr<SegmentDescriptor>,std::less<>> mSegmentDescriptors;
  std::vector<std::shared_ptr<Input>> mInputs;
  Dictionary<ISymbol const> mSymbols;
  std::vector<std::shared_ptr<Output>> mOutputs;

  int64_t mGlobalProgress;
};


}

