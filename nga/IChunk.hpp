#pragma once

#include "ICompilationContext.hpp"
#include "CpuType.hpp"
#include "Pos.hpp"

namespace nga
{
class Segment;
class ILinkingContext;
class Value;

class IChunk
{
public:

  virtual ~IChunk() = default;

  virtual uint32_t size( nga::ILinkingContext & lc ) const = 0;
  virtual void emit( nga::ILinkingContext & lc ) const = 0;
  virtual void setPosCpu( Pos pos, CpuType cpu = CpuType::Type::UNDEFINED ) = 0;
  virtual Pos getPos() const = 0;
  virtual std::tuple<bool, std::string_view, nga::Pos> listingLine() const = 0;

};

}
