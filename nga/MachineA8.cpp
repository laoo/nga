#include "pch.hpp"
#include "MachineA8.hpp"
#include "Ex.hpp"
#include "MachineContext.hpp"
#include "Segment.hpp"
#include "Project.hpp"


namespace nga::machine
{

MachineA8::MachineA8() : Machine6502{ Machine::Type::A8 }
{
}

void MachineA8::registerLUA( sol::state& lua )
{
  lua["A8"] = []( sol::table const& tab )
  {
    project().setMachine( std::make_shared<MachineA8>() );
  };
}

}

