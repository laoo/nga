#pragma once

#include "Pos.hpp"

namespace nga
{

class IInputSource
{
public:
  IInputSource() : mId{} {}
  virtual ~IInputSource() = default;

  virtual std::filesystem::path const& path() const = 0;
  
  Pos pos( uint32_t pos ) const
  {
    return { mId, pos };
  }

protected:
  friend class Project;
  void setId( uint32_t id )
  {
    assert( id > 0 );
    mId = id;
  }

  uint32_t mId;
};

}

