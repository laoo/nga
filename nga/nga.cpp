#include "pch.hpp"
#include "Ex.hpp"
#include "Log.hpp"
#include "Project.hpp"
#include "Worker.hpp"

int main( int argc, char** argv )
{
  try
  {
    L_SET_LOGLEVEL( ::Log::LL_DEBUG );

    if ( argc == 2 )
    {
      auto absPath = std::filesystem::absolute( argv[1] );
      auto parent = absPath.parent_path();

      L_DEBUG << "Processing " << absPath.generic_string();

      auto & proj = nga::project();

      std::filesystem::current_path( parent );

      if ( absPath.extension() == ".lua" )
      {
        proj.processLUA( absPath );
        proj.wait();
        proj.process();
      }
      else
      {
        L_ERROR << "Unsupported project file " << absPath;
      }
    }
    else
    {
      L_INFO << "nga.exe project.lua";
    }
  }
  catch( Ex const& ex )
  {
    L_ERROR << ex.what();
  }
}
