#pragma once

#include "CpuType.hpp"

namespace nga
{

class Segment;
class ILinkingContext;

struct ValueObject;

class Value
{
public:

  Value( Value&& other );
  Value& operator=( Value && other );
  ~Value();

  Value( Value const& ) = delete;
  Value & operator=( Value const& ) = delete;

  Value operator-() &&;
  Value operator*( Value && right ) &&;
  Value operator/( Value && right ) &&;
  Value operator%( Value && right ) &&;
  Value operator+( Value && right ) &&;
  Value operator-( Value && right ) &&;
  Value operator<<( Value && right ) &&;
  Value operator>>( Value && right ) &&;
  Value operator&( Value && right ) &&;
  Value operator|( Value && right ) &&;

  int size( CpuType cpu ) const;
  std::pair<Segment *, int32_t> segmentOffset() const;
  Segment * segment() const;
  bool isOffset() const;

  std::optional<int32_t> evaluate() const;
  int32_t evaluate( ILinkingContext& ctx ) const;

  static Value create( int32_t value );
  static Value create( Segment* instance, int32_t offset );

private:
  Value( Segment* instance, int32_t offset );

private:
  Segment* inst;
  int32_t value;
};

}
