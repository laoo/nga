#pragma once


namespace nga
{

struct Pos
{
  uint32_t id;
  uint32_t pos;

  explicit operator bool() const
  {
    return id != 0;
  }
};

}
