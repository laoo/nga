#include "pch.hpp"
#include "MachineC64.hpp"
#include "Ex.hpp"
#include "MachineContext.hpp"
#include "Segment.hpp"
#include "Project.hpp"


namespace nga::machine
{

MachineC64::MachineC64() : Machine6502{ Machine::Type::C64 }
{
}

void MachineC64::registerLUA( sol::state& lua )
{
  lua["C64"] = []( sol::table const& tab )
  {
    project().setMachine( std::make_shared<MachineC64>() );
  };
}


}

