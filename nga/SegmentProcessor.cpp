#include "pch.hpp"
#include "SegmentProcessor.hpp"
#include "SegmentDescriptor.hpp"
#include "Project.hpp"
#include "Machine.hpp"
#include "MachineContext.hpp"
#include "Ex.hpp"
#include "ISymbol.hpp"

namespace nga
{

SegmentProcessor::SegmentProcessor( Segment& segment, MachineContext const& machineContext, Tag ) : mSegment{ segment }, mMachineContext{ machineContext },
  mFiber{ std::bind( &SegmentProcessor::functor, this, std::placeholders::_1 ) }, mProcessed{}, mProcessedChunkIndex{}, mPhase{ Phase::NOT_STARTER }, mCurrentProgress{}
{
}

std::shared_ptr<SegmentProcessor> SegmentProcessor::create( Segment& segment, MachineContext const& machineContext )
{
  auto result = std::make_shared<SegmentProcessor>( segment, machineContext, Tag{} );

  project().machine()->registerSegmentProcessor( result );

  return result;
}

Segment& SegmentProcessor::processedSegment()
{
  return mSegment;
}

bool SegmentProcessor::process()
{
  if ( mProcessed )
    return false;

  mFiber = std::move( mFiber ).resume();

  if ( mEx )
    std::rethrow_exception( mEx );

  return !mProcessed;
}

boost::context::fiber SegmentProcessor::functor( boost::context::fiber&& f )
{
  try
  {
    mPhase = Phase::SIZING;

    mFiber = std::move( f );

    mSegment.sizeSegment( *this );

    mPhase = Phase::EMITING;

    mSegment.emit( *this );

    mPhase = Phase::DONE;

    mSegment.commit();

    mProcessed = true;
  }
  catch ( ... )
  {
    mEx = std::current_exception();
  }

  return std::move( mFiber );
}

SegmentProcessor::Phase SegmentProcessor::phase() const
{
  return mPhase;
}

int32_t SegmentProcessor::processedSegmentAddressSize() const
{
  return mSegment.addressSize();
}

Value SegmentProcessor::processedChunkAddress()
{
  return mSegment.chunkOffsetValue( *this, mProcessedChunkIndex );
}

std::optional<Value> SegmentProcessor::anonymousLabelAddressFromCurrentChunk( int distance )
{
  return mSegment.anonymousLabelAddressFromCurrentChunk( *this, mProcessedChunkIndex, distance );
}

void SegmentProcessor::err( std::string_view ctx, std::string_view name ) const
{
  Ex ex{};
  ex << "Undefined symbol " << name;
  if ( !ctx.empty() )
  {
    ex << " in context " << ctx;
  }
  throw ex;
}

void SegmentProcessor::visit( std::string_view ctx, std::string_view name, nga::SegmentReferenceKind kind )
{
  if ( auto symbol = mMachineContext.findSymbol( ctx, name ) )
  {
    symbol->visit( *this, kind );
    return;
  }

  err( ctx, name );
}

int32_t SegmentProcessor::symbolSize( std::string_view ctx, std::string_view name, nga::CpuType cpu )
{
  if ( auto symbol = mMachineContext.findSymbol( ctx, name ) )
  {
    return symbol->size( *this, cpu );
  }

  err( ctx, name );
}

Value SegmentProcessor::symbolValue( std::string_view ctx, std::string_view name )
{
  if ( auto s = mMachineContext.findSymbol( ctx, name ) )
  {
    if ( auto seg = s->segment() )
    {
      if ( !seg->constructed() )
      {
        throw Ex{} << "Symbol " << name <<" belongs to unconstructed segment";
      }
    }

    return s->value( *this );
  }

  err( ctx, name );
}

int32_t SegmentProcessor::segmentAddress()
{
  return getSegmentAddress( mSegment );
}

int32_t SegmentProcessor::segmentAddress( Segment* segment )
{
  return getSegmentAddress( *segment );
}

int32_t SegmentProcessor::segmentSize( Segment* segment )
{
  auto proc = segment->getProcessor();

  for ( ;; )
  {
    if ( proc->phase() > Phase::SIZING )
      return segment->size();

    if ( mCurrentProgress == mMachineContext.getProgress() )
    {
      throw Ex{ "Can't size segment" };
    }

    project().machine()->pushProcessor( segment->getProcessor() );
    mCurrentProgress = mMachineContext.getProgress();
    mFiber = std::move( mFiber ).resume();
  }
}

std::span<uint8_t const> SegmentProcessor::segmentData( Segment* segment )
{
  auto proc = segment->getProcessor();

  for ( ;; )
  {
    if ( proc->phase() > Phase::EMITING )
      return segment->data();

    if ( mCurrentProgress == mMachineContext.getProgress() )
    {
      throw Ex{ "Can't access segment data" };
    }

    project().machine()->pushProcessor( segment->getProcessor() );
    mCurrentProgress = mMachineContext.getProgress();
    mFiber = std::move( mFiber ).resume();
  }
}

void SegmentProcessor::setProcessedChunkIndex( size_t idx )
{
  mProcessedChunkIndex = idx;
}

int32_t SegmentProcessor::namedOperatorSize( std::string_view ctx, std::string_view name, nga::CpuType cpu, std::string_view operatorName, std::string_view arg )
{
  if ( auto symbol = mMachineContext.findSymbol( ctx, name ) )
  {
    if ( auto seg = symbol->segment() )
    {
      seg->flagConstructed();
      return seg->segmentDescriptor().namedOperatorSize( operatorName, arg );
    }
    else
    {
      throw Ex{ "Can't construct out-of-segment symbol" };
    }
  }
  else
  {
    Ex ex{};
    ex << "Undefined symbol " << name;
    if ( !ctx.empty() )
    {
      ex << " in context " << ctx;
    }
    throw ex;
  }
}

Value SegmentProcessor::namedOperatorValue( std::string_view ctx, std::string_view name, std::string_view operatorName, std::string_view arg )
{
  if ( auto symbol = mMachineContext.findSymbol( ctx, name ) )
  {
    if ( auto seg = symbol->segment() )
    {
      seg->flagConstructed();

      return seg->segmentDescriptor().namedOperatorValue( *this, seg, operatorName, arg );
    }
    else
    {
      throw Ex{ "Can't construct out-of-segment symbol" };
    }
  }
  else
  {
    Ex ex{};
    ex << "Undefined symbol " << name;
    if ( !ctx.empty() )
    {
      ex << " in context " << ctx;
    }
    throw ex;
  }
}

void SegmentProcessor::emitI8( int v )
{
  mSegment.emitByte( v );
}

void SegmentProcessor::emitBE16( int v )
{
  mSegment.emitByte( ( v & 0xff00 ) >> 8 );
  mSegment.emitByte( v & 0xff );
}

void SegmentProcessor::emitI8( Value const& v, nga::CpuType cpu )
{
  if ( v.size( cpu ) == 1 )
  {
    int32_t value = v.evaluate( *this );
    mSegment.emitByte( value & 0xff );
  }
  else
  {
    throw Ex{ "Expected 8-bit value" };
  }
}

int SegmentProcessor::emitRel8( Value const& dest, Value const& src, int offset )
{
  int distance = getRel( dest, src, offset );

  if ( distance < -128 )
  {
    return -128 - distance;
  }
  else if ( distance > 127 )
  {
    return distance - 127;
  }
  else
  {
    mSegment.emitByte( distance & 0xff );
    return 0;
  }
}

int SegmentProcessor::getRel( Value const& dest, Value const& src, int offset )
{
  auto srcValue = src.evaluate( *this );
  auto dstValue = dest.evaluate( *this );

  return dstValue - srcValue - offset;
}

void SegmentProcessor::emitLE16( Value const& v, nga::CpuType cpu )
{
  if ( v.size( cpu ) <= 2 )
  {
    int32_t value = v.evaluate( *this );

    mSegment.emitByte( value & 0x00ff );
    mSegment.emitByte( ( value & 0xff00 ) >> 8 );
  }
  else
  {
    throw Ex{ "Expected 16-bit value" };
  }
}

void SegmentProcessor::emitBE16( Value const& v, nga::CpuType cpu, uint16_t mask )
{
  if ( v.size( cpu ) <= 2 )
  {
    int32_t value = v.evaluate( *this );

    mSegment.emitByte( ( value & 0xff00 ) >> 8 );
    mSegment.emitByte( value & 0x00ff );
  }
  else
  {
    throw Ex{ "Expected 16-bit value" };
  }
}

void SegmentProcessor::emitBE32( Value const& v )
{
  int32_t value = v.evaluate( *this );

  mSegment.emitByte( ( value >> 24 ) & 0xff );
  mSegment.emitByte( ( value >> 16 ) & 0xff );
  mSegment.emitByte( ( value >> 8 ) & 0xff );
  mSegment.emitByte( value & 0xff );
}

void SegmentProcessor::reserve( size_t size )
{
  mSegment.reserve( size );
}

void SegmentProcessor::emitBlock( std::span<uint8_t const> block )
{
  mSegment.emitBlock( block );
}

void SegmentProcessor::addRelocation( int32_t off )
{
  mSegment.addRelocation( off );
}

int32_t SegmentProcessor::getSegmentAddress( Segment& segment )
{
  for ( ;; )
  {
    if ( auto opt = segment.address() )
    {
      return *opt;
    }

    if ( mCurrentProgress == mMachineContext.getProgress() )
    {
      throw Ex{ "Can't allocate segment" };
    }

    if ( !project().machine()->allocateSegmentsOfThisClass( &segment ) )
    {
      mCurrentProgress = mMachineContext.getProgress();
      mFiber = std::move( mFiber ).resume();
    }
  }
}

}

