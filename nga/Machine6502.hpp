#pragma once

#include "Machine.hpp"

namespace nga::machine
{

class Machine6502 : public Machine
{
public:

  Machine6502( Type type );
  ~Machine6502() override = default;

  static void registerLUA( sol::state& lua );

};

}

