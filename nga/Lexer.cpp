#include "pch.hpp"
#include "Lexer.hpp"
#include "Ex.hpp"

namespace nga
{
namespace
{
Symbol const* findKind( Reductor const & r, Symbol::Type type )
{
  auto it = std::find_if( r.symbolTable.begin(), r.symbolTable.end(), [type]( Symbol const& s )
  {
    return s.type == type;
  } );

  if ( it == r.symbolTable.end() )
    return nullptr;
  else
    return &*it;
}

bool contains( CharacterSetRangeList const& list, char16_t c )
{
  for ( auto const* range = list.begin; range != list.end; ++range )
  {
    if ( c < range->begin )
      return false;
    if ( c <= range->end )
      return true;
  }

  return false;
}

}

Lexer::Lexer( std::shared_ptr<Reductor const> r ) :
  mReductor{ std::move( r ) },
  mEOF{ mReductor->findSymbolByName( u"EOF" ) },
  mWhitespace{ mReductor->findSymbolByName( u"Whitespace" ) },
  mComment{ mReductor->findSymbolByName( u"Comment" ) },
  mId{ mReductor->findSymbolByName( u"Id" ) },
  mLabel{ mReductor->findSymbolByName( u"Label" ) },
  mMacroInvLit{ mReductor->findSymbolByName( u"MacroInvLit" ) },
  mMacroInvParam{ mReductor->findSymbolByName( u"MacroInvParam" ) },
  mInitialDFA{ mReductor->initialDFAState },
  mText{},
  mOffset{},
  mGroupStack{},
  mLexems{},
  mMacroMode{}
{
}

void Lexer::resetText( std::string_view text )
{
  mText = text;
  mOffset = 0;
  mGroupStack.clear();
  mLexems = 0;
  mOffset = 0;
  mMacroMode = false;
}

int Lexer::lookahead( size_t aIdx )
{
  if ( mOffset + aIdx >= mText.size() )
    return EOFChar;
  else
    return mText[mOffset + aIdx];
}

std::string_view Lexer::lookaheadString( size_t size ) const
{
  return std::string_view{ &mText[mOffset], size + 1 };
}

Lexeme Lexer::lookaheadDFA( Group::AdvanceMode advanceMode )
{
  int currentDFA = mInitialDFA;
  int target = -1;
  int lastAcceptState = -1;
  size_t lastAcceptPosition = 0;

  int c = lookahead( 0 );

  if ( c == EOFChar )
    return Lexeme{ mEOF, {} };

  if ( advanceMode == Group::AdvanceMode::Character )
    return Lexeme{ mComment, lookaheadString( 0 ) };

  for ( size_t currentPosition = 0; ; c = lookahead( ++currentPosition ) )
  {
    if ( c != EOFChar )
    {
      char16_t ch = ( char16_t )c;
      auto dfa = std::find_if( mReductor->dfaTable[currentDFA].begin, mReductor->dfaTable[currentDFA].end, [&]( DFAState::Edge const& e )
      {
        return contains( mReductor->characterSetTable[e.charSetIndex], ch );
      } );

      if ( dfa != mReductor->dfaTable[currentDFA].end )
      {
        currentDFA = dfa->targetIndex;
        if ( mReductor->dfaTable[currentDFA].terminalIndex )
        {
          lastAcceptState = currentDFA;
          lastAcceptPosition = currentPosition;
        }
        continue;
      }
    }

    if ( lastAcceptState == -1 )
    {
      throw Ex{} << "Syntax Error: '" << lookaheadString( lastAcceptPosition ) << "'";
    }
    else
    {
      return Lexeme{ &mReductor->symbolTable[*mReductor->dfaTable[lastAcceptState].terminalIndex], lookaheadString( lastAcceptPosition ) };
    }
  }
}

void Lexer::comsumeBuffer( size_t aCnt )
{
  mOffset += aCnt;
}

Lexeme Lexer::makeFirstIdLabel( Lexeme lexeme )
{
  if ( mLexems++ == 0 )
  {
    if ( lexeme.symbol == mId )
    {
      //label can't have dot in it
      if ( lexeme.content.find_first_of( '.' ) == std::string_view::npos )
        lexeme.symbol = mLabel;
    }
  }

  return lexeme;
}

Lexeme Lexer::produceMacro()
{
  int c = lookahead( 0 );

  while ( c != EOFChar && std::isspace( c ) )
  {
    comsumeBuffer( 1 );
    c = lookahead( 0 );
  }

  if ( c == EOFChar )
    return Lexeme{ mEOF, {} };

  size_t currentPosition = 0;

  if ( c == ';' )
  {
    do
    {
      c = lookahead( ++currentPosition );
    } while ( c != EOFChar );

    Lexeme result{ mComment, lookaheadString( currentPosition ) };
    comsumeBuffer( result.content.size() );
    return result;
  }

  while ( c != EOFChar && !std::isspace( c ) && c != ';' )
  {
    c = lookahead( ++currentPosition );
  }

  Lexeme result{ mMacroInvParam, lookaheadString( currentPosition - 1 ) };
  comsumeBuffer( result.content.size() );
  return result;
}

Lexeme Lexer::produceLexeme()
{
  if ( mMacroMode )
    return produceMacro();

  Group::AdvanceMode advanceMode = Group::AdvanceMode::Token;

  for ( ;; )
  {
    Lexeme t = lookaheadDFA( advanceMode );

    bool nestGroup = false;
    if ( t.symbol->type == Symbol::GroupStart )
    {
      if ( mGroupStack.empty() )
        nestGroup = true;
      else
        nestGroup = false;
    }

    if ( nestGroup )
    {
      assert( t.symbol->groupIdx );
      advanceMode = mReductor->groupTable[*t.symbol->groupIdx].advanceMode;
      comsumeBuffer( t.content.size() );
      mGroupStack.push_back( t );
    }
    else if ( mGroupStack.empty() )
    {
      //The token is ready to be analyzed.
      comsumeBuffer( t.content.size() );
      return makeFirstIdLabel( t );
    }
    else if ( t.symbol->type == Symbol::End )
    {
      //EOF always stops the loop. The caller function (Parse) can flag a runaway group error.
      return t;
    }
    else if ( mGroupStack.back().symbol->groupIdx && mReductor->groupTable[*mGroupStack.back().symbol->groupIdx].endIndex == t.symbol->idx )
    {
      //End the current group
      Lexeme pop = std::move( mGroupStack.back() );
      mGroupStack.pop_back();

      if ( mReductor->groupTable[*pop.symbol->groupIdx].endingMode == Group::Closed )
      {
        pop.content = { pop.content.data(), pop.content.size() + t.content.size() };
        comsumeBuffer( pop.content.size() );
      }

      if ( mGroupStack.empty() )
      {
        return Lexeme{ &mReductor->symbolTable[mReductor->groupTable[*pop.symbol->groupIdx].containerIndex], pop.content };
      }
      else
      {
        mGroupStack.back().content = { mGroupStack.back().content.data(), mGroupStack.back().content.size() + pop.content.size() };
      }
    }
    else
    {
      //We are in a group, Append to the Token on the top of the stack.
      //Take into account the Token group mode  
      Lexeme & top = mGroupStack.back();

      assert( top.symbol->groupIdx );

      if ( advanceMode == Group::Token )
      {
        top.content = { top.content.data(), top.content.size() + t.content.size() };
        comsumeBuffer( t.content.size() );
      }
      else
      {
        top.content = { top.content.data(), top.content.size() + 1 };
        comsumeBuffer( 1 );
      }
    }
  }
}

bool Lexer::setMacroMode( nga::Lexeme& lexeme )
{
  if ( mMacroMode )
    return false;
  else
  {
    lexeme.symbol = mMacroInvLit;
    return mMacroMode = true;
  }
}

}
