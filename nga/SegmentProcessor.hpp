#pragma once

#include "ILinkingContext.hpp"

namespace nga
{

class Segment;
class MachineContext;

class SegmentProcessor : public ILinkingContext
{
  struct Tag {};
public:

  enum class Phase : int
  {
    NOT_STARTER = 0,
    SIZING = 1,
    EMITING = 2,
    DONE = 3
  };

  SegmentProcessor( Segment& segment, MachineContext const& machineContext, Tag );
  ~SegmentProcessor() override = default;

  static std::shared_ptr<SegmentProcessor> create( Segment& segment, MachineContext const& machineContext );


  bool process();

  boost::context::fiber functor( boost::context::fiber&& f );

  Phase phase() const;

  Segment& processedSegment() override;
  int32_t processedSegmentAddressSize() const override;
  Value processedChunkAddress() override;
  std::optional<Value> anonymousLabelAddressFromCurrentChunk( int distance ) override;
  [[noreturn]] void err( std::string_view ctx, std::string_view name ) const;
  void visit( std::string_view ctx, std::string_view name, nga::SegmentReferenceKind kind ) override;
  int32_t symbolSize( std::string_view ctx, std::string_view name, nga::CpuType cpu ) override;
  Value symbolValue( std::string_view ctx, std::string_view name ) override;
  int32_t segmentAddress() override;
  int32_t segmentAddress( Segment* segment ) override;
  int32_t segmentSize( Segment* segment ) override;
  std::span<uint8_t const> segmentData( Segment* segment ) override;
  void setProcessedChunkIndex( size_t idx ) override;
  int32_t namedOperatorSize( std::string_view ctx, std::string_view name, nga::CpuType cpu, std::string_view operatorName, std::string_view arg ) override;
  Value namedOperatorValue( std::string_view ctx, std::string_view name, std::string_view operatorName, std::string_view arg ) override;

  void emitI8( int v ) override;
  void emitBE16( int v ) override;
  void emitI8( Value const& v, nga::CpuType cpu ) override;
  int emitRel8( Value const& dest, Value const& src, int offset ) override;
  void emitLE16( Value const& v, nga::CpuType cpu ) override;
  void emitBE16( Value const& v, nga::CpuType cpu, uint16_t mask ) override;
  void emitBE32( Value const& v ) override;
  void reserve( size_t size ) override;
  void emitBlock( std::span<uint8_t const> block ) override;
  int getRel( Value const& dest, Value const& src, int offset ) override;

  void addRelocation( int32_t off ) override;

private:
  int32_t getSegmentAddress( Segment& segment );
private:

  Segment & mSegment;
  MachineContext const& mMachineContext;
  boost::context::fiber mFiber;
  bool mProcessed;
  std::exception_ptr mEx;
  size_t mProcessedChunkIndex;
  Phase mPhase;
  int64_t mCurrentProgress;

};

}
