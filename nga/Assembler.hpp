#pragma once

#include "Input.hpp"

namespace nga::input
{

class Assembler : public Input
{
public:
  Assembler( sol::table const& tab );

  ~Assembler() override = default;

private:
  std::filesystem::path mBasePath;
  std::regex mNamePattern;
};

}
