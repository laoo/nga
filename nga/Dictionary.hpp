#pragma once

namespace nga
{

template<typename T>
class Dictionary
{
public:
  Dictionary() : mEntries{}
  {
  }

  void add( std::string key, std::shared_ptr<T> value )
  {
    mEntries.push_back( std::pair( key, value ) );
  }

  std::shared_ptr<T> find( std::string_view key ) const
  {
    auto it = std::find_if( mEntries.cbegin(), mEntries.cend(), [&]( auto const& p )
    {
      return p.first == key;
    } );

    if ( it != mEntries.cend() )
      return it->second;
    else
      return {};
  }

  std::shared_ptr<T> find( std::string_view ctx, std::string_view key ) const
  {
    for ( int i = 0;; ++i )
    {
      auto prefix = part( ctx, i );

      if ( prefix.empty() )
      {
        auto it = std::find_if( mEntries.cbegin(), mEntries.cend(), [&]( auto const& p )
        {
          return p.first == key;
        } );

        if ( it != mEntries.cend() )
          return it->second;
        else
          return {};
      }
      else
      {
        auto it = std::find_if( mEntries.cbegin(), mEntries.cend(), [&]( auto const& p )
        {
          if ( p.first.size() != prefix.size() + 1 + key.size() )
            return false;

          auto [pre, str1] = std::mismatch( prefix.cbegin(), prefix.cend(), p.first.cbegin(), p.first.cend() );
          if ( pre != prefix.cend() || *str1++ != '.' )
            return false;

          auto [nam, str2] = std::mismatch( key.cbegin(), key.cend(), str1, p.first.cend() );

          return nam == key.cend();
        } );

        if ( it != mEntries.cend() )
          return it->second;
      }
    }
  }

private:

  std::string_view part( std::string_view ctx, int cnt ) const
  {
    std::string_view result{ ctx };

    while ( cnt-- > 0 )
    {
      if ( !result.empty() )
      {
        auto pos = result.find_last_of( '.' );
        if ( pos != std::string_view::npos )
        {
          result = std::string_view{ result.data(), pos };
        }
        else
        {
          return {};
        }
      }
      else
      {
        return result;
      }
    }
    return result;
  }

private:
  std::vector<std::pair<std::string, std::shared_ptr<T>>> mEntries;

};

}
