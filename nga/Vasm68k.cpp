#include "pch.hpp"
#include "Vasm68k.hpp"
#include "in_elf.hpp"
#include "Ex.hpp"
#include "Log.hpp"
#include "Project.hpp"

namespace nga::input
{
void Vasm68k::processVasm( std::filesystem::path src )
{
  auto config = project().config();
  auto bin = config.getBin() / "vasm.exe";
  auto dst = config.getTmp() / src;
  dst.replace_extension( ".o" );
  auto lst = config.getTmp() / src;
  lst.replace_extension( ".lst" );

  if ( std::filesystem::exists( dst ) )
  {
    if ( std::filesystem::last_write_time( dst ) > std::filesystem::last_write_time( src ) )
    {
      return elf::import( dst, *this );
    }
  }

  std::filesystem::create_directories( dst.parent_path() );

  ////L_DEBUG << "Running: " << cmd;
  std::string cmd = bin.string() + " -Felf -quiet -no-opt -L " + lst.string() + " -o " + dst.string() + " " + src.string();
  
  boost::process::ipstream is;
  if ( auto result = boost::process::system( cmd, boost::process::std_err > is ) )
  {
    std::string err;
    std::string line;

    while ( std::getline( is, line ) && !line.empty() )
      err.append( line );

    throw Ex{} << err;
  }
  else
  {
    std::string info;
    std::string line;

    while ( std::getline( is, line ) && !line.empty() )
      info.append( line );

    if ( !info.empty() )
      L_INFO << info;

    elf::import( dst, *this );
  }
}

Vasm68k::Vasm68k( sol::table const & tab ) : mBasePath{}, mNamePattern{}
{
  if ( auto opt = tab.get<sol::optional<std::string_view>>( "Path" ) )
  {
    project().async( [&, path=*opt]
      {
        processVasm( path );
      } );

    return;
  }

  if ( auto opt = tab.get<sol::optional<std::string_view>>( "BasePath" ) )
  {
    mBasePath = *opt;
  }
  else
  {
    throw Ex{ "No BasePath nor Path in vasm68k Input node" };
  }

  if ( auto opt = tab.get<sol::optional<std::string_view>>( "NamePattern" ) )
  {
    try
    {
      mNamePattern = std::regex( opt->cbegin(), opt->cend() );
    }
    catch ( std::regex_error const& ex )
    {
      throw Ex{} << "Bad NamePattern in vasm68k Input node: " << ex.what();
    }
  }
  else
  {
    throw Ex{ "No NamePattern in vasm68k Input node" };
  }

  std::filesystem::recursive_directory_iterator it{ mBasePath };
  for ( auto const& p : it )
  {
    if ( p.is_regular_file() )
    {
      std::cmatch m;
      if ( std::regex_search( p.path().filename().string().c_str(), m, mNamePattern ) )
      {
        project().async( [&, path=p.path()]
          {
            processVasm( path );
          } );
      }
    }
  }
}

}
