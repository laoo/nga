#include "pch.hpp"
#include "SegmentStore.hpp"
#include "Ex.hpp"
#include "Segment.hpp"
#include "Project.hpp"

namespace nga
{

SegmentStore::SegmentStore() : mSymbols{}, mCtx{}
{
}

SegmentStore::~SegmentStore()
{
}

Segment * SegmentStore::newSegment()
{
  mAllSegments.push_back( project().newSegment() );
  return mAllSegments.back();
}

void SegmentStore::declareSymbol( std::string_view name, std::shared_ptr<ISymbol const> symbol )
{
  std::scoped_lock<std::mutex> lock{ mMutex };

  mSymbols.push_back( std::pair<std::string, std::shared_ptr<ISymbol const>>( name, std::move( symbol ) ) );
}


std::span<std::pair<std::string, std::shared_ptr<ISymbol const>> const>  SegmentStore::symbols() const
{
  return { mSymbols.data(), mSymbols.size() };
}

void SegmentStore::setMachineContext( MachineContext * ctx )
{
  mCtx = ctx;
  for ( auto & seg : mAllSegments )
  {
    seg->setMachineContext( ctx );
  }
}

MachineContext const* SegmentStore::getMachineContext() const
{
  return mCtx;
}

}
