#pragma once

#include "Ex.hpp"

#define ONE_THREAD

namespace nga
{

class Worker
{
public:

  Worker();
  ~Worker() noexcept( true );

  template<typename FUN>
  void run( FUN const& aFunction )
  {
    {
      std::unique_lock<std::mutex> amlock( mAsyncMutex );
      mWorkingJobs += 1;
    }

    auto fun = [=]
    {
      try
      {
        aFunction();
      }
      catch ( Ex const& )
      {
        mExPtr = std::current_exception();
      }

      std::unique_lock<std::mutex> amlock( mAsyncMutex );
      if ( --mWorkingJobs < 1 )
      {
        mFinishCondition.notify_one();
      }

    };

#ifdef ONE_THREAD
    fun();
#else
    mService.post( fun );
#endif
  }

  void wait()
  {
    std::unique_lock<std::mutex> amlock( mAsyncMutex );
    mFinishCondition.wait( amlock, [&]()
    {
      return mWorkingJobs < 1;
    } );

    if ( mExPtr )
    {
      std::rethrow_exception( mExPtr );
    }
  }

private:
  boost::thread_group mThreadGroup;
  boost::asio::io_service mService;
  std::optional<boost::asio::io_service::work> mWork;
  std::condition_variable mFinishCondition;
  std::mutex mAsyncMutex;
  int mWorkingJobs;
  std::exception_ptr mExPtr;
};

}


