#pragma once

#include "Ex.hpp"

namespace nga
{
struct CharacterSetRangeList
{
  struct Range
  {
    char16_t begin;
    char16_t end;
  };

  constexpr CharacterSetRangeList( Range const* b, Range const* e ) : begin{ b }, end{ e } {}

  Range const* begin;
  Range const* end;
};

struct Symbol
{
  char16_t const* name;
  enum Type
  {
    Nonterminal = 0,
    Content = 1,
    Noise = 2,
    End = 3,
    GroupStart = 4,
    GroupEnd = 5,
    Error = 7
  } type;
  size_t idx;
  std::optional<size_t> groupIdx;

  constexpr Symbol( char16_t const* n, Type t, size_t i, std::optional<size_t> g ) : name{ n }, type{ t }, idx{ i }, groupIdx{ g } {}

};

struct DFAState
{
  struct Edge
  {
    int charSetIndex;
    int targetIndex;
  };
  Edge const* begin;
  Edge const* end;
  std::optional<int> terminalIndex;

  constexpr DFAState( Edge const* begin, Edge const* end, std::optional<int> terminalIndex ) : begin{ begin }, end{ end }, terminalIndex{ terminalIndex } {}

};

struct Group
{
public:

  enum AdvanceMode
  {
    Token = 0,
    Character = 1
  };

  enum EndingMode
  {
    Open = 0,
    Closed = 1
  };

  char16_t const* name;
  int containerIndex;
  int startIndex;
  int endIndex;
  AdvanceMode advanceMode;
  EndingMode endingMode;

  constexpr Group( char16_t const* name, int containerIndex, int startIndex, int endIndex, AdvanceMode advanceMode, EndingMode endingMode ) :
    name{ name }, containerIndex{ containerIndex }, startIndex{ startIndex }, endIndex{ endIndex }, advanceMode{ advanceMode }, endingMode{ endingMode }
  {}
};

struct Production
{
  int nonterminal;

  constexpr Production( int nonterminal ) : nonterminal{ nonterminal } {}
};

struct LALRState
{
  struct Action
  {
    enum Type
    {
      Shift = 1,
      Reduce = 2,
      Goto = 3,
      Accept = 4
    } type;
    int symbolIndex;
    int targetIndex;
  };
  Action const* begin;
  Action const* end;

  constexpr LALRState( Action const* begin, Action const* end ) : begin{ begin }, end{ end } {}
};

struct Lexeme
{
  Symbol const* symbol;
  std::string_view content;
};

struct Token
{
  Lexeme lexeme;
  int state;
  std::shared_ptr<void> node;

  template<typename T>
  std::shared_ptr<T> cast()
  {
    auto t = (T*)node.get();
    return std::shared_ptr<T>( std::move( node ), t );
  }

};

class ICompilationContext;

class Reductor
{
public:
  Reductor( nga::ICompilationContext* ctx,
    std::span<CharacterSetRangeList const> characterSetTable,
    std::span<Symbol const> symbolTable,
    std::span<Group const> groupTable,
    std::span<Production const> productionTable,
    std::span<DFAState const> dfaTable,
    std::span<LALRState const> lalrTable,
    int const initialDFAState,
    int const initialLALRState )
  : characterSetTable{ characterSetTable },
    symbolTable{ symbolTable },
    groupTable{ groupTable },
    productionTable{ productionTable },
    dfaTable{ dfaTable },
    lalrTable{ lalrTable },
    initialDFAState{ initialDFAState },
    initialLALRState{ initialLALRState },
    mCtx{ ctx }
  {
  }

  virtual ~Reductor() = default;

  nga::Symbol const* findSymbolByName( char16_t const* name ) const
  {
    auto it = std::find_if( symbolTable.begin(), symbolTable.end(), [n = std::u16string_view{ name }]( nga::Symbol const& s )
    {
      return n == s.name;
    } );

    if ( it == symbolTable.end() )
      throw Ex{} << "Can't find symbol " << boost::locale::conv::utf_to_utf<char>( name );

    return &*it;
  }

  virtual std::shared_ptr<void> reduce( std::vector<Token> & stack, size_t reduction ) const = 0;

  std::span<CharacterSetRangeList const> characterSetTable;
  std::span<Symbol const> symbolTable;
  std::span<Group const> groupTable;
  std::span<Production const> productionTable;
  std::span<DFAState const> dfaTable;
  std::span<LALRState const> lalrTable;
  int const initialDFAState;
  int const initialLALRState;

protected:
  nga::ICompilationContext* mCtx;

};

}
