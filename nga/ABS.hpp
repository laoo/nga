#pragma once

#include "Output.hpp"
#include "Value.hpp"

namespace nga::output
{

class ABS : public Output
{
public:
  ABS( sol::table const& tab );
  ~ABS() override = default;

  static void registerLUA( sol::state& lua );

  void setEntry( Value v ) override;
  void addSegment( Segment* segment ) override;
  void commit() override;

private:
  std::filesystem::path mPath;
  std::optional<Value> mEntry;
  std::vector<Segment*> mSegments;
};

}

