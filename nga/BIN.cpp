#include "pch.hpp"
#include "BIN.hpp"
#include "Ex.hpp"
#include "Segment.hpp"
#include "Project.hpp"
#include "Machine.hpp"
#include "Log.hpp"


namespace nga::output
{
using namespace std::string_view_literals;

void BIN::registerLUA( sol::state& lua )
{
  lua["BIN"] = []( sol::table const& tab ) -> std::shared_ptr<Output>
  {
    return std::make_shared<BIN>( tab );
  };
}

BIN::BIN( sol::table const& tab ) : mPath{}, mEntry{}, mSegments{}
{
  if ( auto opt = tab.get<sol::optional<std::string>>( "OutputPath" ) )
  {
    mPath = std::move( *opt );
  }
  else
  {
    throw Ex{ "No Path in BIN Output" };
  }
}

void BIN::setEntry( Value v )
{
  if ( mEntry )
    throw Ex{ "Entry redefinition" };

  mEntry = std::move( v );
}

void BIN::addSegment( Segment* segment )
{
  mSegments.push_back( segment );
}

void BIN::commit()
{
  std::vector<Block> blocks;

  for ( auto const& segment : mSegments )
  {
    blocks.push_back( { *segment->address(), segment->data() } );
  }

  if ( blocks.empty() )
  {
    throw Ex{ "Empty output" };
  }

  std::sort( blocks.begin(), blocks.end(), []( Block const& left, Block const& right )
  {
    return left.address < right.address;
  } );

 
  std::vector<uint8_t> buffer;

  for ( size_t i = 0; i < blocks.size(); ++i )
  {
    auto const& block = blocks[i];
    buffer.reserve( buffer.size() + block.data.size() );
    std::copy( block.data.begin(), block.data.end(), std::back_inserter( buffer ) );
  }

  std::ofstream fout{ project().config().getOut() / mPath, std::ios::binary };
  fout.write( (char const*)buffer.data(), buffer.size() );
}

}
