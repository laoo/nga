#pragma once

#include "IInputDataSource.hpp"

namespace nga
{

class SourceDataFile : public IInputDataSource
{
public:
  SourceDataFile( std::filesystem::path path );
  ~SourceDataFile() override = default;

  std::filesystem::path const& path() const override;
  std::span<uint8_t const> data() const override;
  std::istringstream stream() const override;

private:
  std::string mContent;
  std::filesystem::path mPath;
};

}

