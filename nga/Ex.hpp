#pragma once

#include <sstream>
#include <exception>
#include <format>


class Ex : public std::exception
{
public:

  Ex() : exception{}, mStack{}, mSS{ std::make_shared<std::stringstream>() }, mText{ std::make_shared<std::string>() }
  {
  }

  Ex( char const* str ) : exception{}, mStack{}, mSS{ std::make_shared<std::stringstream>( str) }, mText{ std::make_shared<std::string>() }
  {
  }

  void addStackTrace( std::string stackTrace, std::string text = {} )
  {
    mStack.push_back( { std::move( stackTrace ), std::move( text ) } );
  }

  template<typename T>
  Ex & operator<<( T const& t )
  {
    *mSS << t;
    return *this;
  }

  char const* what() const override
  {
    if ( !mStack.empty() )
    {
      mText->append( mStack[0].first );
      mText->append( ": " );
    }

    mText->append( mSS->str() );

    if ( !mStack.empty() )
    {
      mText->append( "\n" );
      mText->append( mStack[0].second );
    }

    if ( mStack.size() > 1 )
    {
      mText->append( "\ninstantiated from:\n" );
      for ( size_t i = 1; i < mStack.size(); ++i )
      {
        mText->append( mStack[i].first );
        mText->append( ":\t" );
        mText->append( mStack[i].second );
        mText->append( "\n" );
      }
    }

    return mText->c_str();
  }

private:
  std::vector<std::pair<std::string,std::string>> mStack;
  std::shared_ptr<std::stringstream> mSS;
  std::shared_ptr<std::string> mText;
};

#define INTERNAL_ERROR ( throw Ex{} << __FILE__ << "(" << __LINE__ << "): Internal NGA Error " )
