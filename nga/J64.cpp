#include "pch.hpp"
#include "J64.hpp"
#include "Ex.hpp"
#include "Segment.hpp"
#include "Project.hpp"
#include "Machine.hpp"
#include "Log.hpp"
#include "Univ.h"


namespace nga::output
{
using namespace std::string_view_literals;

void J64::registerLUA( sol::state& lua )
{
  lua["J64"] = []( sol::table const& tab ) -> std::shared_ptr<Output>
  {
    return std::make_shared<J64>( tab );
  };
}

J64::J64( sol::table const& tab ) : mPath{}, mEntry{}, mSegments{}
{
  if ( auto opt = tab.get<sol::optional<std::string>>( "OutputPath" ) )
  {
    mPath = std::move( *opt );
  }
  else
  {
    throw Ex{ "No Path in J64 Output" };
  }
}

void J64::setEntry( Value v )
{
  if ( mEntry )
    throw Ex{ "Entry redefinition" };

  mEntry = std::move( v );
}

void J64::addSegment( Segment* segment )
{
  mSegments.push_back( segment );
}

void J64::commit()
{
  if ( mEntry )
  {
    auto [inst, off] = mEntry->segmentOffset();

    if ( !inst )
      throw Ex{ "Entry must be a segment offset" };

    auto optAddress = inst->address();

    if ( !optAddress )
      INTERNAL_ERROR;

    int address = *optAddress + off;

    if ( address != 0x802000 )
      throw Ex{ "Entry must be $802000" };
  }

  struct Block
  {
    int32_t address;
    std::span<uint8_t const> data;
  };

  std::vector<Block> blocks;

  for ( auto const& segment : mSegments )
  {
    blocks.push_back( { *segment->address(), segment->data() } );
  }

  if ( blocks.empty() )
  {
    throw Ex{ "Empty output" };
  }

  std::sort( blocks.begin(), blocks.end(), []( Block const& left, Block const& right )
  {
    return left.address < right.address;
  } );

  if ( blocks.front().address != 0x802000 )
    L_WARNING << "J64 file started at address $" << std::hex << blocks.front().address << " which is different than obligatory $802000";

  if ( blocks.back().address + blocks.back().data.size() > 0xdfffff )
    L_WARNING << "J64 file ends at address $" << std::hex << blocks.back().address + blocks.back().data.size() << " which is grater than $dfffff";

  std::vector<uint8_t> buffer;
  buffer.reserve( blocks.back().address + blocks.back().data.size() - 0x800000 );

  std::copy_n( Univ_bin, Univ_bin_len, std::back_inserter( buffer ) );

  int32_t cursor = 0x802000;
  for ( size_t i = 0; i < blocks.size(); ++i )
  {
    auto const& block = blocks[i];

    for ( ;cursor < block.address; ++cursor )
    {
      buffer.push_back( 0xff );
    }

    std::copy( block.data.begin(), block.data.end(), std::back_inserter( buffer ) );
  }

  size_t size = buffer.size();
  size_t minimalSize = ( ( size + 1024 * 1024 - 1 ) / ( 1024 * 1024 ) ) * 1024 * 1024;
  buffer.resize( minimalSize, 0xff );

  std::ofstream fout{ project().config().getOut() / mPath, std::ios::binary };
  fout.write( (char const*)buffer.data(), buffer.size() );
}

}
