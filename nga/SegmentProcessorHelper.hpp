#pragma once

// reimplementing boost::context::detail::invoke using post c++17 syntax
#define BOOST_CONTEXT_DETAIL_INVOKE_H

#ifdef BOOST_HAS_ABI_HEADERS
# include BOOST_ABI_PREFIX
#endif

namespace boost::context::detail
{

template< typename Fn, typename ... Args >
typename std::enable_if< std::is_member_pointer< typename std::decay< Fn >::type >::value, typename std::invoke_result< Fn, Args ... >::type >::type invoke( Fn&& fn, Args && ... args )
{
  return std::mem_fn( fn )( std::forward< Args >( args ) ... );
}

template< typename Fn, typename ... Args >
typename std::enable_if< !std::is_member_pointer< typename std::decay< Fn >::type >::value, typename std::invoke_result< Fn, Args ... >::type >::type invoke( Fn&& fn, Args && ... args )
{
  return std::forward< Fn >( fn )( std::forward< Args >( args ) ... );
}

}

#ifdef BOOST_HAS_ABI_HEADERS
#include BOOST_ABI_SUFFIX
#endif

#include <boost/context/fiber.hpp>
