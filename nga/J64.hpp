#pragma once

#include "Output.hpp"
#include "Value.hpp"

namespace nga::output
{

class J64 : public Output
{
public:
  J64( sol::table const& tab );
  ~J64() override = default;

  static void registerLUA( sol::state& lua );

  void setEntry( Value v ) override;
  void addSegment( Segment* segment ) override;
  void commit() override;

private:
  std::filesystem::path mPath;
  std::optional<Value> mEntry;
  std::vector<Segment*> mSegments;
};

}

