#include "pch.hpp"
#include "GCC.hpp"
#include "in_elf.hpp"
#include "Ex.hpp"
#include "Log.hpp"
#include "Project.hpp"

namespace nga::input
{
void GCC::processAsm( std::filesystem::path src )
{
  auto config = project().config();
  auto bin = config.getBin() / "as.exe";
  auto dst = config.getTmp() / src;
  dst.replace_extension( ".o" );

  if ( std::filesystem::exists( dst ) )
  {
    if ( std::filesystem::last_write_time( dst ) > std::filesystem::last_write_time( src ) )
    {
      return elf::import( dst, *this );
    }
  }

  std::filesystem::create_directories( dst.parent_path() );

  std::string cmd = bin.string() + " -m68000 -o " + dst.string() + " " + src.string();

  boost::process::ipstream is;
  if ( auto result = boost::process::system( cmd, boost::process::std_err > is ) )
  {
    std::string err;
    std::string line;

    while ( std::getline( is, line ) && !line.empty() )
      err.append( line );

    throw Ex{} << err;
  }
  else
  {
    std::string info;
    std::string line;

    while ( std::getline( is, line ) && !line.empty() )
      info.append( line );

    if ( !info.empty() )
      L_INFO << info;

    return elf::import( dst, *this );
  }
}

std::filesystem::path GCC::processC( std::filesystem::path src )
{
  auto config = project().config();
  auto bin = config.getBin() / "cc1.exe";
  auto dst = config.getTmp() / src;
  dst.replace_extension( ".gas" );

  if ( std::filesystem::exists( dst ) )
  {
    if ( std::filesystem::last_write_time( dst ) > std::filesystem::last_write_time( src ) )
    {
      return dst;
    }
  }

  std::filesystem::create_directories( dst.parent_path() );

  std::string cmd = bin.string() + " -quiet -m68000 -mfastcall -O3 -o " + dst.string() + " " + src.string();
  
  boost::process::ipstream is;
  if ( auto result = boost::process::system( cmd, boost::process::std_err > is ) )
  {
    std::string err;
    std::string line;

    while ( std::getline( is, line ) && !line.empty() )
      err.append( line );

    throw Ex{} << err;
  }
  else
  {
    std::string info;
    std::string line;

    while ( std::getline( is, line ) && !line.empty() )
      info.append( line );

    if ( !info.empty() )
      L_INFO << info;

    return dst;
  }
}

GCC::GCC( sol::table const & tab )
{
  if ( auto opt = tab.get<sol::optional<std::string_view>>( "BasePath" ) )
  {
    mBasePath = *opt;
  }
  else
  {
    throw Ex{ "No BasePath in gcc Input node" };
  }

  if ( auto opt = tab.get<sol::optional<std::string_view>>( "NamePattern" ) )
  {
    try
    {
      mNamePattern = std::regex( opt->cbegin(), opt->cend() );
    }
    catch ( std::regex_error const& ex )
    {
      throw Ex{} << "Bad BasePath in gcc Input node: " << ex.what();
    }
  }
  else
  {
    throw Ex{ "No NamePattern in gcc Input node" };
  }

  std::filesystem::recursive_directory_iterator it{ mBasePath };
  for ( auto const& p : it )
  {
    if ( p.is_regular_file() )
    {
      std::cmatch m;
      if ( std::regex_search( p.path().filename().string().c_str(), m, mNamePattern ) )
      {
        project().async( [&, path=p.path()]
        {
          auto s = processC( path );
          processAsm( s );
        } );
      }
    }
  }
}

}
