#include "pch.hpp"
#include "MachineJaguar.hpp"
#include "Project.hpp"

namespace nga::machine
{

MachineJaguar::MachineJaguar() : Machine{ Processor::PROC_JAGUAR, Type::JAGUAR }
{
}

void MachineJaguar::registerLUA( sol::state& lua )
{
  lua["Jaguar"] = []( sol::table const& tab )
  {
    project().setMachine( std::make_shared<MachineJaguar>() );
  };
}

}

