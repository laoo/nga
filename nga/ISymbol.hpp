#pragma once

#include "Value.hpp"
#include "CpuType.hpp"
#include "SegmentReferenceKind.hpp"

namespace nga
{
class ILinkingContext;

class ISymbol
{
public:

  virtual ~ISymbol() = default;

  virtual uint32_t size( ILinkingContext & lc, nga::CpuType cpu ) const = 0;
  virtual Value value( ILinkingContext & lc ) const = 0;
  virtual Segment* segment() const = 0;
  virtual void visit( ILinkingContext& lc, nga::SegmentReferenceKind kind ) const = 0;

};

}
