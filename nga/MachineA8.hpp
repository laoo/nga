#pragma once

#include "Machine6502.hpp"

namespace nga::machine
{

class MachineA8 : public Machine6502
{
public:

  MachineA8();
  ~MachineA8() override = default;

  static void registerLUA( sol::state& lua );

};

}

