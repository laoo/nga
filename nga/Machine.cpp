#include "pch.hpp"
#include "Machine.hpp"
#include "Ex.hpp"
#include "Machine6502.hpp"
#include "Machine68000.hpp"
#include "MachineJaguar.hpp"
#include "Project.hpp"
#include "Input.hpp"
#include "MachineContext.hpp"
#include "Output.hpp"
#include "SegmentDescriptor.hpp"
#include "SegmentDescriptorClass.hpp"
#include "Segment.hpp"
#include "ISymbol.hpp"
#include "SegmentProcessor.hpp"
#include "Log.hpp"

namespace nga
{

Machine::Machine( Processor proc, Type type ) : mProc{ proc }, mType{ type }, mMachineContext{}, mSegmentDescriptorClasses{}, mSegmentDescriptorClassMap{}
{
}

Machine::~Machine()
{
}

void Machine::registerLUA( sol::state & lua )
{
  machine::Machine6502::registerLUA( lua );
  machine::Machine68000::registerLUA( lua );
  machine::MachineJaguar::registerLUA( lua );
}

void Machine::postprocess( sol::state& lua )
{
  if ( !mMachineContext )
    throw Ex{ "No context defined" };

  sol::table const& tab = lua["_G"];
  for ( auto kv : tab )
  {
    sol::object const& value = kv.second;
    sol::object const& key = kv.first;
    auto vtype = value.get_type();

    if ( vtype == sol::type::userdata )
    {
      if ( value.is<SegmentDescriptor*>() )
      {
        if ( key.is<std::string>() )
          mMachineContext->addSegmentDescriptor( key.as<std::string>(), std::shared_ptr<SegmentDescriptor>{ value.as<SegmentDescriptor*>() } );
        else
          throw Ex{ "Segment Descriptor needs a name" };
      }
    }
  }
}

Machine::Type Machine::type() const
{
  return mType;
}

Machine::Processor Machine::proc() const
{
  return mProc;
}

void Machine::process()
{
  mMachineContext->collectInputs();

  auto pEntrySegment = getEntrySegment();

  pushProcessor( pEntrySegment->getProcessor() );

  do
  {
    while ( auto currentProcessor = popProcessor() )
    {
      if ( currentProcessor->process() )
        pushProcessor( currentProcessor );
    }

  } while ( !allocatePendingSegments() );

  mMachineContext->commit();
}

void Machine::setContext( std::shared_ptr<MachineContext> ctx )
{
  if ( mMachineContext )
    throw Ex{ "Multiple contexts defined" };

  mMachineContext = std::move( ctx );
}

void Machine::registerSegmentProcessor( std::shared_ptr<SegmentProcessor> processor )
{
  if ( mSegmentDescriptorClassMap.find( &processor->processedSegment() ) != mSegmentDescriptorClassMap.end() )
    INTERNAL_ERROR;

  auto it = std::find_if( mSegmentDescriptorClasses.begin(), mSegmentDescriptorClasses.end(), [=]( std::shared_ptr<SegmentDescriptorClass> const& sdc )
  {
    return sdc->overlaps( processor->processedSegment().segmentDescriptor().domain() );
  } );

  if ( it != mSegmentDescriptorClasses.end() )
  {
    ( *it )->addProcessor( processor );
    mSegmentDescriptorClassMap.insert( { &processor->processedSegment(), it->get() } );
  }
  else
  {
    auto sdc = std::make_shared<SegmentDescriptorClass>();
    sdc->addProcessor( processor );
    mSegmentDescriptorClassMap.insert( { &processor->processedSegment(), sdc.get() } );
    mSegmentDescriptorClasses.push_back( sdc );
  }
}

bool Machine::allocateSegmentsOfThisClass( Segment* proc )
{
  auto it = mSegmentDescriptorClassMap.find( proc );
  if ( it == mSegmentDescriptorClassMap.end() )
    INTERNAL_ERROR;

  if ( auto proc = it->second->allocateSegments() )
  {
    pushProcessor( proc );
    return false;
  }
  else
  {
    return true;
  }
}

bool Machine::allocatePendingSegments()
{
  for ( auto const& segmentDescriptorClass : mSegmentDescriptorClasses )
  {
    if ( auto proc = segmentDescriptorClass->allocateSegments() )
    {
      pushProcessor( proc );
      return false;
    }
  }

  return true;
}

void Machine::pushProcessor( std::shared_ptr<SegmentProcessor> proc )
{
  if ( std::ranges::find( mSegmentProcessorsQueue, proc ) == mSegmentProcessorsQueue.cend() )
  {
    mSegmentProcessorsQueue.push_back( std::move( proc ) );
  }
}

std::shared_ptr<SegmentProcessor> Machine::popProcessor()
{
  if ( mSegmentProcessorsQueue.empty() )
    return {};

  auto currentProcessor = std::move( mSegmentProcessorsQueue.front() );
  mSegmentProcessorsQueue.pop_front();
  return currentProcessor;
}

Segment* Machine::getEntrySegment() const
{
  auto pEntrySymbol = mMachineContext->findSymbol( {}, "main" );

  if ( !pEntrySymbol )
    throw Ex{ "Undefined entry symbol 'main'" };

  auto pSegment = pEntrySymbol->segment();
  if ( !pSegment )
    throw Ex{} << "Entry symbol 'main' does not belong to any segment";

  pSegment->visit( pEntrySymbol );

  return pSegment;
}

}
