#include "pch.hpp"
#include "CpuType.hpp"
#include "Ex.hpp"

using namespace std::string_view_literals;

nga::CpuType::CpuType( Type t ) : mType{ t }
{
}

std::string_view nga::CpuType::name() const
{
  switch ( mType )
  {
  case Type::NO_CPU:
    return "No CPU"sv;
  case Type::MOS6502:
    return "6502"sv;
  case Type::G65SC02:
    return "65SC02"sv;
  case Type::WDC65C02:
    return "65C02"sv;
  case Type::M68000:
    return "68000"sv;
  default:
    return "Undefined"sv;
  }
}

int32_t nga::CpuType::valueSize( int32_t value ) const
{
  switch ( mType )
  {
  case Type::M68000:
    if ( value < std::numeric_limits<int16_t>::min() )
      return 4;
    if ( value < std::numeric_limits<int8_t>::min() )
      return 2;
    if ( value <= std::numeric_limits<int8_t>::max() )
      return 1;
    if ( value <= std::numeric_limits<int16_t>::max() )
      return 2;
    else
      return 4;
  case Type::MOS6502:
  case Type::G65SC02:
  case Type::WDC65C02:
    return value >= 0 && value < 256 ? 1 : 2;
  case Type::TOM:
  case Type::JERRY:
    return 4;
  default:
    INTERNAL_ERROR;
    break;
  }
}

std::string_view nga::CpuType::supportedCPUs()
{
  return "6502, 65sc02, 65c02, 68000"sv;
}

nga::CpuType::operator Type() const
{
  return mType;
}
