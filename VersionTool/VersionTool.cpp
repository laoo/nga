
#include <algorithm>

#pragma warning( push )
#pragma warning( disable : 4244 )
#include <boost/process.hpp>
#pragma warning( pop )

#include <iostream>
#include <regex>
#include <cstdlib>
#include <sstream>
#include <fstream>
#include <filesystem>

int main( int argc, char** argv )
{
  if ( argc < 2 )
    return -1;

  std::filesystem::path path{ argv[1] };

  std::string gitPath = "git.exe";
  if ( argc == 3 )
    gitPath = argv[2];


  std::string oldString{}, newString{};

  if ( std::filesystem::exists( path ) )
  {
    std::ifstream fin{ path, std::ios::binary };
    oldString.resize( (size_t)std::filesystem::file_size( path ) );
    fin.read( oldString.data(), oldString.size() );
  }

  boost::process::ipstream is;
  auto result = boost::process::system( gitPath + " describe --long", boost::process::std_out > is );

  std::string line{};
  if ( !std::getline( is, line ) || line.empty() )
    return -1;

  std::regex rx{ R"reg(^v(\d+)\.(\d+)(?:-(\d+)-g(.*))?)reg" };
  std::smatch sm;
  if ( std::regex_search( line, sm, rx ) )
  {
    std::stringstream ss;
    ss << "#pragma once" << std::endl;
    ss << "namespace nga\n{"  << std::endl;
    ss << "static constexpr auto version_major = " << sm.str( 1 ) << ";" << std::endl;
    ss << "static constexpr auto version_minor = " << sm.str( 2 ) << ";" << std::endl;
    ss << "static constexpr auto version_patch = " << sm.str( 3 ) << ";" << std::endl;
    ss << "static constexpr auto version_string = \"" << sm.str( 1 ) << "." << sm.str( 2 ) << "." << sm.str( 3 ) << " (" << sm.str( 4 ) << ")\"" << ";" << std::endl;
    ss << "}" << std::endl;
    newString = ss.str();
  }

  if ( newString != oldString )
  {
    std::ofstream fout{ path, std::ios::binary };
    fout.write( newString.data(), newString.size() );
  }

  return 0;
}
